module.exports = function (grunt) {

	require('load-grunt-tasks')(grunt);
	grunt.loadTasks('external/tasks');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
		branch: 'undefined',

        clean: {
            all: {
                files: [
                    {src: ['output/', 'builds/']}
                ]
            }
        },

		copy: {
			development: {
				files: [
					{src: ['assets/**', '!assets/sounds/se/**', '!assets/sounds/Music/**', '!assets/sounds/SFX/**'], dest: 'output/'},
                    {
                        expand: true,
                        cwd: './static/',
                        src: ['forcer/**', 'config/**', 'replay/**'],
                        dest: 'output/'
                    },
					{expand: true, cwd: './static', src: ['index.html', 'replay.html', 'dev-inclusion.js'], dest: 'output/',
					rename: function(dest, src){ return dest + src.replace('dev-',''); }}
				]
			},
			production: {
				files: [
					{src: ['assets/**', '!assets/sounds/se/**', '!assets/sounds/Music/**', '!assets/sounds/SFX/**'], dest: 'output/'},
					// {expand: true, cwd: './static/', src: ['static/**/**', '!static/*.*'], dest: 'output/'},
					// {expand: true, cwd: './static', src: ['info.pdf'], dest: 'output/static/'},
                    {
                        expand: true,
                        cwd: './static/',
                        src: ['config/**', 'replay/**', 'inclusion.js'],
                        dest: 'output/'
                    },
					// {expand: true, cwd: './static', src: ['inclusion.js'], dest: 'output/'}
				]
			}
		},

		genmanifest: {
			main: {
				files: [{src: ['output/**'], dest: 'manifest.json'}]
			}
		},

        browserify: {
			development: {
                options: {
					transform: [
					['babelify', {presets: ['es2015']}]
					],
                    browserifyOptions: {
                        debug: true,
						insertGlobalVars: {
							DEBUG: function(){return true;},
							gameVersion: function(){ return "'" + grunt.config.get('pkg.version') + "'"; }
						}
                    }
                },
                files: {
                    'output/main.js': ['src/main.js']
                }
            },
			production: {
				options: {
					transform: [
					['babelify', {presets: ['es2015']}]
					],
					browserifyOptions: {
						debug: false,
						insertGlobalVars: {
							DEBUG: function(){return false;},
							gameVersion: function(){ return "'" + grunt.config.get('pkg.version') + "'"; }
						}
					}
				},
				files: {
					'output/main.js': ['src/main.js']
				}
			}
        },

		uglify: {
            options: {
                compress: {
					properties: true,
					dead_code: true,
					global_defs: {
						DEBUG: false
					},
					passes:2
				},
                preserveComments: false
            },
            production: {
                files: {
                    'output/main.js': ['output/main.js']
                }
            }
        },

		connect: {
            local: {
                options: {
                    port: 8888,
                    base: 'output',
                    hostname: '*',
                    keepalive: false,
					middleware: function(connect, options, middlewares) {
					  middlewares.unshift(function(req, res, next) {
						  res.setHeader('Access-Control-Allow-Origin', '*');
						  res.setHeader('Access-Control-Allow-Methods', '*');
						  next();
					  });
					  return middlewares;
					}
                }
            }
        },

        watch: {
			script: {
				files: ['src/**/*.js'],
				tasks: ['browserify:development'],
				options: {atBegin: true}
			}
        },

		html_pdf: {
			dist: {
				options: {
					format: 'A4',
					orientation: 'portrait',
					quality: '75',
					header: {
					  height: "0mm",
					  contents: {
							default: '', // fallback value
					  }
					},
					footer: {
					  height: "0mm",
					  contents: {
							default: '', // fallback value
					  }
					},
				},
				files: {
					'static/info.pdf': ['static/html/info.html',],
				},
			},
		},
		audiosprite: {
	        all: {
	            files: {
	                src: './assets/sounds/se/*',
	                dest: './audiosprite.json'
	            },
	            option: {
	                output: './assets/sounds/audiosprite',
	                export: 'mp3',
	                format: 'howler',
	            },
	        }
	    }

    });

    grunt.registerTask('run', ['connect', 'watch:script']);
    grunt.registerTask('build_development', ['clean:all', 'copy:development', 'genmanifest', 'browserify:development', 'uglify']);
    grunt.registerTask('build_production', ['clean:all', 'copy:production', 'genmanifest', 'browserify:production', 'uglify']);
    grunt.registerTask('default', ['build_development']);

	grunt.registerTask('build', 'build', function(){
		var done = this.async();
		var branchName = grunt.option('branch');
		var buildNr = grunt.option('buildNr');
		if(branchName === 'develop' || branchName === 'master'){
			if(!buildNr)
				grunt.fail.warn('build Nr not specified. Use --buildNr=<buildNr> to set.');
			if(isNaN(buildNr))
				grunt.fail.warn('build Nr is not a number.');

			var version = grunt.config.get('pkg.version').split('.');
			version[2] = buildNr;
			version = version.join('.');
			grunt.log.writeln('Starting build: ' + branchName + ' ver' + version);

			grunt.config.set('pkg.version', version);
			grunt.config.set('branch', branchName);

			var pkg = grunt.config.get('pkg');
			grunt.log.writeln('Updating package.json');
			grunt.file.write('package.json', JSON.stringify(pkg, null, 2));

			if(branchName === 'develop'){
				grunt.task.run('build_development');
			}else if(branchName === 'master'){
				grunt.task.run('build_production');
			}

			done();
		}else{
			grunt.log.writeln('Building feature branch: ' + branchName);
			grunt.task.run('build_development');
			done();
		}
	});
};
