if(!window.replayData)
    gAPI.loader.default({
        titleName: 'MOE MOE CUISINE',
        preset: 'slot',
		resources: [
            'config/fonts.css',
			'main.js'
		]
    })
else
    gAPI.loader.default({
        titleName: 'MOE MOE CUISINE Replay',
		resources: [
			'replay/outcome-visualizer.css',
			'replay/translations.js',
			'replay/replay.js'
		]
    })
