"use strict";

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Forcer = function () {
    function Forcer() {
        _classCallCheck(this, Forcer);

        var body = $('body');
        this.container = $('<div></div>');
        this.container.attr('id', 'forcer');
        body.prepend(this.container);
        this.init();
        this.forcedResults = [];
        this.definedResults = {
            "Small Win": "64 50 77 2 7 0\n",
            "Big Win": "4 14 14 141 142 1\n",
            "Wild Win": "28 29 55 35 13 1\n",
            "Anticipation": "39 40 34 12 0 1\n",
            "Bonus": "39 40 52 12 0 1\n",
            "Long Bonus": "14 23 4 11 35 1\n",
            "No wins": "40 40 40 40 40 1\n",
            "Bonus in Bonus": "14 23 4 0 0 1\n",
            "Big win in Bonus": "10 10 14 21 24\n "
        };

        var _this = this;

        var _play = gAPI.net.play.bind(gAPI.net);
        gAPI.net.play = function (action, bet, gameData) {
            var result = _this.forcedResults.shift();
            return !result ? _play.call(null, action, bet, gameData) : _play.call(null, 'force', bet, {
                forcedResult: result,
                action: action,
                gamaData: gameData
            });
        };
        gAPI.casinoUI.on('menuOpen', function () {
            $('#forcer').css('visibility', 'visible');
        });
        gAPI.casinoUI.on('menuClose', function () {
            $('#forcer').css('visibility', 'hidden');
        });
    }

    _createClass(Forcer, [{
        key: 'init',
        value: function init() {
            this.container.removeClass("opened");
            this.container.html('<div id="forcerButton"></div>');
            this.open = false;
            this.accessButton = $('#forcerButton');
            this.accessButton.on('click', this.openForcer.bind(this));
        }
    }, {
        key: 'openForcer',
        value: function openForcer(event) {
            event.stopPropagation();
            gAPI.casinoUI.hideUI();
            this.container.addClass("opened");
            this.container.html('<div id="forcerConsole" class="forcerVertical">' + '<div id="forcerTitle" class="forcerHorizontal">MOE MOE CUISINE forcer console</div>' + '<div id="forcerInputControls" class="forcerHorizontal"><div id="forcerSelectBuffer" class="forcerVertical"><select id="spinSelect" name="spinSelect" class="forcerVertical">' + '<option>Manual</option>' + '<option>Small Win</option>' + '<option>Wild Win</option>' + '<option>Anticipation</option>' + '<option>Bonus</option>' + '<option>Big Win</option>' + '<option>Long Bonus</option>' + '<option>No wins</option>' + '<option>Bonus in Bonus</option>' + '<option>Big win in Bonus</option>' + '</select></div>' + '<div id="forcerSubmit" class="forcerHorizontal">Enter</div></div>' + '<div id="reelInputs" class="forcerHorizontal"></div>' + '<div id="forcedResultsBuffer" class="forcerHorizontal"><textarea name="forcedResults" id="forcerResults"></textarea></div>' + '<div id="forcerExitButton"></div>' + '</div>');
            $('#forcerExitButton').on('click', this.closeForcer.bind(this));

            this.spinSelect = $('#spinSelect');
            this.resultsOutput = $('#forcerResults');
            this.reelInputs = $('#reelInputs');
            this.forcerSubmit = $('#forcerSubmit');

            var forcedString = "";
            for (var i = 0; i < this.forcedResults.length; i++) {
                for (var k = 0; k < this.forcedResults[i].length; k++) {
                    if (k + 1 == this.forcedResults[i].length) {
                        forcedString += this.forcedResults[i][k];
                    } else {
                        forcedString += this.forcedResults[i][k] + " ";
                    }
                }
                forcedString += "\n";
            }
            this.resultsOutput.val(forcedString);

            if (this.spinSelect.val() === "Manual") {
                this.reelInputs.html('<input type="number" class="reelInput" id="reelInput1" name="reelInput1" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput2" name="reelInput2" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput3" name="reelInput3" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput4" name="reelInput4" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput5" name="reelInput5" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput6" name="reelInput6" value = 1 min="0" max="999" pattern="[0-9]*">');
            } else {
                this.reelInputs.html('');
            }

            this.spinSelect.on('input', function () {
                if (this.spinSelect.val() === "Manual") {
                    this.reelInputs.html('<input type="number" class="reelInput" id="reelInput1" name="reelInput1" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput2" name="reelInput2" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput3" name="reelInput3" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput4" name="reelInput4" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput5" name="reelInput5" min="0" max="999" pattern="[0-9]*">' + '<input type="number" class="reelInput" id="reelInput6" name="reelInput6" value = 1 min="0" max="999" pattern="[0-9]*">');
                } else {
                    this.reelInputs.html('');
                }
            }.bind(this));

            this.forcerSubmit.on('click', function () {
                if (this.spinSelect.val() === "Manual" && $('#reelInput1').val() != "" && $('#reelInput2').val() != "" && $('#reelInput3').val() != "" && $('#reelInput4').val() != "" && $('#reelInput5').val() != "" && $('#reelInput6').val() != "") {
                    var val1 = $('#reelInput1').val();
                    var val2 = $('#reelInput2').val();
                    var val3 = $('#reelInput3').val();
                    var val4 = $('#reelInput4').val();
                    var val5 = $('#reelInput5').val();
                    var val6 = $('#reelInput6').val();
                    var textAreaValue = this.resultsOutput.val();
                    textAreaValue += val1 + " " + val2 + " " + val3 + " " + val4 + " " + val5 + " " + val6 + "\n";
                    this.resultsOutput.val(textAreaValue);
                    this.setForcedResults();
                } else if (this.spinSelect.val() !== "Manual") {
                    var _textAreaValue = this.resultsOutput.val();
                    _textAreaValue += this.definedResults[this.spinSelect.val()];
                    this.resultsOutput.val(_textAreaValue);
                    this.setForcedResults();
                }
            }.bind(this));

            this.resultsOutput.on('input', this.setForcedResults.bind(this));
        }
    }, {
        key: 'setForcedResults',
        value: function setForcedResults() {
            this.forcedResults = [];
            var resultStrings = this.resultsOutput.val().split(/\r|\n/);
            for (var i = 0; i < resultStrings.length; i++) {
                this.forcedResults.push(resultStrings[i].split(/\s| /));
            }
            for (var i = 0; i < this.forcedResults.length; i++) {
                for (var k = 0; k < this.forcedResults[i].length; k++) {
                    this.forcedResults[i][k] = parseInt(this.forcedResults[i][k]);
                }
            }
            for (var i = this.forcedResults.length - 1; i >= 0; i--) {
                if (this.forcedResults[i].filter(isNaN).length > 0) {
                    this.forcedResults.splice(i, 1);
                }
            }
        }
    }, {
        key: 'closeForcer',
        value: function closeForcer(event) {
            event.stopPropagation();
            gAPI.casinoUI.showUI();
            this.init();
        }
    }]);
    return Forcer;
}();

window.forcer = new Forcer();
