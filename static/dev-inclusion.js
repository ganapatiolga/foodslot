window.DEBUG = true

if(!window.replayData)
    gAPI.loader.default({
        titleName: 'MOE MOE CUISINE',
        preset: 'slot',
		resources: [
            'config/fonts.css',
			'main.js',
            'forcer/forcer.js',
            'forcer/forcer.css'
		]
    })
else
    gAPI.loader.default({
        titleName: 'MOE MOE CUISINE Replay',
		resources: [
			'replay/outcome-visualizer.css',
			'replay/translations.js',
			'replay/replay.js'
		]
    })
