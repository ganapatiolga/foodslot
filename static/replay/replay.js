var StringBuilder = function(){
    var content = '';
    var sb = null;
    sb = {
        for: function(arr, callback){
            for(var i = 0; i < arr.length; i++)
                content += callback(arr[i], i);
            return sb;
        },
        if: function(condition, callback){
            if(!!condition)
                content += callback(condition);
            return sb;
        },
        append: function(str){
            if(str != null)
                content += str;
            return sb;
        },
        build: function(){
            return content;
        },
        sp: function(l){
            l = l || 1;
            content += new Array(l+1).join('&#160;');
            return sb;
        }
    };
    return sb;
};

function getParameterByName(name){
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

var locale = getParameterByName('locale')
var labels = TRANSLATIONS[locale] || TRANSLATIONS['en']

var symbolMap = {
    0: conf.libURL.static + conf.game + '/paytable/symbol_wild.png',
    1: conf.libURL.static + conf.game + '/paytable/symbol_bonus.png',
    2: conf.libURL.static + conf.game + '/paytable/symbol1_simple.png',
    3: conf.libURL.static + conf.game + '/paytable/symbol2_simple.png',
    4: conf.libURL.static + conf.game + '/paytable/symbol3_simple.png',
    5: conf.libURL.static + conf.game + '/paytable/symbol4_simple.png',
    6: conf.libURL.static + conf.game + '/paytable/symbol5_simple.png',
    7: conf.libURL.static + conf.game + '/paytable/symbol6_simple.png',
    8: conf.libURL.static + conf.game + '/paytable/symbol5_single.png',
    9: conf.libURL.static + conf.game + '/paytable/symbol4_single.png',
    10: conf.libURL.static + conf.game + '/paytable/symbol3_single.png',
    11: conf.libURL.static + conf.game + '/paytable/symbol2_single.png',
    12: conf.libURL.static + conf.game + '/paytable/symbol1_single.png'
};

var paylineConf = [
    [0,0,0,0,0],
    [1,1,1,1,1],
    [2,2,2,2,2],
    [0,1,2,1,0],
    [2,1,0,1,2],
    [1,0,0,0,1],
    [1,2,2,2,1],
    [0,0,1,2,2],
    [2,2,1,0,0],
    [1,0,1,2,1],
    [1,2,1,0,1],
    [0,1,1,1,0],
    [2,1,1,1,2],
    [0,1,0,1,0],
    [2,1,2,1,2],
    [1,1,0,1,1],
    [1,1,2,1,1],
    [0,0,2,0,0],
    [2,2,0,2,2],
    [0,2,2,2,0],
    [2,0,0,0,2],
    [1,0,2,0,1],
    [1,2,0,2,1],
    [0,2,0,2,0],
    [2,0,2,0,2],
    [0,2,1,0,2],
    [2,0,1,2,0],
    [1,0,2,1,2],
    [0,2,1,2,0],
    [2,1,0,0,1],
    [0,1,2,2,1],
    [1,0,1,0,1],
    [1,2,1,2,1],
    [0,1,0,1,2],
    [2,1,2,0,0],
    [2,0,0,1,2],
    [1,2,2,0,0],
    [0,0,1,1,2],
    [2,2,0,1,0],
    [0,0,2,2,2]
];

var getSymbolId = function(s, r, d){ return 'symbol' + s + '_' + r + '_' + d; };
var getWinId = function(w, d){ return 'win' + w + '_' + d; };
var getContextId = function(d){ return 'ctx' + d; };

var Symbol = function(imgPath, id, clazz = null){
    var img = new Image();
    img.src = imgPath;
    img.id = id;
    if(clazz != null)
        img.className = clazz
    return img.outerHTML;
};

var SelectedChar = function(id){
    switch (id) {
        case 1: return Symbol(symbolMap[12], null, "selectedCharImg");
        case 2: return Symbol(symbolMap[11], null, "selectedCharImg");
        case 3: return Symbol(symbolMap[10], null, "selectedCharImg");
        case 4: return Symbol(symbolMap[9], null, "selectedCharImg");
        case 5: return Symbol(symbolMap[8], null, "selectedCharImg");
    }
}

var sysmbolResolver = function(replace, id){
    var result = null;
    if(id === 13)
        result = replace[13]
    else if (id === 14)
        result = replace[14]
    else
        result = id
    return result;
}

var drawPayline = function(ctx, paylineId){
    while (ctx.lastChild) { ctx.removeChild(ctx.lastChild); }

    if (paylineId === undefined) return;

    var points = [];
    var payline = paylineConf[paylineId];

    points.push({x: 0, y: payline[0] * 100 + 50});
    for(var i = 0; i < payline.length; i++)
        points.push({x: i * 100 + 50, y: payline[i] * 100 + 50});
    points.push({x: payline.length * 100, y: payline[payline.length - 1] * 100 + 50});

    for(var i = 0; i < points.length-1; i++){
        var line = window.document.createElementNS('http://www.w3.org/2000/svg', 'line');
        line.setAttribute('x1', points[i].x);
        line.setAttribute('y1', points[i].y);
        line.setAttribute('x2', points[i+1].x);
        line.setAttribute('y2', points[i+1].y);
        line.setAttribute('stroke', '#ffffff');
        line.setAttribute('stroke-width', 4);
        line.setAttribute('stroke-linecap', 'round');

        ctx.appendChild(line);
    }
};

var WinController = function(reelDisplay, d){
    var currentElement = null;
    var ctx = null;

    window.addEventListener('load', function(){
        ctx = window.document.getElementById(getContextId(d));
        ctx.setAttribute('viewBox', '0 0 500 300');
        setTimeout( () => {
          ctx.setAttribute('width', ctx.parentNode.clientWidth + 'px');
          ctx.setAttribute('height', ctx.parentNode.clientHeight + 'px');
        }, 100)
        window.addEventListener('resize', function(){
            ctx.setAttribute('width', ctx.parentNode.clientWidth + 'px');
            ctx.setAttribute('height', ctx.parentNode.clientHeight + 'px');
        }, false);
    }, false);

    return {
        addWin: function(elementId, data){
            window.setTimeout(function(){
                window.document.getElementById(elementId)
                    .addEventListener('click', function(event){
                        event.stopPropagation();
                        if(currentElement != null)
                            currentElement.style.removeProperty('background-color');
                        currentElement = event.target;
                        currentElement.style.setProperty('background-color', '#ca431c');

                        for(var r = 0; r < reelDisplay.length; r++)
                            for(var s = 0; s < reelDisplay[r].length; s++)
                                window.document.getElementById(getSymbolId(s, r, d)).style
                                    .setProperty('opacity', data.positions[r] !== s ? '0.3' : '1.0');

                        drawPayline(ctx, data.paylineId)
                }, false);
            }, 0);
        }
    }
};


if(replayData.length > 1){
	for(let i = replayData.length - 1; i >= 0; i--){
		if(!replayData[i].balance){ replayData[i].balance = replayData[i + 1].balance; }
		if(!replayData[i].currency){ replayData[i].currency = replayData[i + 1].currency; }
	}
}

var htmlContent = StringBuilder()
.append('<div class="ov_wrapper">')
    .for(replayData.reverse(), function(data, d){
        if(data.gameData.action !== 'battle' ){
            var winController = WinController(data.gameData.spinResult.reelDisplay, d)
            return StringBuilder()
            .append('<div class="ov_container">')
                .append('<div class="ov_side">')
                    .append('<div class="ov_label">')
                        .append(labels.roundInfo)
                        .append('<div class="ov_info">').append(data.gameRoundId).append('</div>')
                    .append('</div>')
                    .append('<div class="ov_label">')
                        .append(labels.selectedChars)
                        .append('<div class="ov_info">')
                            .append((function(action){

                                let actionAsCharArr = action.split('')
                                let len = actionAsCharArr.length
                                let sb = StringBuilder()
                                for (var i = len-3; i < len; i++) {
                                    sb.append(SelectedChar(parseInt(actionAsCharArr[i])))
                                }
                                return sb.build()
                            })(data.gameData.action))
                        .append('</div>')
                    .append('</div>')
                    .append('<div class="ov_label">')
                        .append(labels.time)
                        .append('<div class="ov_info">').append((new Date(data.timestamp)).toLocaleString()).append('</div>')
                    .append('</div>')
    				.if(data.bet > 0, function(){ return StringBuilder()
    					.append('<div class="ov_label">')
                        .append(labels.wager)
                        .append('<div class="ov_info">').append(Number(data.bet).toFixed(2)).sp().append(data.currency).append('</div>')
    					.append('</div>').build(); })
                    .append('<div class="ov_label">')
                        .append(labels.yield)
                        .append('<div class="ov_info">').append(Number(data.win || 0).toFixed(2)).sp().append(data.currency).append('</div>')
                    .append('</div>')
    				.if(data.balance > 0, function(){ return StringBuilder()
    					.append('<div class="ov_label">')
                        .append(labels.balance)
                        .append('<div class="ov_info">').append(Number(data.balance).toFixed(2)).sp().append(data.currency).append('</div>')
    					.append('</div>').build(); })
                .append('</div>')
                .append('<div class="ov_display">')
                .append('<svg class="ov_ctx" id="'+getContextId(d)+'"></svg>')
                .for(data.gameData.spinResult.reelDisplay, function(reel, r){
                    return StringBuilder()
                    .append('<div class="ov_stripe">')
                    .for(reel, function(symbol, s){
                        return StringBuilder()
                        .append('<div class="ov_item">')
                            .append(Symbol(symbolMap[sysmbolResolver(data.gameData.spinResult.replace, symbol)], getSymbolId(s, r, d)))
                        .append('</div>')
                        .build();
                    })
                    .append('</div>')
                    .build();
                })
                .append('</div>')
                .append('<div class="ov_side">')
                    .append('<div class="ov_label">')
                        .append(labels.paylines)
                    .append('</div>')
                    .append('<div class="ov_list">')
                    .for(data.gameData.spinResult.wins, function(win){
                        winController.addWin(getWinId(win.paylineId, d), win);
                        return StringBuilder()
                        .append('<div class="ov_list_item" id="'+getWinId(win.paylineId, d)+'")">')
                            .append(win.paylineId+1).sp(3).append(Number(win.win).toFixed(2)).sp().append(data.currency)
                        .append('</div>')
                        .build();
                    })
                    .if(data.gameData.spinResult.scatterResult, function(win){
                        winController.addWin(getWinId('Sc', d), win);
                        return StringBuilder()
                        .append('<div class="ov_list_item" id="'+getWinId('Sc', d)+'")">')
                            .append(labels.scatter).sp(3).append(Number(win.win).toFixed(2)).sp().append(data.currency)
                        .append('</div>')
                        .build();
                    })
                    .append('</div>')
                .append('</div>')
            .append('</div>')
            .build();
        } else {
            return StringBuilder()
            .append('<div class="ov_container">')
                .append('<div class="ov_side">')
                    .append('<div class="ov_label">')
                        .append(labels.roundInfo)
                        .append('<div class="ov_info">').append(data.gameRoundId).append('</div>')
                    .append('</div>')
                    .append('<div class="ov_label">')
                        .append(labels.time)
                        .append('<div class="ov_info">').append((new Date(data.timestamp)).toLocaleString()).append('</div>')
                    .append('</div>')
    				.if(data.bet > 0, function(){ return StringBuilder()
    					.append('<div class="ov_label">')
                        .append(labels.wager)
                        .append('<div class="ov_info">').append(Number(data.bet).toFixed(2)).sp().append(data.currency).append('</div>')
    					.append('</div>').build(); })
                    .append('<div class="ov_label">')
                        .append(labels.yield)
                        .append('<div class="ov_info">').append(Number(data.win || 0).toFixed(2)).sp().append(data.currency).append('</div>')
                    .append('</div>')
    				.if(data.balance > 0, function(){ return StringBuilder()
    					.append('<div class="ov_label">')
                        .append(labels.balance)
                        .append('<div class="ov_info">').append(Number(data.balance).toFixed(2)).sp().append(data.currency).append('</div>')
    					.append('</div>').build(); })
                .append('</div>')
                .append('<div class="ov_dominant">')
                    .append('<div class="ov_label" style="text-align: center;">')
                        .append(labels.dominantSymbol)
                        .append('<div class="ov_info">')
                            .append(Symbol(symbolMap[data.gameData.character], null, "dominantCharImg"))
                        .append('</div>')
                    .append('</div>')
                .append('</div>')
            .append('</div>')
            .build();
        }


    })
.append('</div>')
.build();

window.document.body.innerHTML = htmlContent;
