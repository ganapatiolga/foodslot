import '../../core';
import { cleanUpCallbacks } from './FightScene';

let soundOptions = { volume: 1.0 }

//TODO: Core.scale is undefine at this point. Resolve------------

let charConf = []

export function getPositions() {
    return [new PIXI.Point(300 / Core.scaleFactor, 520 / Core.scaleFactor),
            new PIXI.Point(160 / Core.scaleFactor, 620 / Core.scaleFactor),
            new PIXI.Point(440 / Core.scaleFactor, 570 / Core.scaleFactor)]
}

export class FightingCharacter extends PIXI.Container {
    constructor(charId) {
        super();

        charConf = charConf.length ? charConf :
            ['assets/' + Core.scale + '/dBones/fight/0161_0161_05/skeleton.json', //redGirl
            'assets/' + Core.scale + '/dBones/fight/0492_0492_05/skeleton.json',  //greenGirl
            'assets/' + Core.scale + '/dBones/fight/0443_0442_06/skeleton.json',  //yellowGirl differs from img
            'assets/' + Core.scale + '/dBones/fight/0652_0521_06/skeleton.json',  //whiteGirl
            'assets/' + Core.scale + '/dBones/fight/0079_0079_05/skeleton.json']; //blackGirl

        this.unit = Core.assets.animations.getDragonBones(charConf[charId], null, Core.scaleFactor);
        super.addChild(this.unit);
        this.unit.scale.set(-0.7 / Core.scaleFactor, 0.7 / Core.scaleFactor);
        this.unit.animation.gotoAndPlay('run');
        this.charId = charId; //

        this.shadow = new Shadow(2, 1.5);
        this.shadow.position.y = this.unit.position.y + 10 / Core.scaleFactor;
        this.shadow.visible = true;
        super.addChild(this.shadow);
        this.unit.bringToFront();

        this.cleanUpCharInvokeEffectInvokeAnimAttackAnim = function () {
            if (this.InvokeEffect) {

                this.invokeEffect.clear();
                if (this.invokeEffect.parent)
                    this.invokeEffect.parent.removeChild(this.invokeEffect);
                delete this.invokeEffect;
            }
            if (this.invokeAnim) {

                this.invokeAnim.clear();
                if (this.invokeAnim.parent)
                    this.invokeAnim.parent.removeChild(this.invokeAnim);
                delete this.invokeAnim;
            }
            if (this.attackAnim) {

                this.attackAnim.clear();
                if (this.attackAnim.parent)
                    this.attackAnim.parent.removeChild(this.attackAnim);
                delete this.attackAnim;
            }
        }.bind(this);
        cleanUpCallbacks.push(this.cleanUpCharInvokeEffectInvokeAnimAttackAnim);

        this.cleanUpDeathAnimAndUnit2 = function () {
            if (this.deathAnim) {

                this.deathAnim.clear();
                if (this.deathAnim.parent)
                    this.deathAnim.parent.removeChild(this.deathAnim);
                delete this.deathAnim;
            }
            if (this.unit) {
                this.unit.clear();
                if (this.unit.parent)
                    this.unit.parent.removeChild(this.unit);
                delete this.unit;
            }
        }.bind(this);
        cleanUpCallbacks.push(this.cleanUpDeathAnimAndUnit2);
    }
    attack(stage, fightScene) {
        /*
        var char = this;
        this.unit.once('Complete', function () {
            char.unit.animation.gotoAndPlay('wait');
            char.turnEnd();
        });
        char.invokeEffect = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/wrappedEffect/skeleton.json', null, Core.scaleFactor);
        char.invokeEffect.scale.set(-1.0 / Core.scaleFactor, 1.0 / Core.scaleFactor);
        char.addChild(char.invokeEffect);
        char.invokeEffect.animation.gotoAndPlay('start');

        switch (char.charId) {
            case 0:
                char.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_235/skeleton.json', null, Core.scaleFactor);
                char.attackAnim.visible = false;
                char.attackAnim.position.set(820 / Core.scaleFactor, 380 / Core.scaleFactor);
                char.attackAnim.scale.set(2 / Core.scaleFactor, 2 / Core.scaleFactor);
                stage.addChild(char.attackAnim);

                this.tween = new TimelineMax();

                this.tween.add(function () {
                    char.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_401_6_3/skeleton.json', null, Core.scaleFactor);
                    char.invokeAnim.animation.gotoAndPlay('preview');
                    Core.assets.sounds.playSound('skillSE_in_1', false, soundOptions);
                    stage.addChild(char.invokeAnim);
                    char.invokeAnim.position.set(char.position.x, char.position.y);
                    char.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
                    char.invokeAnim.bringToFront();
                }, 0.1);
                this.tween.add(function () {
                    char.invokeEffect.animation.gotoAndPlay('end');
                    char.unit.animation.gotoAndPlay('atk_normal_0_1');
                }, 0.7);
                this.tween.add(function () {
                    TweenMax.to(char.invokeAnim, 0.5, { alpha: 0 });
                    Core.assets.sounds.playSound('unit_0161_0161_05_6', false, soundOptions);
                }, 1.1);
                this.tween.add(function () {
                    char.attackAnim.visible = true;
                    char.attackAnim.animation.gotoAndPlay('start');
                    Core.assets.sounds.playSound('commandSE_15', false, soundOptions);
                    Core.assets.sounds.playSound('skillSE_ex_32', false, soundOptions);
                }, 1.6);
                this.tween.add(function () {
                    Core.assets.sounds.playSound('skillSE_sh_39', false, soundOptions);
                }, 1.7);
                break;//readhead

            case 1:

                this.tween1 = new TimelineMax();

                this.tween1.add(function () {
                    char.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_202_2_1/skeleton.json', null, Core.scaleFactor);
                    char.invokeAnim.animation.gotoAndPlay('preview');
                    Core.assets.sounds.playSound('skillSE_in_2', false, soundOptions);
                    stage.addChild(char.invokeAnim);
                    char.invokeAnim.position.set(char.position.x, char.position.y);
                    char.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
                    char.invokeAnim.bringToFront();
                }, 0.1);
                this.tween1.add(function () {
                    char.invokeEffect.animation.gotoAndPlay('end');
                    TweenMax.to(char.invokeAnim, 0.5, { alpha: 0 });
                    Core.assets.sounds.playSound('unit_0530_0529_06_7', false, soundOptions);
                }, 0.9);
                this.tween1.add(function () {
                    TweenMax.to(char.position, 0.2, { x: '+=' + 100 / Core.scaleFactor });
                    char.unit.animation.gotoAndPlay('step_f');
                }, 1.1);
                this.tween1.add(function () {
                    char.unit.animation.gotoAndPlay('atk_normal_0_1');
                    char.unit.bringToFront();
                }, 1.3);
                this.tween1.add(function () {
                    Core.assets.sounds.playSound('commandSE_15', false, soundOptions);
                }, 1.4);

                this.tween1.add(function () {
                    Core.assets.sounds.playSound('commandSE_15', false, soundOptions);
                }, 1.6);

                this.tween1.add(function () {
                    char.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_176/skeleton.json', null, Core.scaleFactor);
                    char.attackAnim.position.set(900 / Core.scaleFactor, 500 / Core.scaleFactor);
                    char.attackAnim.scale.set(2 / Core.scaleFactor, 2 / Core.scaleFactor);
                    stage.addChild(char.attackAnim);
                    char.attackAnim.bringToFront();
                }, 1.8);

                this.tween1.add(function () {
                    Core.assets.sounds.playSound('skillSE_em_2', false, soundOptions);
                }, 1.9);
                this.tween1.add(function () {
                    TweenMax.to(char.position, 0.2, { x: '-=' + 100 / Core.scaleFactor });
                    char.unit.animation.gotoAndPlay('step_b');
                }, 2.4);

                this.tween1.add(function () {
                    char.unit.animation.gotoAndPlay('wait');
                }, 2.6);
                break;//green
            case 2:
                char.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_60/skeleton.json', null, Core.scaleFactor);
                char.attackAnim.visible = false;
                char.attackAnim.position.set(900 / Core.scaleFactor, 700 / Core.scaleFactor);
                char.attackAnim.scale.set(1.5 / Core.scaleFactor, 1.5 / Core.scaleFactor);
                stage.addChild(char.attackAnim);

                this.tween2 = new TimelineMax();

                this.tween2.add(function () {
                    char.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_209_4_2/skeleton.json', null, Core.scaleFactor);
                    char.invokeAnim.animation.gotoAndPlay('preview');
                    Core.assets.sounds.playSound('skillSE_in_1', false, soundOptions);
                    stage.addChild(char.invokeAnim);
                    char.invokeAnim.position.set(char.position.x, char.position.y);
                    char.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
                    char.invokeAnim.bringToFront();
                }, 0.1);
                this.tween2.add(function () {
                    char.invokeEffect.animation.gotoAndPlay('end');
                    char.unit.animation.gotoAndPlay('atk_normal_0_1');
                }, 0.6);
                this.tween2.add(function () {
                    TweenMax.to(char.invokeAnim, 0.5, { alpha: 0 });
                }, 1.1);
                this.tween2.add(function () {
                    Core.assets.sounds.playSound('unit_0443_0442_06_6', false, soundOptions);
                }, 1.0);
                this.tween2.add(function () {
                    char.attackAnim.visible = true;
                    char.attackAnim.animation.gotoAndPlay('start');
                }, 1.5);
                this.tween2.add(function () {
                    Core.assets.sounds.playSound('skillSE_ex_17', false, soundOptions);
                }, 1.75);
                break;//catlady
            case 3:
                this.tween3 = new TimelineMax();

                this.tween3.add(function () {
                    char.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_212_8_3/skeleton.json', null, Core.scaleFactor);
                    char.invokeAnim.animation.gotoAndPlay('preview');
                    Core.assets.sounds.playSound('skillSE_in_1', false, soundOptions);
                    stage.addChild(char.invokeAnim);
                    char.invokeAnim.bringToFront();
                    char.invokeAnim.position.set(char.position.x, char.position.y);
                    char.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
                }, 0.1);

                this.tween3.add(function () {
                    char.invokeEffect.animation.gotoAndPlay('end');
                    char.unit.animation.gotoAndPlay('atk_normal_0_1');
                }, 0.4);
                this.tween3.add(function () {
                    Core.assets.sounds.playSound('unit_0652_0521_06_7', false, soundOptions);
                }, 0.8);
                this.tween3.add(function () {
                    TweenMax.to(char.invokeAnim, 0.5, { alpha: 0 });
                }, 1.6);
                this.tween3.add(function () {
                    char.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_148/skeleton.json', null, Core.scaleFactor);
                    char.attackAnim.position.set(900 / Core.scaleFactor, 680 / Core.scaleFactor);
                    char.attackAnim.scale.set(2.0 / Core.scaleFactor, 2.0 / Core.scaleFactor);
                    stage.addChild(char.attackAnim);
                    char.attackAnim.bringToFront();
                }, 1.8);

                this.tween3.add(function () {
                    Core.assets.sounds.playSound('skillSE_ex_35', false, soundOptions);
                }, 2.1);

                this.tween3.add(function () {
                    TweenMax.to(char.attackAnim, 0.5, { alpha: 0 })
                }, 2.8);
                break;//mainmenugirl
            case 4:
                char.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_105/skeleton.json', null, Core.scaleFactor);
                char.attackAnim.visible = false;
                char.attackAnim.position.set(900 / Core.scaleFactor, 450 / Core.scaleFactor);
                char.attackAnim.scale.set(2 / Core.scaleFactor, 2 / Core.scaleFactor);
                stage.addChild(char.attackAnim);

                this.tween4 = new TimelineMax();

                this.tween4.add(function () {
                    char.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_210_5_2/skeleton.json', null, Core.scaleFactor);
                    char.invokeAnim.animation.gotoAndPlay('preview');
                    Core.assets.sounds.playSound('skillSE_in_1', false, soundOptions);
                    stage.addChild(char.invokeAnim);
                    char.invokeAnim.position.set(char.position.x, char.position.y);
                    char.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
                    char.invokeAnim.bringToFront();
                }, 0.1);
                this.tween4.add(function () {
                    char.invokeEffect.animation.gotoAndPlay('end');
                    char.unit.animation.gotoAndPlay('atk_normal_0_1');
                }, 0.6);
                this.tween4.add(function () {
                    Core.assets.sounds.playSound('unit_0079_0079_05_6', false, soundOptions);
                }, 0.9);
                this.tween4.add(function () {
                    TweenMax.to(char.invokeAnim, 0.5, { alpha: 0 });
                }, 1.1);

                this.tween4.add(function () {
                    Core.assets.sounds.playSound('skillSE_sh_16', false, soundOptions);
                    char.attackAnim.visible = true;
                    char.attackAnim.animation.gotoAndPlay('start', 0, 2);
                }, 1.6);
                break;//blackGirl
        }

        this.tween5 = new TimelineMax();

        this.tween5.add(function () {
            TweenMax.to(char.attackAnim, 1, { alpha: 0 });
            char.emit('hit');
        }, 2.8);

        this.tween5.add(function () {
            this.cleanUpCharInvokeEffectInvokeAnimAttackAnim();
        }.bind(this), 4.0);
    */
    }
    turnEnd() {
        super.emit('Complete');
    }
    die() {
        var char = this;
        char.deathAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/dieEffect/skeleton.json', null, Core.scaleFactor);
        char.deathAnim.scale.set(1.5 / Core.scaleFactor, 1.5 / Core.scaleFactor);
        char.shadow.visible = false;
        super.addChild(this.deathAnim);
        char.deathAnim.animation.gotoAndPlay('dead_1');
        char.unit.animation.gotoAndPlay('die');
        TweenMax.to(char.unit, 0.4, { alpha: 0 });

        this.tween6 = new TimelineMax();

        this.tween6.add(function () {
            this.cleanUpDeathAnimAndUnit2();
        }.bind(this), 1.0);
    }
}
//TODO: Core.scale is undefine at this point. Resolve------------
//TODO: would be good to put all configs into one class or something
//TODO: BackGround animation in file named 'FightingCharacter'... no comments...
// var BgAnimationConf = [
//     'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_1/skeleton.json',
//     'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_14/skeleton.json',
//     'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_10/skeleton.json', // yellowGirl
//     'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_2/skeleton.json',
//     'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_9/skeleton.json' // blackGirl and dragon
// ];

let BgAnimationConf = []
export class BgAnimation extends PIXI.Container {
    constructor(BgId) {
        super();

        BgAnimationConf = BgAnimationConf.length ? BgAnimationConf : [
            'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_1/skeleton.json',
            'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_14/skeleton.json',
            'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_10/skeleton.json', // yellowGirl
            'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_2/skeleton.json',
            'assets/' + Core.scale + '/dBones/fight/skillBgAnimation_9/skeleton.json'   // blackGirl and dragon
        ];

        this.skillBgAnimation = Core.assets.animations.getDragonBones(BgAnimationConf[BgId], null, Core.scaleFactor);
        this.skillBgAnimation.scale.set(1.8 / Core.scaleFactor, 1.6 / Core.scaleFactor);
        super.addChild(this.skillBgAnimation);
    }
}
var charImageConf = [
    'unit2.png', //redGirl
    'unit4.png', //greenGirl
    'unit3.png', //yellowGirl
    'unit5.png', //whiteGirl
    'unit1.png'  //blackGirl
];

export class CharImage extends PIXI.Container {
    constructor(charId) {
        super();
        this.charImage = new Core.Sprite({ name: 'bigPicture', sprite: charImageConf[charId] });
        super.addChild(this.charImage);
        this.charImage.anchor.set(0.5, 0.5);
        this.charImage.position.set(200 / Core.scaleFactor, 370 / Core.scaleFactor);
        this.charImage.scale.set(-2.0, 2.0);
    }
}
export class Shadow extends PIXI.Container {
    constructor(scaleX, scaleY) {
        super();
        this.shadow = new Core.Sprite({ name: 'shadow', sprite: 'shadowCircle.png' });
        super.addChild(this.shadow);
        this.shadow.anchor.set(0.5, 0.5);
        this.shadow.scale.set(scaleX / Core.scaleFactor, scaleY / Core.scaleFactor);
    }
}
