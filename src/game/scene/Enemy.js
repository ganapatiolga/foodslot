import '../../core';
import { BgAnimation } from './FightingCharacter';
import { Shadow } from './FightingCharacter';
import {cleanUpCallbacks} from './FightScene';

let soundOptions = { volume: 1.0 }
export class Enemy extends PIXI.Container {
    constructor(center, stage) {
        super();
        this.visible = false;
        this.position.set(center.x + 250 / Core.scaleFactor, center.y + 270 / Core.scaleFactor);
        this.scale.set(0.55 / Core.scaleFactor, 0.55 / Core.scaleFactor);
        this.stage = stage;
        this.center = center;

        this.invokeAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/invokeEffect_210_5_2/skeleton.json', null, Core.scaleFactor);

        this.shadow = new Shadow(7.5, 5);
        this.shadow.visible = true;
        super.addChild(this.shadow);

        this.arrivalAnimation = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/unitArrivalAnimation/skeleton.json', null, Core.scaleFactor);
        this.arrivalAnimation.position.set(this.center.x + 250 / Core.scaleFactor, this.center.y + 230 / Core.scaleFactor);
        this.arrivalAnimation.scale.set(1.0 / Core.scaleFactor, 1.0 / Core.scaleFactor);

        this.deathAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/dieEffect/skeleton.json', null, Core.scaleFactor);
        this.deathAnim.scale.set(5.0, 5.0);
        this.deathAnim.y = -100 / Core.scaleFactor;

        var BgAnim = new BgAnimation(4);
        BgAnim.position.set(this.center.x, this.center.y);
        this.stage.addChild(BgAnim);
        this.BgAnim = BgAnim;
        var graphics = new PIXI.Graphics();
        graphics.beginFill(0x000000);
        graphics.drawRect(0, 0, 1, 1);
        var rectangle = graphics.generateTexture();
        this.blackBg = new PIXI.Sprite(rectangle);
        this.blackBg.anchor.set(0.5, 0.5);
        this.blackBg.width = 5000 / Core.scaleFactor;
        this.blackBg.height = 5000 / Core.scaleFactor;
        stage.addChild(this.blackBg);
        this.blackBg.visible = false;

        this.cleanUpDeathAnimAndUnit = function () {
            if (this.deathAnim) {
                this.deathAnim.clear();
                if(this.deathAnim.parent)
                    this.deathAnim.parent.removeChild(this.deathAnim);
                delete this.deathAnim;
            }
            if (this.unit) {
                this.unit.clear();
                if(this.unit.parent)
                    this.unit.parent.removeChild(this.unit);
                delete this.unit;
            }
        }.bind(this);

        cleanUpCallbacks.push(this.cleanUpDeathAnimAndUnit);

          this.cleanUpAttackAnimBgAnimInvokeAnim = function () {

            if (this.attackAnim) {
                this.attackAnim.clear();
                if(this.attackAnim.parent)
                     this.attackAnim.parent.removeChild(this.attackAnim);
                delete this.attackAnim;
            }
            if (this.BgAnim.skillBgAnimation) {
                this.BgAnim.skillBgAnimation.clear();
                if(this.BgAnim.skillBgAnimation.parent)
                     this.BgAnim.skillBgAnimation.parent.removeChild(this.BgAnim.skillBgAnimation);
                delete this.BgAnim.skillBgAnimation;
            }

            if (this.invokeAnim) {
                this.invokeAnim.clear();
                if(this.invokeAnim.parent)
                    this.invokeAnim.parent.removeChild(this.invokeAnim);
                delete this.invokeAnim;
            }
        }.bind(this);
        cleanUpCallbacks.push(this.cleanUpAttackAnimBgAnimInvokeAnim);

          this.cleanUpArrivalAnimation = function () {
            if (this.arrivalAnimation) {
                this.arrivalAnimation.clear();
                if(this.arrivalAnimation.parent)
                    this.arrivalAnimation.parent.removeChild(this.arrivalAnimation);
                delete this.arrivalAnimation;
            }
        }.bind(this);
        cleanUpCallbacks.push(this.cleanUpArrivalAnimation);

    }
    arrive() {
        var enemy = this;
        enemy.stage.addChild(enemy.arrivalAnimation);
        enemy.arrivalAnimation.bringToFront();
        enemy.arrivalAnimation.animation.gotoAndPlay('start');

        this.tween = new TimelineMax();

        this.tween.add(function () {
            this.cleanUpArrivalAnimation();
        }.bind(this), 4.0);
    }
    attack(character) {
        var enemy = this;
        var distance = enemy.position.x - character.position.x - (character.width * 2);
        TweenMax.to(enemy, 0.8, { x: "-=" + distance, ease: Power3.easeInOut });

        this.tween1 = new TimelineMax();

        this.tween1.add(function () {
            enemy.unit.animation.gotoAndPlay('atk_normal_0_1');
        }, 0.2);
        this.tween1.add(function () {
//            Core.assets.sounds.playSound('unit_0839_0837_06_7', false, soundOptions);
            enemy.emit('hit');
        }, 0.6);
        this.tween1.add(function () {
            TweenMax.to(enemy, 0.4, {x: "+=" + distance, ease: Power3.easeInOut });
        }, 1.1);
        this.tween1.add(function () {
            enemy.unit.animation.gotoAndPlay('wait');
            enemy.turnEnd();
        }, 1.5);
    }
    specialAttack(char2, char3, container) {
        var enemy = this;
        var blackBg = this.blackBg;
        var camera = container;
        enemy.attackAnim = Core.assets.animations.getDragonBones('assets/' + Core.scale + '/dBones/fight/skillEffect_180/skeleton.json', null, Core.scaleFactor);
        enemy.attackAnim.position.set(char2.position.x, (char2.position.y + 80 / Core.scaleFactor));
        enemy.attackAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);

        blackBg.visible = true;
        enemy.BgAnim.skillBgAnimation.animation.gotoAndPlay('start');
        enemy.BgAnim.bringToFront();
        blackBg.bringToFront();
        enemy.bringToFront();
        char2.bringToFront();
        char3.bringToFront();

        this.tween2 = new TimelineMax();

        this.tween2.add(function () {
//            Core.assets.sounds.playSound('skillSE_in_1', false, soundOptions);
            enemy.invokeAnim.animation.gotoAndPlay('preview');
            TweenMax.to(enemy.invokeAnim, 3, { alpha: 0 });
            enemy.invokeAnim.position.set(enemy.center.x + 250 / Core.scaleFactor, enemy.center.y + 230 / Core.scaleFactor);
            enemy.invokeAnim.scale.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
            enemy.stage.addChild(enemy.invokeAnim);
            enemy.invokeAnim.bringToFront();
            TweenMax.to(camera.position, 0.3, { x: -300 / Core.scaleFactor, y: 0 });
            TweenMax.to(camera.scale, 0.3, { x: 1.1, y: 1.1 })
        }, 0.1);
        this.tween2.add(function () {
            blackBg.visible = false;
            enemy.unit.animation.gotoAndPlay('atk_skill_0_1');
//            Core.assets.sounds.playSound('unit_0839_0837_06_3', false, soundOptions)
        }, 0.3);
        this.tween2.add(function () {
            TweenMax.to(camera.position, 0.3, { x: 0, y: 0 });
            TweenMax.to(camera.scale, 0.3, { x: 1.0, y: 1.0 })
        }, 0.8);
        this.tween2.add(function () {
            enemy.attackAnim.animation.gotoAndPlay('start');
            enemy.stage.addChild(enemy.attackAnim);
            enemy.attackAnim.bringToFront();
//            Core.assets.sounds.playSound('skillSE_sh_38', false, soundOptions);
        }, 1.8);
        this.tween2.add(function () {
            enemy.attackAnim.animation.gotoAndPlay('start');
//            Core.assets.sounds.playSound('skillSE_sh_38', false, soundOptions);
        }, 1.9);
        this.tween2.add(function () {
//            Core.assets.sounds.playSound('skillSE_sh_39', false, soundOptions)
        }, 2.1);
        this.tween2.add(function () {
//            Core.assets.sounds.playSound('skillSE_ex_35', false, soundOptions);
            enemy.emit('hit');
        }, 2.3);
        this.tween2.add(function () {
            enemy.unit.animation.gotoAndPlay('wait');
            enemy.BgAnim.skillBgAnimation.animation.gotoAndPlay('end');
            enemy.turnEnd();
        }, 3.5);

        this.tween2.add(function () {
            this.cleanUpAttackAnimBgAnimInvokeAnim();
        }.bind(this), 4.0);
    }
    die() {
//        Core.assets.sounds.playSound('unit_0839_0837_06_9', false, soundOptions);
        var char = this;
        char.unit.animation.gotoAndPlay('die');

        this.tween3 = new TimelineMax();

        this.tween3.add(function () {
            TweenMax.to(char.unit, 1.0, { alpha: 0 });
        }, 0.2);
        this.tween3.add(function () {
            char.shadow.visible = false;
            char.addChild(char.deathAnim);
            char.deathAnim.animation.gotoAndPlay('dead_1');
        }, 0.3);
        this.tween3.add(function () {
           this.cleanUpDeathAnimAndUnit();
        }.bind(this), 1.0);

    }

    turnEnd() {
        super.emit('Complete');
    }
}
