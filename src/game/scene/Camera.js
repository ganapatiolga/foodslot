import '../../core';

export class Camera extends PIXI.Container{
     constructor(stage){
          super();
          stage.addChild(this);
     }
     shake(animation) {
         var camera = this;
         try {
             animation.bringToFront()
         } catch (e) {
             // Do Nothing
         }

        this.tween = new TimelineMax();

        this.tween.add(function(){
              TweenMax.to(camera, 0.04, {x:"+="+110/Core.scaleFactor, y:"-="+110/Core.scaleFactor, yoyo:true})
        }, 0.005);

        this.tween.add(function(){
            TweenMax.to(camera, 0.04, {y:"+="+80/Core.scaleFactor, yoyo:true})
        }, 0.05);

        this.tween.add(function(){
            TweenMax.to(camera, 0.04, {x:"-="+90/Core.scaleFactor, y: "-="+90/Core.scaleFactor, yoyo:true})
        }, 0.1);

        this.tween.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+60/Core.scaleFactor, yoyo:true})
        }, 0.15);

        this.tween.add(function(){
             TweenMax.to(camera, 0.04, {x:"+="+100/Core.scaleFactor, y: "-="+50/Core.scaleFactor, yoyo:true})
        }, 0.20);
        this.tween.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+40/Core.scaleFactor, yoyo:true})
        }, 0.25);

        this.tween.add(function(){
            TweenMax.to(camera, 0.04, {x:"-="+90/Core.scaleFactor, y: "-="+30/Core.scaleFactor, yoyo:true})
        }, 0.30);

        this.tween.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+30/Core.scaleFactor, yoyo:true})
        }, 0.35);

        this.tween.add(function(){
            TweenMax.to(camera.position, 0.05, {x:0, y:0, yoyo:true})
        }, 0.40);
     }
     impact(){
         var camera = this;

         this.tween2 = new TimelineMax();

         this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {x:"+="+30/Core.scaleFactor, y:"-="+30/Core.scaleFactor, yoyo:true})
         },0.005);

         this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+15/Core.scaleFactor, yoyo:true})
         }, 0.04);

          this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {x:"-="+20/Core.scaleFactor, y: "-="+20/Core.scaleFactor, yoyo:true})
         }, 0.1);

          this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+30/Core.scaleFactor, yoyo:true})
         }, 0.15);

          this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {x:"+="+15/Core.scaleFactor, y: "-="+20/Core.scaleFactor, yoyo:true})
         }, 0.20);

          this.tween2.add(function(){
             TweenMax.to(camera, 0.04, {y:"+="+10/Core.scaleFactor, yoyo:true})
         }, 0.25);

          this.tween2.add(function(){
            TweenMax.to(camera.position, 0.05, {x:0, y:0, yoyo:true})
         }, 0.30);
     }
     moveTo(posX, posY, scaleX, scaleY, duration){
       var camera = this;
       TweenMax.to(camera.position, duration, {x: posX, y: posY});
       TweenMax.to(camera.scale, duration, {x: scaleX, y: scaleY});
     }

}
