import '../../core';
var PIXI = require('pixi.js');

export class Cinema{
    constructor(stage){
        this.graphics = new PIXI.Graphics();
        this.graphics.beginFill(0x000000);
        this.graphics.drawRect(0, 0, 1, 1);
        this.rectangle = this.graphics.generateTexture();

        var middleHeight = stage.height/2;

        this.cinemaUp = new PIXI.Sprite(this.rectangle);
        this.cinemaUp.position.x = -1000/Core.scaleFactor;
        this.cinemaUp.position.y = middleHeight- 1300/Core.scaleFactor;
        this.cinemaUp.width = stage.width * 5/Core.scaleFactor;
        this.cinemaUp.height = 1000/Core.scaleFactor;
        stage.addChild(this.cinemaUp);
        this.cinemaUp.visible = false;

        this.cinemaDown = new PIXI.Sprite(this.rectangle);
        this.cinemaDown.position.x = -1000/Core.scaleFactor;
        this.cinemaDown.position.y = middleHeight+ 300/Core.scaleFactor;
        this.cinemaDown.width = stage.width*5/Core.scaleFactor;
        this.cinemaDown.height = 1000/Core.scaleFactor;
        stage.addChild(this.cinemaDown);
        this.cinemaDown.visible = false;
    }
    on(){
        this.cinemaUp.visible = true;
        this.cinemaDown.visible = true;
    }
    off(){
        this.cinemaUp.visible = false;
        this.cinemaDown.visible = false;
    }
}
