import '../../core';
import { FightingCharacter, getPositions, CharImage, BgAnimation } from './FightingCharacter';
import { FreeSpinsBattlePopup } from '../panels/freeSpinsBattlePopup';

var EventEmitter = require('eventemitter3');
const soundOptions = { volume: 1.0 }

export class Scene extends EventEmitter {
    constructor() {
        super();
    }
}
PIXI.DisplayObject.prototype.bringToFront = function () {
    this.parent.addChild(this);
};

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a
}

var cleanUpCallbacks = [];

export class FightScene {
    constructor(scene, camera) {
        this.camera = camera;
        this.tm = null;

        this.endCallback = scene.onFightEnded.bind(scene);
        var winningChar = scene.fightResult - 7;
        this.scene = scene;

        var charList = ['bonus_chr_yel', 'bonus_chr_bla', 'bonus_chr_gre', 'bonus_chr_whi', 'bonus_chr_red'];
        var i = 0;
        this.rouletteChar = new Array(3);
        for (var j in scene.selectedChars) {
            if ((scene.selectedChars[j] !== undefined) && (scene.selectedChars[j] === true)) {
                this.rouletteChar[i] = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/bonusin/' + charList[j] + '.json');
                this.rouletteChar[i].position.set(this.scene.center.x, this.scene.center.y);
                ++i;
            }
        }
        this.rouletteChar = shuffle(this.rouletteChar);

        this.rouletteResult = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/bonusin/' + charList[scene.fightResult - 8] + '.json');
        this.rouletteBasket = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/bonusin/bonus_obj_seiro.json');
        this.rouletteBasket.position.set(this.scene.center.x, this.scene.center.y);

        this.rouletteBG = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/bonusin/bonus_bgd.json');
        this.rouletteBG.w = this.rouletteBG.width;
        this.rouletteBG.h = this.rouletteBG.height;


        this.playTurn(scene.stage);
    }
    playTurn(stage) {
        var game = this;

        this.tm = new TimelineMax({paused: true});

        this.tm.add(function() {
            this.scene.stage.addChild(this.rouletteBasket);
            this.rouletteBasket.width = this.rouletteBasket.width / 2 / Core.scaleFactor;
            this.rouletteBasket.height = this.rouletteBasket.height / 2 / Core.scaleFactor;
            this.rouletteBasket.state.setAnimation(0, 'in_aori', true);
            this.rouletteBasket.state.onComplete = function(trackIndex, loopCount) {
                this.update(335 / 30);
            }.bind(this.rouletteBasket);
            this.scene.stage.addChild(this.rouletteChar[0]);
            this.rouletteChar[0].width = this.rouletteChar[0].width / 2 / Core.scaleFactor;
            this.rouletteChar[0].height = this.rouletteChar[0].height / 2 / Core.scaleFactor;
            this.rouletteChar[0].state.setAnimation(0, 'in', false);
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_GirlSpell_Small', false, {volume: 0.5});
        }.bind(this), '+=0.7')
        .add(function() {
            this.scene.stage.addChild(this.rouletteChar[1]);
            this.rouletteChar[1].width = this.rouletteChar[1].width / 2 / Core.scaleFactor;
            this.rouletteChar[1].height = this.rouletteChar[1].height / 2 / Core.scaleFactor;
            this.rouletteChar[1].state.setAnimation(0, 'in', false);
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_GirlSpell_Small', false, {volume: 0.5});
        }.bind(this), '+=' + (335 - 246)  / 30)
        .add(function() {
            this.scene.stage.addChild(this.rouletteChar[2]);
            this.rouletteChar[2].width = this.rouletteChar[2].width / 2 / Core.scaleFactor;
            this.rouletteChar[2].height = this.rouletteChar[2].height / 2 / Core.scaleFactor;
            this.rouletteChar[2].state.setAnimation(0, 'in', false);
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_GirlSpell_Small', false, {volume: 0.5});
        }.bind(this), '+=' + (424 - 335)  / 30)
        .add(function() {
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_Spin&Spell', false, {fadeIn: {time: 3.0}, volume: 0.5});
        }.bind(this), '+=' + 89 / 30)
        .add(function() {
            this.rouletteResult.width = this.rouletteResult.width / 2 / Core.scaleFactor;
            this.rouletteResult.height = this.rouletteResult.height / 2 / Core.scaleFactor;
            this.rouletteResult.state.setAnimation(1, 'out', true);
            this.rouletteResult.position.set(this.scene.center.x, this.scene.center.y);
            this.scene.stage.addChild(this.rouletteResult);
            this.rouletteResult.state.onComplete = function(trackIndex, loopCount) {
                this.update(90 / 30);
            }.bind(this.rouletteResult);

            this.rouletteBasket.state.setAnimation(0, 'kettei', true);
            this.rouletteBasket.state.onComplete = function(trackIndex, loopCount) {
                this.update(90 / 30);
            }.bind(this.rouletteBasket);
        }.bind(this), '+=' + (528 - 5 - 424) / 30)
        .add(function() {game.endCallback();}, '+=2.1');

        this.tm.play();
    }
}
export class FightingBonusScene extends Scene {
    constructor(stage, topStage, center, selectedChars) {
        super();
        cleanUpCallbacks = [];
        this.center = center;
        this.stage = stage;

        this.topStage = topStage;

        this.rouletteBG = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/bonusin/bonus_bgd.json');
        this.rouletteBG.w = this.rouletteBG.width;
        this.rouletteBG.h = this.rouletteBG.height;

        this.cleanUpBossEffect = function () {
        }.bind(this);

        cleanUpCallbacks.push(this.cleanUpBossEffect);
        this.selectedChars = selectedChars;

        this.buildBackground();

        var characters = this.characters;

        this.freeSpinsBattlePopup = new FreeSpinsBattlePopup()
        this.freeSpinsBattlePopup.scale.set(1.0, 1.0);
        this.freeSpinsBattlePopup.position.set(this.center.x, this.center.y);
        this.freeSpinsBattlePopup.visible = false;

        this.topStage.addChild(this.freeSpinsBattlePopup);

        TweenMax.delayedCall((90 - 55) / 30, function() {
            this.freeSpinsBattlePopup.bringToFront();
            this.freeSpinsBattlePopup.show();
            this.freeSpinsBattlePopup.alpha = 0;
        }.bind(this));

        TweenMax.delayedCall((235 - 55) / 30, function() {
            this.freeSpinsBattlePopup.hide(() => {this.rect.visible = false});
        }.bind(this));



        this.rect = new PIXI.Graphics();
        this.rect.beginFill();
        this.rect.drawRect(-1000 / Core.scaleFactor, -1000 / Core.scaleFactor, 3000 / Core.scaleFactor, 3000 / Core.scaleFactor);
        this.rect.endFill();
        this.rect.alpha = 0.6;
        this.stage.addChild(this.rect);
        this.rect.visible = true;

        TweenMax.delayedCall((235 - 55) / 30, function() {
            TweenMax.to(this.rect, (245 - 235) / 30, {alpha : 0, repeat: -1});
        }.bind(this));
    }

    buildBackground() {
        TweenMax.delayedCall(0.5, function() {
            let stage = this.stage;

            this.rouletteBG.state.addAnimation(0, 'in_loop', true, 1.5);
            this.rouletteBG.width = this.rouletteBG.w / 2 / Core.scaleFactor;
            this.rouletteBG.height = this.rouletteBG.h / 2 / Core.scaleFactor;
//            this.rouletteBG.position.set(this.center.x, this.center.y - 40 / Core.scaleFactor);
            this.rouletteBG.position.set(this.center.x, this.center.y);

            stage.addChild(this.rouletteBG);

            this.rouletteBG.state.onComplete = function(trackIndex, loopCount) {
                this.update(45 / 30);
            }.bind(this.rouletteBG);
        }.bind(this));
    }

    showFreeSpinsBattlePopUp() {
        this.rect.alpha = 0.6;
        this.rect.visible = true;

        this.rect.bringToFront();
        this.freeSpinsBattlePopup.bringToFront();
        this.freeSpinsBattlePopup.show();
    }
    removeFreeSpinsBattlePopUp() {
        this.freeSpinsBattlePopup.hide(() => {this.rect.visible = false});
    }
    playRoulette(result) {
        this.fightResult = result;
        this.fightScene = new FightScene(this, this.camera);
    }

    onFightEnded() {
        super.emit('AnimationCompleted');
    }
}

export { cleanUpCallbacks }
