import { symbolParts } from '../slot/symbol';

var PIXI = require('pixi.js');

var textureArray = {
    '0': 'simpleGlow.png',
    '1': 'singleGlow.png',
    '2': 'doubleGlow.png',
    '3': 'tripleGlow.png',
};

class PaylineGlow extends PIXI.Container {
    constructor() {
        super();
        let glow = new PIXI.Sprite();
        this.glow = glow;
        this.addChild(this.glow);
        this.glow.scale.set(1.25, 1.05);
    }

    setStack(stack, textureId) {
        if (stack === 0) {
            this.glow.alpha = 0.0;
        } else {
            this.glow.alpha = 0.5;

            if (textureId < 8)
                stack = 0;

            this.glow.texture = Core.assets.getTexture(textureArray[stack]);
        }
    }

    animate(time) {
        this.timeline = new TimelineMax({onComplete:this.removeFrame,onCompleteScope: this});
        this.timeline.fromTo(this.glow, time / 2, { alpha: 0 }, { alpha: 0.5, ease: Expo.easeOut }, 0);
        this.timeline.fromTo(this.glow, time / 2, { alpha: 0.5 }, { alpha: 0, ease: Power2.easeIn }, time / 2)
    }

    cancelAnimations() {
        if (this.timeline) {
            this.timeline.kill();
        }
        this.glow.alpha = 0.0
    }

    removeDarkenPanel() {
        this.glow.alpha = 0.0;
    }

    setDarkenPanel() {
        this.glow.alpha = 0.5;
        this.glow.texture = Core.assets.getTexture('noGlow.png');
    }
}

export { PaylineGlow };
