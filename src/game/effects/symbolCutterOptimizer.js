import '../../core';
import { TextureCutter } from '../textureCutter';

Core.inloading.push(() => {
    try{
        for(var i = 1; i <= 5; i++ ){
           let  triplet = TextureCutter(Core.assets.getTexture("symbol"+ i +"_triple.png"), i);
            Core.assets.textures["symbol_triple"+ i +".png"] = triplet[2];
            Core.assets.textures["symbol_double"+ i +".png"] = triplet[1];
            Core.assets.textures["symbol_single"+ i +".png"] = triplet[0];
        }
    }catch(err){
        //ignore
        console.error(err)
    }
});
