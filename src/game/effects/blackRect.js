import '../../core';
var PIXI = require('pixi.js');

const BlackRect = function(parent, aValue = 0.65) {
    let el = new PIXI.Graphics();
    el.beginFill(0x000000);
    el.drawRect(-1000 / Core.scaleFactor, -1000 / Core.scaleFactor, 2000 / Core.scaleFactor, 2000 / Core.scaleFactor);
    el.endFill();
    el.alpha = 0;
    el.position.set(parent.width / 2, parent.height / 2);
    parent.addChild(el);

    let tween = null;

    parent.darken = function(value){
        let tweenTo = value ? aValue : 0.0;
        if (tween) tween.kill();
        tween = TweenMax.to(el, 0.5, {alpha: tweenTo});
    };

    return parent;
};

export {BlackRect}