import '../../core';
var PIXI = require('pixi.js');

const Glowworm = stage => {
    var textures = [PIXI.Texture.fromImage('dot.png')];

    let leftMargin = stage.width / 2 - 500 / Core.scaleFactor - ({'low': 5, 'med': 9, 'high': 18})[Core.scale];
    let rightMargin = stage.width / 2 + 500 / Core.scaleFactor + ({'low': 5, 'med': 9, 'high': 18})[Core.scale];

    let container = Core.ParticleEmitter(textures, {
        spawnRect: {
            left: -200 / Core.scaleFactor,
            right: stage.width + 200 / Core.scaleFactor,
            top: stage.height / 3,
            bottom: stage.height
        },
        minVel: {
            x: 0.01,
            y: -5 / Core.scaleFactor
        },
        maxVel: {
            x: 0.01,
            y: 6 / Core.scaleFactor
        },
        acc: {
            x: 0,
            y: -0.5 / Core.scaleFactor
        },
        lifetime: 15,
        addAtBack: true,
        frequency: 3,
        spawnAmount: ({'low': 4, 'med': 6, 'high': 10})[Core.scale],
        alpha: 0,
        customFn: (obj) => {
            let changeAplha = val => obj.alpha = Math.min(1.0, obj.alpha + val);
            let yesNo = (val = 0.5) => Math.random() > val;

            if (obj.position.x > leftMargin && obj.position.x < rightMargin) obj.x = Math.random() * leftMargin * 0.95 + (yesNo() ? 0 : rightMargin);
            if (yesNo(0.90)) obj.age -= 0.05;
            if (obj.alpha < 1) changeAplha(0.01);
            if (obj.alpha > 0 && obj.age + 1 > obj.maxLife) changeAplha(-0.05);
            if (0.2 > obj.alpha < 0.8 && yesNo()) changeAplha(yesNo() ? -0.1 : 0.1);
            obj.position.x += Math.sin(obj.age) / 8 / Core.scaleFactor;
        }
    });

    stage.addChild(container);

    return container;
};

export { Glowworm };