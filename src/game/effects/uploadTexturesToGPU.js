import '../../core';

let ctx = null;

Core.globalContext.once('contextChange', engine => ctx = engine.ctx);

Core.inloading.push(() => {
    try{
        let texturesToUpload = ['symbol_single1_blur.png', 'symbol_single1.png' ,'anticipation_frame.png'];

        if(ctx.renderer === null)
            throw new Error('Renderer is not initialized');
        if(ctx.renderer.type !== PIXI.WEBGL_RENDERER)
            throw new Error('Renderer is not in WebGL mode');

        texturesToUpload.map(textureName => Core.assets.getTexture(textureName))
            .map(texture => texture.baseTexture ? texture.baseTexture : texture)
            .forEach(texture => texture._glTextures.length <= 0 ? ctx.renderer.bindTexture(texture) : null)

    }catch(err){
    }
});

