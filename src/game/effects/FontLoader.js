import '../../core';

Core.inloading.push(() => new Promise(resolve => {
    const fontList = ['Noto Serif Cond Blk', 'Noto Serif Cond'].map(font => {
        var div = document.createElement('div');
        div.innerHTML = `loading font ${font}`;
        div.style.position = 'absolute';

        var width = div.offsetWidth;
        div.style.fontFamily = font;

        document.body.appendChild(div);

        return [div, width];
    });

    var id = setInterval(() => {
        for (var idx = 0; idx < fontList.length;) {
            var font = fontList[idx];
            if (font[0].offsetWidth !== font[1]) {
                font[0].parentNode.removeChild(font[0]);
                fontList.splice(idx, 1);
            } else {
                idx++;
            }
        }

        if ( ! fontList.length) {
            clearInterval(id);
            resolve();
        }
    }, 100);

}));