import '../../core';

const Lightbeam = (stage, numberOfBeams = 32) => {
    const beamSprites = [];
    const timeline = new TimelineMax();

    const container = new PIXI.Container();
    container.name = 'lightbeam';

    for (let i = 0; i < numberOfBeams; i++) {
        let beam = new Core.Sprite({
            name: 'beam' + i,
            sprite: 'beam.png',
            rotation: 0.5,
            alpha: 0,
            pivotX: 1,
            pivotY: 0,
        });

        timeline.add(createAnimation(stage, beam), Math.random() * 4);
        beamSprites.push(beam);
        container.addChild(beam);
    }

    stage.addChild(container);
};

const createAnimation = (stage, beam) => {
    beam.position.set(300 / Core.scaleFactor + Math.random() * stage.width, -beam.height - 50 / Core.scaleFactor);
    beam.scale.x = Math.max(0.5, Math.random() * 4);
    beam.scale.y = Math.max(1, Math.random() * 6);

    return TweenMax.to(beam,  5 + Math.random() * 10, {yoyo: true, repeat: -1, alpha: 0.2 + Math.random() * 0.4, rotation: Math.random() < 0.5 ? 0.48 : 0.52, ease: Linear.easeIn});
};

export { Lightbeam }