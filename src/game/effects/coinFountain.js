import '../../core';

var cf1 = null;

var CoinFountain = function(){
    if(cf1)return cf1;

    var textures = [Core.assets.getTexture('coin.png'),
                   Core.assets.getTexture('coin1.png'),
                   Core.assets.getTexture('coin2.png'),
                   Core.assets.getTexture('coin3.png')];

    let container = Core.ParticleEmitter(textures, {
        spawnRect: {
            left: -150 / Core.scaleFactor,
            right: 150 / Core.scaleFactor,
            top: -150 / Core.scaleFactor,
            bottom: 150 / Core.scaleFactor
        },
        minVel: {
            x: -1500/Core.scaleFactor,
            y: -1500/Core.scaleFactor
        },
        maxVel: {
            x: 1500/Core.scaleFactor,
            y: 1500/Core.scaleFactor
        },
        acc: {
            x: 0,
            y: 1000 / Core.scaleFactor
        },
        addAtBack: true,
        frequency: 0.1,
        spawnAmount: 10
    });
    Object.assign(container, {

        start: (base => () => {
            base()
        })(container.start),
        stop: (base => () => {
            base()
        })(container.stop)
    });
    cf1 = container;
    return container;
};

export {CoinFountain}
