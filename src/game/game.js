var EventEmitter = require('eventemitter3');
var GeneralConfig = require('./config/general.json');
var PIXI = require('pixi.js');

import './scaleDetector';
import '../core';
import { Camera } from './scene/Camera';
import { FightingBonusScene } from './scene/FightScene';

import { Glowworm } from './effects/glowworm';
import { SlotMachine } from './slot/slotMachine';
import { Overlay } from './overlay';
import { UI } from './ui/ui';
import { GameController } from './controller/gameController';
import { detectScale } from './scaleDetector';

import { PixiSpine } from '../core/animations.js';

PIXI.GC_MODES.DEFAULT = PIXI.GC_MODES.AUTO;

export class Game extends EventEmitter {
    constructor() {
        super();
        this.engine = new Core.Engine();
        detectScale();
        this.engine.loader.on('LoadingCompleted', () => {
            this.emit('LoadingFinished')
        });
        this.ui = new UI();
        this.controller = new GameController(this);
        this.firstTimeCharSelect = true;
        this.musicOnPlay = {
            mainBGM: false,
            battleMusicLoop: false,
            bonusMusicLoop: false
        }
        this.background = undefined;
        this.lightBeam = undefined;
    }

    load(outerProgressHandler) {
        let resourcesToLoad = Core.manifest.data.assets[Core.scale].textures.loading.files
            .concat(Core.manifest.data.assets[Core.scale].textures.files)
            .concat(Core.manifest.data.assets[Core.scale].spine.files)
            .concat(Core.manifest.data.assets.files)
            .concat(Core.manifest.data.config.files);

        let previousProgress = 0;
        let maxnumToload = 0;

        const progressHandler = function (loader, resource) {
            maxnumToload = maxnumToload < loader._numToLoad ? loader._numToLoad : maxnumToload;
            let progress = 99 * Math.min((1 - loader._numToLoad / maxnumToload), 1.0);
            let currentProgress = progress > previousProgress ? progress : previousProgress;
            // gAPI.loader.default.progress = currentProgress / 100;
            outerProgressHandler(currentProgress / 100);
            previousProgress = currentProgress;
        };
        this.engine.loader.load(resourcesToLoad, progressHandler);
    }

    initSwipeOverlay() {
        gAPI.casinoUI.hideLoader();
        this.clearScreen();

        let overlayElement = this.engine.stage.getElement('overlay');
        overlayElement.setMainFrame(0, 0, 1136 / Core.scaleFactor, 640 / Core.scaleFactor);
        this.overlay = new Overlay(overlayElement);

        this.engine.stage.layout = 'config/layouts/main.evfl';
    }

    initCharacterSelectionScene(oldData) {
        this.clearScreen();

        gAPI.casinoUI.hideLoader();
        this.clearScreen();
        var stage = this.engine.stage.getElement('frame');

        this.buildBackground(true, true, true);

        this.ui.initCharacterSelection(stage, oldData);
        this.engine.stage.layout = 'config/layouts/main.evfl';
    }

    initMainGameScene(resumeData, isLoaded = false) {
        this.clearScreen();

        gAPI.casinoUI.enableAutoPlay();

        // if (!(data.resume && data.resume.freeSpins)) { //old code
        //TODO: make sure it works as expected and find way to do it better
        if (!(resumeData && (resumeData.action.startsWith('freeSpin') || resumeData.action === 'battle' ))) {
            // Core.assets.sounds.stopSound('assets/sounds/music/bonusMusicLoop.mp3', { fadeOut: { time: 0.5 } });
            Core.assets.sounds.stopSound('Music_Bonus_Loop', { fadeOut: { time: 0.5 } });
            Core.assets.sounds.stopSound('Music_FreeSpinIntro_Loop', { fadeOut: { time: 0.5 } });
            this.musicOnPlay.bonusMusicLoop = false;
            if (this.musicOnPlay.mainBGM === false) {
                // Core.assets.sounds.playMusic('assets/sounds/music/main.mp3', true, { fadeIn: { time: 0.5 }, volume: 0.02 });
                if ((this.slotMachine === undefined) || (this.slotMachine.score === undefined) || (!this.slotMachine.score.freeSpins)) {
                    Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, { fadeIn: { time: 0.5 }, volume: 0.5 });
                }
                this.musicOnPlay.mainBGM = true;
            }
        }

        let overlayFrameElement = this.engine.stage.getElement('overlayFrame');
        this.overlay.init(overlayFrameElement);
        let stage = this.engine.stage.getElement('frame');
        let ui = this.engine.stage.getElement('ui');

        let backgroundSprites = this.buildBackground();

        this.slotMachine = new SlotMachine(stage, overlayFrameElement, false, isLoaded);
        this.slotMachine.score.freeSpins = false;

        this.ui.initUI(ui);

        if (resumeData && resumeData.spinResult) {
            this.slotMachine.slotPanel.setSymbols(resumeData.spinResult.reelDisplay);
        }

        this.engine.stage.layout = 'config/layouts/main.evfl';
    }

    buildBackground(freeGame = false, position = false, addCenter = false) {
        let stage = this.engine.stage.getElement('frame');

        if (this.background !== undefined) {
            console.log('background not deleted');
        }

        let bg = {};
        let skeleton = '';
        let label = '';

        if (freeGame === false) {
            skeleton = 'assets/' + Core.scale + '/spine/background/main_bg.json'
            label = 'animation';
        } else {
            skeleton = 'assets/' + Core.scale + '/spine/freespin/FS_bgd.json'
            label = 'loop';
        }

        bg = Core.assets.animations.getSpineSkeleton(skeleton);
        bg.state.setAnimation(0, label, true);
        bg.state.onComplete = function(trackIndex, loopCount) {
            this.update(1 / 30);
        }.bind(bg);

        bg.width = stage.width * 1.60;
        bg.height = stage.height * ((freeGame === false) ? 1.90 : 1.80);

        bg.position.set(stage.width / 2, stage.height / 2);

        stage.addChild(bg);

        this.background = bg;

        return bg;
    }

    showLightBeam(stage) {
        if (this.lightBeam !== undefined) {
            console.log('lightBeam not deleted');
        }

        let effect = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/freespin/FS_eff.json');
        effect.state.setAnimation(0, 'FS_eff01', true);
        effect.state.onComplete = function(trackIndex, loopCount) {
            this.update(1 /30);
        }.bind(effect);

        effect.state.timeScale = 1.0;

        stage.addChild(effect);

        effect.width = effect.width / Core.scaleFactor;
        effect.height = effect.height / Core.scaleFactor;
        effect.y = stage.height / 2;
        effect.x = stage.width / 2;

        this.lightBeam = effect;

        return effect;
    }

    initFightingCutscene() {
        this.clearScreen();
        // Core.assets.sounds.stopSound('assets/sounds/music/bonusMusicLoop.mp3', { fadeOut: { time: 2.0 } });
        // Core.assets.sounds.stopSound('assets/sounds/music/main.mp3', { fadeOut: { time: 2.0 } });
        Core.assets.sounds.stopSound('Music_Bonus_Loop', { fadeOut: { time: 2.0 } });
        Core.assets.sounds.stopSound('Music_BaseGame_Loop', { fadeOut: { time: 2.0 } });
        this.musicOnPlay.mainBGM = false;
        this.musicOnPlay.bonusMusicLoop = false;
        var overlayFrameElement = this.engine.stage.getElement('overlayFrame');
        this.overlay.init(overlayFrameElement);
        var stage = this.engine.stage.getElement('frame');
        var topStage = this.engine.stage.getElement('topFrame');
        var viewport = new Camera(stage);

        this.scene = new FightingBonusScene(viewport, topStage, new PIXI.Point(stage.width / 2, stage.height / 2), this.data);
        this.engine.stage.layout = 'config/layouts/main.evfl';
    }

    initFreeSpinsScene() {
        if (this.scene && this.scene.fightScene) {
            Core.assets.sounds.stopSound('SFX_FreeSpinIntro_GirlSpell_Small');
            Core.assets.sounds.stopSound('SFX_FreeSpinIntro_Spin&Spell');
            this.scene.fightScene.tm.kill();
        }

        this.clearScreen();
        this.scene = null;
        gAPI.casinoUI.disableAutoPlay();
        // Core.assets.sounds.stopSound('assets/sounds/music/main.mp3', { fadeOut: { time: 2.0 } });
        Core.assets.sounds.stopSound('Music_BaseGame_Loop', { fadeOut: { time: 2.0 } });
        this.musicOnPlay.mainBGM = false;
        // Core.assets.sounds.playMusic('assets/sounds/music/bonusMusicLoop.mp3', true, { fadeIn: { time: 2.0 }, volume: 0.07 });
        this.musicOnPlay.bonusMusicLoop = true;
        var overlayFrameElement = this.engine.stage.getElement('overlayFrame');
        var stage = this.engine.stage.getElement('frame');

        this.buildBackground(true, true);

        this.ui.skipButton.visible = false;

        this.slotMachine = new SlotMachine(stage, overlayFrameElement, true);

        // gAPI.casinoUI.setFreeSpinsTotalWin(this.slotMachine.score.score);

        // this.slotMachine.score.freeSpinsTotalWin = this.slotMachine.score.score;
        gAPI.casinoUI.setFreeSpinsTotalWin(this.slotMachine.score.freeSpinsTotalWin);

        this.showLightBeam(stage);

        this.engine.stage.layout = 'config/layouts/main.evfl';
    }

    clearScreen() {
        var layoutConf = GeneralConfig.layout;
        var background = this.engine.stage.getElement('background');
        var overlayFrameElement = this.engine.stage.getElement('overlayFrame');
        var middleBg = this.engine.stage.getElement('divider');
        var stage = this.engine.stage.getElement('frame');
        var topStage = this.engine.stage.getElement('topFrame');
        var ui = this.engine.stage.getElement('ui');
        ui.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);
        background.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);
        middleBg.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);
        stage.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);
        overlayFrameElement.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);
        topStage.setMainFrame(0, 0, layoutConf.stage.width / Core.scaleFactor, layoutConf.stage.height / Core.scaleFactor);

        if (this.background !== undefined) {
            delete this.background;
            this.background = undefined;
        }

        if (this.lightBeam !== undefined) {
            delete this.lightBeam;
            this.lightBeam = undefined;
        }
    }
}
