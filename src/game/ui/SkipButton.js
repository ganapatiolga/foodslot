import '../../core';

export class SkipButton extends PIXI.Container {
    constructor(){
        super();

        let skipText = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame.skip;
        this.interactive = true;

        let text = new PIXI.Text(skipText, {
            fontFamily: 'Noto Serif Cond Blk',
            fontSize: 45 / Core.scaleFactor + "px",
            align: 'center',
            fill: '#51354F',
            dropShadow: true,
            dropShadowAngle: Math.PI / 2,
            dropShadowColor: '#050505',
            dropShadowDistance: 2 / Core.scaleFactor });
        text.alpha = 0.75
        text.anchor.set(0.5);

        let buttonBg = this.createButtonBackground(text.width)

        text.x = buttonBg.width / 2
        text.y = buttonBg.height / 2

        this.addChild(buttonBg)
        this.addChild(text)

        this.interactive = true;
    }

    createButtonBackground(textWidth){
        let buttonBg = new PIXI.Container()
        let button = new PIXI.Sprite(Core.assets.getTexture('bonus_skip_btn_000.png'));
        buttonBg.addChild(button)

        return buttonBg;
    }
}
