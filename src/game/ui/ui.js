var PIXI = require('pixi.js');
var EventEmitter = require('eventemitter3');
import { reelData } from '../model/reelStrips';
import '../../core';
import { SkipButton } from './SkipButton.js';

var charSelectConf = [
    'CS_charadeta05',
    'CS_charadeta04',
    'CS_charadeta03',
    'CS_charadeta02',
    'CS_charadeta01'
];

var selectFlag = ['char4', 'char3', 'char2', 'char1', 'char0'];

var payoutsConfig = [
    [2, 0.75, 0.375],
    [2.5, 1, 0.5],
    [5, 1.25, 0.75],
    [7.5, 2, 1],
    [10, 2.5, 1.25]
];

class CharacterSelectButton extends PIXI.Container {
    constructor(charId) {
        super();
        this.charId = charId;

        this.button = new PIXI.Container();
        super.addChild(this.button);

        this.charButtons = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/charSelect/CS_chardata/CS_chardata.json');

        this.charButtons.state.setAnimation(0, charSelectConf[charId] + '_gray', true);
        this.charButtons.state.onComplete = function(trackIndex, loopCount) {
            this.update(1 / 30);
        }.bind(this.charButtons);
        this.button.addChild(this.charButtons);

        this.button.animationName = {gray : charSelectConf[charId] + '_gray', push : charSelectConf[charId] + '_push'};
        this.button.width = this.button.width * 0.75 / Core.scaleFactor;
        this.button.height = this.button.height * 0.75 / Core.scaleFactor;

        this.payouts = this.buildCharPayouts(payoutsConfig[charId][0], payoutsConfig[charId][1], payoutsConfig[charId][2]);

        this.button.addChild(this.payouts);
        this.selected = false;
        this.interactive = true;
        this.on('tap', this.toggle.bind(this));
        this.on('click', this.toggle.bind(this));
        this.init_flag();
    }

    buildPayoutText(num) {
        return new PIXI.Text(num, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", lineHeight: 30 / Core.scaleFactor, fontWeight: 'bold', align: 'center', fill: 'white'});
    }

    buildCharPayouts(multiplier5x, multiplier4x, multiplier3x) {
        const betMultiplier = 40;
        var alpha = 0.7;

        var payout = new PIXI.Container();

        var payoutTexts = new PIXI.Container();
        payout.addChild(payoutTexts);

        var five = this.buildPayoutText('5');
        five.position.set(0, 0);
        payoutTexts.addChild(five);

        var four = this.buildPayoutText('4');
        four.position.set(0, 35 / Core.scaleFactor);
        payoutTexts.addChild(four);

        var three = this.buildPayoutText('3');
        three.position.set(0, 70 / Core.scaleFactor);
        payoutTexts.addChild(three);

        Object.defineProperty(payout, 'textAlpha', {
            get() { return alpha },
            set(value) { five.alpha = four.alpha = three.alpha = alpha = value }
        })

        const alignTexts = () => payoutTexts.position.set((556 - payoutTexts.width) / 4 / Core.scaleFactor, (320 - payoutTexts.height) / 4 / Core.scaleFactor);

        const getFormattedPayout = multiplier => gAPI.casinoUI.formatCurrency(betMultiplier * (gAPI.casinoUI.getCurrentCreditValue() || gAPI.casinoUI.getCreditValues()[0]) * multiplier);
        Object.assign(payout, {
            updateText() {
                five.text = '5  ' + getFormattedPayout(multiplier5x);
                five.updateText();
                four.text = '4  ' + getFormattedPayout(multiplier4x);
                four.updateText();
                three.text = '3  ' + getFormattedPayout(multiplier3x);
                three.updateText();
                alignTexts();
            }
        });

        payout.scale.set(1.2 * Core.scaleFactor, 1.2 * Core.scaleFactor);

        payout.textAlpha = alpha;
        payout.updateText();

        return payout;
    }

    toggle() {
        super.emit('select', { action: !this.selected, id: this.charId });
    }

    init_flag() {
        for (var i = 0; i < selectFlag.length; i++) selectFlag[i] = false;
    }

    toggleAction() {
        this.selected = !this.selected;
        if (this.selected) {
            this.charButtons.state.setAnimation(0, this.button.animationName.push, true);
            this.charButtons.state.onComplete = function(trackIndex, loopCount) {
                this.update(31 / 30);
            }.bind(this.charButtons);
            TweenMax.fromTo(this.payouts, 0.5, { textAlpha: 0.7 }, { textAlpha: 1 });
            selectFlag[this.charId] = true;
            Core.assets.sounds.playSound('GameUnitSelection', false, { volume: 0.3 });
        } else {
            this.charButtons.state.setAnimation(0, this.button.animationName.gray, true);
            this.charButtons.state.onComplete = function(trackIndex, loopCount) {
                this.update(1 / 30);
            }.bind(this.charButtons);
            TweenMax.fromTo(this.payouts, 0.5, { textAlpha: 1 }, { textAlpha: 0.7 });
            selectFlag[this.charId] = false;
            Core.assets.sounds.playSound('GameUnitSelection', false, { volume: 0.3 });
        }
    }
}

class SelectedCharacterIcon extends PIXI.Container {
    constructor(num) {
        super();
        this.icon = new PIXI.Container();
        super.addChild(this.icon);

        var icon = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/charSelect/CS_selectframe/CS_selectframe.json');
        icon.state.setAnimation(0, 'CS_selectframe0' + num + '_loop', true);
        icon.state.onComplete = function(trackIndex, loopCount) {
            this.update(0 / 30);
        }.bind(icon);
        this.icon.addChild(icon);
        icon.scale.set(0.5 / Core.scaleFactor, 0.5 / Core.scaleFactor);
        this.iconAnimation = icon;
    }

    selected(iconNo, num) {
        this.iconAnimation.state.setAnimation(0, 'CS_selectframe0' + iconNo + '_select0' + (5 - num), true);

        this.iconAnimation.state.onComplete = function(trackIndex, loopCount) {
            this.update(20 / 30);
        }.bind(this.iconAnimation);
    }
    unselected(iconNo) {
        this.iconAnimation.state.setAnimation(0, 'CS_selectframe0' + iconNo + '_loop', true);
        this.iconAnimation.state.onComplete = function(trackIndex, loopCount) {
            this.update(0 / 30);
        }.bind(this.iconAnimation);
    }
}

class BetFrame extends PIXI.Container {
    constructor() {
        super();
        this.betFrame = new PIXI.Container();
        super.addChild(this.betFrame);
        var frame = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/charSelect/CS_betframe/CS_betframe.json');
        frame.state.setAnimation(0, 'CS_betframe_loop', true);
        frame.state.onComplete = function(trackIndex, loopCount) {
            this.update(1 / 30);
        }.bind(frame);
        this.betFrame.addChild(frame);
    }
}

    export class UI extends EventEmitter {
        constructor() {
            super();
            this.switchButton = null;
            this.skipButton = null;
        }

        initUI(stage) {
            this.switchButton = new Core.Sprite({ name: 'switchButton', sprite: 'chara-select-normal.png' });

            this.switchButton.interactive = true;
            this.switchButton.on('tap', this.backToCharacter.bind(this));
            this.switchButton.on('click', this.backToCharacter.bind(this));
            this.switchButton.on('mouseover', function () {this.switchButton.sprite = 'chara-select-hover.png'; }.bind(this));
            this.switchButton.on('mouseout', function () {this.switchButton.sprite = 'chara-select-normal.png'; }.bind(this));
            this.switchButton.on('mousedown', function () {this.switchButton.sprite = 'chara-select-down.png'; }.bind(this));

            this.switchButton.anchor.set(0.5, 0.5);
            this.switchButton.scale.set(0.25 / Core.scaleFactor / 2, 0.25 / Core.scaleFactor / 2);
            this.switchButton.position.set(0.05 * stage.width, stage.height * 0.7 + this.switchButton.height / 2);
            this.switchButton.visible = true;
            stage.addChild(this.switchButton);

            this.skipButton = new SkipButton();
            this.skipButton.on('tap', this.skipBattle.bind(this));
            this.skipButton.on('click', this.skipBattle.bind(this));
            this.skipButton.position.set(stage.width / 2 - this.skipButton.width / 2, stage.height * 0.85 - this.skipButton.height / 2);
            this.skipButton.visible = false;
            stage.addChild(this.skipButton);
        }

        updateSelect(sel) {
            var bet = 0;
            var betContent = '';

            if (sel.action === false) {
                this.selectCharacters[sel.id].toggleAction();
                this.enablePlayButton(false);
            } else {
                var selected = 0;
                for (var i = 0; i < this.selectCharacters.length; i++)
                    if (this.selectCharacters[i].selected)
                        selected++;
                if (selected < 3)
                    this.selectCharacters[sel.id].toggleAction();
                if (selected === 2) {
                    this.enablePlayButton(true);
                    if ((this.selectString !== undefined) && (reelData.data[this.selectString].bet !== undefined)) {
                        bet = reelData.data[this.selectString].bet;
                        var value = gAPI.casinoUI.getCurrentCreditValue() || gAPI.casinoUI.getCreditValues()[0];
                        betContent = gAPI.casinoUI.formatCurrency(value * bet);
                    }
                }
            }

            for (var i = 1; i <= 3; ++i) {
                this.selectedIcons[i].unselected(i);
            }

            var iconCount = 1;
            for (var i = this.selectCharacters.length - 1; i >= 0; --i) {
                if (this.selectCharacters[i].selected) {
                    this.selectedIcons[iconCount].selected(iconCount, i);
                    ++iconCount;
                }
            }
            if ((selected >= 2) && (betContent === '')) {
                betContent = this.betTextContent.text;
            }
            this.betTextContent.text = betContent;
            this.betTextContent.updateText();

        }

        enablePlayButton(value) {
            //TODO enablePlayButton method should just ENABLE play button and do NOTHING else!!!!
            if (value) {
                this.canPlay = true;
                this.selectionText1.visible = false;
                this.playButton.visible = true;
                this.selectedChars = [];
                this.selectString = '';
                for (var i = selectFlag.length - 1, j = 0; i >= 0; i-- , j++) {
                    if (selectFlag[i] === true) {
                        this.selectString += String(j + 1);
                        this.selectedChars[i] = selectFlag[i];
                    }
                }
                this.playButton.state.setAnimation(0, 'CS_but_loop', true);
                this.playButton.state.onComplete = function(trackIndex, loopCount) {
                    this.update(1 / 30);
                }.bind(this.playButton);

                //TODO
                gAPI.casinoUI.setBetMultiplier(reelData.data[this.selectString].bet);
                gAPI.casinoUI.setCurrentCreditValue(gAPI.casinoUI.getCreditValues()[0]);
                gAPI.casinoUI.enableBetChange();
            } else {
                gAPI.casinoUI.setCurrentCreditValue(0);
                this.updateBetSize();
                this.canPlay = false;
                this.playButton.state.setAnimation(0, 'CS_but_gray', true);
                this.playButton.state.onComplete = function(truckIndex, loopCount) {
                    this.update(1 / 30);
                }.bind(this.playButton);

                this.selectionText1.visible = true;

                //TODO:
                gAPI.casinoUI.setBetMultiplier(0);
                gAPI.casinoUI.setCurrentCreditValue(0);
                gAPI.casinoUI.disableBetChange();
            }
        }
        updateBetSize() {
            this.selectCharacters.forEach(c => c.payouts.updateText())

            var bet = 0;
            var betContent = '';
            if ((this.selectString !== undefined) && (reelData.data[this.selectString].bet !== undefined)) {
                bet = reelData.data[this.selectString].bet;
                var value = gAPI.casinoUI.getCurrentCreditValue() || gAPI.casinoUI.getCreditValues()[0];
                betContent = gAPI.casinoUI.formatCurrency(value * bet);
            }
            this.betTextContent.text = betContent;
            this.betTextContent.updateText();
        }
        onPlay() {
            if (!this.canPlay) return;
            this.playButton.interactive = false;
            this.playButton.removeAllListeners();
            for (var i = 0; i < this.selectCharacters.length; i++) {
                this.selectCharacters[i].interactive = false;
                this.selectCharacters[i].removeAllListeners();
            }
            Core.assets.sounds.playSound('spin_button sound', false, { volume: 0.3 });
            this.playButton.state.setAnimation(0, 'CS_but_push', true);
            this.playButton.state.onComplete = function(trackIndex, loopCount) {
                this.update(31 / 30);
            }.bind(this.playButton);
            var ui = this;

            //TODO: It's an extra call of selectCharacters to make paytableHack work if characters were not changed on char selection page.
            //TODO:      Possible solution is not to delete character data, until any of character are unselected, and remove this double select call
            reelData.selectCharacters(this.selectString);
            TweenMax.delayedCall(1.0, function () {
                ui.emit('CharacterSelected', this.selectedChars);
            }.bind(this));
        }

        initCharacterSelection(stage, data) {
            //TODO:
            gAPI.casinoUI.setBetMultiplier(0);
            gAPI.casinoUI.setCurrentCreditValue(0);
            gAPI.casinoUI.disableBetChange();

            //Paytable hack
            if(!document.getElementById('opacityPaytable')){
                var element = document.createElement("style");
                element.id = "opacityPaytable";
                document.getElementsByTagName("body")[0].appendChild(element);
            }
            document.getElementById('opacityPaytable').innerHTML = "";
            //

            stage.interactive = true;

            var texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;

            gAPI.casinoUI.on('betChange', this.updateBetSize.bind(this));

            this.playButton = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/charSelect/CS_button/CS_button.json');
            this.playButton.scale.set(1.0 / 2 / Core.scaleFactor, 1.0 / 2 / Core.scaleFactor);
            this.playButton.state.setAnimation(0, 'CS_but_loop', true);
            this.playButton.state.onComplete = function(trackIndex, loopCount) {
                this.update(1 / 30);
            }.bind(this.playButton);

            this.playButton.interactive = true;
            this.playButton.visible = false;
            this.canPlay = false;
            stage.addChild(this.playButton);

            this.playButton.position.set(stage.width * 3 / 5, stage.height * 3 / 5 - 75 / Core.scaleFactor);
            this.playButton.on('tap', this.onPlay.bind(this));
            this.playButton.on('click', this.onPlay.bind(this));

            this.iconsContainer = new PIXI.Container();
            this.selectedIcons = [];
            for (var i = 1; i <= 3; ++i) {
                var icon = new SelectedCharacterIcon(i);
                this.iconsContainer.addChild(icon);
                this.selectedIcons[i] = icon;
            }
            this.iconsContainer.x = stage.width / 2 - this.iconsContainer.width / 4;
            this.iconsContainer.y = stage.height * 2 / 3 - this.iconsContainer.height / 4;

            stage.addChild(this.iconsContainer);

            this.buttonsContainer = new PIXI.Container();
            this.selectCharacters = [];

            // stage中央にcharButtonのパーツが置かれているためクリックした位置によっては他のボタンが
            // 反応してしまう。それを避けるためstage中央に置かれるcharNo 2のcharButtonを手前に
            // 描画させることでstage中央にあるパーツを覆い隠す
            for (var c = charSelectConf.length - 1; c >= 0; c--) {
                if (c == 2)
                    continue
                this.createCharButton(stage, c);
            }
            this.createCharButton(stage, 2);

            this.buttonsContainer.y = (stage.height - this.buttonsContainer.height) / 2 - 30 / Core.scaleFactor;
            stage.addChild(this.buttonsContainer);

            var secondColumnCharacter = this.selectCharacters[1];
            var bottomCharacter = this.selectCharacters[0];

            var bottomRightSlotSpace = {
                x: this.buttonsContainer.x + secondColumnCharacter.x + secondColumnCharacter.width / 2,
                y: this.buttonsContainer.y + bottomCharacter.y + bottomCharacter.height / 2
            };

            this.selectionText1 = new PIXI.Text(texts.characters, { fontFamily: 'Noto Serif Cond', fontSize: 50 / Core.scaleFactor + "px", lineHeight: 30 / Core.scaleFactor, fontWeight: 'bold', align: 'center', fill: 'white'});

            this.selectionText1.updateText();
            Core.utils.scaleDown(this.selectionText1, 440 / Core.scaleFactor);
            this.selectionText1.anchor.set(0.5,0.5);
            this.selectionText1.position.set(stage.width / 2, (stage.height + 125 / Core.scaleFactor) / 2);
            stage.addChild(this.selectionText1);

            TweenMax.to(this.selectionText1, 1, {alpha: 0.75, yoyo: true, repeat: -1});


            this.betFrameContainer = new PIXI.Container();
            var betFrame = new BetFrame();
            this.betFrameContainer.addChild(betFrame);

            betFrame.scale.set(0.75 / Core.scaleFactor, 0.75 / Core.scaleFactor);
            betFrame.position.set(0, 0);

            this.betTextTitle = new PIXI.Text('BET', { fontFamily: 'Noto Serif Cond', fontSize: 30 / Core.scaleFactor + "px", lineHeight: 30 / Core.scaleFactor, fontWeight: 'bold', align: 'left', fill: 'white'});
            this.betTextTitle.position.set(betFrame.x + (150 / Core.scaleFactor), betFrame.y + (175 / Core.scaleFactor));
            this.betFrameContainer.addChild(this.betTextTitle);

            this.betTextContent = new PIXI.Text('', { fontFamily: 'Noto Serif Cond', fontSize: 50 / Core.scaleFactor + "px", lineHeight: 30 / Core.scaleFactor, fontWeight: 'bold', align: 'right', fill: 'white'});
            // TODO:: 右寄せ対応
            this.betTextContent.position.set(betFrame.x + (200 / Core.scaleFactor), betFrame.y + (220 / Core.scaleFactor));
            this.betFrameContainer.addChild(this.betTextContent);
            this.betFrameContainer.position.set(stage.width * 3 / 5 - 200 / Core.scaleFactor, stage.height * 3 / 5 - 150 / Core.scaleFactor);

            stage.addChild(this.betFrameContainer);

            for (let i = 0; i < this.selectCharacters.length; i++){
                if (data && data[i]) this.selectCharacters[i].toggle();
            }
        }

        createCharButton(stage, c) {
            var charButton = new CharacterSelectButton(c);
            charButton.position.set(stage.width / 2, stage.height / 2 - charButton.height / 5 / Core.scaleFactor + ((Core.scale == 'high') ? 200 : 0));
            charButton.on('select', this.updateSelect.bind(this));
            this.buttonsContainer.addChild(charButton);
            this.selectCharacters[c] = charButton;
            charButton.payouts.position.set(
                stage.width / 4 + (1 - c) * 290 - 250 / Core.scaleFactor + ((Core.scale == 'high') ? -20 : (Core.scale == 'med') ? 0 : 20),
                (Core.scale == 'high') ? -200 : -240
            );
        }

        showCharacters(characters) {
            var initialDelay = 0.8;
            var delay = 0.1;
            for (let i = 0; i < characters.length; i++) {
                initialDelay += delay;
                let character = characters[i];
                TweenMax.delayedCall(initialDelay, character.appearAnimation.bind(character));
            }
            TweenMax.delayedCall(initialDelay + characters.length * delay, function () {
                for (let i = 0; i < characters.length; i++) characters[i].character.interactive = true;
            });

        }

        //TODO: looks like not used
        // startSpin(eventData) {
        //     this.switchButton.sprite = 'play-btn.png';
        //     super.emit('StartSpin');
        // }

        backToCharacter(eventData) {
            this.switchButton.interactive = false;
            super.emit('SwitchButton');
        }

        skipBattle(eventData) {
            Core.assets.sounds.playSound('spin_button sound', false, { volume: 0.3 });
            super.emit('SkipButton');
        }
    }
