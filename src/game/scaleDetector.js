import '../core';

var MobileDetect = require('mobile-detect');

export function detectScale() {
    var md = new MobileDetect(window.navigator.userAgent);
    var width = Math.max(window.screen.width, window.screen.height);

    var isMobile;
    if (md.mobile() === null) {
        isMobile = false;
    }
    else {
        isMobile = true;
    }

//this is for Pc only
    if (!isMobile && width * window.devicePixelRatio > 1280) {
        Core.scale = 'high';
        Core.scaleFactor = 0.5;
    } else if (width * window.devicePixelRatio <= 1282 && isMobile) {
        Core.scale = 'low';
        Core.scaleFactor = 2.0;
    } else {
        Core.scale = 'med';
        Core.scaleFactor = 1.0;
    }

//this is for Ipad 1 and 2
    if (md.is("iPad") && window.devicePixelRatio === 1) {
        Core.scale = 'low';
        Core.scaleFactor = 2.0;
    }

    Core.lang = gAPI.casinoUI.getLocalePrefix();
}