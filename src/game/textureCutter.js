var PIXI = require('pixi.js');

const TextureCutter = (texture, symbolId) => {
    let cf = {};

    const WIDTH = 200 / Core.scaleFactor;

    const SINGLE_H = 336 / 2 / Core.scaleFactor;
    const DOUBLE_H = 336 / 2 * 2 / Core.scaleFactor;
    const TRIPLE_H = 336 / 2 * 3 / Core.scaleFactor;

    /*
    if (symbolId === 1) {//redGirl = 8
        cf = {
            singleOffset: new PIXI.Point(0, 34.5 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0, 15.5 / Core.scaleFactor),
        }
    } else if (symbolId === 2) {//yellowGirl = 10
        cf = {
            singleOffset: new PIXI.Point(0, 39 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0, 10 / Core.scaleFactor),
        }
    } else if (symbolId === 3) {//blackGirl = 12
        cf = {
            singleOffset: new PIXI.Point(0, 68.5 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0.5 / Core.scaleFactor, 41.5 / Core.scaleFactor),
        }
    } else if (symbolId === 4) {//greenGirl= 9
        cf = {
            singleOffset: new PIXI.Point(0, 41 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0.5 / Core.scaleFactor, 16.5 / Core.scaleFactor),
        }
    } else if (symbolId === 5) {//whiteGirl= 11
        cf = {
            singleOffset: new PIXI.Point(0, 29.5 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0.5 / Core.scaleFactor, 29 / Core.scaleFactor)
        }
    }
    */
    if (symbolId === 1) {//redGirl = 8
        cf = {
            singleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
        }
    } else if (symbolId === 2) {//yellowGirl = 10
        cf = {
            singleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
        }
    } else if (symbolId === 3) {//whiteGirl = 12
        cf = {
            singleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0 / Core.scaleFactor, 0 / Core.scaleFactor),
        }
    } else if (symbolId === 4) {//greenGirl= 9
        cf = {
            singleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0 / Core.scaleFactor, 0 / Core.scaleFactor),
        }
    } else if (symbolId === 5) {//blackGirl= 11
        cf = {
            singleOffset: new PIXI.Point(0, 0 / Core.scaleFactor),
            doubleOffset: new PIXI.Point(0 / Core.scaleFactor, 0 / Core.scaleFactor)
        }
    }

    cf.tripleOffset = new PIXI.Point(0, 0);

    cf.singleSize = new PIXI.Point(WIDTH, SINGLE_H);
    cf.doubleSize = new PIXI.Point(WIDTH, DOUBLE_H);
    cf.tripleSize = new PIXI.Point(WIDTH, TRIPLE_H);

    let rotate = texture.rotate === 2;

    let singleSize = new PIXI.Point(rotate ? cf.singleSize.y : cf.singleSize.x, rotate ? cf.singleSize.x : cf.singleSize.y);
    let single = new PIXI.Texture(texture.baseTexture, new PIXI.Rectangle(texture.frame.x + (rotate ? cf.singleOffset.y : cf.singleOffset.x),
        texture.frame.y + (rotate ? cf.singleOffset.x : cf.singleOffset.y),
        singleSize.x, singleSize.y), new PIXI.Rectangle(0, 0, WIDTH, SINGLE_H), null, texture.rotate);

    let doubleSize = new PIXI.Point(rotate ? cf.doubleSize.y : cf.doubleSize.x, rotate ? cf.doubleSize.x : cf.doubleSize.y);
    let double = new PIXI.Texture(texture.baseTexture, new PIXI.Rectangle(texture.frame.x + (rotate ? cf.doubleOffset.y : cf.doubleOffset.x),
        texture.frame.y + (rotate ? cf.doubleOffset.x : cf.doubleOffset.y),
        doubleSize.x, doubleSize.y), new PIXI.Rectangle(0, 0, WIDTH, DOUBLE_H), null, texture.rotate);

    let tripleSize = new PIXI.Point(rotate ? cf.tripleSize.y : cf.tripleSize.x, rotate ? cf.tripleSize.x : cf.tripleSize.y);
    let triple = new PIXI.Texture(texture.baseTexture, new PIXI.Rectangle(texture.frame.x + (rotate ? cf.tripleOffset.y : cf.tripleOffset.x),
        texture.frame.y + (rotate ? cf.tripleOffset.x : cf.tripleOffset.y),
        tripleSize.x, tripleSize.y), new PIXI.Rectangle(0, 0, WIDTH, TRIPLE_H), null, texture.rotate);

    single.fullTexture = texture;
    double.fullTexture = texture;
    triple.fullTexture = texture;
    return [single, double, triple]
};

export { TextureCutter }
