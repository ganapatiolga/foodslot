var EventEmitter = require('eventemitter3');
import { AnimationController } from './animationController';
import { EventChain, GameEventType } from './eventController';
import '../../core';
import { changeMS } from '../slot/symbol'
import { MENU_STATE } from "./UIController"

export var GAME_STATE = {
    idle: 0,
    spin: 1,
    bonus: 2
};


export class SlotController extends EventEmitter {
    // constructor(game, uiController, connection) {
    constructor(game, uiController) {
        super();
        // this.connection = connection;
        this.freeSpins = 0;
        this.firstFreeSpin = true;
        this.game = game;
        this.uiController = uiController;
        // this.uiController.on('StartSpinClicked', this.startSpin.bind(this));
        this.animationController = new AnimationController();
        // this.autoPlayLimitChecker = new AutoPlayLimitChecker();
        this.state = GAME_STATE.idle;
        this.bonus = false;
        this.luckySymbol = null;

        this.cutInTable = Core.config.getSettings('config/cutin.json');
        this.wins = Core.config.getSettings('config/wins.json').wins;

        this.cutInMovie = {};
    }

    startSpin(evt) {
        //TODO remove if no bugs found, since casino UI now cares about spin counting.
        // if (evt && evt.fromAutoPlay && this.state !== GAME_STATE.spin) {
        //     gAPI.casinoUI.setAutoPlayCount(gAPI.casinoUI.getAutoPlayCount() - 1);
        // }

        // if (this.state === GAME_STATE.idle) {
        //     if (gAPI.casinoUI.getAutoPlay() && this.autoPlayLimitChecker.AutoPlay === false) {
        //         this.autoPlayLimitChecker.AutoPlay = true;
        //         this.autoPlayLimitChecker.getLimits();
        //         this.autoPlayLimitChecker.reInit();
        //     }
            console.log(`DEBUG: start spin happened`)
            this.state = GAME_STATE.spin;
            this.spinning = true
            Core.assets.sounds.playSound('SFX_SM_Spin_Start', false, { volume: 0.3 });

            this.cancelAnimations();
            this.game.slotMachine.score.startSpinSignal();
            this.animationController.startSpin(this.game.slotMachine);
            // this.connection.once('dataReceived', this.onResultReceived.bind(this));
            // this.connection.startRound(!!(this.freeSpins > 0));
            // gAPI.casinoUI.disableBetChange();
            gAPI.casinoUI.updateBottomBar();

            if (this.freeSpins > 0) {
                this.bonus = true;
                this.freeSpins--;
                gAPI.casinoUI.setFreeSpinsCount(this.freeSpins);
            }
        // }
    }
    onResultReceived(res) {
        let controller = this;
        if ((res.spinResult !== undefined) && (res.spinResult.replace !== undefined) && (res.spinResult.replace[13] !== undefined) && (res.spinResult.replace[14] !== undefined)) {
            changeMS(res.spinResult.replace[13], res.spinResult.replace[14]);
        }

        if(res.action === 'battle'){
            this.eventChain = new EventChain(res, this.bonus, false);

            // if (this.bonus && this.freeSpins <= 0 && !res.spinResult.scatterResult) {
            //     this.bonus = false;
            // }

            // gAPI.casinoUI.updateBottomBar();
            this.nextEvent();

            // this.animationController.emit('AnimationsFinished');
        } else {
            this.animationController.once('ReelsStopped', function () {
                controller.onReelsStopped(res);
            });

            let animation = this.getCutInMovie(res);

            if (this.cutInMovie !== undefined) {
                delete this.cutInMovie;
            }

            if (animation !== null) {
                this.cutInMovie = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/cutin/cutin.json');

                let stage = this.game.engine.stage.getElement('frame');
                this.cutInMovie.position.set(stage.width / 2, stage.height / 2);
                this.cutInMovie.width = (this.cutInMovie.width / Core.scaleFactor) * 0.67;
                this.cutInMovie.height = (this.cutInMovie.height / Core.scaleFactor) * 0.67;

                TweenMax.delayedCall(0.5, function() {
                    this.cutInMovie.visible = true;
                    stage.addChild(this.cutInMovie);

                    if (animation.indexOf('_cha_') === -1) {
                        this.cutInMovie.state.setAnimation(0, animation, false);
                        TweenMax.delayedCall(2.0, function() {
                            let fadeOut = TweenMax.to(this, 0.2, {alpha: 0.0, onComplete: function() {
                                this.visible = false;
                                this.state.onComplete = '';
                            }.bind(this)});
                        }.bind(this.cutInMovie));

                        TweenMax.delayedCall(2.5, function() {
                            this.animationController.stopSpin(this.game.slotMachine, res, this.cutInMovie);
                        }.bind(this));
                    } else {
                        this.cutInMovie.state.setAnimation(0, animation.replace(/_cha_/, '_def_'), true);
                        this.cutInMovie.state.addAnimation(0, animation, true, 1.0);
                        TweenMax.delayedCall(1.0, function() {
                            Core.assets.sounds.playMusic('SFX_Symbol_DominantAppear', false, { volume: 0.5 });
                        });
                        TweenMax.delayedCall(3.0, function() {
                            let fadeOut = TweenMax.to(this, 0.2, {alpha: 0.0, onComplete: function() {
                                this.visible = false;
                                this.state.onComplete = '';
                            }.bind(this)});
                        }.bind(this.cutInMovie));
                        TweenMax.delayedCall(3.5, function() {
                            this.animationController.stopSpin(this.game.slotMachine, res, this.cutInMovie);
                        }.bind(this));
                    }
                    this.cutInMovie.state.onComplete = function(trackIndex, loopCount) {
                        this.update(156 / 30);
                    }.bind(this.cutInMovie);
                    Core.assets.sounds.playMusic('SFX_Symbol_DominantAppear', false, { volume: 0.5 });
                }.bind(this));
            } else {
                this.animationController.stopSpin(this.game.slotMachine, res, this.cutInMovie);
            }
        }
    }

    getCutInMovie(res) {
        let winLevel = null;
        let factor = Math.floor(res.totalWin / res.totalBet);

        if (res.spinResult.wins.length == 0)
            return null;

        for (var key in this.wins) {
            if ((this.wins[key]['low'] <= factor) && (this.wins[key]['high'] >= factor)) {
                winLevel =  this.wins[key]['win'];
                break;
            }
        }

        var idToGirlName = [null, null, null, null, null, null, null, null,
            'yellowGirl', 'blackGirl', 'greenGirl', 'whiteGirl', 'redGirl', null, null];

        let id = 0;
        if ((this.bonus) && (this.luckySymbol !== null)) {
            id = this.luckySymbol;
        } else {
            var results = Array(15);
            results.fill(0);

            if ((res.spinResult === undefined) || (res.spinResult.reelDisplay === undefined))
                return null;

            for (var key in res.spinResult.reelDisplay) {
                for (var key2 in res.spinResult.reelDisplay[key]) {
                    ++results[res.spinResult.reelDisplay[key][key2]];
                }
            }

            let max = -1;
            for (var i = 0; i < results.length; ++i) {
                if (max < results[i]) {
                    max = results[i];
                    id = i;
                }
            }
        }
        let targetGirl = idToGirlName[id];
        let gameType = (res.freeGamesLeft === undefined) ? 'normal' : 'freespin';

        if (targetGirl === null)
            return null;

        /*
        let odd = Math.floor(Math.random() * 128) + 128;

        if (odd >= 257)
            odd = 256;

        if (odd >= 205)
            console.log('%cODD : ' + odd, 'color: red');
        else
            console.log('ODD : ' + odd);

        let table = this.cutInTable[gameType][targetGirl][winLevel];
        let tmp = 0;
        for (var i in table) {
            tmp += parseInt(table[i].odds);
            if (tmp >= odd) {
                console.log(tmp + ' : ' + table[i].cutIn);
                return table[i].cutIn;
            }
        }
        /*/
        let odd = Math.floor(Math.random() * 257);
        let table = this.cutInTable[gameType][targetGirl][winLevel];
        let tmp = 0;
        for (var i in table) {
            tmp += parseInt(table[i].odds);
            if (tmp >= odd) {
                return table[i].cutIn;
            }
        }
        //*/
        console.log ('ERROR: cutInMovie not found');
        return null;
    }

    playCutscene(cutscene, result, fromLoading) {
        let controller = this;
        let swipeOnTime = 1.0;
        let nextEventDelay = 1.0;

        controller.game.overlay.freeSwipeOn(swipeOnTime, true);

        TweenMax.delayedCall(0.4, function () {
            gAPI.casinoUI.hideUI();
            gAPI.casinoUI.enableBottomBar();
            gAPI.casinoUI.setQuickSpinEnabled(true);
        });

        // 描画の都合上指定の時間より5フレーム早く処理開始
        TweenMax.delayedCall((50 / 30) * swipeOnTime, function () {
            // Core.assets.sounds.playMusic('assets/sounds/music/battleMusicLoop.mp3', true, { fadeIn: { time: 2.0 }, volume: 0.1 });
//            Core.assets.sounds.playMusic('Music_Roulette_Loop', true, { fadeIn: { time: 2.0 }, volume: 0.1 });
            Core.assets.sounds.playMusic('Music_FreeSpinIntro_Loop', true, { fadeIn: { time: 2.0 }, volume: 0.5 });
            controller.game.musicOnPlay.battleMusicLoop = true;
            this.game.initFightingCutscene();
            TweenMax.delayedCall(0.15, function () {
                this.game.overlay.freeSwipeOff(1.0, true);
                if(fromLoading) {
                    nextEventDelay = 0.1
                }
                TweenMax.delayedCall(nextEventDelay, function () {
                    this.nextEvent();
                }.bind(this));
            }.bind(this));
        }.bind(this));
    }

    continueCutscene(result) {
        let stage = this.game.engine.stage.getElement('frame');
        this.game.ui.skipButton.visible = true;
        stage.addChild(this.game.ui.skipButton);
        this.uiController.initSkipButtonListener();
        this.game.scene.playRoulette(result);
        TweenMax.delayedCall(1.0, function () {
            this.game.scene.once('AnimationCompleted', function () {
                this.game.ui.removeListener('SkipButton');
                if (this.game.musicOnPlay.battleMusicLoop) {
                    // Core.assets.sounds.stopSound('assets/sounds/music/battleMusicLoop.mp3', { fadeOut: { time: 2 } });
                    this.game.slotMachine.score.stopBigWinMusic();
                    this.game.musicOnPlay.battleMusicLoop = false;
                }
                this.animationController.emit('AnimationsFinished');
            }.bind(this));
        }.bind(this));
    }

    cancelAnimations() {

        this.game.slotMachine.slotPanel.setTint(0xFFFFFF);
        this.animationController.cancelAnimations();

    }

    bonusTrigger(bonusEvent) {
        this.state = GAME_STATE.bonus;
        // Core.assets.sounds.stopSound('assets/sounds/music/main.mp3', { fadeOut: { time: 0.5 } });
        Core.assets.sounds.stopSound('Music_BaseGame_Loop', { fadeOut: { time: 0.5 } });
        this.game.musicOnPlay.mainMusicLoop = false;
        // Core.assets.sounds.stopSound('assets/sounds/music/bonusMusicLoop.mp3', { fadeOut: { time: 0.5 } });
        Core.assets.sounds.stopSound('Music_Bonus_Loop', { fadeOut: { time: 0.5 } });
        Core.assets.sounds.stopSound('Music_FreeSpinIntro_Loop', { fadeOut: { time: 0.5 } });

        this.game.slotMachine.slotPanel.setTint(0x777777);

        for (var s = 0; s < bonusEvent.positions.length; s++)
            if (bonusEvent.positions[s] !== -1) {
                this.game.slotMachine.slotPanel.animateSymbol(s, bonusEvent.positions[s], true);
            }
        this.bonusTriggerTween = TweenMax.delayedCall(4, function () {
            this.animationController.emit('AnimationsFinished');
        }.bind(this));

        gAPI.casinoUI.once('screenClick', function () {
            Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 });
            this.animationController.removeAllListeners('AnimationsFinished');
            this.bonusTriggerTween.kill();
            this.nextEvent();
        }.bind(this));
    }

    startFreeSpins(bonus) {
        // let controller = this;
        this.game.slotMachine.score.freeSpinsTotalWin = bonus.totalAmountWon;
        this.freeSpins = bonus.freeSpins
        this.cancelAnimations();
        this.game.overlay.freeSwipeOn(1.0, false);

        TweenMax.delayedCall(1.0, function () {
            this.game.initFreeSpinsScene();
            this.game.overlay.freeSwipeOff(1.0, false);
        }.bind(this));
        TweenMax.delayedCall(1.6, function () {
            gAPI.casinoUI.showUI();
            gAPI.casinoUI.startFreeSpinsMode(this.freeSpins, true);
            this.animationController.emit('AnimationsFinished');
        }.bind(this));
    }

    onReelsStopped(res) {
        if (res.spinResult && res.spinResult.scatterResult) {
            gAPI.casinoUI.removeAllListeners('screenClick');
            //TODO response data has info if it's bonus or not....  do we really need internal possibly bugged flag???
            if (!this.bonus) {
                gAPI.casinoUI.setAutoPlay(false);
                gAPI.casinoUI.disableAutoPlay();
            }
            gAPI.casinoUI.disableSpinButton();
            // if (this.autoPlayLimitChecker.AutoPlay) { this.autoPlayLimitChecker.AutoPlay = false; }
            this.animationController.loop = false;
        } else {
            // if (gAPI.casinoUI.getAutoPlay()) {
            //     this.autoPlayLimitChecker.getLimits();
            //     gAPI.casinoUI.disableSpinButton();
                // if (this.autoPlayLimitChecker.checkLimits(res)) {
                //     this.animationController.loop = false;
                // } else {
                //     gAPI.casinoUI.setAutoPlay(false);
                //     this.autoPlayLimitChecker.AutoPlay = false;
                //     gAPI.casinoUI.enableBetChange();
                //     super.emit('ResultShown');
                //     this.animationController.loop = true;
                //
                // }
            // } else if (this.bonus) {
            if (this.bonus) {
                this.animationController.loop = false;
            } else if (!(res.action === 'battle')){
                gAPI.casinoUI.disableSpinButton();
                // gAPI.casinoUI.enableBetChange();
                super.emit('ResultShown');
                this.animationController.loop = true;
            }
        }
        if(res.nextAction[0] === 'battle'){
            this.bonus = true
        }

        this.eventChain = new EventChain(res, this.bonus, true);
        if (this.bonus && this.freeSpins <= 0 && !res.spinResult.scatterResult) {
            this.luckySymbol = null;
            this.bonus = false;
        }

        gAPI.casinoUI.updateBottomBar();

        this.nextEvent();
    }

    doBattle(result, fromLoading) {
        //TODO: Looks like this new method won't be used. To delete if so
        this.latestBattleResult = result.gameData.character;
                //const charmap = { 1: 8, 2: 9, 3: 10, 4: 11, 5: 12 }
        changeMS(result.character, result.gameData.character);
        this.playCutscene('fight', result.gameData.character);
        gAPI.casinoUI.setAutoPlay(false);
    }

    nextEvent() {
        gAPI.casinoUI.removeAllListeners('screenClick');
        var currentEvent = this.eventChain.getEvent();
        this.currentEvent = currentEvent;
        console.log('%c Next event:' + this.currentEvent.type, 'color:green');
        switch (currentEvent.type) {
            case GameEventType.allpaylines:
                //TODO: remove after QA
                // if(!gAPI.casinoUI.getAutoPlay()){
                //     gAPI.casinoUI.disableSpinButton();
                // }
                console.log('%c This.BONUS = ' + this.bonus + '%c This.freespins = ' + this.freeSpins, 'color:blue');
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                this.animationController.showAllPaylines(this.game.slotMachine, null, currentEvent);
                break;
            case GameEventType.paylines:
                //TODO: remove after QA
                // if(!gAPI.casinoUI.getAutoPlay()){
                //     gAPI.casinoUI.disableSpinButton();
                // }

                this.game.slotMachine.score.once('BigWinEnd', function() {
                    gAPI.casinoUI.removeAllListeners('screenClick');
                }.bind(this));

                this.animationController.once('AnimationsFinished', function () {
                    if (this.game.slotMachine.LastScatterCount !== 5) {
                        gAPI.casinoUI.removeAllListeners('screenClick');
                        this.animationController.cancelAnimations();
                        this.game.slotMachine.bigWin.cancelCoins();
                        this.nextEvent();
                    } else {
                        this.nextEvent();
                    }
                }.bind(this));

                gAPI.casinoUI.once('screenClick', function () {
                    Core.assets.sounds.playSound('SFX_SM_Spin_End', false, {volume: 0.16});
                    this.animationController.removeAllListeners('AnimationsFinished');
                    this.animationController.cancelAnimations();
                    this.game.slotMachine.score.wrapperSignal();
                    this.game.slotMachine.score.cancelAnimations(this.nextEvent.bind(this));
                }.bind(this));

                this.animationController.showPaylines(this.game.slotMachine, null, currentEvent);
                this.animationController.animateWinnerChars(this.game.slotMachine, currentEvent); //TODO HERE
                break;
            case GameEventType.expand:
                /*if (!this.freeSpins > 0){
                 gAPI.casinoUI.hideUI();
                 }*/
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                this.animationController.showExpands(this.game.slotMachine, null, currentEvent);
                break;
            case GameEventType.freeSpins:
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                this.startFreeSpins(currentEvent);
                break;
            case GameEventType.bonusTriggerOne:
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                gAPI.casinoUI.disableBetChange();
                gAPI.casinoUI.disableAutoPlay();

                this.game.slotMachine.score.once('BigWinEnd', function() {
                    // coin shower cancel listener removed
                    gAPI.casinoUI.removeAllListeners('screenClick');
                    this.bonusTrigger(currentEvent.bonus);
                }.bind(this));

                // cancels the coin shower
                gAPI.casinoUI.once('screenClick', function () {
                    this.game.slotMachine.score.cancelAnimations(function() {}, false);
                    this.game.slotMachine.score.wrapperSignal(false);
                    TweenMax.delayedCall(2, () => {
                        this.game.slotMachine.score.emit("BigWinEnd");
                });
                }.bind(this));

                break;
            case GameEventType.bonusTrigger:
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                gAPI.casinoUI.disableBetChange();
                gAPI.casinoUI.disableAutoPlay();
                this.bonusTrigger(currentEvent.bonus);

                break;
            case GameEventType.battleStart:
                var proceed = () => {
                    gAPI.casinoUI.hideWrapper();
                    //TODO fix it!!!

                    this.latestBattleResult = currentEvent.character;
                    //const charmap = { 1: 8, 2: 9, 3: 10, 4: 11, 5: 12 } //TODO: figure out if it still needed.
                    changeMS(currentEvent.character, currentEvent.character);
                    this.playCutscene('fight', currentEvent.character, currentEvent.fromLoading);
                    gAPI.casinoUI.setAutoPlay(false);
                };

                gAPI.casinoUI.removeAllListeners('screenClick');
                if (this.uiController.menuState === MENU_STATE.opened) {
                    gAPI.casinoUI.on("menuClose", () => {
                        proceed()
                    });
                } else {
                    proceed();
                }

                break;
            case GameEventType.battleFinish:
                this.animationController.once('AnimationsFinished', this.nextEvent.bind(this));
                this.continueCutscene(this.latestBattleResult);
                break;
            case GameEventType.popUp:
                if (currentEvent.popup === 'freeSpins') {
                    this.freeSpins = currentEvent.freeGamesLeft;
                    gAPI.casinoUI.removeAllListeners('screenClick');
                    gAPI.casinoUI.hideUI();
                    this.cancelAnimations();
                    this.game.slotMachine.score.showFreeSpinsPopUp(currentEvent.addedFreeGames);

                    let closePopUp = TweenMax.delayedCall(5.0, function () {
                        gAPI.casinoUI.hideWrapper();
                        this.animationController.removeAllListeners('AnimationsFinished');
                        gAPI.casinoUI.removeAllListeners('screenClick');
                        if (this.bonus)
                            this.game.slotMachine.slotPanel.symbolAnimationManager.cancelAllAnimations();

                        if (this.freeSpins === 0) gAPI.casinoUI.disableBottomBar();
                        this.game.slotMachine.score.removeFreeSpinsPopUp();

                        if (!this.firstFreeSpin) {
                            gAPI.casinoUI.showUI();
                            gAPI.casinoUI.showCasinoUI();
                            // Core.assets.sounds.playMusic('assets/sounds/music/bonusMusicLoop.mp3', true, { fadeIn: { time: 2.0 }, volume: 0.07});
                            Core.assets.sounds.playMusic('Music_Bonus_Loop', true, { fadeIn: { time: 2.0 }, volume: 0.8});
                        }
                        TweenMax.delayedCall(0.25, function () {
                            this.nextEvent() }.bind(this));
                    }.bind(this));

                    TweenMax.delayedCall(1.5, function () {
                        gAPI.casinoUI.once('screenClick', function () {
                            Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 });
                            this.animationController.removeAllListeners('AnimationsFinished');
                            closePopUp.kill();
                            if(this.bonus)
                                this.game.slotMachine.slotPanel.symbolAnimationManager.cancelAllAnimations();
                            this.game.slotMachine.score.removeFreeSpinsPopUp();
                            if (!this.firstFreeSpin) {
                                gAPI.casinoUI.showUI();
                                // Core.assets.sounds.playMusic('assets/sounds/music/bonusMusicLoop.mp3', true, { fadeIn: { time: 2.0 }, volume: 0.07 });
                                Core.assets.sounds.playMusic('Music_Bonus_Loop', true, { fadeIn: { time: 2.0 }, volume: 0.8 });
                            }
                            TweenMax.delayedCall(0.25, function () {
                                this.nextEvent()
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                } else if (currentEvent.popup === 'battlePopUp') {
                    gAPI.casinoUI.removeAllListeners('screenClick');
                    gAPI.casinoUI.hideUI();
                    this.game.scene.showFreeSpinsBattlePopUp();
                    let closePopUp = TweenMax.delayedCall(5.0, function () {
                        gAPI.casinoUI.removeAllListeners('screenClick');
                        this.game.scene.removeFreeSpinsBattlePopUp();
                        this.nextEvent();
                    }.bind(this));

                    TweenMax.delayedCall(1.5, function () {
                        gAPI.casinoUI.once('screenClick', function () {
                            Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 });
                            closePopUp.kill();
                            this.game.scene.removeFreeSpinsBattlePopUp();
                            this.nextEvent();
                        }.bind(this));
                    }.bind(this));
                } else if (currentEvent.popup === 'dominantPopUp') {
                    gAPI.casinoUI.removeAllListeners('screenClick');
                    gAPI.casinoUI.showCasinoUI();
                    gAPI.casinoUI.updateBottomBar();
                    gAPI.casinoUI.hideUI();
                    gAPI.casinoUI.disableSpinButton();

                    TweenMax.delayedCall(1.0, function () {
                        this.game.slotMachine.score.showDominantPopUp(this.latestBattleResult);
                        this.luckySymbol = this.latestBattleResult;
                        this.latestBattleResult = null;
                        //TODO: remove duplicated code
                        let closePopUp = TweenMax.delayedCall(5.0, function () {
                            gAPI.casinoUI.removeAllListeners('screenClick');
                            this.game.slotMachine.score.removeDominantPopUp();
                            gAPI.casinoUI.showUI();

                            TweenMax.delayedCall(0.5, function() {
                                gAPI.casinoUI.emit('spinStart');
                            });

                            this.nextEvent();
                        }.bind(this));
                        TweenMax.delayedCall(1.5, function() {
                            gAPI.casinoUI.once('screenClick', function () {
                                Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 });
                                closePopUp.kill();
                                this.game.slotMachine.score.removeDominantPopUp();
                                gAPI.casinoUI.showUI();

                                TweenMax.delayedCall(0.5, function() {
                                    gAPI.casinoUI.emit('spinStart');
                                });
                                this.nextEvent();
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                } else if (currentEvent.popup === 'endFreeSpins') {
                    Core.assets.sounds.stopSound('mega_epic_win', {fadeOut: {time: 0.5}});
                    gAPI.casinoUI.hideUI();
                    gAPI.casinoUI.disableSpinButton();
                    this.game.slotMachine.bigWin.cancel();

                    TweenMax.delayedCall(68 / 30, function () {
                        this.game.slotMachine.score.stopBigWinMusic();
                        if (this.freeSpins) {
                            Core.assets.sounds.playMusic('Music_Bonus_Loop', true, {fadeIn : {time : 0.5}, volume : 0.8});
                        }
                    }.bind(this));
                    this.game.slotMachine.score.cancelAnimations();
                    this.game.slotMachine.score.cancelPaylineAnimation();
                    this.game.slotMachine.score.showFreeSpinsEndPopUp();
                    this.animationController.removeAllListeners('AnimationsFinished');
                    this.firstFreeSpin = true;

                    let closePopUp = TweenMax.delayedCall(6.0, function () {
                        gAPI.casinoUI.removeAllListeners('screenClick');
                        /*TweenMax.delayedCall(1.5, function () {
                         //gAPI.casinoUI.showUI();
                         })*/
                        this.game.slotMachine.score.removeFreeSpinsEndPopUp();
                        TweenMax.delayedCall(0.25, function () {
                            this.game.overlay.swipeOn(1.4);
                            TweenMax.delayedCall(1.4, function () {
                                this.game.initMainGameScene();
                                this.game.overlay.swipeOff(1.4);
                                TweenMax.delayedCall(2.0, function () {
                                    Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, {fadeIn : {time : 2.0}, volume : 0.5});
                                });
                                TweenMax.delayedCall(1.0, function(){
                                    gAPI.casinoUI.endFreeSpinsMode();
                                    gAPI.casinoUI.enableBetChange();
                                    gAPI.casinoUI.enableAutoPlay();
                                    gAPI.casinoUI.enableSpinButton();
                                    gAPI.casinoUI.showUI();
                                    this.nextEvent();
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));


                    TweenMax.delayedCall(1.5, function() {
                        gAPI.casinoUI.once('screenClick', function () {
                            Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 });
                            closePopUp.kill();
                            /*TweenMax.delayedCall(1.5, function () {
                             //gAPI.casinoUI.showUI();
                             })*/
                            this.game.slotMachine.score.removeFreeSpinsEndPopUp();
                            TweenMax.delayedCall(0.25, function () {
                                this.game.overlay.swipeOn(1.4);
                                TweenMax.delayedCall(1.4, function () {
                                    this.game.initMainGameScene();
                                    this.game.overlay.swipeOff(1.4);
                                    TweenMax.delayedCall(2.0, function () {
                                        Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, {fadeIn : {time : 2.0}, volume : 0.5});
                                    });
                                    TweenMax.delayedCall(1.4, function(){
                                        gAPI.casinoUI.endFreeSpinsMode();
                                        gAPI.casinoUI.enableBetChange();
                                        gAPI.casinoUI.enableAutoPlay();
                                        gAPI.casinoUI.enableSpinButton();
                                        gAPI.casinoUI.showUI();
                                        this.nextEvent();
                                    }.bind(this));
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }
                break;
            case GameEventType.winresume:
                this.game.slotMachine.score.setTotalWin(currentEvent.winbet);
                this.nextEvent();
                break;
            case GameEventType.end:
                // this.connection.finallyShowWinOnBottomBar(); //TODO ??? looks like not needed anymore.
                this.game.slotMachine.score.cancelPaylineAnimation();
                if (currentEvent.repeat) { this.animationController.showPaylines(this.game.slotMachine, null, currentEvent.repeat, true, false); }
                gAPI.casinoUI.removeAllListeners('screenClick');
                this.animationController.removeAllListeners('AnimationsFinished');
                this.onAnimationFinished();
                break;
        }

    }

    onAnimationFinished(delay = 0.5) {
        if(!this.bonus && !gAPI.casinoUI.getAutoPlay()) {
            //this.uiController.enableSwitchButton();
        }
        // if (this.bonus && this.freeSpins <= 0) {
        //     this.bonus = false;
        //     this.endFreeSpins(res);
        // } else {
        //     this.game.controller.emit('AnimationsFinished')
        //     this.state = GAME_STATE.idle;
        // }
        this.game.controller.emit('AnimationsFinished')


        //if free spins in progres
        // if (this.freeSpins > 0 && !this.firstFreeSpin) {
        //
        //     this.cancelAnimations();
        //     this.state = GAME_STATE.idle;
        //     this.startSpin();
        // } else if (gAPI.casinoUI.getAutoPlay() === true && gAPI.casinoUI.getAutoPlayCount() > 0 && this.freeSpins <= 0) {
        //
        //     this.cancelAnimations();
        //     this.state = GAME_STATE.idle;
        //     gAPI.casinoUI.enableStopButton();
        //
        //     this.emit('AutoPlayFinished');
        //
        //     this.startSpin();
        // } else {
        //     this.game.slotMachine.score.cancelAnimations();
        //     gAPI.casinoUI.setAutoPlay(false);
        //
        //     if (this.freeSpins > 0) {
        //         this.firstFreeSpin = false;
        //     }
        //     else{
        //         gAPI.casinoUI.enableBetChange();
        //     }
        //     // this.autoPlayLimitChecker.AutoPlay = false;
        //     gAPI.casinoUI.showUI();
        //     // this.uiController.enableSpinButton();
        //     //TODO: should be removed???
        //     gAPI.casinoUI.enableSpinButton();
        //     this.uiController.enableSwitchButton();
        //     this.game.controller.emit('AnimationsFinished')
        //     this.state = GAME_STATE.idle;
        // }
    }
}

class AutoPlayLimitChecker extends EventEmitter {
    constructor() {
        super();
        this.AutoPlay = false;
        this.cashChangeCounter = 0;
        this.onAnyWin = false;
        this.singleWinExceeds = false;
        this.ifCashIncreasesBy = false;
        this.ifCashDecreasesBy = false;
        this.singleWinExceedsLimit = 0;
        this.ifCashIncreasesByLimit = 0;
        this.ifCashDecreasesByLimit = 0;
    }
    getLimits() {
        this.onAnyWin = gAPI.casinoUI.getOnAnyWin();
        this.singleWinExceeds = gAPI.casinoUI.getSingleWinExceeds();
        this.ifCashIncreasesBy = gAPI.casinoUI.getIfCashIncreasesBy();
        this.ifCashDecreasesBy = gAPI.casinoUI.getIfCashDecreasesBy();
        this.singleWinExceedsLimit = gAPI.casinoUI.getSingleWinExceedsLimit();
        this.ifCashIncreasesByLimit = gAPI.casinoUI.getIfCashIncreasesByLimit();
        this.ifCashDecreasesByLimit = gAPI.casinoUI.getIfCashDecreasesByLimit();
    }

    // checkLimits(res) {
    //     if (this.onAnyWin && res.totalWin > 0) {
    //         console.log("AutoPlay stopped because limit 1");
    //         this.reInit();
    //         this.AutoPlay = false;
    //         return false
    //     } else if (this.singleWinExceeds && res.totalWin > this.singleWinExceedsLimit) {
    //         console.log("AutoPlay stopped because limit 2");
    //         this.reInit();
    //         this.AutoPlay = false;
    //         return false
    //     } else if (this.ifCashIncreasesBy || this.ifCashDecreasesBy) {
    //         this.cashChangeCounter -= gAPI.casinoUI.getBet(); /* TODO: TRACK BALANCE CHANGE */
    //         this.cashChangeCounter += res.totalWin;
    //         if (this.ifCashIncreasesBy && this.cashChangeCounter >= this.ifCashIncreasesByLimit) {
    //             console.log("AutoPlay stopped because limit 3");
    //             this.reInit();
    //             this.AutoPlay = false;
    //             return false
    //         } else if (this.ifCashDecreasesBy && this.cashChangeCounter * -1 >= this.ifCashDecreasesByLimit) {
    //             console.log("AutoPlay stopped because limit 4");
    //             this.reInit();
    //             this.AutoPlay = false;
    //             return false
    //         }
    //     }
    //     if (gAPI.casinoUI.getAutoPlayCount() === 0) {
    //         console.log("AutoPlay count ran out");
    //         this.reInit();
    //         this.AutoPlay = false;
    //         return false
    //     } else {
    //         return true;
    //     }
    // }
    //
    // reInit() {
    //     this.cashChangeCounter = 0;
    //     this.onAnyWin = false;
    //     this.singleWinExceeds = false;
    //     this.ifCashIncreasesBy = false;
    //     this.ifCashDecreasesBy = false;
    //     this.singleWinExceedsLimit = 0;
    //     this.ifCashIncreasesByLimit = 0;
    //     this.ifCashDecreasesByLimit = 0;
    // }
}
