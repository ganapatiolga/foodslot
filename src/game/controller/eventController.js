let EventEmitter = require('eventemitter3');

let GameEventType = {
    allpaylines: 'allpaylines',
    paylines: 'paylines',
    allSymbolAnimations: 'allSymbolAnimations',
    expand: 'expand',
    freeSpins: 'freeSpins',
    expanding: 'expanding',
    battleStart: 'battleStart',
    bonusTriggerOne: 'bonus trigger one',
    bonusTrigger: 'bonus trigger',
    battleFinish: 'battleFinish',
    winresume: 'winResume',
    end: 'end',
    popUp: 'popup'

};

let expSymbols = [8, 9, 10, 11, 12];

class EventChain extends EventEmitter {
    constructor(result, isFreespin = false, fromLoading) {
        super();
        this.result = result;
        console.log(result);
        this.events = [];
        this.repeat = true;
        this.onRepeat = null;

        //TODO: winbet = { win, bet }; Do we need separate win, bet??? Also, we have win bet data in result
        let bet = gAPI.casinoUI.getBet();
        let win = result.totalWin;
        let winbet = { win, bet };

        //TODO: check if it's possible just to check wins array existance
        if (result.spinResult && result.spinResult.wins && result.spinResult.wins.length > 0 || result.totalWin > 0) {
            if(result.winResume){
                //-------------------------------------
                //Show win on resume
                //-------------------------------------
                let winResumeEvent = this.createWinResumeEvent(winbet);
                let expandEvent = this.createExpandingEvent(result, true);
                this.events.push(winResumeEvent);
                if(expandEvent)
                    this.events.push(expandEvent);
            }
            else {
                //-------------------------------------
                //Show win after spin
                //-------------------------------------
                let paylineEvent = this.createPaylineEvent(result);
                let expandEvent = this.createExpandingEvent(result);
                this.events.push(this.createAllPaylinesEvent(result.spinResult.wins, winbet, result.spinResult.wins.length !== 1));

                if (expandEvent)
                    this.events.push(expandEvent);
                this.events.push(paylineEvent);
                this.onRepeat = paylineEvent;
            }

        }

        //Bonus enter animation
        if (result.spinResult && result.spinResult.scatterResult && (result.action != 'battle')){
            if (result.spinResult.scatterResult.freeGames === 25 && result.spinResult.wins.length === 0) {
                this.events.push({ type: GameEventType.bonusTriggerOne, bonus: result.spinResult.scatterResult });
            } else {
                this.events.push({ type: GameEventType.bonusTrigger, bonus: result.spinResult.scatterResult });
            }
            this.onRepeat = null;
        }

        //Free spins popup
        if (result.spinResult && result.spinResult.scatterResult && (result.action != 'battle')){
            this.events.push({ type: GameEventType.popUp,
                               popup: 'freeSpins',
                               addedFreeGames: result.spinResult.scatterResult.freeGames,
                               freeGamesLeft: result.freeGamesLeft || result.spinResult.scatterResult.freeGames
            });
            this.repeat = false;
        }


        if (result.action === 'battle') {
            this.events.push({ type: GameEventType.battleStart, result: 'placeholder', character: result.character, fromLoading: fromLoading});
            this.events.push({ type: GameEventType.popUp, popup: 'battlePopUp' });
            this.events.push({ type: GameEventType.battleFinish });
            this.events.push({ type: GameEventType.freeSpins, freeSpins: result.freeGamesLeft, totalAmountWon: result.totalAmountWon });
            this.events.push({ type: GameEventType.popUp, popup: 'dominantPopUp' })
        }

        if (result.freeGamesLeft === 0) {
            this.events.push({ type: GameEventType.popUp, popup: 'endFreeSpins' });
            this.repeat = false;
        }

        let endEvent = { type: GameEventType.end };

        //TODO: result.spinResult.wins check... is it needed??
        if (result.spinResult && result.spinResult.wins && result.spinResult.wins.length > 0 && this.repeat === true) {
            endEvent.repeat = this.createPaylineEvent(result);
        }

        //TODO: at this point spin animation is not actually finished yet. May cause minor visual bug at some point
        this.events.push(endEvent);
    }
    createWinResumeEvent(winbet) {
        let winResumeEvent = { type: GameEventType.winresume, skipExpandAnimation: true, winbet};
        return winResumeEvent;
    }

    createAllPaylinesEvent(paylineWins, winbet, show = false) {
        let allPaylinesEvent = { type: GameEventType.allpaylines, allPaylines: [], winbet, show };
        for (let i = 0; i < paylineWins.length; i++) {
            let paylinesId = paylineWins[i].paylineId;
            allPaylinesEvent.allPaylines.push({ paylinesId: paylinesId, win: paylineWins[i] });
        }
        return allPaylinesEvent;
    }

    createSymbolMatrix(result) {
        let paylines = Core.config.getSettings('config/paylines.json').paylines;
        let paylineWins = result.spinResult.wins;
        let symbolMatrix = []
        let reels = result.spinResult.reelDisplay

        for (let r = 0; r < reels.length; r++) {
            symbolMatrix[r] = [];
            for (let s = 0; s < reels[r].length; s++)
                symbolMatrix[r][s] = false;
        }

        for (let i = 0; i < paylineWins.length; i++) {
            let paylineId = paylineWins[i].paylineId;
            let win = paylineWins[i];
            for (let reelId = 0; reelId < win.positions.length; reelId++)
                if (win.positions[reelId] !== -1)
                    symbolMatrix[reelId][paylines[paylineId][reelId]] = true;
        }
        return symbolMatrix
    }

    createPaylineEvent(result) {
        let paylines = Core.config.getSettings('config/paylines.json').paylines;
        let paylineWins = result.spinResult.wins;
        let totalWin = result.totalWin;
        let winbet = {bet: result.totalBet, win: result.totalWin}
        let paylineEvent = { type: GameEventType.paylines, paylines: [], totalWin: 0, winbet };

        for (let i = 0; i < paylineWins.length; i++) {
            let paylineId = paylineWins[i].paylineId;
            paylineEvent.paylines.push({ paylineId: paylineId, win: paylineWins[i] });
        }
        paylineEvent.totalWin = totalWin;
        paylineEvent.resultMatrix = this.createSymbolMatrix(result)
        paylineEvent.result = result;

        return paylineEvent;
    }

    createExpandingEvent(result, skipExpandAnimation) {
        let paylines = Core.config.getSettings('config/paylines.json').paylines;
        let inWins = this.createSymbolMatrix(result)
        let reels = result.spinResult.reelDisplay
        let isAny = false;
        let reelExp = [];
        for (let r = 0; r < reels.length; r++) {
            let expand = null;
            for (let i = 0; i < expSymbols.length; i++) {
                let symbolId = expSymbols[i];
                if (reels[r][0] === symbolId && (inWins[r][0] || inWins[r][1] || inWins[r][2]) &&
                    reels[r][1] === symbolId &&
                    reels[r][2] === symbolId) {
                    expand = { reel: r, id: symbolId, symbols: [0, 1, 2] };
                    isAny = true;
                    break;
                } else if (reels[r][0] === symbolId && (inWins[r][0] || inWins[r][1]) &&
                    reels[r][1] === symbolId) {
                    expand = { reel: r, id: symbolId, symbols: [0, 1] };
                    isAny = true;
                    break;
                } else if (reels[r][1] === symbolId && (inWins[r][1] || inWins[r][2]) &&
                    reels[r][2] === symbolId) {
                    expand = { reel: r, id: symbolId, symbols: [1, 2] };
                    isAny = true;
                    break;
                }
            }
            if (expand)
                reelExp.push(expand);
        }
        if (isAny)
            return { type: GameEventType.expand, data: reelExp, skipExpandAnimation: skipExpandAnimation };
        else
            return null;
    }
    getEvent() {
        if (this.events.length <= 0) { throw "No more events"; }
        let currentEvent = this.events[0];
        this.events.splice(0, 1);
        return currentEvent;
    }
}

export { EventChain, GameEventType };
