var EventEmitter = require('eventemitter3');
import '../../core';
import { setFastSpin } from './animationController';
import {cleanUpCallbacks} from '../scene/FightScene';
import {default as SCREEN} from '../../core/screen';
import {reelData} from '../model/reelStrips';

export class UIController extends EventEmitter {
    constructor(game) {
        super();
        this.game = game;
        this.menuState = MENU_STATE.closed;
        this.windowHasFocus = true;

        gAPI.wrapper.init({
            casinoUI: {
                paytable: "static/html/paytable.html",
                infoPanel: "static/html/info.html",
                skin: conf.libURL + 'frontend/wrapper/static/css/desktop-high-crypbattle.css'
            }
        }).then(ctx => {
            this.initCasinoUIListeners();
            this.enforceLandscape();
        });

        this.game.ui.on('SwitchButton', function () {
            game.controller.showCharacterSelection();
            gAPI.casinoUI.hideSpinButton();
        });
    }

    initSkipButtonListener() {
        this.game.ui.removeListener('SkipButton');
        this.game.ui.once('SkipButton', function () {
            this.game.ui.skipButton.visible = false;

            if (this.game.scene.tween) {
                this.game.scene.tween.kill();
                TweenMax.killAll(); // for stopping battle sounds
            }
            this.game.controller.slotController.animationController.emit('AnimationsFinished');

            TweenMax.delayedCall(0.8,function(){
                for (let i = 0; i < cleanUpCallbacks.length; i++)
                cleanUpCallbacks[i]();
            }.bind(this));

            this.game.slotMachine.score.stopBigWinMusic();
            this.game.musicOnPlay.battleMusicLoop = false;
        }.bind(this));
    }

    initCasinoUIListeners() {
        gAPI.casinoUI.on('menuClose', this.closeMenu.bind(this));
        gAPI.casinoUI.on('menuOpen', this.openMenu.bind(this));
        // gAPI.casinoUI.on('openFullScreen', this.fullScreenMenu.bind(this));
        // gAPI.casinoUI.on('closedFullScreen', this.closedFullScreenMenu.bind(this));
        gAPI.casinoUI.on('muteSoundsToggle', this.toggleSoundMute.bind(this));
        // gAPI.casinoUI.on('muteMusicToggle', this.toggleMusicMute.bind(this));
        // gAPI.casinoUI.on('soundDialogue', this.soundDialogue.bind(this));
        gAPI.casinoUI.on('playButtonSound', this.playButtonSound.bind(this));
        gAPI.casinoUI.on('spinStart', this.startSpin.bind(this));
        gAPI.casinoUI.on('focus', this.receiveFocus.bind(this));
        gAPI.casinoUI.on('unFocus', this.loseFocus.bind(this));
        gAPI.casinoUI.on('QuickSpinToggle', setFastSpin);
        gAPI.casinoUI.on('stopSpin', this.stopSpin.bind(this));
        gAPI.casinoUI.on('betChangeDisabled', this.disableSwitchButton.bind(this));
        gAPI.casinoUI.on('betChangeEnabled', this.enableSwitchButton.bind(this));

        SCREEN.on('Resize', this.enforceLandscape.bind(this));
        //
        // gAPI.casinoUI.setPdfURL('/static/info.pdf');
        //
        // if(DEBUG){
        //     this.forcer = new Forcer();
        //     this.forcer.accessButton.hide();
        // }
        // this.onMenuCloseShowSwitchButton = false;
    }
    //
    openMenu(eventData) {
        if (this.menuState === MENU_STATE.closed) {
            this.menuState = MENU_STATE.opened;
            Core.assets.sounds.pauseSounds();
            Core.assets.sounds.playSound('SFX_UI_MenuOpen', false);
            this.game.engine.pause();
        }
        if (this.game.ui.switchButton) {
            this.onMenuCloseShowSwitchButton = this.game.ui.switchButton.visible;
        }
    }

    closeMenu(eventData) {
        if (this.menuState === MENU_STATE.opened) {
            this.menuState = MENU_STATE.closed;
            this.game.engine.resume();
            Core.assets.sounds.playSound('SFX_UI_Close', false);
            Core.assets.sounds.resumeSounds();
        }

        if (this.onMenuCloseShowSwitchButton)
            this.game.ui.switchButton.visible = true;
    }
    //
    // fullScreenMenu() {
    //     super.emit('MenuOpened');
    // }
    //
    // closedFullScreenMenu() {
    //     super.emit('MenuClosed');
    // }

    // soundDialogue(sound) {
    //     super.emit('CanInitialize');
    //     this.initializationReady = true;
    //     if (sound) {
    //         Core.assets.sounds.muteSounds = false;
    //         Core.assets.sounds.muteMusic = false;
    //     } else {
    //         Core.assets.sounds.muteSounds = true;
    //         Core.assets.sounds.muteMusic = true;
    //     }
    //
    // }

    //TODO: StartSpin disable in crypbattle, but no sound controls there. To check...
    startSpin(eventData) {
        //TODO returned start spin to disable switch button. To move this logic somewhere else??
        // let isAutoPlay = eventData && eventData.fromAutoPlay ? true : false
        // super.emit('StartSpinClicked', {fromAutoPlay: isAutoPlay});

        // Core.assets.sounds.playSound('start', false, { volume: 0.1 });
        Core.assets.sounds.playSound('SFX_UI_SpinStart', false, { volume: 1 });

        // gAPI.casinoUI.disableSpinButton();
        //this.disableSwitchButton()
    }

    stopSpin(){
        this.game.slotMachine.slotPanel.emit('stopSpin');
        Core.assets.sounds.playSound('SFX_UI_SpinStop', false, { volume: 0.2 });
    }

    enableSwitchButton(visible = true) {
        if(this.game.ui.switchButton){
            this.game.ui.switchButton.sprite = 'chara-select-normal.png';
            this.game.ui.switchButton.interactive = true;
            this.game.ui.switchButton.visible = visible;
        }
    }

    disableSwitchButton() {
        if(this.game.ui.switchButton){
            this.game.ui.switchButton.sprite = 'chara-select-inactive.png';
            this.game.ui.switchButton.interactive = false; 
        }
    }

    // toggleMusicMute(music) {
    //     Core.assets.sounds.muteMusic = !music;
    // }

    toggleSoundMute(sound) {
        Core.assets.sounds.muteSounds = !sound
    }

    enforceLandscape() {
        let minWidth = 128;
        let minHeight = 128;
        if (window.innerWidth < minWidth || window.innerHeight < minHeight || window.innerWidth < window.innerHeight || window.innerWidth / window.innerHeight >= 3) {
            if (gAPI.casinoUI.isErrorShown(603)) {
                Core.assets.sounds.pause();
                this.game.engine.pause();
            }
        } else {
            if (!gAPI.casinoUI.isErrorShown(603) && this.windowHasFocus) {
                Core.assets.sounds.resumeMusic();
                if (this.game.engine.clockPaused && this.menuState === MENU_STATE.closed) {
                    this.game.engine.resume();
                    Core.assets.sounds.resumeSounds()
                }
            }
        }
    }

    playButtonSound() {
        Core.assets.sounds.playSound('SFX_UI_General', false);
    }

    receiveFocus() {
        this.windowHasFocus = true;
        if(!gAPI.casinoUI.isErrorShown(603)) {
            Core.assets.sounds.resumeMusic()
            if (this.menuState === MENU_STATE.closed) {
                Core.assets.sounds.resumeSounds()
            }
        }
    }

    loseFocus() {
        this.windowHasFocus = false;
        Core.assets.sounds.pause();
    }
}

export var MENU_STATE = {
    opened: 0,
    closed: 1
};
