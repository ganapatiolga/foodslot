var EventEmitter = require('eventemitter3');
import '../../core';

let fastSpin = 1.0;
const setFastSpin = toggle => fastSpin = toggle ? 0.2 : 1.0;

class AnimationController extends EventEmitter {
    constructor() {
        super();
        this.animations = {};
        this.paylineTime = 1.0;
        this.startSpinTime = 0.9;
        this.stopSpinTime = this.startSpinTime;
        this.anticipationTime = 6.0;
        this.minSpinTime = 0.7;
        this.startTime = 0;
        this.expandAnimationTime = 1;
    }

    showExpands(slotMachine, scene, expandEvent) {
        let timeline = new TimelineMax({ paused: true });
        let timeOffset = expandEvent.skipExpandAnimation ? 0 : 0.3;
        for (let e = 0; e < expandEvent.data.length; e++) {
            let anim = expandEvent.data[e];
            timeline.add(function () {
                slotMachine.slotPanel.expandSymbols(anim.reel, anim.symbols, this.expandAnimationTime, expandEvent.skipExpandAnimation);
            }, timeOffset);
            if(!expandEvent.skipExpandAnimation) {
                timeOffset += 0.085;
            }
        }
        timeline.add(function () {
            this.emit('AnimationsFinished');
        }.bind(this), timeOffset);
        timeline.play();
    }

    showAllPaylines(slotMachine, scene, allPaylinesEvent) {
        let allPaylineWins = allPaylinesEvent.allPaylines;
        let option = { volume: 0.5 };

        if (allPaylineWins.length === 0) {
            super.emit("AnimationsFinished");
            return;
        }

        let delayTime = this.paylineTime;

        let isBigWin = false;

        if (allPaylinesEvent.winbet.win / allPaylinesEvent.winbet.bet > 15) {
            delayTime += 0.2;
            isBigWin = true;
            Core.assets.sounds.playSound('Music_Jingle_SmallWin_2', false, option);
        } else if (allPaylinesEvent.winbet.win / allPaylinesEvent.winbet.bet >= 5) {
            Core.assets.sounds.playSound('Music_Jingle_SmallWin_2', false, option);
        } else if (allPaylinesEvent.winbet.win > 0) {
            Core.assets.sounds.playSound('Music_Jingle_SmallWin', false, option);
        }

        if (!allPaylinesEvent.show) {
            super.emit("AnimationsFinished");
            return;
        }

        for (let i = 0; i < allPaylineWins.length; i++) {
            let paylineId = allPaylineWins[i].paylinesId;
            let payline = slotMachine.paylines.getPayline(paylineId);
            payline.playAnimation(delayTime);
            slotMachine.paylines.animate(delayTime);
        }
        TweenMax.delayedCall(delayTime, function () {
            this.emit("AnimationsFinished");
        }.bind(this));
    }

    animateWinnerChars(slotMachine, CurrentEvent) {
        let isBigWin = CurrentEvent.winbet.win / CurrentEvent.winbet.bet > 15.0 ;
        let  resultMatrix = CurrentEvent.resultMatrix;
        let animationLoop = function () {
            for (let i = 0; i < resultMatrix.length; i++) {
                for (let j = 0; j < resultMatrix[0].length; j++) {
                    if (resultMatrix[i][j] === true )
                        slotMachine.slotPanel.animateSymbol(i, j, this.paylineTime * 2, isBigWin);
                }
            }

            this.animations.animatedCharsCycle = {
                timeout: TweenMax.delayedCall(this.paylineTime * 2, animationLoop),
                cancelCallback: slotMachine.slotPanel.symbolAnimationManager.cancelAllAnimations.bind(slotMachine.slotPanel)
            };
        }.bind(this);
        animationLoop();
    }

    showPaylines(slotMachine, scene, paylineEvent, loop = false, firstCycle = true) {
        let paylineWins = paylineEvent.paylines;
        let isOnlySimpleChar = true;

        slotMachine.score.totalWin = paylineEvent.totalWin;
        if (firstCycle) {
            slotMachine.score.countTotalWin(paylineEvent);
        }
        if (paylineWins.length === 0) {
            if ( ! this.loop) super.emit("AnimationsFinished");
            return;
        }

        //This is for checking if only simple symbols are winners
        isOnlySimpleChar = paylineEvent.resultMatrix.every((arr, k) => arr.every((el, j) => {
            if (el === false) return true;
            let textureId = slotMachine.slotPanel.getSymbol(k, j).textureId;
            return textureId > 1 && textureId < 8;
        }));

        let i = 0;
        let isBigWin = paylineEvent.winbet.win / paylineEvent.winbet.bet > 15.0 ;

        let cycle = function () {
            let paylineId = paylineWins[i].paylineId;
            let win = paylineWins[i].win;
            let payline = slotMachine.paylines.getPayline(paylineId);

            if ((firstCycle == true) && (!isBigWin)) {
                Core.assets.sounds.playSound('SFX_SM_RowHit', false, { volume: 0.2});
            }

            payline.playAnimation(this.paylineTime);
            slotMachine.paylines.animate(this.paylineTime);

            if (!loop) slotMachine.slotPanel.tintSymbols()

            // for (var reel = 0; reel < win.pattern.length; reel++)
            for (let reel = 0; reel < win.positions.length; reel++) {
                if(win.positions[reel] < 0 ) continue;
                slotMachine.slotPanel.animateGlow(reel, payline.reels[reel], this.paylineTime);
            }

            slotMachine.score.showPaylineScore(payline.centerPos, win.win, this.paylineTime);

            let setAnimationPaylinesCycle = function(timeout = true) {
            // function setAnimationPaylinesCycle(timeout = true) {
                this.animations.paylinesCycle = {
                    timeout: timeout ? TweenMax.delayedCall(this.paylineTime, cycle) : null,
                    cancelCallbacks: [
                        payline.cancelAnimation.bind(payline),
                        slotMachine.score.cancelPaylineAnimation.bind(slotMachine.score),
                        slotMachine.paylines.cancelAnimation.bind(slotMachine.paylines),
                        slotMachine.score.cancelAnimations.bind(slotMachine.score),
                        slotMachine.slotPanel.cancelAllAnimations.bind(slotMachine.slotPanel),
                        slotMachine.unTintPanel.bind(slotMachine)
                    ]
                }
            }.bind(this)

            if (++i >= paylineWins.length) {
                if (loop) {
                    i = 0;
                } else {
                    let addTime = paylineEvent.paylines.length % 2 === 0 || isOnlySimpleChar ? 0 : 1.0;
                    if (paylineEvent.totalWin / slotMachine.score.bet > 15) {
                        slotMachine.bigWin.cancelCoins();
                        addTime += 3.0;
                    } else {
                        addTime += 1.0;
                    }

                    TweenMax.delayedCall(addTime, function () {
                        slotMachine.slotPanel.tintSymbols(0xFFFFFF);
                        this.emit('AnimationsFinished');
                    }.bind(this));

                    setAnimationPaylinesCycle(false);
                    return;
                }
            }

            setAnimationPaylinesCycle();
        }.bind(this);

        //happens only at the start!
        cycle();
    }

    startSpin(slotMachine) {
        this.startTime = Date.now();
        slotMachine.slotPanel.startSpin(fastSpin < 1.0 ? this.startSpinTime * 0.85 : this.startSpinTime, 0.1);
    }

    stopSpin(slotMachine, res, isCutIn = null) {
        if(res.totalWin === 0){
            gAPI.casinoUI.setCurrentWin(0);
        }
        let elapsedTime = 0.001 * Math.abs(Date.now() - this.startTime);
        let timeToWait = (((fastSpin < 1.0 ? 0.85 * this.startSpinTime : this.minSpinTime + this.startSpinTime)) - elapsedTime);
        let stopSpinAlreadyCalled = false;

        if(this.waitTimeout){
            this.waitTimeout.kill();
            delete this.waitTimeout;
            slotMachine.slotPanel.removeListener('stopSpin')
        }

        slotMachine.slotPanel.once('stopSpin', () => {
            if(this.waitTimeout){
                this.waitTimeout.kill();
                delete this.waitTimeout;
            }
            if(timeToWait > 0){
                timeToWait = 0;
                slotMachine.slotPanel.stopSpin(res.spinResult.reelDisplay, 0.1, [], {}, {}, isCutIn);
                stopSpinAlreadyCalled = true;
                slotMachine.slotPanel.once('ReelsStopped', () => {
                    this.emit('ReelsStopped');
                })
            } else {
                slotMachine.slotPanel.emit('superFast');
            }
        });

        if (timeToWait > 0) {
            this.waitTimeout = TweenMax.delayedCall(timeToWait, function () {
                this.stopSpin(slotMachine, res, isCutIn);
            }.bind(this));
            return;
        }

        let anticipationReels = {};
        let spinStopTime = 0;
        let count = 0;
        const scatterHit = [];

        for (let i = 0; i < res.spinResult.reelDisplay.length; i++) {
            let hasScatter = false;

            if ((count === 2) || (count === 3) || (count === 4)) {
                anticipationReels[i] = {time: 0.5 * this.anticipationTime, speed: 1};
            }

            for (let k = 0; k < res.spinResult.reelDisplay[i].length; k++) {
                if (res.spinResult.reelDisplay[i][k] === 1) {
                    count++;
                    hasScatter = true;
                }
            }
            if (hasScatter)
                scatterHit[i] = count
        }
        slotMachine.LastScatterCount = count;
        count = fastSpin < 1.0 ? -1 : count - 2;
        if (!stopSpinAlreadyCalled){
            if (count >= 0) {
                spinStopTime = this.stopSpinTime + this.anticipationTime * 0.5 * count
            }
            else {
                spinStopTime = this.stopSpinTime;
                anticipationReels = {};
            }
            slotMachine.slotPanel.stopSpin(res.spinResult.reelDisplay, (spinStopTime), fastSpin < 1.0 ? [0.1, 0.05, 0.03, 0.02, 0.01] : [0.5, 0.4, 0.3, 0.2, 0.1], anticipationReels, scatterHit, isCutIn);
            slotMachine.slotPanel.once('ReelsStopped', function () {
                this.emit('ReelsStopped');
            }.bind(this));
        }
    }

    cancelAnimations() {
        if (this.animations.paylinesCycle) {
            if(this.animations.paylinesCycle.timeout)
                this.animations.paylinesCycle.timeout.kill();
            for (let i = 0; i < this.animations.paylinesCycle.cancelCallbacks.length; i++)
                this.animations.paylinesCycle.cancelCallbacks[i]();
            delete this.animations.paylinesCycle;
        }

        if (this.animations.animatedCharsCycle) {
            if(this.animations.animatedCharsCycle.timeout)
                this.animations.animatedCharsCycle.timeout.kill();
            this.animations.animatedCharsCycle.cancelCallback();
            delete this.animations.animatedCharsCycle;
        }
    }
}

export { setFastSpin, AnimationController }
