let EventEmitter = require('eventemitter3');
import { SlotController } from './slotController';
import { UIController } from './UIController';
import { changeMS } from '../slot/symbol';
import '../../core';
import '../effects/symbolCutterOptimizer';
import '../effects/FontLoader';
import '../effects/uploadTexturesToGPU';
import {reelData} from '../model/reelStrips';

const ResumeData = {}
export class GameController extends EventEmitter {
    constructor(game) {
        super();
        this.game = game;
    }

    init(initData) {
        if(window.conf.resumeData){
            ResumeData.fromLoading = true;
        }
        this.changeReelConfig(initData.reels);
        this.uiController = new UIController(this.game);

        //TODO: something later...  for now make it work without connection controller
        if (window.conf.resumeData) {
            let action = conf.resumeData.nextAction[0];
            if (conf.resumeData.action !== 'battle') {
                action = conf.resumeData.action;
            }

            let resumeSet = action.substring(action.length - 3);

            conf.resumeData.set = resumeSet;

            let bet = reelData.data[resumeSet].bet;

            gAPI.casinoUI.setBetMultiplier(bet);
            gAPI.casinoUI.setCurrentCreditValue(conf.resumeData.totalBet / bet);

            reelData.selectCharacters(resumeSet);
        }

        gAPI.casinoUI.gameVersion = '0.57'; //TODO remove?
        gAPI.casinoUI.on('menuOpen', this.pause.bind(this));
        gAPI.casinoUI.on('menuClose', this.resume.bind(this));

        gAPI.casinoUI.on('betChange', () => {
            if(this.game.charSet && gAPI.casinoUI.getBetMultiplier() == reelData.data[this.game.charSet].bet || gAPI.casinoUI.getBetMultiplier() == 0){ return; }
            conf.gameData.reels.forEach(reel => {
                if(reel.bet == gAPI.casinoUI.getBetMultiplier()){
                    let charStr = reel.id.substring(reel.id.length - 3);
                    reelData.selectCharacters(charStr);
                    this.game.charSet = charStr;
                }
            })
        });

        // this.uiController.once('CanInitialize', this.afterSoundDialogue.bind(this));
        // if (this.uiController.initializationReady) { this.afterSoundDialogue(); }
    }

    afterSoundDialogue(resumeData) {
        if (!this.reelConfig) {
            this.reelConfig = Core.config.getSettings('config/reels.json');
        }

        //TODO temp from loading solution, uncomment and refactor if needed. Or delete if not
        // if(resumeData){
        //     resumeData.fromLoading = true;
        // }

        this.loadingFinished = true;
        //Timeline used for CBP-337 fix.
        let timeline = new TimelineMax();
        timeline.call(() => {
            // if (this.connectionFinished) {
                this.game.initSwipeOverlay();
                gAPI.casinoUI.hideWrapper();
                //TODO: check if 0.8 delay is needed....   if delayed call needed at all
                // TweenMax.delayedCall(0.8, function(){
            TweenMax.delayedCall(0.0, function(){
                    gAPI.casinoUI.createBottomBar();
                    gAPI.casinoUI.disableAutoPlay();
                    gAPI.casinoUI.addUIElements();
                    gAPI.casinoUI.hideSpinButton();
                    gAPI.casinoUI.disableBetChange();
                }.bind(this));

                // this.showCharacterSelection(ResumeData.fromLoading);
                this.showCharacterSelection(ResumeData.fromLoading);
                //TODO: fix orientation hadling
                this.uiController.enforceLandscape();
            // }
        })
    }

    showCharacterSelection(fromLoading = false) {
        window.setTimeout(function(){window.dispatchEvent(new Event('resize'))}, 0);
        gAPI.casinoUI.disableAutoPlay();
        if(!fromLoading)
            Core.assets.sounds.stopSound('Music_Bonus_Loop', { fadeOut: { time: 2.0 } });
            Core.assets.sounds.stopSound('Music_FreeSpinIntro_Loop', { fadeOut: { time: 2.0 } });

        if (this.game.musicOnPlay.mainBGM === false && !fromLoading) {
           TweenMax.delayedCall(3, function() {
                Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, {fadeIn: {time:2.0}, volume: 0.5});
                this.game.musicOnPlay.mainBGM = true;
            }.bind(this));
        }
        if (this.slotController) { this.slotController.animationController.cancelAnimations(); }

//        let swipeTime = fromLoading ? 0 : 0.8;
//        if(!fromLoading){
//            this.game.overlay.swipeOn(0.8);
//        }

        let swipeTime = fromLoading ? 0 : 1.0
        if(!fromLoading){
            this.game.overlay.swipeOn(1.0);
        }

        //Hail to the async programming!!!
        TweenMax.delayedCall(swipeTime, function () {
            if (fromLoading === false) {

                this.game.overlay.swipeOff(0.8);
                gAPI.casinoUI.showCasinoUI();
                this.game.initCharacterSelectionScene(this.game.data);
                this.game.ui.once('CharacterSelected', function (data) {
                    this.game.data = data;
                    let charStr = ""
                    for(let i = 1; i <= 5; i++){
                        if(data[5 - i]) charStr += i
                    }
                    this.game.charSet = charStr;
                    this.startGame(fromLoading);
                }.bind(this));
            } else if (fromLoading === true) {
                this.game.firstTimeCharSelect = false;
                this.game.data = [];
                window.conf.resumeData.set.split('').forEach(e => this.game.data[5 - e] = true); // magic
                this.game.charSet = window.conf.resumeData.set;
                this.startGame(fromLoading);
            }
        }.bind(this));
    }

    processResumeData(data) {
        console.log(`window.conf.resumeData`)
        console.log(window.conf.resumeData)
        //fdg

        // if (window.conf.resumeData && (ResumeData.fromLoading === true)) {
        //
        //
        //     //TODO: something definitely has to be done
        //     data = data ? data : {}
        //     let resumeData = window.conf.resumeData;
        //
        //     let freeSpins = resumeData.action.substring(0,4) === 'free' || resumeData.action === 'battle';
        //     data.resume = {
        //         action: resumeData.action,
        //         // spinResult: resumeData.action === 'battle' ? {totalWin: 0} : this.connectionController.translator.translateResults(resumeData, freeSpins),
        //         // spinResult: resumeData.action === 'battle' ? {totalWin: 0} : convertResult(resumeData, freeSpins),
        //         // spinResult: resumeData.action === 'battle' ? {totalWin: 0} : resumeData.spinResult,
        //         spinResult: resumeData.spinResult,
        //         scatterResult: resumeData.action === 'battle' ? null : resumeData.spinResult.scatterResult,
        //         freeSpinsLeft: resumeData.hasOwnProperty('freeGamesLeft') ? resumeData.freeGamesLeft : 10,
        //         freeSpinsTotalWin: resumeData.action === 'battle' ? resumeData.totalWin : resumeData.totalAmountWon,
        //         freeSpins: freeSpins,
        //         character: resumeData.action === 'battle' ? resumeData.character : resumeData.spinResult.replace[13],
        //         initialBet: resumeData.initialBet
        //     };
        //     data.resumeResult = resumeData.action === 'battle' ? null : resumeData.spinResult.reelDisplay;
        //     ResumeData.fromLoading = false;
        // }
        console.log(`Internal resume data`)
        console.log(data)
        return data;
    }

    resumeFreeSpins(data){
        gAPI.casinoUI.showError(604, true);

        let character = data.character || data.spinResult.replace[13]
        if(character) {
            changeMS(character, character);
        }
        this.slotController.game.initFreeSpinsScene();

        let freeSpinsTotalWin = data.totalAmountWon - data.totalWin
        this.slotController.game.slotMachine.score.freeSpinsTotalWin = freeSpinsTotalWin
        gAPI.casinoUI.setFreeSpinsTotalWin(freeSpinsTotalWin);

        this.slotController.freeSpins = data.freeGamesLeft;

        this.slotController.firstFreeSpin = false;
        this.slotController.bonus = true;

        let freeSpinsLeft = data.spinResult.scatterResult ? data.freeGamesLeft - data.spinResult.scatterResult.freeGames : data.freeGamesLeft
        gAPI.casinoUI.startFreeSpinsMode(freeSpinsLeft, true);

        gAPI.casinoUI.disableBetChange();
        gAPI.casinoUI.once('alertClosed', function() {
            this.slotController.game.slotMachine.score.showDominantPopUp(character);
            this.slotController.luckySymbol = character;
            let closePopUp = TweenMax.delayedCall(5.0, function () {
                gAPI.casinoUI.removeAllListeners('screenClick');
                continueFreeSpins();
            }.bind(this));

            gAPI.casinoUI.once('screenClick', function () {
                closePopUp.kill();
                continueFreeSpins();
            }.bind(this));

            const continueFreeSpins = () => {
                this.slotController.game.slotMachine.score.removeDominantPopUp();
                this.slotController.animationController.startSpin(this.slotController.game.slotMachine);
                this.slotController.onResultReceived(data);
            }
        }.bind(this));
    }

    resumeScatterResult(data){
        gAPI.casinoUI.showError(604, true);

        this.slotController.animationController.startSpin(this.slotController.game.slotMachine);
        gAPI.casinoUI.disableBetChange();

        gAPI.casinoUI.disableAutoPlay();
        gAPI.casinoUI.disableSpinButton();
        //this.uiController.disableSwitchButton();
        gAPI.casinoUI.enableStopButton();

        gAPI.casinoUI.once('alertClosed', function(){
            this.slotController.onResultReceived(data);
        }.bind(this));
    }

    resumeGame(resumeData) {

        // Doesn't create new instance
        //TODO: new SlotController should be created in one place
        this.slotController = new SlotController(this.game, this.uiController, this.connectionController);
        gAPI.casinoUI.setCurrentCreditValue(resumeData.initialBet.value);
        if (resumeData.action.startsWith('free')) {
            this.resumeFreeSpins(resumeData)
        } else if (resumeData.action === 'battle') {
            this.game.clearScreen()
            TweenMax.delayedCall(0.1, function(){
                this.slotController.onReelsStopped(resumeData);
            }.bind(this))
        } else if (resumeData.spinResult && resumeData.spinResult.scatterResult){
            this.resumeScatterResult(resumeData)
        } else {
            //TODO: Check resumeData.totalWin during different resume scenarios
            if (resumeData.totalWin > 0) {
                gAPI.casinoUI.disableSpinButton();
                gAPI.casinoUI.disableBetChange();
                //this.uiController.disableSwitchButton();
            }

            resumeData.winResume = true;//TODO: remove it!!!!!
            this.slotController.onReelsStopped(resumeData);

            this.emit('AnimationsFinished')
        }
    }

    startGame(fromLoading) {
        let resumeData = window.conf.resumeData;
        let isBattle = fromLoading && resumeData && resumeData.action === 'battle'
        let startGameDelay = 1.0;
        gAPI.casinoUI.hideUI();

        if(isBattle) {
            gAPI.casinoUI.hideCasinoUI()
            startGameDelay = 0.1
        }

        isBattle ? null: this.game.overlay.swipeOn(1.0);

        TweenMax.delayedCall(startGameDelay, function () {
            if(!isBattle){
                this.game.overlay.swipeOff(1.0);
                gAPI.casinoUI.showUI();
                TweenMax.delayedCall(2.5, function() {
                    gAPI.casinoUI.showSpinButton();
                    gAPI.casinoUI.enableSpinButton();
                });
            } else {
                gAPI.casinoUI.showSpinButton();
                gAPI.casinoUI.enableSpinButton();
            }
            this.game.initMainGameScene(fromLoading ? resumeData : null, true);

            if (this.game.firstTimeCharSelect && !(resumeData)) {
                this.slotController = new SlotController(this.game, this.uiController, this.connectionController);
                this.game.firstTimeCharSelect = false;
                this.emit('AnimationsFinished');
            } else if (!this.game.firstTimeCharSelect && fromLoading) {
                if(resumeData) {
                    this.resumeGame(resumeData);
                }
            }

            //TODO: fix it
            if(!isBattle) {
                gAPI.casinoUI.showCasinoUI();
            }
        }.bind(this));
    }

    pause() {
        this.game.engine.pause();
    }

    resume() {
        this.game.engine.resume();
    }

    //TODO remove.
    onConnectionFinished() {
        this.connectionFinished = true;
        if (this.loadingFinished) {
            this.showCharacterSelection(true);
            gAPI.casinoUI.createBottomBar();
            gAPI.casinoUI.addUIElements();
            this.uiController.enforceLandscape();
        }
    }

    changeReelConfig(reelConfig) {
        this.reelConfig = reelConfig;
        // this.freeSpinReelConfig = {reels: reelConfig.freeReels}; //Code from crypcrusade. To remove if not needed
    }
}
