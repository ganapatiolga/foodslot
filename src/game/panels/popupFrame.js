var PIXI = require('pixi.js');

export class PopupFrame extends PIXI.Container {
    constructor(){
        super();

        this.popupFrame = new PIXI.Container();


        this.frame1 = new Core.Sprite({ name: 'bonus', sprite: 'pop-up_000.png' });
        this.frame1.anchor.set(0, 0);
        this.frame1.scale.set(-1.0, 1.0);
        this.frame1.position.set(0, -this.frame1.height);
        this.popupFrame.addChild(this.frame1);

        this.frame2 = new Core.Sprite({ name: 'bonus', sprite: 'pop-up_000.png' });
        this.frame2.anchor.set(1, 0);
        this.frame2.scale.set(1.0, 1.0);
        this.frame2.position.set(this.frame2.width, -this.frame2.height);
        this.popupFrame.addChild(this.frame2);

        this.frame3 = new Core.Sprite({ name: 'bonus', sprite: 'pop-up_000.png' });
        this.frame3.anchor.set(0, 1);
        this.frame3.scale.set(-1.0, -1.0);
        this.frame3.position.set(0, 0);
        this.popupFrame.addChild(this.frame3);

        this.frame4 = new Core.Sprite({ name: 'bonus', sprite: 'pop-up_000.png' });
        this.frame4.anchor.set(1, 1);
        this.frame4.scale.set(1.0, -1.0);
        this.frame4.position.set(this.frame4.width, 0);
        this.popupFrame.addChild(this.frame4);

        super.addChild(this.popupFrame);
    }
}
