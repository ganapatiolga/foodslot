var PIXI = require('pixi.js');
import { SlotSymbol } from '../slot/symbol';
import { PopupFrame } from './popupFrame';

export class DominantPopup extends PIXI.Container {
    constructor(){
        super();

        this.texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;
        this.frame = new PopupFrame();
        this.seal = null;
        super.addChild(this.frame);

        this.headerText = new PIXI.Text(this.texts.domsymbol, { fontFamily: 'Noto Serif Cond Blk', fontSize: 51 / Core.scaleFactor + "px", align: 'center', fill: '#810b0e', dropShadow: true, dropShadowAngle: Math.PI / 2, dropShadowColor: '#050505', dropShadowDistance: 2 / Core.scaleFactor });
        this.headerText.updateText();
        Core.utils.scaleDown(this.headerText, 625 / Core.scaleFactor);
        this.headerText.anchor.set(0.5, 0.5);
        this.headerText.position.set(0, -95 / Core.scaleFactor);
        super.addChild(this.headerText);

        this.dominantText = new PIXI.Text(this.texts.dominantwin, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", fontWeight: 'bold', lineHeight: 35 / Core.scaleFactor, align: 'left', fill: '#3f1816'});
        this.dominantText.updateText()
        Core.utils.scaleDown(this.dominantText, 350 / Core.scaleFactor);
        this.dominantText.anchor.set(0, 0.0);
        this.dominantText.position.set( -252.5 / Core.scaleFactor, -40 / Core.scaleFactor);
        super.addChild(this.dominantText);

        this.dominantSymbol = new SlotSymbol(1);
        this.dominantSymbol.sprite.symbol.anchor.set(0.5, 0.5);
        this.dominantSymbol.position.set(157.5 / Core.scaleFactor, 40 / Core.scaleFactor);
        super.addChild(this.dominantSymbol);
    }

    show(charId) {
        if (this.seal === null) {
            this.seal = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/freespin/bonus/bonus.json');
            this.seal.scale.set(1 / 2 / Core.scaleFactor, 1 / 2 / Core.scaleFactor);
        }
        Core.assets.sounds.playSound('Music_Jingle_PopUp', false, {fadeOut: {time:0.5}, volume: 0.5});
        this.seal.visible = false;
        super.addChild(this.seal);
        this.dominantSymbol.texture = charId;
        this.alpha = 0;
        this.visible = true;

        TweenMax.to(this, 0.5, { alpha: 1.0 });
        let tm = new TimelineMax();
        tm.add(function() {
            this.seal.state.setAnimation(0, 'hanko', false);
            this.seal.visible = true;
        }.bind(this), 1.0);
    }

    hide(delayedCallback) {
        TweenMax.to(this, 0.25, { alpha: 0 });
        TweenMax.delayedCall(0.3, function () {
            this.visible = false;
            delayedCallback();
        }.bind(this));
    }
}
