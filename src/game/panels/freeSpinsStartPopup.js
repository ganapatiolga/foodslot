var PIXI = require('pixi.js');
import { PopupFrame } from './popupFrame';

export class FreeSpinsStartPopup extends PIXI.Container {
    constructor(){
        super();

        this.texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;
        this.frame = new PopupFrame();
        super.addChild(this.frame);

        this.congratulationText = new PIXI.Text(this.texts.congrats, { fontFamily: 'Noto Serif Cond Blk', fontSize: 51 / Core.scaleFactor + "px", align: 'center', fill: '#810b0e', dropShadow: true, dropShadowAngle: Math.PI / 2, dropShadowColor: '#050505', dropShadowDistance: 2 / Core.scaleFactor});
        this.congratulationText.updateText();
        Core.utils.scaleDown(this.congratulationText, 625 / Core.scaleFactor);
        this.congratulationText.anchor.set(0.5, 0.5);
        this.congratulationText.position.set(0, -95.5 / Core.scaleFactor);
        super.addChild(this.congratulationText);

        this.wonText = new PIXI.Text(this.texts.youwon, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", fontWeight: 'bold', letterSpacing: -2.5 / Core.scaleFactor, lineHeight: 30 / Core.scaleFactor, align: 'center', fill: '#3f1816'});
        this.wonText.updateText();
        Core.utils.scaleDown(this.wonText, 625 / Core.scaleFactor);
        this.wonText.anchor.set(0.5, 0.5);
        this.wonText.position.set(0, -22 / Core.scaleFactor);
        super.addChild(this.wonText);

        this.freespinText = new PIXI.Text(this.texts.freespins, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", fontWeight: 'bold', letterSpacing: -2.5 / Core.scaleFactor, lineHeight: 30 / Core.scaleFactor, align: 'center', fill: '#3f1816'});
        this.freespinText.updateText();
        Core.utils.scaleDown(this.freespinText, 620 / Core.scaleFactor);
        this.freespinText.anchor.set(0.5, 1.0);
        this.freespinText.position.set(0, 126.5 / Core.scaleFactor);
        super.addChild(this.freespinText);

        this.freeSpinsPopUpText = new PIXI.extras.BitmapText("0", { font: 68 / Core.scaleFactor + "px redfont" });
        this.freeSpinsPopUpText.anchor.set(0.5, 0.5);
        this.freeSpinsPopUpText.position.set(0, 30 / Core.scaleFactor);
        super.addChild(this.freeSpinsPopUpText);
    }

    show(number) {
        this.freeSpinsPopUpText.text = number;
        this.visible = true;
        this.alpha = 0;

        TweenMax.to(this, 0.5, { alpha: 1.0 });
    }

    hide(delayedCallback) {
        TweenMax.to(this, 0.25, { alpha: 0 });
        TweenMax.delayedCall(0.3, function () {
            this.visible = false;
            delayedCallback();
        }.bind(this));
    }
}
