var PIXI = require('pixi.js');
import { PopupFrame } from './popupFrame';

export class FreeSpinsBattlePopup extends PIXI.Container {
    constructor(){
        super();

        let texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;
        this.frame = new PopupFrame();
        super.addChild(this.frame);

        this.freeSpinsBattleText = new PIXI.Text(texts.battle, { fontFamily: 'Noto Serif Cond Blk', fontSize: 51 / Core.scaleFactor + "px", align: 'center', fill: '#810b0e', dropShadow: true, dropShadowAngle: Math.PI / 2, dropShadowColor: '#050505', dropShadowDistance: 2 / Core.scaleFactor });
        this.freeSpinsBattleText.updateText();
        Core.utils.scaleDown(this.freeSpinsBattleText, 625 / Core.scaleFactor);
        this.freeSpinsBattleText.anchor.set(0.5, 0.5);
        this.freeSpinsBattleText.position.set(0, -86.5 / Core.scaleFactor);
        super.addChild(this.freeSpinsBattleText);

        var fontSize = ((Core.scale != 'high') ? 30 : 35) / Core.scaleFactor

        this.freeSpinsBattleTwoText = new PIXI.Text(texts.battleexplain, { fontFamily: 'Noto Serif Cond', fontSize: fontSize + "px", fontWeight: 'bold', lineHeight: 36 / Core.scaleFactor, align: 'center', fill: '#3f1816'});
        this.freeSpinsBattleTwoText.updateText();
        Core.utils.scaleDown(this.freeSpinsBattleTwoText, 625 / Core.scaleFactor);
        this.freeSpinsBattleTwoText.anchor.set(0.5, 0.5);
        this.freeSpinsBattleTwoText.position.set(0, 22 / Core.scaleFactor);
        super.addChild(this.freeSpinsBattleTwoText);
    }

    show() {
//        Core.assets.sounds.playSound('Music_Jingle_PopUp', false, {fadeOut: {time: 0.5}, volume: 0.5});
        this.visible = true;
        this.alpha = 0;

        TweenMax.to(this, (100 - 90) / 30, { alpha: 1.0 });
    }

    hide(delayedCallback) {
        TweenMax.to(this, (245 - 235) / 30, { alpha: 0 });
        TweenMax.delayedCall(0.3, function () {
            this.visible = false;
            delayedCallback();
        }.bind(this));
    }
}
