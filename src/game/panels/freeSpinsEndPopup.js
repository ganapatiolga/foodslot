var PIXI = require('pixi.js');
import { PopupFrame } from './popupFrame';

export class FreeSpinsEndPopup extends PIXI.Container {
    constructor(){
        super();

        this.texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;
        this.frame = new PopupFrame();
        super.addChild(this.frame);

        this.endCongratulationText = new PIXI.Text(this.texts.congrats, { fontFamily: 'Noto Serif Cond Blk', fontSize: 51 / Core.scaleFactor + "px", align: 'center', fill: '#810b0e', dropShadow: true, dropShadowAngle: Math.PI / 2, dropShadowColor: '#050505', dropShadowDistance: 2 / Core.scaleFactor });
        this.endCongratulationText.updateText();
        Core.utils.scaleDown(this.endCongratulationText, 625 / Core.scaleFactor);
        this.endCongratulationText.anchor.set(0.5, 0.5);
        this.endCongratulationText.position.set(0, -95.5 / Core.scaleFactor);
        super.addChild(this.endCongratulationText);

        this.endWonText = new PIXI.Text(this.texts.youwon, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", fontWeight: 'bold', letterSpacing: -2.5 / Core.scaleFactor, lineHeight: 30 / Core.scaleFactor, align: 'center', fill: '#3f1816'});
        this.endWonText.updateText();
        Core.utils.scaleDown(this.endWonText, 625 / Core.scaleFactor);
        this.endWonText.anchor.set(0.5, 0.5);
        this.endWonText.position.set(0, -22 / Core.scaleFactor);
        super.addChild(this.endWonText);

        this.endFreeSpinsPopUpText = new PIXI.extras.BitmapText("0", { font: 68 / Core.scaleFactor + "px redfont" });
        this.endFreeSpinsPopUpText.anchor.set(0.5, 0.5);
        this.endFreeSpinsPopUpText.position.set(0, 30 / Core.scaleFactor);
        super.addChild(this.endFreeSpinsPopUpText);

        this.endWonTextLast = new PIXI.Text(this.texts.infreespins, { fontFamily: 'Noto Serif Cond', fontSize: 35 / Core.scaleFactor + "px", fontWeight: 'bold', letterSpacing: -2.5 / Core.scaleFactor, lineHeight: 30 / Core.scaleFactor, align: 'center', fill: '#3f1816' });
        this.endWonTextLast.updateText();
        Core.utils.scaleDown(this.endWonTextLast, 625 / Core.scaleFactor);
        this.endWonTextLast.anchor.set(0.5, 1.0);
        this.endWonTextLast.position.set(0, 126.5 / Core.scaleFactor);
        super.addChild(this.endWonTextLast);
    }

    show(winAmount) {
        this.endFreeSpinsPopUpText.text = winAmount;
        this.alpha = 0;
        this.visible = true;

        TweenMax.to(this, 0.5, { alpha: 1.0 });
    }

    hide(delayedCallback) {
        TweenMax.to(this, 0.25, { alpha: 0 });
        TweenMax.delayedCall(0.3, function () {
            this.visible = false;
            delayedCallback();
        }.bind(this));
    }
}
