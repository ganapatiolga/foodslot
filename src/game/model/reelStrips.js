// let reelStripsData = null;
let reelStripsData = translateInit(conf.gameData).reelData;
let characters = null;


function translateInit(data){
    let translation = {};
    let reelData = {};
    for(let i = 0; i < data.reels.length; i++){
        let name = data.reels[i].id.slice(data.reels[i].id.length-3);
        reelData[name] = reelData[name] || {};
        reelData[name].characters = name.split('').map(a => parseInt(a));
        if(data.reels[i].bet)
            reelData[name].bet = data.reels[i].bet;
        if(data.reels[i].id.indexOf('freespin') !== -1){
            reelData[name].FG = data.reels[i].reelStrips
        }else{
            reelData[name].MG = data.reels[i].reelStrips
        }
    }

    translation.reelData = reelData;
    translation.currency = 'EUR';
    return translation;
}

function paytableHack(selectedChars){
    if(!document.getElementById('opacityPaytable')){ 
        var element = document.createElement("style");
        element.id = "opacityPaytable";
        document.getElementsByTagName("body")[0].appendChild(element);
    }
    document.getElementById('opacityPaytable').innerHTML = `${
        [1, 2, 3, 4, 5].filter(charId => selectedChars.indexOf(charId) == -1)
        .map(charId => `#highSymbol${charId}`).join(',')
    }{ opacity: 0.5; }`
}

let reelData = {
    setFromInit: data => reelStripsData = data.reelStrips,
    // setFromInit: () => {
    //
    // },
    selectCharacters: data => {
        //console.log(`DEBUG: select ${data}`);

        paytableHack(data);
        if(reelStripsData === null)
        return;
        if(reelStripsData[data] === undefined)
            throw new Error(data + ' character selection id pattern not found');
        characters = data
    },
    getReels: fg => {
        if(fg)
            return reelStripsData[characters].FG;
        else
            return reelStripsData[characters].MG;
    },
    getSelectedId: () => characters,
    get data(){ return reelStripsData }
};

export {reelData}