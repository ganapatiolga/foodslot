var PIXI = require('pixi.js');
import '../../core';
import { DominantPopup } from '../panels/dominantPopup';
import { FreeSpinsStartPopup } from '../panels/freeSpinsStartPopup';
import { FreeSpinsEndPopup } from '../panels/freeSpinsEndPopup';

export class Score extends PIXI.Container {
    constructor(size) {
        super();

        this.amountWonPerPaylineBg = new Core.Sprite({ name: 'amountWonPerPayline Background', sprite: 'win.png' });
        this.amountWonPerPaylineBg.anchor.set(0.5, 0.6);
        this.amountWonPerPaylineBg.scale.set(0.8 / Core.scaleFactor / 2, 0.8 / Core.scaleFactor / 2);
        this.amountWonPerPaylineBg.visible = false;
        super.addChild(this.amountWonPerPaylineBg);

        this.amountWonPerPayline = new PIXI.extras.BitmapText("0", { font: 39 / Core.scaleFactor + "px redfont", align: "center" });
        this.amountWonPerPayline.anchor.set(0.5, 0.77);
        this.amountWonPerPayline.visible = false;
        super.addChild(this.amountWonPerPayline);

        this.rect = new PIXI.Graphics();
        this.rect.beginFill();
        this.rect.drawRect(-1000 / Core.scaleFactor, -1000 / Core.scaleFactor, 3000 / Core.scaleFactor, 3000 / Core.scaleFactor);
        this.rect.endFill();
        this.rect.alpha = 0;
        super.addChild(this.rect);

        this.freeSpinsStartPopup = new FreeSpinsStartPopup();
        this.freeSpinsStartPopup.position.set(size.x / 2, size.y / 2);
        this.freeSpinsStartPopup.visible = false;
        super.addChild(this.freeSpinsStartPopup);

        this.freeSpinsEndPopup = new FreeSpinsEndPopup();
        this.freeSpinsEndPopup.position.set(size.x / 2, size.y / 2);
        this.freeSpinsEndPopup.visible = false;
        super.addChild(this.freeSpinsEndPopup);

        this.dominantPopup = new DominantPopup();
        this.dominantPopup.position.set(size.x / 2, size.y / 2);
        this.dominantPopup.visible = false;
        super.addChild(this.dominantPopup);

        this.color = { r: 255, g: 255, b: 255 };

        this.win = 0;
        this.totalWin = 0;
        this._freeSpinsTotalWin = 0;
        this.size = size;
        this.bet = 0;
        this.isPlayMusicBigWin = false;
    }

    get freeSpinsTotalWin(){
        return this._freeSpinsTotalWin
    }

    set freeSpinsTotalWin(value){
        this._freeSpinsTotalWin = value
    }
    showDominantPopUp(charId) {
        Core.assets.sounds.stopSound('Music_FreeSpinIntro_Loop', { fadeOut:{time:1.0}});
        Core.assets.sounds.playSound('Music_Bonus_Intro', false, { volume: 0.5 });
        this.rect.alpha = 0.6;
        this.dominantPopup.show(charId);
    }

    removeDominantPopUp() {
        Core.assets.sounds.stopSound('Music_Bonus_Intro', { fadeOut:{time:1.0}});
        Core.assets.sounds.playMusic('Music_Bonus_Loop', true, {fadeIn: {time: 0.5},  volume: 0.8 });
        this.dominantPopup.hide(() => this.rect.alpha = 0);
    }

    showFreeSpinsPopUp(number) {
        Core.assets.sounds.stopSound('Music_BaseGame_Loop', {fadeOut: {time: 0.3}});
        TweenMax.delayedCall(0.5, function () {
            this.rect.alpha = 0.6;
            this.freeSpinsStartPopup.show(number);
            Core.assets.sounds.playSound('Music_Jingle_PopUp_Congratulations', false, {fadeIn: {time: 0.5}, volume: 0.5});
        }.bind(this));
    }

    removeFreeSpinsPopUp() {
        if (this.freeSpins) {
            Core.assets.sounds.playMusic('Music_Bonus_Loop', {volume: 0.8});
        } else {
            Core.assets.sounds.stopSound('Music_Bonus_Loop', {fadeOut: {time:1}});
            Core.assets.sounds.playSound('Music_BaseGame_End', false, {fadeIn: {time: 0.5}, volume: 0.3 });
        }
        this.freeSpinsStartPopup.hide(() => {this.rect.alpha = 0});
    }

    showFreeSpinsEndPopUp() {
        Core.assets.sounds.stopSound('Music_Bonus_Loop', {fadeOut: {time:1}});
        Core.assets.sounds.playSound('Music_Jingle_PopUp_Congratulations', false, {fadeIn: {time: 0.5}, volume: 0.5});
        this.rect.alpha = 0.6;
        this.freeSpinsEndPopup.show(gAPI.casinoUI.formatCurrency(this.freeSpinsTotalWin));
    }

    removeFreeSpinsEndPopUp() {
        Core.assets.sounds.playSound('Music_Bonus_End', false, {fadeIn: {time: 0.5}, volume: 0.5 });
        this.freeSpinsEndPopup.hide( () => {this.rect.alpha = 0;});
    }

    countWin(time, winbet) {
        this.resetWinPerSpin();
        this.bigWinState = 0;
        this.tempFreeSpinsTotal = this.freeSpinsTotalWin;
        this.bigWin = winbet.win / winbet.bet > 15.0;
        this.score = winbet.win;
        this.bet = winbet.bet;
        this.winbet = winbet;

        if (winbet.win / winbet.bet > 5)
            Core.assets.sounds.playSound('CountUp_Music_Loop', true, { volume: 0.3 });

        var updateFrequency = 6;
        var winUpdateFrequencyCounter = updateFrequency - 1;
        var updateCasinoUIWin = function() {
            if (winUpdateFrequencyCounter++ === updateFrequency) {
                winUpdateFrequencyCounter = 0;
                gAPI.casinoUI.setCurrentWin(this.win);
                if (this.freeSpins) gAPI.casinoUI.setFreeSpinsTotalWin(this.freeSpinsTotalWin);
            }
        }.bind(this);

        var updateHandler = function () {
            if (this.bigWinState === 0 && this.win / winbet.bet >= 30)
                this.slotMach.bigWin.setWinHeader(this.bigWinState = 1);
            else if (this.bigWinState === 1 && this.win / winbet.bet >= 50)
                this.slotMach.bigWin.setWinHeader(this.bigWinState = 2);
            else if (this.bigWinState === 2 && this.win / winbet.bet >= 75)
                this.slotMach.bigWin.setWinHeader(this.bigWinState = 3);

            this.slotMach.bigWin.setWinAmount(this.win);
            updateCasinoUIWin();
        }

        if (this.bigWin) {
            this.slotMach.bigWin.setWinHeader(this.bigWinState);
            Core.assets.sounds.stopSound('Music_BaseGame_Loop', {fadeOut:{time:2.0}});
            Core.assets.sounds.stopSound('Music_Bonus_Loop', {fadeOut:{time:2.0}});
            Core.assets.sounds.playSound('Music_BigWin_Loop', true, { volume: 0.5});
            this.isPlayMusicBigWin = true;
            this.slotMach.bigWin.show();
        }

        this.countAnimation = TweenMax.to(this, time, {
            win: "+=" + this.score, onUpdate: updateHandler.bind(this), ease: Linear.easeNone, onComplete: function () {
                gAPI.casinoUI.setCurrentWin(this.win);
                if (this.freeSpins) gAPI.casinoUI.setFreeSpinsTotalWin(this.freeSpinsTotalWin);
                Core.assets.sounds.stopSound('CountUp_Music_Loop');
                if (this.bigWin) {
                    TweenMax.delayedCall(0.5, function () {
                        this.emit('BigWinEnd');
                        this.slotMach.bigWin.cancel();

                        TweenMax.delayedCall(68 / 30, function() {
                            this.stopBigWinMusic();
                            if (!this.freeSpinsEndPopup.visible) {
                                if (this.freeSpins) {
                                    Core.assets.sounds.playMusic('Music_Bonus_Loop', true, {fadeIn : {time : 0.5}, volume : 0.8});
                                } else {
                                    Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, {fadeIn : {time : 0.5}, volume : 0.5});
                                }
                            }
                        }.bind(this));
                    }.bind(this));
                }
            }.bind(this)
        });

        if (this.freeSpins)
            this.freeSpinCountAnimation = TweenMax.to(this, time, { freeSpinsTotalWin: "+=" + this.score, ease: Linear.easeNone });
    }

    showPaylineScore(pos, score, time) {
        var paylineScore = this.amountWonPerPayline;
        var paylineScoreBg = this.amountWonPerPaylineBg;
        paylineScore.text = gAPI.casinoUI.formatCurrency(parseFloat(score));

        if (paylineScore.text.length >= 6) {
            let scale = Math.max(0.9 - 0.05 * (paylineScore.text.length - 6), 0.55);
            paylineScore.scale.set(scale, scale);
        }

        paylineScore.position.set(pos.x, pos.y);
        paylineScoreBg.position.set(pos.x, pos.y);
        paylineScore.visible = true;
        paylineScore.alpha = 0;
        paylineScoreBg.visible = true;
        paylineScoreBg.alpha = 0;
        this.animation = new TimelineMax();
        this.animation.to(paylineScore, time / 2, {alpha: 1, ease: Circ.easeOut}, 0)
        .to(paylineScore, time / 2, {alpha: 0, ease: Circ.easeIn}, time / 2)
        .to(paylineScoreBg, time / 2, {alpha: 1, ease: Circ.easeOut}, 0)
        .to(paylineScoreBg, time / 2, {alpha: 0, ease: Circ.easeIn}, time / 2)
    }

    setTotalWin(winbet) {
        this.winbet = winbet;
        gAPI.casinoUI.setCurrentWin(winbet.win);
    }

    countTotalWin(res) {
        if (res.paylines.length > 0) {
            this.countWin(res.paylines.length, res.winbet);
        } else {
            if (res.result.spinResult.scatterResult) {
                if (res.result.spinResult.scatterResult.freeGames < 25) {
                    this.countWin(2, res.winbet);
                } else {
                    this.countWin(16, res.winbet);
                }
            }
        }
    }

    cancelPaylineAnimation() {
        if (this.animation)
            this.animation.kill();
        this.amountWonPerPayline.visible = false;
        this.amountWonPerPaylineBg.visible = false;
    }

    cancelAnimations(next = () => { }, withMusic = true) {
        Core.assets.sounds.stopSound('CountUp_Music_Loop');
        this.slotMach.bigWin.cancelCoins();

        if (this.countAnimation) {
            this.countAnimation.kill();
        }
        if (this.freeSpinCountAnimation) {
            this.freeSpinCountAnimation.seek(this.freeSpinCountAnimation.endTime());

            if (this.freeSpins) {
                gAPI.casinoUI.setFreeSpinsTotalWin(this.freeSpinsTotalWin);
            }
            this.freeSpinCountAnimation.kill();
        }

        if (this.score / this.bet > 15) {
            TweenMax.delayedCall(gAPI.casinoUI.getQuickSpin() ? 1 : 2.2, function(){
                this.slotMach.bigWin.cancel();
                this.stopBigWinMusic();
                TweenMax.delayedCall(68 / 30, function() {
                    if (withMusic && !this.isPlayMusicBigWin && !this.freeSpinsEndPopup.visible) {
                        if (this.freeSpins) {
                            Core.assets.sounds.playMusic('Music_Bonus_Loop', true, {volume : 0.8});
                        } else {
                            Core.assets.sounds.playMusic('Music_BaseGame_Loop', true, {volume : 0.5});
                        }
                    }
                }.bind(this));

                TweenMax.delayedCall(65 / 30, function() {
                    next();
                });
            }.bind(this));
        } else {
            next()
        }
    }

    resetWinPerSpin() {
        this.win = 0;
        this.winbet = null;
        this.bigWinState = 0;
    }

    wrapperSignal(withMusic = true) {
        this.startSpinSignal(withMusic);
        if (this.winbet)
            if (this.winbet.win / this.winbet.bet > 15.0) {
                this.slotMach.bigWin.show();
                if (this.winbet.win / this.winbet.bet >= 30 && this.winbet.win / this.winbet.bet < 50) {
                    this.bigWinState = 1;
                }
                else if (this.winbet.win / this.winbet.bet >= 50 && this.winbet.win / this.winbet.bet < 75) {
                    this.bigWinState = 2;
                }
                else if (this.winbet.win / this.winbet.bet >= 75) {
                    this.bigWinState = 3;
                }
                this.slotMach.bigWin.setWinHeader(this.bigWinState, true);
                this.slotMach.bigWin.cancelCoins();

            }
        //TODO: just check if it can happen at all after refactoring is done
        // gAPI.casinoUI.setCurrentWin(this.winbet.win);
        gAPI.casinoUI.setCurrentWin(this.winbet ? this.winbet.win : -666);

        if (this.freeSpins) {
            this.freeSpinsTotalWin = this.tempFreeSpinsTotal + this.winbet.win;
            gAPI.casinoUI.setFreeSpinsTotalWin(this.freeSpinsTotalWin);

        }
    }

    startSpinSignal(withMusic = true) {
        if (this.bigWin) {
            this.slotMach.bigWin.setWinAmount(this.winbet.win);
        }
        this.cancelAnimations(function(){}, withMusic);
    }

    stopBigWinMusic() {
        Core.assets.sounds.stopSound('Music_BigWin_Loop', { fadeOut: { time: 2.0 } });
        this.isPlayMusicBigWin = false;
    }
}
