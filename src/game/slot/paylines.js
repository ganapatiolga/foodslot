var PIXI = require('pixi.js');
import '../../core';

Math.sign = function(value){ if(value >= 0){ return 1; }else{ return -1; } };

var drawLine = function(points, color, outlineColor){
    if(points.length <= 0)return null;

    var graphics = new PIXI.Graphics();
    graphics.lineStyle(4/Core.scaleFactor, outlineColor, 1);
    graphics.moveTo(points[0].x,points[0].y);

    for(var i = 1; i < points.length; i++)
        graphics.lineTo(points[i].x,points[i].y);

    color = "0xffffc7";  // PAYLINE COLOR OVERRIDE
    var lineWidth = 4 / Core.scaleFactor;
    graphics.lineStyle(lineWidth, color, 1);

    graphics.moveTo(points[0].x,points[0].y);
    for(var i = 1; i < points.length; i++)
        graphics.lineTo(points[i].x,points[i].y);

    // pixi.jsのバージョンアップに伴いpayLineの描画範囲が変わってしまった
    // (0, 0) -> (0, 1)に太さ0, アルファ0のlineを引くことで
    // 原点を明示的にすることで対応
    // よりよい対応が見つかり次第、差し替える
    graphics.lineStyle(0, "0x000000", 0);
    graphics.moveTo(0, 0);
    graphics.lineTo(0, 1);

    var boundsRect = graphics.getBounds();
    var texture = graphics.generateCanvasTexture();

    return {texture:texture, bounds:boundsRect};
};

const offsets = [0,0,0];

class Payline extends PIXI.Container{
    constructor(symbolPositions){
        super();
        this.interactive = false;
        this.visible = false;
        this.linePoints = [];
        this.reels = [];

        for(var reel = 0; reel < symbolPositions.length; reel++)
            this.reels[reel] = symbolPositions[reel];
    }
    createLine(slotPanel, color, outlineColor){
        let yOffset = 0;

        offsets[this.reels[0]] = -Math.sign(offsets[this.reels[0]])*(Math.abs(offsets[this.reels[0]]) + 1);

        var halfSymbolSize = slotPanel.reels[0].symbolSizeWidth/2;
        for (var reel = 0; reel < this.reels.length; reel++) {
            var symbolPos = slotPanel.getSymbolCenterPos(reel,this.reels[reel]);
            this.linePoints.push(new PIXI.Point(symbolPos.x, symbolPos.y + yOffset));
            if (reel === 0){
                this.linePoints.unshift(new PIXI.Point(symbolPos.x-halfSymbolSize, symbolPos.y + yOffset));
            } else if(reel === this.reels.length-1) {
                this.linePoints.push(new PIXI.Point(symbolPos.x+halfSymbolSize, symbolPos.y + yOffset));
            }
        }
        var middleReel = Math.floor((this.reels.length)/2);
        this.centerPos = slotPanel.getSymbolCenterPos(middleReel,this.reels[middleReel]);

        var lineObj = drawLine(this.linePoints, color, outlineColor);
        var texture = lineObj.texture;
        var lineSprite = new PIXI.Sprite(texture);
        lineSprite.position.set(lineObj.bounds.x,lineObj.bounds.y);
        super.addChild(lineSprite);
        this.line = lineSprite;
    }
    playAnimation(time){
        this.line.alpha = 0;
        this.visible = true;
        var payline = this;
        this.animation = new TimelineMax({onComplete: function(){payline.visible = false;}});
        this.animation.to(this.line, time/2, {alpha: 1.0, ease:Expo.easeOut})
             .to(this.line, time/2, {alpha: 0.0, ease:Power2.easeIn});
    }
    cancelAnimation(){
        this.line.alpha = 0;
        this.visible = false;
        if(this.animation)
            this.animation.kill();
    }
}

export class Paylines extends PIXI.Container{
    constructor(config, slotPanel){
        super();
        this.interactive = false;
        this.visible = true;

        this.lines = [];
        for(var line = 0; line < config.paylines.length; line++){
            var payline = new Payline(config.paylines[line]);
            payline.createLine(slotPanel, Core.config.getSettings('config/paylines.json').colors[line], Core.config.getSettings('config/paylines.json').borderColor);
            this.lines[line] = payline;
            super.addChild(payline);
        }
        var mask = new PIXI.Graphics();
        mask.beginFill();
        mask.drawRect(slotPanel.frameOffset, slotPanel.frameOffset, slotPanel.frame.width, slotPanel.frame.height);
        mask.endFill();
        mask.visible = false;
        super.addChild(mask);
        this.sweepingMask = mask;
        this.sweepingMask.x = -this.sweepingMask.graphicsData[0].shape.width;
    }
    animate(time){
        this.sweepingMask.visible = true;
        this.animation = new TimelineMax({onComplete: this.cancelAnimation.bind(this)});
        super.mask = this.sweepingMask;
        this.animation.to(this.sweepingMask, time/2, {x: 0, ease: Expo.easeOut})
             .to(this.sweepingMask, time/2, {x: this.sweepingMask.graphicsData[0].shape.width, ease: Expo.easeIn});
    }
    cancelAnimation(){
        this.sweepingMask.visible = false;
        super.mask = null;
        this.sweepingMask.x = -this.sweepingMask.width;
        if(this.animation)
            this.animation.kill();
    }
    getPayline(paylineId){
        if(paylineId >=  this.lines.length)return null;
        return this.lines[paylineId];
    }
}
