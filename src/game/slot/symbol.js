import '../../core';
var PIXI = require('pixi.js');

var symbolParts = [
    { id: 'symbol', name: 'symbol' },
    { id: 'frame', name: 'symbolFrame' }
];

let ms1 = 11;
let ms2 = 4;

const changeMS = function (MS1, MS2) {
    ms1 = MS1;
    ms2 = MS2;
};

class SymbolGraphics extends PIXI.Container {
    constructor(symbolId, stack = 1) {
        super();
        this.symbolId = symbolId;
        for (var i = 0; i < symbolParts.length; i++) {
            this[symbolParts[i].name] = new PIXI.Sprite();
            super.addChild(this[symbolParts[i].name]);
        }
        this.setFrom(symbolId, stack);
        this.scaleRate = 1 / Core.scaleFactor / 2;
    }

    setFrom(symbolId, stack = 1) {
        var symbolsConf = Core.config.getSettings('config/symbols.json').symbols;
        var symbolConf = symbolsConf[symbolId];
        this.symbolId = symbolId;
        if (stack === 0) {
            this.visible = false;
            return;
        }
        this.visible = true;
        if (stack === 2)
            symbolConf = symbolConf.double;
        if (stack === 3)
            symbolConf = symbolConf.triple;
        if (symbolConf === undefined)
            return;
        for (var i = 0; i < symbolParts.length; i++) {
            var part = symbolParts[i].id;
            var name = symbolParts[i].name;

            if (symbolConf[part] === undefined) {
                this[name].visible = false;
            } else {
                this[name].scale.set(this.scaleRate, this.scaleRate);

                if (symbolParts[i].name !== 'symbolFrame' && symbolParts[i].name !== 'glow') {
                    this[name].texture = Core.assets.getTexture(symbolConf[part]);
                }

                if (symbolParts[i].name !== 'glow') {
                    this[name].visible = true;
                }
            }
        }
    }

    set tint(tint) {
        for (var i = 0; i < symbolParts.length; i++)
            if (this[symbolParts[i].name].visible) {
                if (symbolParts[i].name != 'symbolFrame')
                    this[symbolParts[i].name].mytint = tint;
            }
    }
}

class SlotSymbol extends PIXI.Container {
    constructor(textureId) {
        super();
        this.textureId = textureId;
        this.sprite = new SymbolGraphics(textureId);
        this.scaleRate = 1 / Core.scaleFactor / 2;

        super.addChild(this.sprite);

        var symbolConf = Core.config.getSettings('config/symbols.json').symbols;
        this.blurredSprite = new PIXI.Sprite(Core.assets.getTexture(symbolConf[this.textureId].blurred));
        this.blurredSprite.scale.set(this.scaleRate * 2, this.scaleRate * 2);
        this.blurredSprite.anchor.set(0, 0);
        this.blurredSprite.position.set(this.sprite.width, this.sprite.height);
        this.blurredSprite.visible = false;
        super.addChild(this.blurredSprite);

        this.isBlurred = false;
        this.stackNum = 1;
    }
    get texture() {
        return 0;
    }
    set texture(textureId) {
        let prevTexId = this.textureId;
        this.textureId = textureId;
        if (this.textureId === 13)
            this.textureId = ms1;
        if (this.textureId === 14)
            this.textureId = ms2;
        if (this.textureId === prevTexId)
            return;
        this.updateSpriteTexture();
    }
    set stack(value) {
        if (this.stackNum === value) return;
        this.stackNum = value;
        this.updateSpriteTexture();
    }
    get stack() {
        return this.stackNum;
    }
    blur(value, time) {
        if (this.isBlurred === value) return;
        this.isBlurred = value;
        var sprite = this.sprite;
        var blurredSprite = this.blurredSprite;
        if (this.isBlurred === true) {
            blurredSprite.visible = true;
            blurredSprite.alpha = 0.0;

            TweenMax.to(blurredSprite, time, { alpha: 1.0, ease: Power2.easeOut });
            TweenMax.to(sprite, time, { alpha: 0.0, ease: Power2.easeIn, onComplete: function () { sprite.visible = false; } });
        } else {
            sprite.visible = true;
            sprite.alpha = 0;

            TweenMax.to(blurredSprite, time, { alpha: 0.0, ease: Power2.easeIn, onComplete: function () { blurredSprite.visible = false; } });
            TweenMax.to(sprite, time, { alpha: 1.0, ease: Power2.easeOut });
        }
    }
    updateSpriteTexture() {
        this.sprite.setFrom(this.textureId, this.stackNum);
        var symbolConf = Core.config.getSettings('config/symbols.json').symbols[this.textureId];

        if (this.textureId !== 0 && this.textureId !== 1) {
            this.blurredSprite.scale.set(this.scaleRate * 2, this.scaleRate * 2);
        } else {
            this.blurredSprite.scale.set(this.scaleRate, this.scaleRate);
        }


        if (this.stackNum === 2)
            symbolConf = symbolConf.double;
        if (this.stackNum === 3)
            symbolConf = symbolConf.triple;
        if (symbolConf)
            this.blurredSprite.texture = Core.assets.getTexture(symbolConf.blurred);
    }
    set tint(tint) {
        this.sprite.tint = tint;
    }
}

export { changeMS, SlotSymbol, symbolParts }
