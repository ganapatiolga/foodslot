var PIXI = require('pixi.js');

import '../../core';
import { Paylines } from './paylines';
import { SlotPanel } from './slotPanel';
import { Score } from './score';
import { BigWin } from './bigWin'

var holder = {};


export class SlotMachine {
    constructor(stage, overlayFrame, whatBgToUse, isLoaded = false) {
        let width = 509 / Core.scaleFactor;
        let height = 261 / Core.scaleFactor;

        var ulc = new PIXI.Point(stage.width / 2 - width + 1 / Core.scaleFactor, stage.height * 0.85 - height * 2);

        this.title = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/title/title.json');

        this.slotPanel = new SlotPanel(whatBgToUse);
        this.slotPanel.position.set(ulc.x, ulc.y);
        this.slotPanel.symbolAnimationManager.position.set(ulc.x, ulc.y);
        this.slotPanel.glowLayer.position.set(ulc.x, ulc.y);


        holder.paylines = holder.paylines || new Paylines(Core.config.getSettings('config/paylines.json'), this.slotPanel);

        holder.score = holder.score || new Score(new PIXI.Point(width * 2, height * 2));

        this.paylines = holder.paylines;
        this.paylines.position.set(ulc.x, ulc.y);

        this.score = holder.score;
        this.score.position.set(ulc.x, ulc.y);
        this.score.win = 0;
        this.score.totalWin = 0;

        this.title.width = this.title.width / 2 / Core.scaleFactor;
        this.title.height = this.title.height / 2 / Core.scaleFactor;
        this.title.position.set(stage.width / 2, stage.height / 2 - 231 / Core.scaleFactor - this.title.height);

        // this.score.freeSpinsTotalWin = 0;

        // this.wins = Core.config.getSettings('config/winCombinations.json').wins;

        stage.addChild(this.title);
        stage.addChild(this.slotPanel);
        stage.addChild(this.slotPanel.symbolAnimationManager);
        stage.addChild(this.slotPanel.glowLayer);

        if (isLoaded) {
            this.title.state.setAnimation(0, 'titleA_loop', true);
        } else {
            if (whatBgToUse) {
                this.title.state.setAnimation(0, 'titleA_loop', true);
                TweenMax.delayedCall(3, function(title) {
                    title.setAnimation(0, 'titleAtoB', false);
                    title.addAnimation(0, 'titleB_loop', true);
                }, [this.title.state]);

                this.score.freeSpins = true;
            } else {
                this.title.state.setAnimation(0, 'titleB_loop',true);
                TweenMax.delayedCall(3, function(title) {
                    title.setAnimation(0, 'titleBtoA', false, 3);
                    title.addAnimation(0, 'titleA_loop', true);
                }, [this.title.state]);
            }
        }

        stage.addChild(this.paylines);
        stage.addChild(this.score);

        this.slotMach = this;
        this.bigWin = BigWin(stage);
        this.score.slotMach = this.slotMach;
    }

    unTintPanel() {
        this.slotPanel.tintSymbols(0xFFFFFF);
    }

}
