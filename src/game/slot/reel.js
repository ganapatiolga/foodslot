import { SlotSymbol } from './symbol';
import '../../core';
var EventEmitter = require('eventemitter3');

var PIXI = require('pixi.js');

class SpinAnimation extends EventEmitter {
    constructor(reel) {
        super();
        this.spinTimeline = new TimelineMax({ paused: true });
        this.reelSpeed = 0;
        this.speedScale = 1.0;
        this.reel = reel;
        this.backEaseConfig = 1;
    }
    get speed() {
        return this.reelSpeed * this.speedScale;
    }
    set speed(value) {
        var scale = value / this.reelSpeed;
        this.speedScale = scale;
        this.spinTimeline.timeScale(scale);
    }
    update() {
        this.reel.updateReelPos(this.reelPosition);
    }
    start(startTime) {
        var reel = this.reel;
        reel.animation.clear();
        reel.update();
        reel.lastStopPosition = reel.reelPosition;

        reel.animation.reelPosition = reel.reelOffset;
        var startDist = reel.reelHeight * reel.symbolSize;
        reel.animation.reelSpeed = (reel.animation.backEaseConfig + 3) * (startDist / startTime);

        reel.animation.spinTimeline.to(reel.animation, startTime, {
            reelPosition: "+=" + startDist, ease: Back.easeIn.config(reel.animation.backEaseConfig),
            onUpdate: reel.animation.update.bind(reel.animation),
            onComplete: function () {
                reel.removeResultSymbols();
            }.bind(this)
        });
        var time = reel.totalLength / reel.animation.reelSpeed;
        reel.animation.spinTimeline.to(reel.animation, time, {
            onStart: function () {
                reel.removeResultSymbols();
            }.bind(this),

            reelPosition: "+=" + reel.totalLength, repeat: -1, ease: Linear.easeNone,
            onUpdate: reel.animation.update.bind(reel.animation)
            }, startTime);
        reel.animation.spinTimeline.add(function () {
            reel.setBlur(true, 0.7 * startTime); }, 0.95 * startTime);
        reel.animation.spinning = true;
        reel.animation.spinTimeline.play();
    }
    stop(result, isStopSpin, offset = 0) {
        var reel = this.reel;
        reel.removeResultSymbols();
        Core.clock.once('tick', function ()      {
            var sign = Math.sign(reel.animation.speed);
            var dif = reel.symbolSize - (reel.reelOffset - reel.symbolSize * Math.floor(reel.reelOffset / reel.symbolSize));
            var to = reel.symbolSize * (Math.floor(reel.reelOffset / reel.symbolSize) + 1);
            var delayTime = dif / reel.animation.speed;

            var reelPosition = reel.reelLayout.length - Math.floor((to) / reel.symbolSize);
            for (var i = 0; i < result.length; i++) {
                var pos = Core.utils.mod((reelPosition - sign * reel.reelHeight + 1 + i - offset), reel.reelLayout.length);
                reel.reelLayout[pos] = result[i];
            }
            var reelStopPos = to + sign * reel.symbolSize * (reel.reelHeight + offset);

            reel.animation.reelPosition = reel.reelOffset;
            var endDist = reelStopPos - to;
            var stopTime = endDist / (reel.animation.speed / (reel.animation.backEaseConfig + 3));

            if (!isFinite(stopTime)){
                /*
                *   Sometimes AutoPlay (or something else) can cause reel.animation.speed to be equal zero
                *   In this case stopTime and delay time variable calculation results infinite value.
                *   Running tween with infinite timings cause's stack overflow and game crash.
                *   It should be fixed on higher level to prevent reel.animation.speed to be equal zero.
                *   This is a temp hot fix for time saving.
                *   Backup values are taken from real spin stop situation.
                *
                *   P.S. Reel speed being set to 0 in clear function. TODO: Fix clear function.
                *   console.log("Catch CRASH!!!!");
                *
                * */
                stopTime = 0.5;
                delayTime = 0.041666667;
            }

            reel.animation.clear();

            //TODO. Remove this check before releasing the game.
            if(DEBUG){ reel.checkLayoutStack(); }
            

            reel.animation.spinTimeline.to(reel.animation, delayTime, { reelPosition: to, ease: Linear.easeNone, onUpdate: reel.animation.update.bind(reel.animation) });
            reel.animation.spinTimeline.to(reel.animation, stopTime, {
                reelPosition: reelStopPos,
                ease: Back.easeOut.config(reel.animation.backEaseConfig),
                onUpdate: reel.animation.update.bind(reel.animation)
            });
            reel.animation.spinTimeline.add(function () {
                reel.setBlur(false, 0.5 * stopTime); }, delayTime);
            reel.animation.spinTimeline.add( (isStopSpin) ? () => { Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.05 }) }
                                                          : () => { Core.assets.sounds.playSound('SFX_SM_Spin_End', false, { volume: 0.16 }) }, delayTime);
            reel.animation.spinTimeline.add(function () {
                reel.animation.spinning = false;
                reel.animation.clear();
                reel.lastStopPosition = reel.reelPosition
                reel.emit('Stopped');
                }, delayTime + stopTime);
            reel.animation.spinTimeline.play();

            reel.animation.tweenController = new TimelineMax({ paused: true });
            reel.animation.tweenController.set(reel.animation.spinTimeline, { timeScale: 1.0 }, delayTime);
            reel.animation.tweenController.to(reel.animation.spinTimeline, stopTime, { timeScale: 3.0, ease: Power2.easeOut }, delayTime);
            reel.animation.tweenController.play();
        }.bind(this));
    }
    clear() {
        if (this.tweenController) {
            this.tweenController.kill();
        }
        this.spinTimeline.kill();
        this.spinTimeline = new TimelineMax({ paused: true });
        this.spinTimeline.timeScale(1.0);
        this.reelSpeed = 0;
        this.speedScale = 1.0;
    }
}

export class Reel extends PIXI.Container {
    constructor(reelConf) {
        super();
        this.scaleRate = 1 / Core.scaleFactor / 2;
        this.lastStopPosition = -1;
        this.reelPosition = 0;
        this.reelOffset = 0;
        this.symbolsConf = Core.config.getSettings('config/symbols.json');
        this.symbolSizeWidth = Core.assets.getTexture(this.symbolsConf.symbols[0].symbol).width * this.scaleRate;
        this.symbolSize = Core.assets.getTexture(this.symbolsConf.symbols[0].symbol).height * this.scaleRate;

        this.reelConf = reelConf;
        this.reelHeight = 3;
        this.initReels(reelConf);
        this.frame = { width: this.width, height: this.height - this.symbolSize };

        this.animation = new SpinAnimation(this);

    }
    initReels(reelConf) {
        this.reelLayout = [];
        this.reelLayoutStack = [];
        this.totalLength = reelConf.length * this.symbolSize;
        for (var i = 0; i < reelConf.length; i++) {
            this.reelLayout[i] = reelConf[i];
            this.reelLayoutStack[i] = 1;
        }

        this.symbols = [];
        for (var i = 0; i < this.reelHeight + 1; i++) {
            var symbol = new SlotSymbol(this.reelLayout[0]);
            this.symbols[i] = symbol;
            symbol.position.set(0, this.symbolSize * i);
            super.addChild(symbol);
        }
        this.setSymbols();
        for (var i = 0; i < this.symbols.length; i++) {
            this.symbols[i].updateSpriteTexture();
        }
    }
    setBlur(blur, time) {
        for (var i = 0; i < this.symbols.length; i++)
            this.symbols[i].blur(blur, time)
    }
    setSymbols() {
        var reelOffset = Core.utils.mod(this.reelOffset, this.symbolSize);
        var reelPosition = this.reelPosition;
        for (var i = 0; i < this.symbols.length; i++) {
            this.symbols[i].y = this.symbolSize * (i - 1) + reelOffset+2;
            var pos = Core.utils.mod((reelPosition + i), this.reelLayout.length);
            this.symbols[i].stack = this.reelLayoutStack[pos];
            this.symbols[i].texture = this.reelLayout[pos];
        }
    }
    update() {
        this.reelOffset = Core.utils.mod(this.reelOffset, this.totalLength);
        this.reelPosition = this.reelLayout.length - Math.floor(this.reelOffset / this.symbolSize);

        this.setSymbols();
    }
    updateReelPos(reelPos) {
        this.reelOffset = reelPos;
        this.update();
    }
    removeResultSymbols() {
        if(this.lastStopPosition < 0) return
        for (var i = 0; i < this.reelHeight; i++) {
            let symbolPosition = Core.utils.mod(this.lastStopPosition + 1 + i, this.reelLayout.length);
            this.reelLayout[symbolPosition] = this.reelConf[symbolPosition];
            this.reelLayoutStack[symbolPosition] = 1;
        }
        this.lastStopPosition = -1;
    }
    checkLayoutStack(){
        for(let stack of this.reelLayoutStack){
            if(stack > 1){
                console.error(`Stack layout problems found. To investigate!!!`)
            }
        }
    }
    setSymbol(symbolId, symbol, stack = 1) {
        var reelPosition = this.reelPosition;
        var symbolPosition = Core.utils.mod(reelPosition + 1 + symbolId, this.reelLayout.length);
        this.reelLayout[symbolPosition] = symbol;
        this.reelLayoutStack[symbolPosition] = stack;
        this.symbols[symbolId + 1].stack = stack;
        this.symbols[symbolId + 1].texture = symbol;
    }
    startSpin(startTime) {
        this.animation.start(startTime);
    }
    stopSpin(result, stopTime, offset = 0, scatter, reelId) {
        let option = {volume: 0.5};
        if(scatter)
            if(scatter === 1 && reelId !== 3 && reelId !== 4 ){
                TweenMax.delayedCall(0.2, function(){ Core.assets.sounds.playSound('Music_BonusSymbol_Hit_01', false, option); })
            }else if(scatter === 2 && reelId !== 4){
                TweenMax.delayedCall(0.2, function(){ Core.assets.sounds.playSound('Music_BonusSymbol_Hit_02', false, option); })
            }else if((scatter === 3) || (scatter === 4)){
                TweenMax.delayedCall(0.2, function(){  Core.assets.sounds.playSound('Music_BonusSymbol_Hit_03', false, option); })
            }
        let isStopSpin = stopTime !== 0.5;
        this.animation.stop(result, isStopSpin);
    }
}
