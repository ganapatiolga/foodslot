//import { buildCorner } from '../slot/AnimatedSymbol';
import { symbolParts } from '../slot/symbol';
import '../../core';
var PIXI = require('pixi.js');

class SymbolAnimation extends PIXI.Container {

    constructor() {
        super();
        this.interactive = false;
        this.interactiveChildren = false;
        this.ws = new Core.AnimatedSprite();
        var mask = new PIXI.Graphics();
        mask.beginFill(0xFF0000);
        mask.drawRect(0, 0, 1, 1);
        mask.position.set(1 / Core.scaleFactor, 1 / Core.scaleFactor);
        mask.endFill();
        this.symbol = new PIXI.Sprite();
        this.addChild(this.symbol);
        this.addChild(this.ws);

        this.addChild(mask);

        this.maskRect = mask;
        this.mask = mask;

        this.soundId = null;
        this.soundDelay = 0;
        this.soundVolume = 0.08

        this.scaleRate = 1 / Core.scaleFactor / 2;
    }

    fromSymbol(symbol) {
        // It will be set to true if animation is being played anyway.
        //Removed because it caused scatter symbol to stack on spinning reel sometimes.
        // this.visible = true;
        this.symbolId = symbol.textureId;
        this.stack = symbol.stack;

        if (this.symbolId === 1) {
            this.symbol.scale.set(this.scaleRate, this.scaleRate);
            this.position.set(symbol.parent.position.x + symbol.position.x, symbol.parent.position.y + symbol.position.y - 332 / Core.scaleFactor);
            this.mask = null;
            this.maskRect.visible = false
        } else {
            this.mask = this.maskRect;
            this.maskRect.visible = true;

            if (symbol.stack === 1) {
                this.mask.scale.set((200 - 2) / Core.scaleFactor, 336 / 2 / Core.scaleFactor - 2 / Core.scaleFactor);
            } else if (symbol.stack === 2) {
                this.mask.scale.set(200 / Core.scaleFactor - 2 / Core.scaleFactor, 336 / 2 * 2 / Core.scaleFactor - 2 / Core.scaleFactor);
            } else if (symbol.stack === 3) {
                this.mask.scale.set(200 / Core.scaleFactor - 2 / Core.scaleFactor, 336 / 2 * 3 / Core.scaleFactor - 2 / Core.scaleFactor);
            }

            this.position.set(symbol.parent.position.x + symbol.position.x, symbol.parent.position.y + symbol.position.y);
        }

        this.ws.position.set(0, 0);

        for (var i = 0; i < symbolParts.length; i++) {

            var symbolsConf = Core.config.getSettings('config/symbols.json').symbols;

            var symbolConf = symbolsConf[this.symbolId];
            var part = symbolParts[i].id;

            if (this.stack === 2)
                symbolConf = symbolConf.double;
            if (this.stack === 3)
                symbolConf = symbolConf.triple;
            if (symbolParts[i].name === "symbol" && symbolConf[part] !== undefined) {
                this.symbol.texture = Core.assets.getTexture(symbolConf['symbol']);
                this.symbol.anchor.set(0.5, 0.5);
                if (this.symbolId === 1 && this.stack === 1) {

                    this.symbol.position.set((200 / Core.scaleFactor) / 2, (this.stack * 168 / Core.scaleFactor) / 2 + 332.5 / Core.scaleFactor);
                    this.symbol.visible = true;
                } else {
                    this.symbol.position.set((200 / Core.scaleFactor) / 2, (this.stack * 168 / Core.scaleFactor) / 2);
                }

            }

            if (this.symbolId === 0) {
                this.ws.position.set(-12.5 / Core.scaleFactor, -67.5 / Core.scaleFactor);

            }
            if (this.symbolId == 1) {
                this.ws.position.set(-63 / Core.scaleFactor, 106 / Core.scaleFactor);
            }
        }
    }

    animate(time, isBigWin, stack = 0) {
        this.cancelAnimation();
        this.visible = true;
        switch (this.symbolId) {
            case 0:
                this.wildAnimation(time);
                break;
            case 1:
                this.bonusAnimation(time);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                this.symbol.visible = true;
                this.symbol.scale.set(this.scaleRate, this.scaleRate);
                break;
            case 8:
                this.yellowGirlAnimation(time, stack);
                break;
            case 9:
                this.blackGirlAnimation(time, stack);
                break;
            case 10:
                this.greenGirlAnimation(time, stack);
                break;
            case 11:
                this.whiteGirlAnimation(time, stack);
                break;
            case 12:
                this.redGirlAnimation(time, stack);
                break;
        }

        this.soundId = isBigWin ? null : this.symbolId > 1 && this.symbolId < 8 ? null : this.soundId;

        if (this.soundId !== null){
            if (this.soundDelay === 0){
                Core.assets.sounds.playSound(this.soundId, false, { volume: this.soundVolume })
            } else {
                TweenMax.delayedCall( this.soundDelay , () => {
                    Core.assets.sounds.playSound(this.soundId, false, { volume: this.soundVolume });
                    this.soundDelay = 0; this.soundVolume = 0.08;
                })
            }
        }
    }

    wildAnimation(time) {
        let images = [
            "symbol_wild_000.png", "symbol_wild_001.png", "symbol_wild_002.png",
            "symbol_wild_003.png", "symbol_wild_004.png", "symbol_wild_005.png",
            "symbol_wild_006.png", "symbol_wild_007.png", "symbol_wild_008.png",
            "symbol_wild_009.png", "symbol_wild_010.png", "symbol_wild_011.png",
            "symbol_wild_012.png", "symbol_wild_013.png", "symbol_wild_014.png",
            "symbol_wild_015.png", "symbol_wild_016.png", "symbol_wild_017.png",
            "symbol_wild_018.png", "symbol_wild_019.png", "symbol_wild_020.png",
            "symbol_wild_021.png", "symbol_wild_022.png", "symbol_wild_023.png",
            "symbol_wild_024.png", "symbol_wild_025.png", "symbol_wild_026.png",
            "symbol_wild_027.png", "symbol_wild_028.png", "symbol_wild_029.png"
        ];

        this.soundId = 'SFX_Symbol_Wild';
        this.soundVolume = 0.1;

        this.ws.textures = images;
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    bonusAnimation(time) {
        let images = [
            "symbol_bonus_00000.png", "symbol_bonus_00001.png", "symbol_bonus_00002.png",
            "symbol_bonus_00003.png", "symbol_bonus_00004.png", "symbol_bonus_00005.png",
            "symbol_bonus_00006.png", "symbol_bonus_00007.png", "symbol_bonus_00008.png",
            "symbol_bonus_00009.png", "symbol_bonus_00010.png", "symbol_bonus_00011.png",
            "symbol_bonus_00012.png", "symbol_bonus_00013.png", "symbol_bonus_00014.png",
            "symbol_bonus_00015.png", "symbol_bonus_00016.png", "symbol_bonus_00017.png",
            "symbol_bonus_00018.png", "symbol_bonus_00019.png", "symbol_bonus_00020.png",
            "symbol_bonus_00021.png", "symbol_bonus_00022.png", "symbol_bonus_00023.png",
            "symbol_bonus_00024.png", "symbol_bonus_00025.png", "symbol_bonus_00026.png",
            "symbol_bonus_00027.png", "symbol_bonus_00028.png", "symbol_bonus_00029.png",
            "symbol_bonus_00030.png", "symbol_bonus_00031.png", "symbol_bonus_00032.png",
            "symbol_bonus_00033.png", "symbol_bonus_00034.png", "symbol_bonus_00035.png",
            "symbol_bonus_00036.png", "symbol_bonus_00037.png", "symbol_bonus_00038.png",
            "symbol_bonus_00039.png", "symbol_bonus_00040.png", "symbol_bonus_00041.png",
            "symbol_bonus_00042.png", "symbol_bonus_00043.png", "symbol_bonus_00044.png",
            "symbol_bonus_00045.png"
        ];

        this.soundId = 'SFX_Symbol_Bonus';
        this.soundDelay = 0.5;
        this.soundVolume = 0.1;

        this.ws.textures = images;
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = 4.0;
        this.ws.play();
    }

    yellowGirlAnimation(time, stack) {
        let images = [
           ["symbol5_single_anm_000.png", "symbol5_single_anm_001.png", "symbol5_single_anm_002.png",
            "symbol5_single_anm_003.png", "symbol5_single_anm_004.png", "symbol5_single_anm_005.png",
            "symbol5_single_anm_006.png", "symbol5_single_anm_007.png", "symbol5_single_anm_008.png",
            "symbol5_single_anm_009.png", "symbol5_single_anm_010.png", "symbol5_single_anm_011.png",
            "symbol5_single_anm_012.png", "symbol5_single_anm_013.png", "symbol5_single_anm_014.png",
            "symbol5_single_anm_015.png", "symbol5_single_anm_016.png", "symbol5_single_anm_017.png",
            "symbol5_single_anm_018.png", "symbol5_single_anm_019.png", "symbol5_single_anm_020.png",
            "symbol5_single_anm_021.png", "symbol5_single_anm_022.png", "symbol5_single_anm_023.png",
            "symbol5_single_anm_024.png", "symbol5_single_anm_025.png", "symbol5_single_anm_026.png",
            "symbol5_single_anm_027.png", "symbol5_single_anm_028.png", "symbol5_single_anm_029.png"],
           ["symbol5_double_anm_000.png", "symbol5_double_anm_001.png", "symbol5_double_anm_002.png",
            "symbol5_double_anm_003.png", "symbol5_double_anm_004.png", "symbol5_double_anm_005.png",
            "symbol5_double_anm_006.png", "symbol5_double_anm_007.png", "symbol5_double_anm_008.png",
            "symbol5_double_anm_009.png", "symbol5_double_anm_010.png", "symbol5_double_anm_011.png",
            "symbol5_double_anm_012.png", "symbol5_double_anm_013.png", "symbol5_double_anm_014.png",
            "symbol5_double_anm_015.png", "symbol5_double_anm_016.png", "symbol5_double_anm_017.png",
            "symbol5_double_anm_018.png", "symbol5_double_anm_019.png", "symbol5_double_anm_020.png",
            "symbol5_double_anm_021.png", "symbol5_double_anm_022.png", "symbol5_double_anm_023.png",
            "symbol5_double_anm_024.png", "symbol5_double_anm_025.png", "symbol5_double_anm_026.png",
            "symbol5_double_anm_027.png", "symbol5_double_anm_028.png", "symbol5_double_anm_029.png"],
           ["symbol5_triple_anm_000.png", "symbol5_triple_anm_001.png", "symbol5_triple_anm_002.png",
            "symbol5_triple_anm_003.png", "symbol5_triple_anm_004.png", "symbol5_triple_anm_005.png",
            "symbol5_triple_anm_006.png", "symbol5_triple_anm_007.png", "symbol5_triple_anm_008.png",
            "symbol5_triple_anm_009.png", "symbol5_triple_anm_010.png", "symbol5_triple_anm_011.png",
            "symbol5_triple_anm_012.png", "symbol5_triple_anm_013.png", "symbol5_triple_anm_014.png",
            "symbol5_triple_anm_015.png", "symbol5_triple_anm_016.png", "symbol5_triple_anm_017.png",
            "symbol5_triple_anm_018.png", "symbol5_triple_anm_019.png", "symbol5_triple_anm_020.png",
            "symbol5_triple_anm_021.png", "symbol5_triple_anm_022.png", "symbol5_triple_anm_023.png",
            "symbol5_triple_anm_024.png", "symbol5_triple_anm_025.png", "symbol5_triple_anm_026.png",
            "symbol5_triple_anm_027.png", "symbol5_triple_anm_028.png", "symbol5_triple_anm_029.png"]
        ];

        this.soundId = 'SFX_Symbol_Dominant';

        this.ws.textures = images[stack];
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    whiteGirlAnimation(time, stack) {
        let images = [
           ["symbol2_single_anm_000.png", "symbol2_single_anm_001.png", "symbol2_single_anm_002.png",
            "symbol2_single_anm_003.png", "symbol2_single_anm_004.png", "symbol2_single_anm_005.png",
            "symbol2_single_anm_006.png", "symbol2_single_anm_007.png", "symbol2_single_anm_008.png",
            "symbol2_single_anm_009.png", "symbol2_single_anm_010.png", "symbol2_single_anm_011.png",
            "symbol2_single_anm_012.png", "symbol2_single_anm_013.png", "symbol2_single_anm_014.png",
            "symbol2_single_anm_015.png", "symbol2_single_anm_016.png", "symbol2_single_anm_017.png",
            "symbol2_single_anm_018.png", "symbol2_single_anm_019.png", "symbol2_single_anm_020.png",
            "symbol2_single_anm_021.png", "symbol2_single_anm_022.png", "symbol2_single_anm_023.png",
            "symbol2_single_anm_024.png", "symbol2_single_anm_025.png", "symbol2_single_anm_026.png",
            "symbol2_single_anm_027.png", "symbol2_single_anm_028.png", "symbol2_single_anm_029.png"],
           ["symbol2_double_anm_000.png", "symbol2_double_anm_001.png", "symbol2_double_anm_002.png",
            "symbol2_double_anm_003.png", "symbol2_double_anm_004.png", "symbol2_double_anm_005.png",
            "symbol2_double_anm_006.png", "symbol2_double_anm_007.png", "symbol2_double_anm_008.png",
            "symbol2_double_anm_009.png", "symbol2_double_anm_010.png", "symbol2_double_anm_011.png",
            "symbol2_double_anm_012.png", "symbol2_double_anm_013.png", "symbol2_double_anm_014.png",
            "symbol2_double_anm_015.png", "symbol2_double_anm_016.png", "symbol2_double_anm_017.png",
            "symbol2_double_anm_018.png", "symbol2_double_anm_019.png", "symbol2_double_anm_020.png",
            "symbol2_double_anm_021.png", "symbol2_double_anm_022.png", "symbol2_double_anm_023.png",
            "symbol2_double_anm_024.png", "symbol2_double_anm_025.png", "symbol2_double_anm_026.png",
            "symbol2_double_anm_027.png", "symbol2_double_anm_028.png", "symbol2_double_anm_029.png"],
           ["symbol2_triple_anm_000.png", "symbol2_triple_anm_001.png", "symbol2_triple_anm_002.png",
            "symbol2_triple_anm_003.png", "symbol2_triple_anm_004.png", "symbol2_triple_anm_005.png",
            "symbol2_triple_anm_006.png", "symbol2_triple_anm_007.png", "symbol2_triple_anm_008.png",
            "symbol2_triple_anm_009.png", "symbol2_triple_anm_010.png", "symbol2_triple_anm_011.png",
            "symbol2_triple_anm_012.png", "symbol2_triple_anm_013.png", "symbol2_triple_anm_014.png",
            "symbol2_triple_anm_015.png", "symbol2_triple_anm_016.png", "symbol2_triple_anm_017.png",
            "symbol2_triple_anm_018.png", "symbol2_triple_anm_019.png", "symbol2_triple_anm_020.png",
            "symbol2_triple_anm_021.png", "symbol2_triple_anm_022.png", "symbol2_triple_anm_023.png",
            "symbol2_triple_anm_024.png", "symbol2_triple_anm_025.png", "symbol2_triple_anm_026.png",
            "symbol2_triple_anm_027.png", "symbol2_triple_anm_028.png", "symbol2_triple_anm_029.png"]
        ];

        this.soundId = 'SFX_Symbol_Dominant';

        this.ws.textures = images[stack];
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    greenGirlAnimation(time, stack) {
        let images = [
           ["symbol3_single_anm_000.png", "symbol3_single_anm_001.png", "symbol3_single_anm_002.png",
            "symbol3_single_anm_003.png", "symbol3_single_anm_004.png", "symbol3_single_anm_005.png",
            "symbol3_single_anm_006.png", "symbol3_single_anm_007.png", "symbol3_single_anm_008.png",
            "symbol3_single_anm_009.png", "symbol3_single_anm_010.png", "symbol3_single_anm_011.png",
            "symbol3_single_anm_012.png", "symbol3_single_anm_013.png", "symbol3_single_anm_014.png",
            "symbol3_single_anm_015.png", "symbol3_single_anm_016.png", "symbol3_single_anm_017.png",
            "symbol3_single_anm_018.png", "symbol3_single_anm_019.png", "symbol3_single_anm_020.png",
            "symbol3_single_anm_021.png", "symbol3_single_anm_022.png", "symbol3_single_anm_023.png",
            "symbol3_single_anm_024.png", "symbol3_single_anm_025.png", "symbol3_single_anm_026.png",
            "symbol3_single_anm_027.png", "symbol3_single_anm_028.png", "symbol3_single_anm_029.png"],
           ["symbol3_double_anm_000.png", "symbol3_double_anm_001.png", "symbol3_double_anm_002.png",
            "symbol3_double_anm_003.png", "symbol3_double_anm_004.png", "symbol3_double_anm_005.png",
            "symbol3_double_anm_006.png", "symbol3_double_anm_007.png", "symbol3_double_anm_008.png",
            "symbol3_double_anm_009.png", "symbol3_double_anm_010.png", "symbol3_double_anm_011.png",
            "symbol3_double_anm_012.png", "symbol3_double_anm_013.png", "symbol3_double_anm_014.png",
            "symbol3_double_anm_015.png", "symbol3_double_anm_016.png", "symbol3_double_anm_017.png",
            "symbol3_double_anm_018.png", "symbol3_double_anm_019.png", "symbol3_double_anm_020.png",
            "symbol3_double_anm_021.png", "symbol3_double_anm_022.png", "symbol3_double_anm_023.png",
            "symbol3_double_anm_024.png", "symbol3_double_anm_025.png", "symbol3_double_anm_026.png",
            "symbol3_double_anm_027.png", "symbol3_double_anm_028.png", "symbol3_double_anm_029.png"],
           ["symbol3_triple_anm_000.png", "symbol3_triple_anm_001.png", "symbol3_triple_anm_002.png",
            "symbol3_triple_anm_003.png", "symbol3_triple_anm_004.png", "symbol3_triple_anm_005.png",
            "symbol3_triple_anm_006.png", "symbol3_triple_anm_007.png", "symbol3_triple_anm_008.png",
            "symbol3_triple_anm_009.png", "symbol3_triple_anm_010.png", "symbol3_triple_anm_011.png",
            "symbol3_triple_anm_012.png", "symbol3_triple_anm_013.png", "symbol3_triple_anm_014.png",
            "symbol3_triple_anm_015.png", "symbol3_triple_anm_016.png", "symbol3_triple_anm_017.png",
            "symbol3_triple_anm_018.png", "symbol3_triple_anm_019.png", "symbol3_triple_anm_020.png",
            "symbol3_triple_anm_021.png", "symbol3_triple_anm_022.png", "symbol3_triple_anm_023.png",
            "symbol3_triple_anm_024.png", "symbol3_triple_anm_025.png", "symbol3_triple_anm_026.png",
            "symbol3_triple_anm_027.png", "symbol3_triple_anm_028.png", "symbol3_triple_anm_029.png"]
        ];

        this.soundId = 'SFX_Symbol_Dominant';

        this.ws.textures = images[stack];
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    blackGirlAnimation(time, stack) {
        let images = [
           ["symbol4_single_anm_000.png", "symbol4_single_anm_001.png", "symbol4_single_anm_002.png",
            "symbol4_single_anm_003.png", "symbol4_single_anm_004.png", "symbol4_single_anm_005.png",
            "symbol4_single_anm_006.png", "symbol4_single_anm_007.png", "symbol4_single_anm_008.png",
            "symbol4_single_anm_009.png", "symbol4_single_anm_010.png", "symbol4_single_anm_011.png",
            "symbol4_single_anm_012.png", "symbol4_single_anm_013.png", "symbol4_single_anm_014.png",
            "symbol4_single_anm_015.png", "symbol4_single_anm_016.png", "symbol4_single_anm_017.png",
            "symbol4_single_anm_018.png", "symbol4_single_anm_019.png", "symbol4_single_anm_020.png",
            "symbol4_single_anm_021.png", "symbol4_single_anm_022.png", "symbol4_single_anm_023.png",
            "symbol4_single_anm_024.png", "symbol4_single_anm_025.png", "symbol4_single_anm_026.png",
            "symbol4_single_anm_027.png", "symbol4_single_anm_028.png", "symbol4_single_anm_029.png"],
           ["symbol4_double_anm_000.png", "symbol4_double_anm_001.png", "symbol4_double_anm_002.png",
            "symbol4_double_anm_003.png", "symbol4_double_anm_004.png", "symbol4_double_anm_005.png",
            "symbol4_double_anm_006.png", "symbol4_double_anm_007.png", "symbol4_double_anm_008.png",
            "symbol4_double_anm_009.png", "symbol4_double_anm_010.png", "symbol4_double_anm_011.png",
            "symbol4_double_anm_012.png", "symbol4_double_anm_013.png", "symbol4_double_anm_014.png",
            "symbol4_double_anm_015.png", "symbol4_double_anm_016.png", "symbol4_double_anm_017.png",
            "symbol4_double_anm_018.png", "symbol4_double_anm_019.png", "symbol4_double_anm_020.png",
            "symbol4_double_anm_021.png", "symbol4_double_anm_022.png", "symbol4_double_anm_023.png",
            "symbol4_double_anm_024.png", "symbol4_double_anm_025.png", "symbol4_double_anm_026.png",
            "symbol4_double_anm_027.png", "symbol4_double_anm_028.png", "symbol4_double_anm_029.png"],
           ["symbol4_triple_anm_000.png", "symbol4_triple_anm_001.png", "symbol4_triple_anm_002.png",
            "symbol4_triple_anm_003.png", "symbol4_triple_anm_004.png", "symbol4_triple_anm_005.png",
            "symbol4_triple_anm_006.png", "symbol4_triple_anm_007.png", "symbol4_triple_anm_008.png",
            "symbol4_triple_anm_009.png", "symbol4_triple_anm_010.png", "symbol4_triple_anm_011.png",
            "symbol4_triple_anm_012.png", "symbol4_triple_anm_013.png", "symbol4_triple_anm_014.png",
            "symbol4_triple_anm_015.png", "symbol4_triple_anm_016.png", "symbol4_triple_anm_017.png",
            "symbol4_triple_anm_018.png", "symbol4_triple_anm_019.png", "symbol4_triple_anm_020.png",
            "symbol4_triple_anm_021.png", "symbol4_triple_anm_022.png", "symbol4_triple_anm_023.png",
            "symbol4_triple_anm_024.png", "symbol4_triple_anm_025.png", "symbol4_triple_anm_026.png",
            "symbol4_triple_anm_027.png", "symbol4_triple_anm_028.png", "symbol4_triple_anm_029.png"]
        ];

        this.soundId = 'SFX_Symbol_Dominant';

        this.ws.textures = images[stack];
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    redGirlAnimation(time, stack) {
        let images = [
           ["symbol1_single_anm_000.png", "symbol1_single_anm_001.png", "symbol1_single_anm_002.png",
            "symbol1_single_anm_003.png", "symbol1_single_anm_004.png", "symbol1_single_anm_005.png",
            "symbol1_single_anm_006.png", "symbol1_single_anm_007.png", "symbol1_single_anm_008.png",
            "symbol1_single_anm_009.png", "symbol1_single_anm_010.png", "symbol1_single_anm_011.png",
            "symbol1_single_anm_012.png", "symbol1_single_anm_013.png", "symbol1_single_anm_014.png",
            "symbol1_single_anm_015.png", "symbol1_single_anm_016.png", "symbol1_single_anm_017.png",
            "symbol1_single_anm_018.png", "symbol1_single_anm_019.png", "symbol1_single_anm_020.png",
            "symbol1_single_anm_021.png", "symbol1_single_anm_022.png", "symbol1_single_anm_023.png",
            "symbol1_single_anm_024.png", "symbol1_single_anm_025.png", "symbol1_single_anm_026.png",
            "symbol1_single_anm_027.png", "symbol1_single_anm_028.png", "symbol1_single_anm_029.png"],
           ["symbol1_double_anm_000.png", "symbol1_double_anm_001.png", "symbol1_double_anm_002.png",
            "symbol1_double_anm_003.png", "symbol1_double_anm_004.png", "symbol1_double_anm_005.png",
            "symbol1_double_anm_006.png", "symbol1_double_anm_007.png", "symbol1_double_anm_008.png",
            "symbol1_double_anm_009.png", "symbol1_double_anm_010.png", "symbol1_double_anm_011.png",
            "symbol1_double_anm_012.png", "symbol1_double_anm_013.png", "symbol1_double_anm_014.png",
            "symbol1_double_anm_015.png", "symbol1_double_anm_016.png", "symbol1_double_anm_017.png",
            "symbol1_double_anm_018.png", "symbol1_double_anm_019.png", "symbol1_double_anm_020.png",
            "symbol1_double_anm_021.png", "symbol1_double_anm_022.png", "symbol1_double_anm_023.png",
            "symbol1_double_anm_024.png", "symbol1_double_anm_025.png", "symbol1_double_anm_026.png",
            "symbol1_double_anm_027.png", "symbol1_double_anm_028.png", "symbol1_double_anm_029.png"],
           ["symbol1_triple_anm_000.png", "symbol1_triple_anm_001.png", "symbol1_triple_anm_002.png",
            "symbol1_triple_anm_003.png", "symbol1_triple_anm_004.png", "symbol1_triple_anm_005.png",
            "symbol1_triple_anm_006.png", "symbol1_triple_anm_007.png", "symbol1_triple_anm_008.png",
            "symbol1_triple_anm_009.png", "symbol1_triple_anm_010.png", "symbol1_triple_anm_011.png",
            "symbol1_triple_anm_012.png", "symbol1_triple_anm_013.png", "symbol1_triple_anm_014.png",
            "symbol1_triple_anm_015.png", "symbol1_triple_anm_016.png", "symbol1_triple_anm_017.png",
            "symbol1_triple_anm_018.png", "symbol1_triple_anm_019.png", "symbol1_triple_anm_020.png",
            "symbol1_triple_anm_021.png", "symbol1_triple_anm_022.png", "symbol1_triple_anm_023.png",
            "symbol1_triple_anm_024.png", "symbol1_triple_anm_025.png", "symbol1_triple_anm_026.png",
            "symbol1_triple_anm_027.png", "symbol1_triple_anm_028.png", "symbol1_triple_anm_029.png"]
        ];

        this.soundId = 'SFX_Symbol_Dominant';
        this.soundVolume = 0.05;

        this.ws.textures = images[stack];
        this.ws.visible = true;
        this.ws.scale.set(this.scaleRate, this.scaleRate);
        this.ws.animationTime = time;
        this.ws.play();
    }

    cancelAnimation() {
        this.ws.stop();
        this.ws.textures = null;
        this.stack = 1;
        this.visible = false;
        this.alpha = 1.0;
        if (this.soundId) {
            Core.assets.sounds.stopSound(this.soundId, {fadeOut: {time: 1.0}});
        }
    }
}

export { SymbolAnimation }
