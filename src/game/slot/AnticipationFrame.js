var PIXI = require('pixi.js');
import '../../core';

let anticipationFrame = {};
let anticipationSpine = {};

const AnticipationFrame = function(reelNr, results){
    let frame = {};
    if (anticipationSpine[reelNr] === undefined) {
        frame = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/aori/aori_eff.json');
        frame.width = frame.width / 2 / Core.scaleFactor;
        frame.height = frame.height / 2 / Core.scaleFactor;
        frame.position.set(frame.width / 2 + 22 / Core.scaleFactor, frame.height / 2 + 92 / Core.scaleFactor);
        anticipationSpine[reelNr] = frame;
    } else {
        frame = anticipationSpine[reelNr];
    }

    frame.state.setAnimation(0, 'aori_eff01', true);
    frame.state.onComplete = function(trackIndex, loopCount) {
        this.update(15 / 30);
    };

    let globalEl = new PIXI.Container();
    let el = new PIXI.Container();

    el.addChild(frame);

    var dimmingRect = new PIXI.Graphics();

    var width =  200 / Core.scaleFactor;
    var height = 168 / Core.scaleFactor;

    dimmingRect.beginFill();
    dimmingRect.drawRect(width, -10000, 10000, 20000);
    dimmingRect.drawRect(-10000, -10000, 10000 - width * reelNr, 20000);

    for(var i = 0; i < reelNr; ++i) {
        var isNotBonus = true;
        for (var j = 0; j < results[i].length; ++j) {
            if (results[i][j] == 1) {
                dimmingRect.drawRect(-width * (reelNr - i), -10000, width, (10000 + height * j));
                dimmingRect.drawRect(-width * (reelNr - i), height * (j + 1), width, 10000);
                isNotBonus = false;
            }
        }
        if (isNotBonus) {
            dimmingRect.drawRect(-width * (reelNr - i), -10000, width, 20000);
        }
    }

    dimmingRect.drawRect(0, -10000, width, 10000);
    dimmingRect.drawRect(0, height * 3, width, 10000);
    dimmingRect.endFill();
    dimmingRect.alpha = 0.7;

    globalEl.addChild(dimmingRect);
    globalEl.addChild(el);

    anticipationFrame[reelNr] = globalEl;
    return globalEl;
};

export {AnticipationFrame}
