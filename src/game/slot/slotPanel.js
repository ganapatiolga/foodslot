var PIXI = require('pixi.js');
import '../../core';
import { Reel, Reel2 } from './reel';
import { SymbolAnimation } from './SymbolAnimation';
import { AnticipationFrame } from './AnticipationFrame';

import { reelData } from '../model/reelStrips'
import { PaylineGlow } from '../effects/paylineGlow'

class SymbolAnimationManager extends PIXI.Container {
    constructor(reelNum) {
        super();
        this.reels = [];
        this.visible = true;
        for (let r = 0; r < reelNum; r++) {
            let reelSymbols = [];
            for (let j = 0; j < 3; j++) {
                let animSymbol = new SymbolAnimation();
                reelSymbols[j] = animSymbol;
                super.addChild(animSymbol);
            }
            this.reels[r] = reelSymbols;
        } //TODO: Maybe more than 5

    }
    cancelAllAnimations() {
        for (let r = 0; r < this.reels.length; r++) {
            for (let j = 0; j < this.reels[0].length; j++) {
                this.reels[r][j].cancelAnimation();
            }
        }
    }
}

class GlowLayer extends PIXI.Container {
    constructor() {
        super();
        this.glows = [];
        for (var r = 0; r < 5; r++) {
            var glowSymbols = [];
            for (var j = 0; j < 3; j++) {
                var glowSymbol = new PaylineGlow();
                glowSymbols[j] = glowSymbol;
                super.addChild(glowSymbol);
            }
            this.glows[r] = glowSymbols;
        }
    }

    cancelAllAnimations() {
        for (var r = 0; r < this.glows.length; r++) {
            for (var j = 0; j < this.glows[0].length; j++) {
                this.glows[r][j].cancelAnimations();
            }
        }
    }
}

export class SlotPanel extends PIXI.Container {
    constructor(isFreeSpin) {
        super();
        this.frameOffset = 8 / Core.scaleFactor;
        this.reels = [];
        this.initReels({ reels: reelData.getReels(isFreeSpin) });
        this.glowLayer = new GlowLayer();
        this.symbolAnimationManager = new SymbolAnimationManager(this.reels.length);

        this.frame = { width: this.width, height: this.reels[0].frame.height};
        this.lastReelId = null;
        this.lastSymbol = null;

        var mask = new PIXI.Graphics();
        mask.beginFill();
        mask.drawRect(this.frameOffset, this.frameOffset - (Core.scaleFactor === 0.5 ? 1 : 0), this.frame.width, this.frame.height - (Core.scaleFactor === 0.5 ? 1 : 2));
        mask.endFill();
        super.addChild(mask);
        super.mask = mask;
    }
    initReels(reelsConf) {
        this.reels = [];
        var offsetX = 0;
        for (var i = 0; i < reelsConf.reels.length; i++) {
            var reel = new Reel(reelsConf.reels[i]);
            this.reels[i] = reel;
            offsetX += this.frameOffset;
            reel.position.set(offsetX, this.frameOffset - (Core.scaleFactor === 0.5 ? 0 : 2));
            offsetX += 192 / Core.scaleFactor;
            super.addChild(reel);
        }

        for (let i = 0; i < this.reels.length; i++)
            this.reels[i].on('Stopped', function () { this.reelsStopped(i); }.bind(this));
    }

    reelsStopped(reelId) {
        for (let i = 0; i < this.reels[reelId].reelHeight; i++)
            if (this.getSymbol(reelId, i).textureId === 1)//1 is scatter
                this.symbolAnimationManager.reels[reelId][i].fromSymbol(this.getSymbol(reelId, i));

        // for (var i = 0; i < this.reels.length; i++)
        //     if (this.reels[i].animation.spinning)
        //         return;
        if((reelId + 1) < this.reels.length) return;

        this.symbolAnimationManager.reels.forEach(symbols => symbols.forEach(symbol => symbol.visible = false));
        super.emit('ReelsStopped');
    }

    getSymbolCenterPos(reelId, symbolId) {
        var reel = this.reels[reelId];
        var symbol = this.getSymbol(reelId, symbolId);
        var cx = reel.x + symbol.position.x + symbol.width / 2.0;
        var cy = reel.y + symbol.position.y + symbol.height / 2.0;
        return new PIXI.Point(cx, cy);
    }
    getSymbol(reelId, symbolId) {
        let retSymbol = this.reels[reelId].symbols[symbolId + 1];
        if(!retSymbol){
            console.error(`No symbol found!!! reelId = ${reelId}, symbolId = ${symbolId}`);
        }
        return retSymbol
    }

    setTint(tint) {
        for (var reelId = 0; reelId < this.reels.length; reelId++)
            for (var symbolId = 0; symbolId < this.reels[reelId].symbols.length; symbolId++){
                if (this.glowLayer.glows[reelId][symbolId]) {
                    if (tint = 0xffffff) {
                        this.glowLayer.glows[reelId][symbolId].removeDarkenPanel();
                    } else {
                        this.glowLayer.glows[reelId][symbolId].setDarkenPanel();
                    }
                }
            }
    }

    animateSymbol(reelId, symbolId, time, isBigWin) {
        var symbol = this.getSymbol(reelId, symbolId);

        if (symbol.stack === 0) {
                symbol = this.getSymbol(reelId, symbolId - 1);
            if (symbol.stack === 0) {
                symbol = this.getSymbol(reelId, symbolId - 2);
            }
        }

        if (this.lastReelId === null && this.lastSymbol === null) {
            this.symbolAnimationManager.reels[reelId][symbolId].fromSymbol(symbol);
            this.symbolAnimationManager.reels[reelId][symbolId].animate(time, isBigWin, symbol.stack - 1);
        }
        else if (this.lastReelId !== reelId && this.lastSymbol.textureId !== symbol.textureId ||
            this.lastReelId === reelId && this.lastSymbol.textureId !== symbol.textureId ||
            this.lastReelId !== reelId && this.lastSymbol.textureId === symbol.textureId) {

            this.symbolAnimationManager.reels[reelId][symbolId].fromSymbol(symbol);
            this.symbolAnimationManager.reels[reelId][symbolId].animate(time, isBigWin, symbol.stack - 1);
        }
        else if (symbolId !== 0 && this.reels[reelId].symbols[symbolId].textureId !== symbol.textureId) {
            this.symbolAnimationManager.reels[reelId][symbolId].fromSymbol(symbol);
            this.symbolAnimationManager.reels[reelId][symbolId].animate(time, isBigWin, symbol.stack - 1);
        }

        this.lastReelId = reelId;
        this.lastSymbol = symbol;
    }

    animateGlow(reelId, symbolId, time, repeat = true) {
        var symbol = this.getSymbol(reelId, symbolId);
        var offset = symbolId;
        if (symbol.stack === 0) {
            symbol = this.getSymbol(reelId, symbolId - 1);
            offset = symbolId - 1;
            if (symbol.stack === 0) {
                symbol = this.getSymbol(reelId, symbolId - 2);
                offset = symbolId - 2;
            }
        }

        switch(symbol.stack) {
            case 3:
                this.glowLayer.glows[reelId][offset + 2].removeDarkenPanel(); // fall through
            case 2:
                this.glowLayer.glows[reelId][offset + 1].removeDarkenPanel(); // fall through
            case 1:
            case 0:
                this.glowLayer.glows[reelId][offset + 0].removeDarkenPanel();
        }

        if (repeat) {
            this.glowLayer.glows[reelId][symbolId].position.set(symbol.parent.position.x + symbol.position.x, symbol.parent.position.y + symbol.position.y);
            this.glowLayer.glows[reelId][symbolId].setStack(symbol.stack, symbol.textureId);
        }

        this.glowLayer.glows[reelId][symbolId].animate(time);
    }

    tintSymbols(tint = 0x777777) {
        for (let reelId = 0; reelId < this.reels.length; ++reelId) {
            for (let symbolId = 0; symbolId < this.reels[reelId].symbols.length; ++symbolId) {
                if (this.glowLayer.glows[reelId][symbolId]) {
                    if (tint == 0xffffff) {
                        this.glowLayer.glows[reelId][symbolId].removeDarkenPanel();
                    } else {
                        this.glowLayer.glows[reelId][symbolId].setDarkenPanel();
                    }
                    var symbol = this.getSymbol(reelId, symbolId);
                    this.glowLayer.glows[reelId][symbolId].position.set(symbol.parent.position.x + symbol.position.x, symbol.parent.position.y + symbol.position.y);

                }
            }
        }
    }

    cancelAllAnimations() {
        this.symbolAnimationManager.cancelAllAnimations();
        this.glowLayer.cancelAllAnimations();
        for (var reelId = 0; reelId < this.reels.length; reelId++) {
            var reel = this.reels[reelId];
            for (var symbolId = 0; symbolId < reel.symbols.length; symbolId++) {
                if (this.glowLayer.glows[reelId][symbolId]) {
                    this.glowLayer.glows[reelId][symbolId].removeDarkenPanel();
                }
            }
        }
    }

    startSpin(time, delay = 0.1) {

        gAPI.casinoUI.setCurrentWin(0); //Added to fix CBP-420 Value of total win staying until the next reels result

        var startTime = Math.max(time - delay * (this.reels.length - 1), 0.1);
        var slotPanel = this;
        if (this.animation)
            this.animation.kill();
        this.animation = new TimelineMax({ paused: true });
        var timeOffset = 0;
        for (let i = 0; i < this.reels.length; i++) {
            this.animation.add(function () {
                slotPanel.reels[i].startSpin(startTime);
            }, timeOffset);
            timeOffset += delay;
        }
        this.animation.play();
        Core.assets.sounds.stopSound('mega_epic_win', {fadeOut: {time: 0.5}});
        Core.assets.sounds.playSound('SFX_SM_Spin_Loop', true, { volume: 0.1 });
    }

    stopSpin(results, time, delay = [], anticipationReels = {}, scatterHit = {}, cutInMovie = null) {
        if (this.animation) {
            this.animation.kill();
            this.animation.remove();
            delete this.animation;
            this.animation = null
        }
        var slotPanel = this;

        this.animation = new TimelineMax({
                paused: true, onComplete: () => {
                Core.assets.sounds.stopSound('SFX_SM_Spin_Loop');
        Core.assets.sounds.stopSound('SFX_SM_Anticipation', { volume: 1, fadeOut: {time: 1.4} });
    }
    });

        let anticipationAnimations = [];
        let stoppedReel = null;
        this.removeListener('superFast');
        this.once('superFast', () => {
            anticipationAnimations.forEach((it, idx) => {
                if (!(it.progress() > 0)){
                    it.kill()
                }
            });
            this.animation.timeScale(10);
        });

        var timeOffset = (cutInMovie) ? 0 : 0;
        var anticipationReelFrames = {};
        let reduceDelay = 0;

        for (let i = 0; i < this.reels.length; i++) {
            var stopTime = Math.max(time - delay[i] * (this.reels.length - 1), 0.1);

            if (anticipationReels[i]) {
                anticipationReelFrames[i] = AnticipationFrame(i, results);
                anticipationReelFrames[i].visible = true;
                anticipationReelFrames[i].alpha = 0;
                anticipationReelFrames[i].position.set(this.reels[i].x, this.reels[i].y + (Core.scaleFactor === 0.5 ? 1 : 2));

                this.symbolAnimationManager.addChildAt(anticipationReelFrames[i], 0);
                this.symbolAnimationManager.antFrame = anticipationReelFrames[i];

                var anticipationTime = anticipationReels[i].time;
                reduceDelay += delay[i];
                this.reels[i].animation.speed = anticipationReels[i].speed * this.reels[i].animation.speed;

                var reel = this.reels[i];

                let anticipationAnimation = new TimelineMax();
                anticipationAnimation
                    .to(anticipationReelFrames[i], 0.5, { alpha: 1.0, ease: Linear.easeNone })
                    .add(() => { Core.assets.sounds.playSound('SFX_SM_Anticipation'/*, false/*, { volume: anticipationVolume }*/) })
                    .to(anticipationReelFrames[i], 5 / 30, { alpha: 0.0, ease: Linear.easeNone, delay: anticipationTime - reduceDelay })
                    .add(() => { Core.assets.sounds.stopSound('SFX_SM_Anticipation', {fadeOut: {time: 1.4}}) })
                    .add(() => { /*reel.setBlur(false, 0)*/ anticipationReelFrames[i].visible = false; });

                anticipationAnimations.push(anticipationAnimation);
                this.animation.add(anticipationAnimation);

                timeOffset = this.animation.duration() - 0.25;
            }
            this.animation.add( () => {
                stoppedReel = i;
                slotPanel.reels[i].stopSpin(results[i], stopTime, 0, scatterHit[i], i);
            }, timeOffset);
            timeOffset += delay[i];
        }
        this.animation.play();
    }

    expandSymbols(reel, symbols, time, skipAnimation) {
        var symbolId;
        var stackSize = symbols.length;
        for (let s = 0; s < symbols.length; s++) {
            let symbol = this.getSymbol(reel, symbols[s]);
            symbolId = symbol.textureId;
            if (s === 0) {
                this.reels[reel].setSymbol(symbols[s], symbol.textureId, stackSize);
            } else {
                this.reels[reel].setSymbol(symbols[s], symbol.textureId, 0);
            }
        }

        if (skipAnimation) return;

        var pos = this.getSymbolCenterPos(reel, symbols[symbols.length - 1]);
        var expandEffect;

        if ((symbolId >= 8) && (symbolId <= 12)) {
            expandEffect = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/hipay/eff_Hipay_kettei.json');
            expandEffect.visible = true;
            expandEffect.scale.set(1.0 / 2 / Core.scaleFactor, 1.0 / 2 / Core.scaleFactor);
            expandEffect.position.set(pos.x + 100 / Core.scaleFactor, (this.reels[reel].height + this.reels[reel].y) / 2 - 15 / Core.scaleFactor);
            expandEffect.state.setAnimation(0, 'animation', false);

            Core.assets.sounds.playSound('SFX_Symbol_Dominant', false, {volume: 1.0});
        }

        this.symbolAnimationManager.addChild(expandEffect);

        var container = this.symbolAnimationManager;
        expandEffect.state.onEnd = function(trackIndex, loopCount) {
            container.removeChild(expandEffect);
            expandEffect.visible = false;
        };
    }

    setSymbols(result) {
        for (let i = 0; i < result.length; i++)
            for (let j = 0; j < result[i].length; j++) {
                this.reels[i].reelLayout[j + 1] = result[i][j];
                this.reels[i].update()
            }
    }
}
