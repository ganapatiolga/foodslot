var PIXI = require('pixi.js');
import { BlackRect } from '../effects/blackRect';
import { CoinFountain } from '../effects/coinFountain';
import '../../core';

const BigWin = function (parent) {
    BlackRect(parent);

    let texts = Core.config.getSettings('config/text_' + Core.lang + '.json').ingame;

    let el = new PIXI.Container();
    el.name = 'bigWin';
    el.position.set(parent.width / 2, parent.height / 2);
    el.alpha = 0;

    let frontEffect = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/getwin/getwin.json');
    frontEffect.width = frontEffect.width / 2 / Core.scaleFactor;
    frontEffect.height = frontEffect.height / 2 / Core.scaleFactor;

    let backEffect = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/getwin/getwin.json');
    backEffect.width = backEffect.width / 2 / Core.scaleFactor;
    backEffect.height = backEffect.height / 2 / Core.scaleFactor;
    let telop = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/getwin/getwin.json');
    telop.width = telop.width / 2 / Core.scaleFactor;
    telop.height = telop.height / 2 / Core.scaleFactor;

    telop.state.lastAnimation = '';
    el.addChild(backEffect);

    let coins = CoinFountain();
    coins.position.set(0, 0);
    coins.scale.set(1 / Core.scaleFactor / 2, 1 / Core.scaleFactor / 2);
    coins.frequency = 0.05;
    coins.spawnAmount = 5;
    el.addChild(coins);
    el.addChild(frontEffect);
    el.addChild(telop);


    let amount = new PIXI.extras.BitmapText('', { font: { size: 140 / Core.scaleFactor, name: 'goldfont' }, align: 'center' });
    amount.y = -90 / Core.scaleFactor;

    let amountContainer = new PIXI.Container();
    amountContainer.addChild(amount);
    el.addChild(amountContainer);

    parent.addChild(el);

    let tm = new TimelineMax({ paused: true, ease: Power2.easeIn })
        .to(amountContainer.scale, 20 / 30, {x: 0.8, y: 0.8})
        .to(amountContainer.scale, 2 / 30,  {x: 1.5, y: 1.5})
        .to(amountContainer.scale, 2 / 30,  {x: 1.0, y: 1.0})
        .to(amountContainer.scale, 2 / 30,  {x: 1.25, y: 1.25})
        .to(amountContainer.scale, 2 / 30,  {x: 1.0, y: 1.0})
        .to(amountContainer.scale, 3 / 30,  {x: 1.05, y: 1.05})
        .to(amountContainer.scale, 3 / 30,  {x: 1.0, y: 1.0});

    let currentState = 0;
    let fadeOutTween = null;
    let anim = null;
    let isPlaying = null;

    Object.assign(el, {
        show: function () {
            isPlaying = true;
            parent.darken(true);
            coins.start();
            if (fadeOutTween) fadeOutTween.kill();
            fadeOutTween = TweenMax.to(el, 0.25, { alpha: 1.0 })
        },
        cancel: function () {
            let prevAmount = amount.text;
            TweenMax.delayedCall(0.1, function() {
                if (amount.text !== prevAmount) return;
                if (!isPlaying) {
                    return;
                }

                if (telop.state.lastAnimation)
                    telop.state.setAnimation(0, telop.state.lastAnimation + '_kakutei', false);
                frontEffect.visible = false;
                frontEffect.state.setEmptyAnimation(0, 0);

                TweenMax.delayedCall(22 / 30, function() {
                    frontEffect.visible = true;
                    frontEffect.state.setAnimation(0, 'eff_kakutei', false);
                });
                frontEffect.state.onComplete = '';

                amountContainer.scale.set(1.0, 1.0);
                amount.updateText();
                tm.restart();

                TweenMax.delayedCall(45 / 30, function() {
                    fadeOutTween = TweenMax.to(el, 20 / 60, {alpha:0.0, onComplete: function() {
                        tm.stop();
                    }
                    });
                });
                TweenMax.delayedCall(65 / 30, function() {
                    coins.stop();
                    parent.darken(false);

                    if (fadeOutTween) fadeOutTween.kill();
                    isPlaying = false;

                    fadeOutTween = TweenMax.to(el, 0.5, {alpha: 0.0, onComplete: function () {
                            currentState = 0;
                            tm.stop()
                        }
                    })
                });
            });
        },
        cancelCoins: function () {
            let prevAmount = amount.text;
            TweenMax.delayedCall(0.1, function() {
                if (amount.text !== prevAmount) return;
                coins.stop();
            });
        },
        setWinHeader: function(state, cancel = false){
            if (state === 0 && currentState === 0 || state === 0 && currentState !== 1 && cancel){
                if (anim) anim.kill();

                frontEffect.visible = true;

                frontEffect.state.addAnimation(0, 'eff_in', false, 0);
                frontEffect.state.onComplete = function(truckIndex, loopCount) {
                    frontEffect.visible = false;
                    frontEffect.state.onComplete = '';
                };

                backEffect.state.addAnimation(0, 'eff_loop', true, 1 / 30 * 5);
                telop.state.addAnimation(0, 'txt_bigwin_in', true, 1 / 30 * 5);
                telop.state.lastAnimation = 'txt_bigwin';
                telop.state.onComplete = function(trackIndex, loopCount) {
                    telop.update(44 / 30);
                };

                currentState = 1;
            } else if (state === 1 && currentState === 1 || state === 1 && currentState !== 2 && cancel){
                if(anim) anim.kill();
                Core.assets.sounds.stopSound('mega_epic_win');
                Core.assets.sounds.playSound('mega_epic_win', false);

                telop.state.setAnimation(0, 'txt_megawin_in', true);
                telop.state.lastAnimation = 'txt_megawin';
                telop.state.onComplete = function(trackIndex, loopCount) {
                    telop.update(44 / 30);
                }
                currentState = 2;
            } else if (state === 2 && currentState === 2 || state === 2 && currentState !== 3 && cancel){
                if (anim) anim.kill();
                Core.assets.sounds.stopSound('mega_epic_win');
                Core.assets.sounds.playSound('mega_epic_win', false);

                telop.state.setAnimation(0, 'txt_supermegawin_in', true);
                telop.state.lastAnimation = 'txt_supermegawin';
                telop.state.onComplete = function(trackIndex, loopCount) {
                    telop.update(44 / 30);
                }

                currentState = 3;
            } else if (state === 3 && currentState === 3 || state === 3 && currentState !== 4 && cancel){
                if (anim) anim.kill();
                Core.assets.sounds.stopSound('mega_epic_win');
                Core.assets.sounds.playSound('mega_epic_win', false);

                telop.state.setAnimation(0, 'txt_epicwin_in', true);
                telop.state.lastAnimation = 'txt_epicwin';
                telop.state.onComplete = function(trackIndex, loopCount) {
                    telop.update(44 / 30);
                }

                currentState = 4;
            }
        },

        setWinAmount: function (value) {
            let previousValue = amount.text.length;
            amount.text = gAPI.casinoUI.formatCurrency(value);
            if (previousValue !== amount.text.length)
                amount.x = (-95 - (amount.text.length - 2) * 30) / Core.scaleFactor;
        }
    });

    return el;
};

export { BigWin }
