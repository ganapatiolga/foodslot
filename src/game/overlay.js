import '../core';

export class Overlay {
    constructor(overlay){
        this.overlay = overlay;
    }
    init(overlayFrame){
    }
    swipeOn(time){
        Core.assets.sounds.playSound('SFX_Animation_CloudMove_In', false);

        this.setSwipe();
        this.swipe.state.setAnimation(0, 'wipe_eff', false);
        this.swipe.state.timeScale = time;
        this.swipe.visible = true;

        this.swipe.state.onComplete = function(trackIndex, loopCount) {
            this.swipe.visible = false;
            this.swipe.state.onComplete = '';
        }.bind(this);
    }
    swipeOff(time){
        Core.assets.sounds.stopSound('SFX_Animation_CloudMove_In');
        Core.assets.sounds.playSound('SFX_Animation_CloudMove_Out', false);
    }

    freeSwipeOn(time, isStart){
        if (!isStart) {
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_Whoosh_End', false, {volume: 1.0});
        }

        this.setSwipe();
        this.swipe.state.setAnimation(0, 'wipe_eff', false);
        this.swipe.state.timeScale = time;
        this.swipe.visible = true;

        this.swipe.state.onComplete = function(trackIndex, loopCount) {
            this.swipe.visible = false;
            this.swipe.state.onComplete = '';
        }.bind(this);
    }
    freeSwipeOff(time, isStart){
        if (isStart) {
            Core.assets.sounds.playSound('SFX_FreeSpinIntro_Whoosh_Start', false, {volume: 1.0});
        }
    }
    playStartBattleAnimation(){
    }

    playWinAnimation(){
    }

    cancelAnimations(){
    }

    setSwipe() {
        this.swipe = Core.assets.animations.getSpineSkeleton('assets/' + Core.scale + '/spine/wipe/wipe.json');

        this.swipe.visible = false;
        this.overlay.addChild(this.swipe);
        this.swipe.x = this.overlay.width / 2;
        this.swipe.y = this.overlay.height / 2;
        this.swipe.scale.set(0.5 / Core.scaleFactor, 0.5 / Core.scaleFactor);
    }
}
