var PIXI = require('pixi.js');

export class Container extends PIXI.Container{
    constructor(parameters){
        super();
        this.interactive = false;
        this.set(parameters);
    }
    set(parameters){
        if(parameters)
        for(var parameter in parameters)
            if(this[parameter] !== undefined){
                this[parameter] = parameters[parameter];
            }else{
                this[parameter] = parameters[parameter];
            }
    }
    getChildByName(name){
        for(var i = 0; i < this.children.length; i++)
            if(this.children[i].name === name)
                return this.children[i];
        return null;
    }
    removeChildByName(name){
        var child = this.getChildByName(name)
        if(child)
        super.removeChild(child);
    }
}