import {Container} from './container';
import {Scene} from './scene';

export class Stage extends Container{
    constructor(){
        super();
        this.currentScene = new Scene('default');
    }
    initFromScene(SceneName, SceneHierarchy){
        this.currentScene = new Scene(SceneName, SceneHierarchy);
        
        if(this.currentScene.stageHierarchy && this.currentScene.stageHierarchy[this.name])
            this.currentScene.apply(this, this.currentScene.stageHierarchy[this.name]);
    }
    get scene(){
        return this.currentScene.name;
    }
}