var PIXI = require('pixi.js');
var display = require('pixi-display');
import {default as SCREEN} from '../screen';
import {default as CONFIG} from '../config';
import {LAYOUT} from '../layout';
import {default as ASSETS} from '../assets';
import {Element} from './element';
import {Stage} from './stage';

export class MainStage extends Stage {
    constructor(){
        super();
        this.name = 'stage';
        this.interactive = true;
        this.visible = true;
        this.displayList = new PIXI.DisplayList();
        this.layer = new PIXI.DisplayGroup(0, true);
        this.upperLayer = new PIXI.DisplayGroup(1, true);
        this.currentLayout = null;
        this.elements = {};
    }
    getElement(name){
        if(!this.elements[name])
            throw(name + ' not found');
        return this.elements[name];
    }
    set layout(layoutName){
        if(!LAYOUT.getLayout(layoutName))return;
        if(this.layout)this.setLayoutElementsVisibility(this.layout, false);
        this.currentLayout = layoutName;
        this.setLayoutElementsVisibility(this.layout, true);
        this.updateLayout();
    }
    get layout(){
        if(!this.currentLayout)return null;
        return LAYOUT.getLayout(this.currentLayout);
    }
    setLayoutElementsVisibility(layout, visibility){
        var elementList = LAYOUT.getViewElementList(layout);
        for(var i = 0; i < elementList.length; i++)
            if(this.elements[elementList[i]])
                this.elements[elementList[i]].visible = visibility;
    }
    updateLayout(){
        if(!this.layout)return;
        this.layout.setSize(SCREEN.width,SCREEN.height);
        LAYOUT.applyAutoLayout(this.layout, this);
        this.layer.update();
    }
    resize(){
        if(SCREEN.width < SCREEN.minWidth || SCREEN.height < SCREEN.minHeight || SCREEN.width < SCREEN.height || SCREEN.width/SCREEN.height >= 3){
            this.visible = false;
        }else{
            this.visible = true;
        }
        this.updateLayout();
    }
    initViews(){
        for(var view in LAYOUT.views){
            var viewLayout = LAYOUT.getLayout(view);
            var elementList = LAYOUT.getViewElementList(viewLayout);
            for(var i = 0; i < elementList.length; i++)
                if(!this.elements[elementList[i]]){
                    var element = new Element(elementList[i]);
                    element.displayGroup = this.layer;
                    this.addChild(element);
                    this.elements[elementList[i]] = element;
                }
            }
    }
    clear(){
        for (var i = this.children.length - 1; i >= 0; i--)	
            this.removeChild(this.children[i]);
        this.displayList = new PIXI.DisplayList();
    }
}