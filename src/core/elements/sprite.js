var PIXI = require('pixi.js');
import { default as assets } from '../assets';

Object.defineProperty(PIXI.Sprite.prototype, 'mytint', {
    get: function () { return this._tint; },
    set: function (value) {
        this._tint = value;
        this._tintRGB = (value >> 16) + (value & 0xff00) + ((value & 0xff) << 16);
        if (this.children) {
            for (let i = 0; i < this.children.length; i++) {
                this.children[i].mytint = value;
            }
        }
    }
});


class Sprite extends PIXI.Sprite {
    constructor(parameters) {
        super();
        this.interactive = false;
        this.textureName = null;
        this.set(parameters);
    }
    set(parameters) {
        if (parameters)
            for (var parameter in parameters)
                if (this[parameter] !== undefined)
                    this[parameter] = parameters[parameter];
    }
    set sprite(textureName) {

        try {
            var texture = assets.getTexture(textureName);
            //if(texture.baseTexture.imageUrl)
            //this.name = texture.baseTexture.imageUrl.match(/([^\/]+)(?=\.\w+$)/)[0] || 'undefined';
            var scaleX = this.scaleX; var scaleY = this.scaleY;
            super.texture = texture;
            super.scale.set(scaleX, scaleY);
            this.textureName = textureName;
            
        } catch (e) {
            throw e;
        }
    }
    get sprite() {
        return this.textureName;
    }
    get scaleX() {
        return super.scale.x;
    }
    get scaleY() {
        return super.scale.y;
    }
    set scaleX(value) {
        super.scale.x = value;
    }
    set scaleY(value) {
        super.scale.y = value;
    }
    get pivotX() {
        return this.anchor.x;
    }
    get pivotY() {
        return this.anchor.y;
    }
    set pivotX(value) {
        this.anchor.x = value;
    }
    set pivotY(value) {
        this.anchor.y = value;
    }
}

class DisplayObjectFactory {
    constructor() {

    }
}

var displayObjectFactory = new DisplayObjectFactory();
export { displayObjectFactory, Sprite };