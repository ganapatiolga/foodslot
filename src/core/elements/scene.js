import {Container} from './container';
import {Sprite} from './sprite';

export class Scene{
    constructor(name, stageHierarchy){
        this.name = name;
        this.stageHierarchy = stageHierarchy;
    }
    apply(root, hierarchy){
        var parameters = hierarchy.parameters;
        if(parameters)
            root.set(parameters);
        
        var elements = hierarchy.elements;
        if(elements){
            if(!root.children)throw 'cannot assign elements to object, which cannot have elements';
            for(var i = root.children.length -1; i >= 0; i--)
                if(root.children[i].name)
                    if(elements[root.children[i].name] === undefined)
                        root.removeChild(root.children[i]);
            for(var elementName in elements){
                var elementData = elements[elementName];
                var element = root.getChildByName(elementName);
                if(!element){
                    if(elementData.parameters.sprite){
                        element = new Sprite(elementData.parameters);
                    }else{
                        element = new Container(elementData.parameters);
                    }
                    root.addChild(element);
                }
                this.apply(element, elementData);
            }
        }
    }
}