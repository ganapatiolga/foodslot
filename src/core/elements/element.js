var PIXI = require('pixi.js');
import {Container} from './container';

export class Element extends Container {
    constructor(name){
        super();
        this.name = name;
        this.frame = {x: 0, y: 0, width: 0, height: 0};
        this.visible = false;
        this.interactive = true;
    }
    setMainSprite(sprite){
        this.clear();
        this.frame.x = sprite.x;
        this.frame.y = sprite.y;
        this.frame.width = sprite.width;
        this.frame.height = sprite.height;
        super.addChild(sprite);
        this.visible = true;
    }
    setMainFrame(x,y,width,height){
        this.clear();
        this.frame.x = x;
        this.frame.y = y;
        this.frame.width = width;
        this.frame.height = height;
        this.visible = true;
    }
    setSize(width, height){
        var scaleX = width/this.width;
        var scaleY = height/this.height;
        this.setScale(scaleX, scaleY);
    }
    setScale(scaleX, scaleY){
        super.scale.set(scaleX, scaleY);
    }
    setPosition(centerX, centerY){
        super.position.set(centerX,centerY);
    }
    get width(){
        return this.frame.width;
    }
    get height(){
        return this.frame.height;
    }
    clear(){
         for (var i = this.children.length - 1; i >= 0; i--)	 
            this.removeChild(this.children[i]);
    }
}