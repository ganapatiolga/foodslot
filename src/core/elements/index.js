export {Container} from './container';
export {Sprite} from './sprite';
export {Element} from './element';
export {Scene} from './scene';
export {MainStage} from './mainStage';
export {Stage} from './stage';
export {AnimatedSprite} from './AnimatedSprite'

/*var ElementType = {
    container: Container,
    sprite: Sprite,
    element: Element, //evfl
    stage?
    dbone/spine animations?
    just animations?
    sound objects?
}

export {ElementType};*/