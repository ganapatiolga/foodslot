 var GreenSock = require('gsap'); 
var PIXI = require('pixi.js');
import {default as ASSETS} from '../assets';

class AnimatedSprite extends PIXI.Sprite {
    constructor(listOfTextures){
        super()
        this.textures = listOfTextures

        this.loop = false;
        this.animationTime = 1.0;
    }

    set textures(value){
         if(!value){
            this.stop()
            this.renderable = false
         }else{
             this.renderable = true
         }
        this.texture = !value ? null : (typeof value[0] === 'string' ? ASSETS.getTexture(value[0]) : value[0])
        this.frames = value;
        this.currentFrame = 0;
    }
    
    updateTexture(){
        if(!this.frames)return;
        let currentFrame = this.frames[Math.floor(this.currentFrame)]
        this.texture = typeof currentFrame === 'string' ? ASSETS.getTexture(currentFrame) : currentFrame;
    }
    
    play(){
        this.stop();
        this.tween = TweenMax.fromTo(this, this.animationTime, {currentFrame: 0}, {
            ease: Linear.easeNone,
            currentFrame: this.frames.length - 1, 
            repeat: this.loop ? -1 : 0,
            onUpdate: this.updateTexture.bind(this)})
    }
    stop(){
        if(this.tween){
            this.tween.kill();
            delete this.tween;
        }
    }
}

export {AnimatedSprite}