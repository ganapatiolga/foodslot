(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["Howler"] = factory();
	else
		root["Howler"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Howler = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _howl = __webpack_require__(2);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HowlerGlobal = function () {

    /**
     * Initialize the global Howler object.
     * @return {Howler}
     */
    function HowlerGlobal() {
        _classCallCheck(this, HowlerGlobal);

        this.init();
    }

    _createClass(HowlerGlobal, [{
        key: 'init',
        value: function init() {
            // Create a global ID counter.
            this._counter = 1000;

            // Internal properties.
            this._codecs = {};
            this._howls = [];
            this._muted = false;
            this._volume = 1;
            this._canPlayEvent = 'canplaythrough';
            this._navigator = typeof window !== 'undefined' && window.navigator ? window.navigator : null;

            // Public properties.
            this.masterGain = null;
            this.noAudio = false;
            this.autoSuspend = true;
            this.ctx = null;

            // Set to false to disable the auto iOS enabler.
            this.mobileAutoEnable = true;

            // Setup the various state values for global tracking.
            this._setup();

            this.initialized = true;
            return this;
        }

        /**
         * Setup the audio context when available, or switch to HTML5 Audio mode.
         */

    }, {
        key: 'setupAudioContext',
        value: function setupAudioContext() {
            // Check if we are using Web Audio and setup the AudioContext if we are.
            try {
                this.ctx = new (window.AudioContext || window.webkitAudioContext)();
                if (!this.ctx) {
                    error.log('Webaudio is not supported!');
                    this.noAudio = true;
                }
            } catch (e) {
                this.noAudio = true;
            }

            // Check if a webview is being used on iOS8 or earlier (rather than the browser).
            // If it is, disable Web Audio as it causes crashing.
            var iOS = /iP(hone|od|ad)/.test(this._navigator && this._navigator.platform);
            var appVersion = this._navigator && this._navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
            var version = appVersion ? parseInt(appVersion[1], 10) : null;

            if (iOS && version && version < 9) {
                console.warn('iOS8 or earlier web audio can cause crashes');
                // this.noAudio = true;
            }

            // Create and expose the master GainNode when using Web Audio (useful for plugins or advanced usage).
            this.masterGain = typeof this.ctx.createGain === 'undefined' ? this.ctx.createGainNode() : this.ctx.createGain();
            var vol = this._muted ? 0 : 1;
            this.masterGain.gain.setValueAtTime(vol, this.ctx.currentTime);
            this.masterGain.connect(this.ctx.destination);

            // Re-run the setup on Howler.
            this._setup();
        }
        /**
         * Create Howl factory function.
         * @param  {Object} Options.
         * @return {Howl} Returns new instance of Howl.
         */

    }, {
        key: 'createHowl',
        value: function createHowl(options) {
            var howl = new _howl.Howl(options);
            return howl;
        }

        /**
         * Get/set the global volume for all sounds.
         * @param  {Float} vol Volume from 0.0 to 1.0.
         * @return {Howler/Float}     Returns this or current volume.
         */

    }, {
        key: 'volume',
        value: function volume(vol) {
            vol = parseFloat(vol);

            // If we don't have an AudioContext created yet, run the setup.
            if (!this.ctx) {
                this.setupAudioContext();
            }

            if (typeof vol !== 'undefined' && vol >= 0 && vol <= 1) {
                this._volume = vol;

                // Don't update any of the nodes if we are muted.
                if (this._muted) {
                    return this;
                }

                // When using Web Audio, we just need to adjust the master gain.
                this.masterGain.gain.setValueAtTime(vol, this.ctx.currentTime);
                return this;
            }

            return this._volume;
        }

        /**
         * Handle muting and unmuting globally.
         * @param  {Boolean} muted Is muted or not.
         */

    }, {
        key: 'mute',
        value: function mute(muted) {
            // If we don't have an AudioContext created yet, run the setup.
            if (!this.ctx) {
                this.setupAudioContext();
            }
            this._muted = muted;

            // With Web Audio, we just need to mute the master gain.
            var vol = muted ? 0 : this._volume;
            this.masterGain.gain.setValueAtTime(vol, this.ctx.currentTime);
            return this;
        }

        /**
         * Unload and destroy all currently loaded Howl objects.
         * @return {Howler}
         */

    }, {
        key: 'unload',
        value: function unload() {
            for (var i = this._howls.length - 1; i >= 0; i--) {
                this._howls[i].unload();
            }

            // Create a new AudioContext to make sure it is fully reset.
            if (this.ctx && typeof this.ctx.close !== 'undefined') {
                this.ctx.close();
                this.ctx = null;
                this.setupAudioContext();
            }
            return this;
        }

        /**
         * Check for codec support of specific extension.
         * @param  {String} ext Audio file extention.
         * @return {Boolean}
         */

    }, {
        key: 'codecs',
        value: function codecs(ext) {
            return (this || Howler)._codecs[ext.replace(/^x-/, '')];
        }

        /**
         * Setup various state values for global tracking.
         * @return {Howler}
         */

    }, {
        key: '_setup',
        value: function _setup() {
            // Keeps track of the suspend/resume state of the AudioContext.
            this.state = this.ctx ? this.ctx.state || 'running' : 'running';

            // Automatically begin the 30-second suspend process
            this._autoSuspend();

            // Test to make sure audio isn't disabled in Internet Explorer.
            try {
                var test = new Audio();
                if (test.muted) {
                    this.noAudio = true;
                }
            } catch (e) {}

            // Check for supported codecs.
            if (!this.noAudio) {
                this._setupCodecs();
            }

            return this;
        }

        /**
         * Check for browser support for various codecs and cache the results.
         * @return {Howler}
         */

    }, {
        key: '_setupCodecs',
        value: function _setupCodecs() {
            var audioTest = null;

            // Must wrap in a try/catch because IE11 in server mode throws an error.
            try {
                audioTest = typeof Audio !== 'undefined' ? new Audio() : null;
            } catch (err) {
                return this;
            }

            if (!audioTest || typeof audioTest.canPlayType !== 'function') {
                return this;
            }

            var mpegTest = audioTest.canPlayType('audio/mpeg;').replace(/^no$/, '');

            // Opera version <33 has mixed MP3 support, so we need to check for and block it.
            var checkOpera = this._navigator && this._navigator.userAgent.match(/OPR\/([0-6].)/g);
            var isOldOpera = checkOpera && parseInt(checkOpera[0].split('/')[1], 10) < 33;

            this._codecs = {
                mp3: !!(!isOldOpera && (mpegTest || audioTest.canPlayType('audio/mp3;').replace(/^no$/, ''))),
                mpeg: !!mpegTest,
                opus: !!audioTest.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ''),
                ogg: !!audioTest.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
                oga: !!audioTest.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
                wav: !!audioTest.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ''),
                aac: !!audioTest.canPlayType('audio/aac;').replace(/^no$/, ''),
                caf: !!audioTest.canPlayType('audio/x-caf;').replace(/^no$/, ''),
                m4a: !!(audioTest.canPlayType('audio/x-m4a;') || audioTest.canPlayType('audio/m4a;') || audioTest.canPlayType('audio/aac;')).replace(/^no$/, ''),
                mp4: !!(audioTest.canPlayType('audio/x-mp4;') || audioTest.canPlayType('audio/mp4;') || audioTest.canPlayType('audio/aac;')).replace(/^no$/, ''),
                weba: !!audioTest.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ''),
                webm: !!audioTest.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ''),
                dolby: !!audioTest.canPlayType('audio/mp4; codecs="ec-3"').replace(/^no$/, ''),
                flac: !!(audioTest.canPlayType('audio/x-flac;') || audioTest.canPlayType('audio/flac;')).replace(/^no$/, '')
            };

            return this;
        }

        /**
         * Mobile browsers will only allow audio to be played after a user interaction.
         * Attempt to automatically unlock audio on the first user interaction.
         * Concept from: http://paulbakaus.com/tutorials/html5/web-audio-on-ios/
         * @return {Howler}
         */

    }, {
        key: '_enableMobileAudio',
        value: function _enableMobileAudio() {
            // Only run this on mobile devices if audio isn't already eanbled.
            var isMobile = /iPhone|iPad|iPod|Android|BlackBerry|BB10|Silk|Mobi/i.test(this._navigator && this._navigator.userAgent);
            var isTouch = !!('ontouchend' in window || this._navigator && this._navigator.maxTouchPoints > 0 || this._navigator && this._navigator.msMaxTouchPoints > 0);
            if (this._mobileEnabled || !this.ctx || !isMobile && !isTouch) {
                return;
            }

            this._mobileEnabled = false;

            // Some mobile devices/platforms have distortion issues when opening/closing tabs and/or web views.
            // Bugs in the browser (especially Mobile Safari) can cause the sampleRate to change from 44100 to 48000.
            // By calling Howler.unload(), we create a new AudioContext with the correct sampleRate.
            if (!this._mobileUnloaded && this.ctx.sampleRate !== 44100) {
                this._mobileUnloaded = true;
                this.unload();
            }

            // Scratch buffer for enabling iOS to dispose of web audio buffers correctly, as per:
            // http://stackoverflow.com/questions/24119684
            this._scratchBuffer = this.ctx.createBuffer(1, 1, 22050);

            // Call this method on touch start to create and play a buffer,
            // then check if the audio actually played to determine if
            // audio has now been unlocked on iOS, Android, etc.
            var unlock = function () {
                // Fix Android can not play in suspend state.
                this._autoResume();

                // Create an empty buffer.
                var source = this.ctx.createBufferSource();
                source.buffer = this._scratchBuffer;
                source.connect(this.ctx.destination);

                // Play the empty buffer.
                if (typeof source.start === 'undefined') {
                    source.noteOn(0);
                } else {
                    source.start(0);
                }

                // Calling resume() on a stack initiated by user gesture is what actually unlocks the audio on Android Chrome >= 55.
                if (typeof this.ctx.resume === 'function') {
                    this.ctx.resume();
                }

                // Setup a timeout to check that we are unlocked on the next event loop.
                source.onended = function () {
                    source.disconnect(0);

                    // Update the unlocked state and prevent this check from happening again.
                    this._mobileEnabled = true;
                    this.mobileAutoEnable = false;

                    // Remove the touch start listener.
                    document.removeEventListener('touchend', unlock, true);
                }.bind(this);
            }.bind(this);

            // Setup a touch start listener to attempt an unlock in.
            document.addEventListener('touchend', unlock, true);

            return this;
        }

        /**
         * Automatically suspend the Web Audio AudioContext after no sound has played for 30 seconds.
         * This saves processing/energy and fixes various browser-specific bugs with audio getting stuck.
         * @return {Howler}
         */

    }, {
        key: '_autoSuspend',
        value: function _autoSuspend() {
            if (!this.autoSuspend || !this.ctx || typeof this.ctx.suspend === 'undefined') {
                return;
            }

            // Check if any sounds are playing.
            for (var i = 0; i < this._howls.length; i++) {
                for (var j = 0; j < this._howls[i]._sounds.length; j++) {
                    if (!this._howls[i]._sounds[j]._paused) {
                        return this;
                    }
                }
            }

            if (this._suspendTimer) {
                clearTimeout(this._suspendTimer);
            }

            // If no sound has played after 30 seconds, suspend the context.
            this._suspendTimer = setTimeout(function () {
                if (!this.autoSuspend) {
                    return;
                }

                this._suspendTimer = null;
                this.state = 'suspending';
                this.ctx.suspend().then(function () {
                    this.state = 'suspended';

                    if (this._resumeAfterSuspend) {
                        delete this._resumeAfterSuspend;
                        this._autoResume();
                    }
                }.bind(this));
            }.bind(this), 30000);

            return this;
        }

        /**
         * Automatically resume the Web Audio AudioContext when a new sound is played.
         * @return {Howler}
         */

    }, {
        key: '_autoResume',
        value: function _autoResume() {
            if (!this.ctx || typeof this.ctx.resume === 'undefined') {
                return;
            }

            /**#Ganalogics if statement
             * After switching tab to some other resource with sounds (safari browser), context state changes to suspended,
             * but Howler.state stays in 'running'
             *
             * TODO: Implement proper state monitoring and handling
             */
            if (this.state === 'running') {
                if (this.ctx && this.ctx.state === 'suspended' || this.ctx.state === 'interrupted') {
                    this.state = 'suspended';
                } else if (this._suspendTimer) {
                    clearTimeout(this._suspendTimer);
                    this._suspendTimer = null;
                    return this;
                }
            }
            if (this.state === 'suspended') {
                /**-------------------------------------------------------------*/
                this.ctx.resume().then(function () {
                    this.state = 'running';

                    // Emit to all Howls that the audio has resumed.
                    for (var i = 0; i < this._howls.length; i++) {
                        this._howls[i]._emit('resume');
                    }
                }.bind(this));

                if (this._suspendTimer) {
                    clearTimeout(this._suspendTimer);
                    this._suspendTimer = null;
                }
            } else if (this.state === 'suspending') {
                this._resumeAfterSuspend = true;
            }

            return this;
        }
    }]);

    return HowlerGlobal;
}();

;

/**
 * Create and export global controller. All contained methods and properties apply
 * to all sounds that are currently playing or will be in the future.
 */
var howlerGlobal = new HowlerGlobal();
exports.Howler = howlerGlobal;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
        value: true
});
exports.Sound = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _howler = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sound = exports.Sound = function () {
        /**
         * Initialize a new Sound object.
         * @return {Sound}
         */
        function Sound(parent) {
                _classCallCheck(this, Sound);

                this.init(parent);
        }

        _createClass(Sound, [{
                key: 'init',
                value: function init(parent) {
                        this._parent = parent;
                        // Setup the default parameters.
                        this._muted = parent._muted;
                        this._loop = parent._loop;
                        this._volume = parent._volume;
                        this._rate = parent._rate;
                        this._seek = 0;
                        this._paused = true;
                        this._ended = true;
                        this._sprite = '__default';

                        // Generate a unique ID for this sound.
                        this._id = ++_howler.Howler._counter;

                        // Add itself to the parent's pool.
                        parent._sounds.push(this);

                        // Create the new node.
                        this.create();

                        return this;
                }

                /**
                 * Create and setup a new sound object, whether HTML5 Audio or Web Audio.
                 * @return {Sound}
                 */

        }, {
                key: 'create',
                value: function create() {
                        var volume = _howler.Howler._muted || this._muted || this._parent._muted ? 0 : this._volume;

                        // Create the gain node for controlling volume (the source will connect to this).
                        this._node = typeof _howler.Howler.ctx.createGain === 'undefined' ? _howler.Howler.ctx.createGainNode() : _howler.Howler.ctx.createGain();
                        this._node.gain.setValueAtTime(volume, _howler.Howler.ctx.currentTime);
                        this._node.paused = true;
                        // this._node.connect(Howler.masterGain);
                        this._node.connect(this._parent.howlGain);

                        return this;
                }

                /**
                 * Reset the parameters of this sound to the original state (for recycle).
                 * @return {Sound}
                 */

        }, {
                key: 'reset',
                value: function reset() {
                        var parent = this._parent;

                        // Reset all of the parameters of this sound.
                        this._muted = parent._muted;
                        this._loop = parent._loop;
                        this._volume = parent._volume;
                        this._rate = parent._rate;
                        this._seek = 0;
                        this._rateSeek = 0;
                        this._paused = true;
                        this._ended = true;
                        this._sprite = '__default';

                        // Generate a new ID so that it isn't confused with the previous sound.
                        this._id = ++_howler.Howler._counter;

                        return this;
                }
        }]);

        return Sound;
}();

;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Howl = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _howler = __webpack_require__(0);

var _sound4 = __webpack_require__(1);

var _loader = __webpack_require__(3);

var _eventEmitter = __webpack_require__(4);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Howl = exports.Howl = function (_EventEmitter) {
    _inherits(Howl, _EventEmitter);

    /**
     * Initialize a new Howl group object.
     * @param  {Object} o Passed in properties for this group.
     * @return {Howl}
     */

    function Howl(o) {
        _classCallCheck(this, Howl);

        //TODO Get rid of init function


        var _this = _possibleConstructorReturn(this, (Howl.__proto__ || Object.getPrototypeOf(Howl)).call(this));

        _this.init(o);

        return _this;
    }

    _createClass(Howl, [{
        key: 'init',
        value: function init(o) {
            // If we don't have an AudioContext created yet, run the setup.
            if (!_howler.Howler.ctx) {
                _howler.Howler.setupAudioContext();
            }

            // Setup user-defined default properties.
            this._autoplay = o.autoplay || false;
            this._format = typeof o.format !== 'string' ? o.format : [o.format];
            this._muted = o.mute || false;
            this._loop = o.loop || false;
            this._pool = o.pool || 5;
            this._preload = typeof o.preload === 'boolean' ? o.preload : true;
            this._rate = o.rate || 1;
            this._sprite = o.sprite || {};
            this._src = typeof o.src !== 'string' ? o.src : [o.src];
            this._volume = o.volume !== undefined ? o.volume : 1;
            this._xhrWithCredentials = o.xhrWithCredentials || false;

            // Setup all other default properties.
            this._duration = 0;
            this._state = 'unloaded';
            this._sounds = [];
            this._endTimers = {};
            this._queue = [];
            this.isPlayDelayed = false;

            // Setup event listeners.
            this._onend = o.onend ? [{ fn: o.onend }] : [];
            this._onfade = o.onfade ? [{ fn: o.onfade }] : [];
            this._onload = o.onload ? [{ fn: o.onload }] : [];
            this._onloaderror = o.onloaderror ? [{ fn: o.onloaderror }] : [];
            this._onplayerror = o.onplayerror ? [{ fn: o.onplayerror }] : [];
            this._onpause = o.onpause ? [{ fn: o.onpause }] : [];
            this._onplay = o.onplay ? [{ fn: o.onplay }] : [];
            this._onstop = o.onstop ? [{ fn: o.onstop }] : [];
            this._onmute = o.onmute ? [{ fn: o.onmute }] : [];
            this._onvolume = o.onvolume ? [{ fn: o.onvolume }] : [];
            this._onrate = o.onrate ? [{ fn: o.onrate }] : [];
            this._onseek = o.onseek ? [{ fn: o.onseek }] : [];
            this._onresume = [];

            // Automatically try to enable audio on iOS.
            if (typeof _howler.Howler.ctx !== 'undefined' && _howler.Howler.ctx && _howler.Howler.mobileAutoEnable) {
                _howler.Howler._enableMobileAudio();
            }

            // Keep track of this Howl group in the global controller.
            _howler.Howler._howls.push(this);

            // If they selected autoplay, add a play event to the load queue.
            if (this._autoplay) {
                this._queue.push({
                    event: 'play',
                    action: function () {
                        this.play();
                    }.bind(this)
                });
            }

            //Code taken from sound class
            //Should be executed after ctx is created, but before soundfile starts to load.

            var volume = _howler.Howler._muted || this._muted ? 0 : this._volume;
            this.howlGain = typeof _howler.Howler.ctx.createGain === 'undefined' ? _howler.Howler.ctx.createGainNode() : _howler.Howler.ctx.createGain();
            this.howlGain.gain.setValueAtTime(volume, _howler.Howler.ctx.currentTime);
            this.howlGain.paused = true;
            this.howlGain.connect(_howler.Howler.masterGain);

            // Load the source file unless otherwise specified.
            if (this._preload) {
                //this.load();
                _loader.Loader.load(this);
            }

            return this;
        }

        /**
         * Play a sound or resume previous playback.
         * @param  {String/Number} sprite   Sprite name for sprite playback or sound id to continue previous.
         * @param  {Boolean} internal Internal Use: true prevents event firing.
         * @return {Number}          Sound ID.
         */

    }, {
        key: 'play',
        value: function play(sprite, internal) {
            var id = null;
            var idWasPassed = false; //# ganalogics line of code
            var pausedSoundsNumber = 0;

            // Determine if a sprite, sound id or nothing was passed
            if (typeof sprite === 'number') {
                id = sprite;
                sprite = null;
                idWasPassed = true; //# ganalogics line of code
            } else if (typeof sprite === 'string' && this._state === 'loaded' && !this._sprite[sprite]) {
                // If the passed sprite doesn't exist, do nothing.
                return null;
            } else if (typeof sprite === 'undefined') {
                console.warn('Play() without id passed will be Deprecated.');
                // Use the default sound sprite (plays the full audio length).
                sprite = '__default';

                // Check if there is a paused sounds that isn't ended.
                // If there is, play that sound. If not, continue as usual.
                for (var i = 0; i < this._sounds.length; i++) {
                    if (this._sounds[i]._paused && !this._sounds[i]._ended) {
                        pausedSoundsNumber++;
                        id = this._sounds[i]._id;
                    }
                }

                if (pausedSoundsNumber === 1) {
                    sprite = null;
                } else {
                    id = null;
                }
            }

            /**----------------------------------------------------------------------------------------------
             #Ganalogics temp(?) hack. Forsing to use unpause function if sound is paused and being resumed without id passed,
             /** */
            /** */if (pausedSoundsNumber > 0 && !idWasPassed) {
                /** */this.unpause();
                /** */return;
                /** */
            }
            /** -------------------------------------------------------------------------------------------*/
            // Get the selected node, or get one from the pool.
            var sound = id ? this._soundById(id) : this._inactiveSound();

            // If the sound doesn't exist, do nothing.
            if (!sound) {
                return null;
            }

            // Select the sprite definition.
            if (id && !sprite) {
                sprite = sound._sprite || '__default';
            }

            // If the sound hasn't loaded, we must wait to get the audio's duration.
            // We also need to wait to make sure we don't run into race conditions with
            // the order of function calls.
            if (this._state !== 'loaded') {
                // Set the sprite value on this sound.
                sound._sprite = sprite;

                // Makr this sounded as not ended in case another sound is played before this one loads.
                sound._ended = false;

                // Add the sound to the queue to be played on load.
                var soundId = sound._id;
                this._queue.push({
                    event: 'play',
                    action: function () {
                        this.play(soundId);
                    }.bind(this)
                });

                return soundId;
            }

            // Don't play the sound if an id was passed and it is already playing.
            if (id && !sound._paused) {
                // Trigger the play event, in order to keep iterating through queue.
                if (!internal) {
                    setTimeout(function () {
                        this._emit('play', sound._id);
                    }.bind(this), 0);
                }

                return sound._id;
            }

            // Make sure the AudioContext isn't suspended, and resume it if it is.
            _howler.Howler._autoResume();

            // Determine how long to play for and where to start playing.
            var seek = Math.max(0, sound._seek > 0 ? sound._seek : this._sprite[sprite][0] / 1000);
            var duration = Math.max(0, (this._sprite[sprite][0] + this._sprite[sprite][1]) / 1000 - seek);
            var timeout = duration * 1000 / Math.abs(sound._rate);

            // Update the parameters of the sound
            sound._paused = false;
            sound._ended = false;
            sound._sprite = sprite;
            sound._seek = seek;
            //added 2 properties to sound
            sound._duration = duration;
            sound._timeout = timeout;

            sound._start = this._sprite[sprite][0] / 1000;
            sound._stop = (this._sprite[sprite][0] + this._sprite[sprite][1]) / 1000;
            sound._loop = !!(sound._loop || this._sprite[sprite][2]);

            if (_howler.Howler.state === 'running') {
                this.playSound(sound, internal);
            } else {
                this.isPlayDelayed = true;
                this.once('resume', this.playSound.bind(this, sound, internal));

                // Cancel the end timer.
                this._clearTimer(sound._id);
            }

            /**---------------------------------------------------------------------------------------------------------
             *  #Ganalogics.
             *  If sound has fade data, resume fade on play()
             *
             /** */if (this._soundById(id) && this._soundById(id)._fadeData) {
                /** */this._resumeFade(id);
                /** */
            }
            /** ------------------------------------------------------------------------------------------------------*/
            return sound._id;
        }
    }, {
        key: 'playSound',
        value: function playSound(sound, internal) {
            var node = sound._node;
            this._refreshBuffer(sound);

            // Setup the playback params.
            var vol = sound._muted || this._muted ? 0 : sound._volume;
            sound._playStart = _howler.Howler.ctx.currentTime;
            this.setWebAudioVolume(sound, vol);
            // Play the sound using the supported method.
            this.playWebAudioSound(sound);

            this.isPlayDelayed = false;
            // Start a new timer if none is present.
            if (sound._timeout !== Infinity) {
                this._endTimers[sound._id] = setTimeout(this._ended.bind(this, sound), sound._timeout);
            }

            if (!internal) {
                setTimeout(function () {
                    this._emit('play', sound._id);
                }.bind(this), 0);
            }
        }

        /**
         * #Ganalogics
         * unpause: function()
         *
         * Original howlers method play, doesn't work, if there are several instances of one sound are playing.
         * Instead of resuming all paused instances it starts playing one instance of a sound from very beginning.
         * This function will continue playback of ALL paused sound.
         * @param  {Number} soundId The sound ID (empty to unpause all in group).
         * @return {Howl}
         */

    }, {
        key: 'unpause',
        value: function unpause(soundId) {
            var id = void 0;
            for (var i = 0; i < this._sounds.length; i++) {
                if (this._sounds[i]._paused && !this._sounds[i]._ended) {
                    id = this._sounds[i]._id;
                    if (soundId && soundId !== id) continue;
                    this.play(id);
                }
            }
            return this;
        }

        /**
         * #Ganalogics
         * _resumeFade: function()
         *
         * Proper unpause handling, if sound was paused during fade
         *
         * @return {Howl}
         */

    }, {
        key: '_resumeFade',
        value: function _resumeFade(id) {
            var sound = this._soundById(id);
            var fadeDuration = sound._fadeData.duration * (sound._fadeData.to - sound._volume) / (sound._fadeData.to - sound._fadeData.from);
            //TODO rework muteFade, which leads fadeDuration to become NaN
            if (isNaN(fadeDuration)) {
                fadeDuration = 0;
            }
            this.fade(sound._volume, sound._fadeData.to, fadeDuration, id);
            return this;
        }

        /** Set of web audio functions moved out of howl methods for easy stubbing in tests
          setWebAudioVolume(sound, vol) {}
        playWebAudioSound(sound){}
        stopWebAudio(sound){}
        fadeWebAudio(sound, to, end){}
        cancelWebAudioFade(sound){}
        setWebAudioLoop(sound){}
        setWeabAudioPlaybackRate(sound, rate){}
          */

    }, {
        key: 'setWebAudioVolume',
        value: function setWebAudioVolume(sound, vol) {
            sound._node.gain.setValueAtTime(vol, _howler.Howler.ctx.currentTime);
        }
    }, {
        key: 'playWebAudioSound',
        value: function playWebAudioSound(sound) {
            if (typeof sound._node.bufferSource.start === 'undefined') {
                sound._loop ? sound._node.bufferSource.noteGrainOn(0, sound._seek, 86400) : sound._node.bufferSource.noteGrainOn(0, sound._seek, sound._duration);
            } else {
                sound._loop ? sound._node.bufferSource.start(0, sound._seek, 86400) : sound._node.bufferSource.start(0, sound._seek, sound._duration);
            }
        }
    }, {
        key: 'stopWebAudio',
        value: function stopWebAudio(sound) {
            if (typeof sound._node.bufferSource.stop === 'undefined') {
                sound._node.bufferSource.noteOff(0);
            } else {
                sound._node.bufferSource.stop(0);
            }
        }
    }, {
        key: 'fadeWebAudio',
        value: function fadeWebAudio(sound, to, end) {
            sound._node.gain.linearRampToValueAtTime(to, end);
        }
    }, {
        key: 'cancelWebAudioFade',
        value: function cancelWebAudioFade(sound) {
            sound._node.gain.cancelScheduledValues(_howler.Howler.ctx.currentTime);
        }
    }, {
        key: 'setWebAudioLoop',
        value: function setWebAudioLoop(sound) {
            sound._node.bufferSource.loopStart = sound._start || 0;
            sound._node.bufferSource.loopEnd = sound._stop;
        }
    }, {
        key: 'setWeabAudioPlaybackRate',
        value: function setWeabAudioPlaybackRate(sound, rate) {
            sound._node.bufferSource.playbackRate.value = rate;
        }

        /**
         * Pause playback and save current position.
         * @param  {Number} id The sound ID (empty to pause all in group).
         * @return {Howl}
         */

    }, {
        key: 'pause',
        value: function pause(id) {
            var _this2 = this;

            // If the sound hasn't loaded, add it to the load queue to pause when capable.
            if (this._state !== 'loaded') {
                this._queue.push({
                    event: 'pause',
                    action: function () {
                        this.pause(id);
                    }.bind(this)
                });

                return this;
            }

            // If no id is passed, get all ID's to be paused.
            var ids = this._getSoundIds(id);

            var _loop = function _loop(i) {
                // Clear the end timer.
                _this2._clearTimer(ids[i]);

                // Get the sound.
                var sound = _this2._soundById(ids[i]);

                _this2._pauseFade(ids[i]);
                if (sound && !sound._paused) {
                    if (sound._node) {
                        var pauseWebAudio = {};
                        pauseWebAudio = function () {
                            // Reset the seek position.
                            sound._seek = this.seek(ids[i]);
                            sound._rateSeek = 0;
                            sound._paused = true;
                            /**---------------------------------------------------------------------------------
                             * #Ganalogics
                             * Override howler's logics, for proper paused fade handling
                             *
                             * Original howler code, that should be overriden for proper paused fade handling
                             // Stop currently running fades.
                             //this._stopFade(ids[i]);
                             *
                             /**-------------------------------------------------------------------------------*/
                            // this._pauseFade(ids[i]);
                            this.stopWebAudio(sound);

                            // Clean up the buffer source.
                            this._cleanBuffer(sound._node);
                            sound._paused = true;
                            if (!arguments[1]) {
                                this._emit('pause', sound ? sound._id : null);
                            }
                        }.bind(_this2);
                        //Make sure the sound has been created
                        if (sound._node.bufferSource) {
                            pauseWebAudio();
                        } else if (_this2._isPlayDelayed) {
                            _this2.once('play', pauseWebAudio);
                        }
                    }
                }
            };

            for (var i = 0; i < ids.length; i++) {
                _loop(i);
            }

            return this;
        }

        /**
         * Stop playback and reset to start.
         * @param  {Number} id The sound ID (empty to stop all in group).
         * @param  {Boolean} internal Internal Use: true prevents event firing.
         * @return {Howl}
         */

    }, {
        key: 'stop',
        value: function stop(id, internal) {
            var _this3 = this;

            // If the sound hasn't loaded, add it to the load queue to stop when capable.
            if (this._state !== 'loaded') {
                this._queue.push({
                    event: 'stop',
                    action: function () {
                        this.stop(id);
                    }.bind(this)
                });

                return this;
            }

            // If no id is passed, get all ID's to be stopped.
            var ids = this._getSoundIds(id);

            var _loop2 = function _loop2(i) {
                // Clear the end timer.
                _this3._clearTimer(ids[i]);

                // Get the sound.
                var sound = _this3._soundById(ids[i]);

                if (sound) {
                    if (sound._node) {
                        var stopWebAudio = function () {
                            // Reset the seek position.
                            sound._seek = sound._start || 0;
                            sound._rateSeek = 0;
                            sound._paused = true;
                            sound._ended = true;

                            // Stop currently running fades.
                            this._stopFade(ids[i]);

                            this.stopWebAudio(sound);
                            // Clean up the buffer source.
                            this._cleanBuffer(sound._node);

                            if (!internal) {
                                this._emit('stop', sound._id);
                            }
                        }.bind(_this3);
                        // Make sure the sound's AudioBufferSourceNode has been created.
                        if (sound._node.bufferSource) {
                            stopWebAudio();
                        } else if (_this3._isPlayDelayed) {
                            _this3.once('play', stopWebAudio);
                        }
                    }
                }
            };

            for (var i = 0; i < ids.length; i++) {
                _loop2(i);
            }

            return this;
        }

        /**
         * Mute/unmute a single sound or all sounds in this Howl group.
         * @param  {Boolean} muted Set to true to mute and false to unmute.
         * @param  {Number} id    The sound ID to update (omit to mute/unmute all).
         * @return {Howl}
         */

    }, {
        key: 'mute',
        value: function mute(muted, id) {
            // If the sound hasn't loaded, add it to the load queue to mute when capable.
            if (this._state !== 'loaded') {
                this._queue.push({
                    event: 'mute',
                    action: function () {
                        this.mute(muted, id);
                    }.bind(this)
                });

                return this;
            }

            // If applying mute/unmute to all sounds, update the group's value.
            if (typeof id === 'undefined') {
                if (typeof muted === 'boolean') {
                    this._muted = muted;
                } else {
                    return this._muted;
                }
            }

            // If no id is passed, get all ID's to be muted.
            var ids = this._getSoundIds(id);

            for (var i = 0; i < ids.length; i++) {
                // Get the sound.
                var _sound = this._soundById(ids[i]);

                if (_sound) {
                    _sound._muted = muted;
                    /** -------------------------------------------------
                     * #Ganalogics
                     /** */if (_sound._fadeData) {
                        /** */this._muteFade(muted, ids[i]);
                        /** */
                    }
                    /**-------------------------------------------------*/
                    if (_sound._node) {
                        var volume = muted ? 0 : _sound._volume;
                        this.setWebAudioVolume(_sound, volume);
                    }
                    //TODO: may be there is a point to move event emitter inside if statement.
                    this._emit('mute', _sound._id);
                }
            }

            return this;
        }

        /**
         * Get/set the volume of this sound or of the Howl group. This method can optionally take 0, 1 or 2 arguments.
         *   volume() -> Returns the group's volume value.
         *   volume(id) -> Returns the sound id's current volume.
         *   volume(vol) -> Sets the volume of all sounds in this Howl group.
         *   volume(vol, id) -> Sets the volume of passed sound id.
         * @return {Howl/Number} Returns this or current volume.
         */

    }, {
        key: 'volume',
        value: function volume() {
            var args = arguments;
            var vol = void 0,
                id = void 0;

            // Determine the values based on arguments.
            if (args.length === 0) {
                // Return the value of the groups' volume.
                return this._volume;
            } else if (args.length === 1 || args.length === 2 && typeof args[1] === 'undefined') {
                // First check if this is an ID, and if not, assume it is a new volume.
                var ids = this._getSoundIds();
                var index = ids.indexOf(args[0]);
                if (index >= 0) {
                    id = parseInt(args[0], 10);
                } else {
                    vol = parseFloat(args[0]);
                }
            } else if (args.length >= 2) {
                vol = parseFloat(args[0]);
                id = parseInt(args[1], 10);
            }

            // Update the volume or return the current volume.
            var sound = void 0;
            if (typeof vol !== 'undefined' && vol >= 0 && vol <= 1) {
                // If the sound hasn't loaded, add it to the load queue to change volume when capable.
                if (this._state !== 'loaded') {
                    this._queue.push({
                        event: 'volume',
                        action: function () {
                            this.volume.apply(this, args);
                        }.bind(this)
                    });

                    return this;
                }

                // Set the group volume.
                if (typeof id === 'undefined') {
                    this._volume = vol;
                }

                // Update one or all volumes.
                id = this._getSoundIds(id);
                for (var i = 0; i < id.length; i++) {
                    // Get the sound.
                    sound = this._soundById(id[i]);

                    if (sound) {
                        sound._volume = vol;
                        // Stop currently running fades.
                        if (!args[2]) {
                            this._stopFade(id[i]);
                        }

                        if (sound._node && !sound._muted) {
                            this.setWebAudioVolume(sound, vol);
                        }
                        this._emit('volume', sound._id);
                    }
                }
            } else {
                sound = id ? this._soundById(id) : this._sounds[0];
                return sound ? sound._volume : 0;
            }

            return this;
        }
    }, {
        key: 'setVolume',
        value: function setVolume(vol, id) {
            var volume = Math.min(1, Math.max(0, vol));
            if (id) {
                this._soundById(id)._node.gain.setValueAtTime(volume, _howler.Howler.ctx.currentTime);
            } else {
                this.howlGain.gain.setValueAtTime(volume, _howler.Howler.ctx.currentTime);
            }
        }
    }, {
        key: 'getVolume',
        value: function getVolume(id) {
            var vol = 0;
            if (id) {
                vol = this._soundById(id)._node.gain.value;
            } else {
                vol = this.howlGain.gain.value;
            }
            return vol;
        }
        /**
         * Fade a currently playing sound between two volumes (if no id is passsed, all sounds will fade).
         * @param  {Number} from The value to fade from (0.0 to 1.0).
         * @param  {Number} to   The volume to fade to (0.0 to 1.0).
         * @param  {Number} len  Time in milliseconds to fade.
         * @param  {Number} id   The sound id (omit to fade all sounds).
         * @return {Howl}
         */

    }, {
        key: 'fade',
        value: function fade(from, to, len, id) {
            // If the sound hasn't loaded, add it to the load queue to fade when capable.
            if (this._state !== 'loaded') {
                this._queue.push({
                    event: 'fade',
                    action: function () {
                        this.fade(from, to, len, id);
                    }.bind(this)
                });

                return this;
            }

            // Set the volume to the start position.
            this.volume(from, id);

            // Fade the volume of one or all sounds.
            var ids = this._getSoundIds(id);
            for (var i = 0; i < ids.length; i++) {
                // Get the sound.
                var _sound2 = this._soundById(ids[i]);

                // Create a linear fade or fall back to timeouts with HTML5 Audio.
                if (_sound2) {
                    /**-----------------------------------------------------------
                     /** #Ganalogics
                     Adding fade data to sound for fade resume after pause
                       /** */_sound2._fadeData = {
                        /** */from: from,
                        /** */to: to,
                        /** */duration: len
                        /** */
                        /**----------------------------------------------------------*/
                        // Stop the previous fade if no sprite is being used (otherwise, volume handles this).
                    };if (!id || _sound2._fadeData) {
                        this._stopFade(ids[i]);
                    }

                    // HTML5 audio not supported anymore, let the native methods do the actual fade.
                    if (!_sound2._muted) {
                        var currentTime = _howler.Howler.ctx.currentTime;
                        var end = currentTime + len / 1000;
                        _sound2._volume = from;
                        this.setWebAudioVolume(_sound2, from);
                        this.fadeWebAudio(_sound2, to, end);
                    }

                    this._startFadeInterval(_sound2, from, to, len, id);
                }
            }

            return this;
        }

        /**
         * Starts the internal interval to fade a sound.
         * @param  {Object} sound Reference to sound to fade.
         * @param  {Number} from The value to fade from (0.0 to 1.0).
         * @param  {Number} to   The volume to fade to (0.0 to 1.0).
         * @param  {Number} len  Time in milliseconds to fade.
         */

    }, {
        key: '_startFadeInterval',
        value: function _startFadeInterval(sound, from, to, len, id) {
            var vol = from;
            var dir = from > to ? 'out' : 'in';
            var diff = Math.abs(from - to);
            var steps = diff / 0.01;
            var stepLen = steps > 0 ? len / steps : len;

            // Since browsers clamp timeouts to 4ms, we need to clamp our steps to that too.
            if (stepLen < 4) {
                steps = Math.ceil(steps / (4 / stepLen));
                stepLen = 4;
            }

            sound._interval = setInterval(function () {
                // Update the volume amount, but only if the volume should change.
                if (steps > 0) {
                    vol += dir === 'in' ? 0.01 : -0.01;
                }

                // Make sure the volume is in the right bounds.
                vol = Math.max(0, vol);
                vol = Math.min(1, vol);

                // Round to within 2 decimal points.
                vol = Math.round(vol * 100) / 100;

                // Change the volume.
                if (typeof id === 'undefined') {
                    this._volume = vol;
                }
                sound._volume = vol;

                // When the fade is complete, stop it and fire event.
                if (to < from && vol <= to || to > from && vol >= to) {
                    clearInterval(sound._interval);
                    sound._interval = null;
                    //#Ganalogics. Delete sound fade data.
                    /**/sound._fadeData = null;
                    //***********************************
                    this.volume(to, sound._id);
                    this._emit('fade', sound._id);
                }
            }.bind(this), stepLen);
        }

        /**
         * #Ganalogics
         * _muteFade: function()
         *
         * Clone of Internal method '_stopFade'. Does same thing, but...
         *    -doesn't fire fade event
         *    -doesn't delete fade data from sound to be able to resume fade
         *    -doesn't stop internal fading interval, to keep track on current fade volume and
         *        fire fade event after it's being finished during mute
         *
         * @param  {Number} id The sound id.
         * @return {Howl}
         */

    }, {
        key: '_muteFade',
        value: function _muteFade(muted, id) {
            var sound = this._soundById(id);

            if (sound && sound._interval) {
                if (muted) {
                    this.cancelWebAudioFade(sound);
                } else {
                    // If sound is muted while fading, fade process runs in background and HAS to be paused before
                    // fade resume
                    //TODO: Improve existing mute fade implementation
                    this._pauseFade(id);
                    this._resumeFade(id);
                }
            }
            return this;
        }

        /**
         * #Ganalogics
         * _pauseFade: function()
         *
         * Clone of Internal method '_stopFade'. Does same thing, but...
         *    -doesn't fire fade event
         *    -doesn't delete fade data from sound to be able to resume fade
         *
         * @param  {Number} id The sound id.
         * @return {Howl}
         */

    }, {
        key: '_pauseFade',
        value: function _pauseFade(id) {
            var sound = this._soundById(id);

            if (sound && sound._interval) {
                this.cancelWebAudioFade(sound);
                clearInterval(sound._interval);
                sound._interval = null;
            }
            return this;
        }

        /**
         * Internal method that stops the currently playing fade when
         * a new fade starts, volume is changed or the sound is stopped.
         * @param  {Number} id The sound id.
         * @return {Howl}
         */

    }, {
        key: '_stopFade',
        value: function _stopFade(id) {
            var sound = this._soundById(id);

            if (sound && sound._interval) {
                this.cancelWebAudioFade(sound);
                clearInterval(sound._interval);
                sound._interval = null;
                /**------------------------------------------*/
                /**    #Ganalogics. Delete sound fade data.
                 /**/sound._fadeData = null;
                /**------------------------------------------*/
                this._emit('fade', id);
            }

            return this;
        }

        /**
         * Get/set the loop parameter on a sound. This method can optionally take 0, 1 or 2 arguments.
         *   loop() -> Returns the group's loop value.
         *   loop(id) -> Returns the sound id's loop value.
         *   loop(loop) -> Sets the loop value for all sounds in this Howl group.
         *   loop(loop, id) -> Sets the loop value of passed sound id.
         * @return {Howl/Boolean} Returns this or current loop value.
         */

    }, {
        key: 'loop',
        value: function loop() {
            var args = arguments;
            var loop = void 0,
                id = void 0,
                sound = void 0;

            // Determine the values for loop and id.
            if (args.length === 0) {
                // Return the grou's loop value.
                return this._loop;
            } else if (args.length === 1) {
                if (typeof args[0] === 'boolean') {
                    loop = args[0];
                    this._loop = loop;
                } else {
                    // Return this sound's loop value.
                    sound = this._soundById(parseInt(args[0], 10));
                    return sound ? sound._loop : false;
                }
            } else if (args.length === 2) {
                loop = args[0];
                id = parseInt(args[1], 10);
            }

            // If no id is passed, get all ID's to be looped.
            var ids = this._getSoundIds(id);
            for (var i = 0; i < ids.length; i++) {
                sound = this._soundById(ids[i]);

                if (sound && sound._node && sound._node.bufferSource) {
                    sound._loop = loop;
                    sound._node.bufferSource.loop = loop;
                    if (loop) {
                        this.setWebAudioLoop(sound);
                    }
                }
            }
            return this;
        }

        /**
         * Get/set the playback rate of a sound. This method can optionally take 0, 1 or 2 arguments.
         *   rate() -> Returns the first sound node's current playback rate.
         *   rate(id) -> Returns the sound id's current playback rate.
         *   rate(rate) -> Sets the playback rate of all sounds in this Howl group.
         *   rate(rate, id) -> Sets the playback rate of passed sound id.
         * @return {Howl/Number} Returns this or the current playback rate.
         */

    }, {
        key: 'rate',
        value: function rate() {
            var args = arguments;
            var rate = void 0,
                id = void 0;

            // Determine the values based on arguments.
            if (args.length === 0) {
                // We will simply return the current rate of the first node.
                id = this._sounds[0]._id;
            } else if (args.length === 1) {
                // First check if this is an ID, and if not, assume it is a new rate value.
                var ids = this._getSoundIds();
                var index = ids.indexOf(args[0]);
                if (index >= 0) {
                    id = parseInt(args[0], 10);
                } else {
                    rate = parseFloat(args[0]);
                }
            } else if (args.length === 2) {
                rate = parseFloat(args[0]);
                id = parseInt(args[1], 10);
            }

            // Update the playback rate or return the current value.
            var sound = void 0;
            if (typeof rate === 'number') {
                // If the sound hasn't loaded, add it to the load queue to change playback rate when capable.
                if (this._state !== 'loaded') {
                    this._queue.push({
                        event: 'rate',
                        action: function () {
                            this.rate.apply(this, args);
                        }.bind(this)
                    });

                    return this;
                }

                // Set the group rate.
                if (typeof id === 'undefined') {
                    this._rate = rate;
                }

                // Update one or all volumes.
                id = this._getSoundIds(id);
                for (var i = 0; i < id.length; i++) {
                    // Get the sound.
                    sound = this._soundById(id[i]);

                    if (sound) {
                        // Keep track of our position when the rate changed and update the playback
                        // start position so we can properly adjust the seek position for time elapsed.
                        sound._rateSeek = this.seek(id[i]);
                        sound._playStart = _howler.Howler.ctx.currentTime;
                        sound._rate = rate;

                        // Change the playback rate.
                        if (sound._node && sound._node.bufferSource) {
                            this.setWeabAudioPlaybackRate(sound, rate);
                        }

                        // Reset the timers.
                        var seek = this.seek(id[i]);
                        var duration = (this._sprite[sound._sprite][0] + this._sprite[sound._sprite][1]) / 1000 - seek;
                        var timeout = duration * 1000 / Math.abs(sound._rate);

                        // Start a new end timer if sound is already playing.
                        if (this._endTimers[id[i]] || !sound._paused) {
                            this._clearTimer(id[i]);
                            this._endTimers[id[i]] = setTimeout(this._ended.bind(this, sound), timeout);
                        }

                        this._emit('rate', sound._id);
                    }
                }
            } else {
                sound = this._soundById(id);
                return sound ? sound._rate : this._rate;
            }

            return this;
        }

        /**
         * Get/set the seek position of a sound. This method can optionally take 0, 1 or 2 arguments.
         *   seek() -> Returns the first sound node's current seek position.
         *   seek(id) -> Returns the sound id's current seek position.
         *   seek(seek) -> Sets the seek position of the first sound node.
         *   seek(seek, id) -> Sets the seek position of passed sound id.
         * @return {Howl/Number} Returns this or the current seek position.
         */

    }, {
        key: 'seek',
        value: function seek() {
            var args = arguments;
            var seek = void 0,
                id = void 0;

            // Determine the values based on arguments.
            if (args.length === 0) {
                // We will simply return the current position of the first node.
                id = this._sounds[0]._id;
            } else if (args.length === 1) {
                // First check if this is an ID, and if not, assume it is a new seek position.
                var ids = this._getSoundIds();
                var index = ids.indexOf(args[0]);
                if (index >= 0) {
                    id = parseInt(args[0], 10);
                } else if (this._sounds.length) {
                    id = this._sounds[0]._id;
                    seek = parseFloat(args[0]);
                }
            } else if (args.length === 2) {
                seek = parseFloat(args[0]);
                id = parseInt(args[1], 10);
            }

            // If there is no ID, bail out.
            if (typeof id === 'undefined') {
                return this;
            }

            // If the sound hasn't loaded, add it to the load queue to seek when capable.
            if (this._state !== 'loaded') {
                this._queue.push({
                    event: 'seek',
                    action: function () {
                        this.seek.apply(this, args);
                    }.bind(this)
                });

                return this;
            }

            // Get the sound.
            var sound = this._soundById(id);

            if (sound) {
                if (typeof seek === 'number' && seek >= 0) {
                    // Pause the sound and update position for restarting playback.
                    var playing = this.playing(id);
                    if (playing) {
                        this.pause(id, true);
                    }

                    // Move the position of the track and cancel timer.
                    sound._seek = seek;
                    sound._ended = false;
                    this._clearTimer(id);

                    // Restart the playback if the sound was playing.
                    if (playing) {
                        this.play(id, true);
                    }

                    this._emit('seek', id);
                } else {
                    var realTime = this.playing(id) ? _howler.Howler.ctx.currentTime - sound._playStart : 0;
                    var rateSeek = sound._rateSeek ? sound._rateSeek - sound._seek : 0;
                    return sound._seek + (rateSeek + realTime * Math.abs(sound._rate));
                }
            }

            return this;
        }

        /**
         * Check if a specific sound is currently playing or not (if id is provided), or check if at least one of the sounds in the group is playing or not.
         * @param  {Number}  id The sound id to check. If none is passed, the whole sound group is checked.
         * @return {Boolean} True if playing and false if not.
         */

    }, {
        key: 'playing',
        value: function playing(id) {
            // Check the passed sound ID (if any).
            if (typeof id === 'number') {
                var _sound3 = this._soundById(id);
                return _sound3 ? !_sound3._paused : false;
            }

            // Otherwise, loop through all sounds and check if any are playing.
            for (var i = 0; i < this._sounds.length; i++) {
                if (!this._sounds[i]._paused) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Get the duration of this sound. Passing a sound id will return the sprite duration.
         * @param  {Number} id The sound id to check. If none is passed, return full source duration.
         * @return {Number} Audio duration in seconds.
         */

    }, {
        key: 'duration',
        value: function duration(id) {
            var duration = this._duration;

            // If we pass an ID, get the sound and return the sprite length.
            var sound = this._soundById(id);
            if (sound) {
                duration = this._sprite[sound._sprite][1] / 1000;
            }

            return duration;
        }

        /**
         * Returns the current loaded state of this Howl.
         * @return {String} 'unloaded', 'loading', 'loaded'
         */

    }, {
        key: 'state',
        value: function state() {
            return this._state;
        }

        /**
         * Unload and destroy the current Howl object.
         * This will immediately stop all sound instances attached to this group.
         */

    }, {
        key: 'unload',
        value: function unload() {
            // Stop playing any active sounds.
            var sounds = this._sounds;
            for (var i = 0; i < sounds.length; i++) {
                // Stop the sound if it is currently playing.
                if (!sounds[i]._paused) {
                    this.stop(sounds[i]._id);
                }

                // Empty out all of the nodes.
                delete sounds[i]._node;

                // Make sure all timers are cleared out.
                this._clearTimer(sounds[i]._id);

                // Remove the references in the global Howler object.
                var index = _howler.Howler._howls.indexOf(this);
                if (index >= 0) {
                    _howler.Howler._howls.splice(index, 1);
                }
            }

            // Delete this sound from the cache (if no other Howl is using it).
            var remCache = true;
            for (var _i = 0; _i < _howler.Howler._howls.length; _i++) {
                if (_howler.Howler._howls[_i]._src === this._src) {
                    remCache = false;
                    break;
                }
            }

            //TODO: Stupid hack, unload() should be a part of loader
            if (_loader.Loader.cache && remCache) {
                delete _loader.Loader.cache[this._src];
            }

            // Clear global errors.
            _howler.Howler.noAudio = false;

            // Clear out `this`.
            this._state = 'unloaded';
            this._sounds = [];

            //TODO something.
            // this = null;

            return null;
        }

        /**
         * Queue of actions initiated before the sound has loaded.
         * These will be called in sequence, with the next only firing
         * after the previous has finished executing (even if async like play).
         * @return {Howl}
         */

    }, {
        key: '_loadQueue',
        value: function _loadQueue() {
            if (this._queue.length > 0) {
                var task = this._queue[0];

                // don't move onto the next task until this one is done
                this.once(task.event, function () {
                    this._queue.shift();
                    this._loadQueue();
                }.bind(this));
                task.action();
            }

            return this;
        }

        /**
         * Fired when playback ends at the end of the duration.
         * @param  {Sound} sound The sound object to work with.
         * @return {Howl}
         */

    }, {
        key: '_ended',
        value: function _ended(sound) {
            var sprite = sound._sprite;

            // Should this sound loop?
            var loop = !!(sound._loop || this._sprite[sprite][2]);

            // Fire the ended event.
            this._emit('end', sound._id);

            // Restart this timer if on a Web Audio loop.
            if (loop) {
                this._emit('play', sound._id);
                sound._seek = sound._start || 0;
                sound._rateSeek = 0;
                sound._playStart = _howler.Howler.ctx.currentTime;

                var timeout = (sound._stop - sound._start) * 1000 / Math.abs(sound._rate);
                this._endTimers[sound._id] = setTimeout(this._ended.bind(this, sound), timeout);
            }

            // Mark the node as paused.
            if (!loop) {
                sound._paused = true;
                sound._ended = true;
                sound._seek = sound._start || 0;
                sound._rateSeek = 0;
                this._clearTimer(sound._id);

                // Clean up the buffer source.
                this._cleanBuffer(sound._node);

                // Attempt to auto-suspend AudioContext if no sounds are still playing.
                _howler.Howler._autoSuspend();
            }

            return this;
        }

        /**
         * Clear the end timer for a sound playback.
         * @param  {Number} id The sound ID.
         * @return {Howl}
         */

    }, {
        key: '_clearTimer',
        value: function _clearTimer(id) {
            if (this._endTimers[id]) {
                clearTimeout(this._endTimers[id]);
                delete this._endTimers[id];
            }
            return this;
        }

        /**
         * Return the sound identified by this ID, or return null.
         * @param  {Number} id Sound ID
         * @return {Object}    Sound object or null.
         */

    }, {
        key: '_soundById',
        value: function _soundById(id) {
            // Loop through all sounds and find the one with this ID.
            for (var i = 0; i < this._sounds.length; i++) {
                if (id === this._sounds[i]._id) {
                    return this._sounds[i];
                }
            }

            return null;
        }

        /**
         * Return an inactive sound from the pool or create a new one.
         * @return {Sound} Sound playback object.
         */

    }, {
        key: '_inactiveSound',
        value: function _inactiveSound() {
            this._drain();

            // Find the first inactive node to recycle.
            for (var i = 0; i < this._sounds.length; i++) {
                if (this._sounds[i]._ended) {
                    return this._sounds[i].reset();
                }
            }

            // If no inactive node was found, create a new one.
            return new _sound4.Sound(this);
        }

        /**
         * Drain excess inactive sounds from the pool.
         */

    }, {
        key: '_drain',
        value: function _drain() {
            var limit = this._pool;
            var cnt = 0;
            // let i = 0;

            // If there are less sounds than the max pool size, we are done.
            if (this._sounds.length < limit) {
                return;
            }

            // Count the number of inactive sounds.
            for (var i = 0; i < this._sounds.length; i++) {
                if (this._sounds[i]._ended) {
                    cnt++;
                }
            }

            // Remove excess inactive sounds, going in reverse order.
            for (var _i2 = this._sounds.length - 1; _i2 >= 0; _i2--) {
                if (cnt <= limit) {
                    return;
                }

                if (this._sounds[_i2]._ended) {
                    // Disconnect the audio source when using Web Audio.
                    if (this._sounds[_i2]._node) {
                        this._sounds[_i2]._node.disconnect(0);
                    }

                    // Remove sounds until we have the pool size.
                    this._sounds.splice(_i2, 1);
                    cnt--;
                }
            }
        }

        /**
         * Get all ID's from the sounds pool.
         * @param  {Number} id Only return one ID if one is passed.
         * @return {Array}    Array of IDs.
         */

    }, {
        key: '_getSoundIds',
        value: function _getSoundIds(id) {
            if (typeof id === 'undefined') {
                var ids = [];
                for (var i = 0; i < this._sounds.length; i++) {
                    ids.push(this._sounds[i]._id);
                }

                return ids;
            } else {
                return [id];
            }
        }

        /**
         * Load the sound back into the buffer source.
         * @param  {Sound} sound The sound object to work with.
         * @return {Howl}
         */

    }, {
        key: '_refreshBuffer',
        value: function _refreshBuffer(sound) {
            // Setup the buffer source for playback.
            sound._node.bufferSource = _howler.Howler.ctx.createBufferSource();

            // sound._node.bufferSource.buffer = cache[this._src];
            sound._node.bufferSource.buffer = _loader.Loader.cache[this._src];

            // Connect to the correct node.
            if (sound._panner) {
                sound._node.bufferSource.connect(sound._panner);
            } else {
                sound._node.bufferSource.connect(sound._node);
            }

            // Setup looping and playback rate.
            sound._node.bufferSource.loop = sound._loop;
            if (sound._loop) {
                sound._node.bufferSource.loopStart = sound._start || 0;
                sound._node.bufferSource.loopEnd = sound._stop;
            }
            sound._node.bufferSource.playbackRate.value = sound._rate;

            return this;
        }

        /**
         * Prevent memory leaks by cleaning up the buffer source after playback.
         * @param  {Object} node Sound's audio node containing the buffer source.
         * @return {Howl}
         */

    }, {
        key: '_cleanBuffer',
        value: function _cleanBuffer(node) {
            if (this._scratchBuffer) {
                node.bufferSource.onended = null;
                node.bufferSource.disconnect(0);
                try {
                    node.bufferSource.buffer = this._scratchBuffer;
                } catch (e) {}
            }
            node.bufferSource = null;

            return this;
        }
    }]);

    return Howl;
}(_eventEmitter.EventEmitter);

;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Loader = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _sound = __webpack_require__(1);

var _howler = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var cache = {};

var Loader = function () {
    function Loader() {
        _classCallCheck(this, Loader);
    }

    _createClass(Loader, [{
        key: 'load',


        /**
         * Load the audio file.
         * @return {Howl}
         */
        value: function load(howl) {
            var url = null;

            // If no audio is available, quit immediately.
            if (_howler.Howler.noAudio) {
                howl._emit('loaderror', null, 'No audio support.');
                return;
            }

            // Make sure our source is in an array.
            if (typeof howl._src === 'string') {
                howl._src = [howl._src];
            }

            url = this.getUrl(howl);

            if (!url) {
                howl._emit('loaderror', null, 'No codec support for selected audio sources.');
                return;
            }
            // console.log(url)
            howl._src = url;
            howl._state = 'loading';

            // If the hosting page is HTTPS and the source isn't show console error
            if (window.location.protocol === 'https:' && url.slice(0, 5) === 'http:') {
                error.log('Potential Mixed Content error');
            }

            // Create a new sound object and add it to the pool.
            new _sound.Sound(howl);

            // Load and decode the audio data for playback.
            this.loadBuffer(howl);

            return howl;
        }

        /**
         * @param howl
         */

    }, {
        key: 'getUrl',
        value: function getUrl(howl) {
            // console.log(howl._src)
            var url = null;
            // Loop through the sources and pick the first one that is compatible.
            for (var i = 0; i < howl._src.length; i++) {
                var ext = void 0,
                    str = void 0;

                if (howl._format && howl._format[i]) {
                    // If an extension was specified, use that instead.
                    ext = howl._format[i];
                } else {
                    // Make sure the source is a string.
                    str = howl._src[i];
                    if (typeof str !== 'string') {
                        howl._emit('loaderror', null, 'Non-string found in selected audio sources - ignoring.');
                        continue;
                    }

                    // Extract the file extension from the URL or base64 data URI.
                    ext = /^data:audio\/([^;,]+);/i.exec(str);
                    if (!ext) {
                        ext = /\.([^.]+)$/.exec(str.split('?', 1)[0]);
                    }

                    if (ext) {
                        ext = ext[1].toLowerCase();
                    }
                    // console.log(ext)
                }

                // Log a warning if no extension was found.
                if (!ext) {
                    console.warn('No file extension was found. Consider using the "format" property or specify an extension.');
                }

                // Check if this extension is available.
                if (ext && _howler.Howler.codecs(ext)) {
                    url = howl._src[i];
                    break;
                }
            }
            return url;
        }
        /**
         * Buffer a sound from URL, Data URI or cache and decode to audio source (Web Audio API).
         * @param  {Howl} self
         */

    }, {
        key: 'loadBuffer',
        value: function loadBuffer(howl) {
            var url = howl._src;

            // Check if the buffer has already been cached and use it instead.
            if (cache[url]) {
                // Set the duration from the cache.
                howl._duration = cache[url].duration;

                // Load the sound into this Howl.
                this.loadSound(howl);

                return;
            }

            if (/^data:[^;]+;base64,/.test(url)) {
                // Decode the base64 data URI without XHR, since some browsers don't support it.
                var data = atob(url.split(',')[1]);
                var dataView = new Uint8Array(data.length);
                for (var i = 0; i < data.length; ++i) {
                    dataView[i] = data.charCodeAt(i);
                }

                this.decodeAudioData(dataView.buffer, howl);
            } else {
                var xhr = this.createXhr(howl, url);
                this.safeXhrSend(xhr);
            }
        }
    }, {
        key: 'createXhr',
        value: function createXhr(howl, url) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.withCredentials = howl._xhrWithCredentials;
            xhr.responseType = 'arraybuffer';
            xhr.onload = function () {
                // Make sure we get a successful response back.
                var code = (xhr.status + '')[0];
                if (code !== '0' && code !== '2' && code !== '3') {
                    howl._emit('loaderror', null, 'Failed loading audio file with status: ' + xhr.status + '.');
                    return;
                }
                this.decodeAudioData(xhr.response, howl);
            }.bind(this);

            xhr.onerror = function () {
                error.log('xhr load error');
            }.bind(this);
            return xhr;
        }

        /**
         * Send the XHR request wrapped in a try/catch.
         * @param  {Object} xhr XHR to send.
         */

    }, {
        key: 'safeXhrSend',
        value: function safeXhrSend(xhr) {
            console.warn('Howler loader is deprecated. Use external loader!');
            try {
                xhr.send();
            } catch (e) {
                xhr.onerror();
            }
        }
    }, {
        key: 'decodeAudioData',


        /**
         * Decode audio data from an array buffer.
         * @param  {ArrayBuffer} arraybuffer The audio data.
         * @param  {Howl}        self
         */
        value: function decodeAudioData(arraybuffer, howl) {
            // Decode the buffer into an audio source.
            _howler.Howler.ctx.decodeAudioData(arraybuffer, function (buffer) {
                if (buffer && howl._sounds.length > 0) {
                    cache[howl._src] = buffer;
                    this.loadSound(howl, buffer);
                }
            }.bind(this), function () {
                howl._emit('loaderror', null, 'Decoding audio data failed.');
            });
        }
    }, {
        key: 'loadSound',


        /**
         * Sound is now loaded, so finish setting everything up and fire the loaded event.
         * @param  {Howl} self
         * @param  {Object} buffer The decoded buffer sound source.
         */
        value: function loadSound(howl, buffer) {
            // Set the duration.
            if (buffer && !howl._duration) {
                howl._duration = buffer.duration;
            }

            // Setup a sprite if none is defined.
            if (Object.keys(howl._sprite).length === 0) {
                howl._sprite = { __default: [0, howl._duration * 1000] };
            }

            // Fire the loaded event.
            if (howl._state !== 'loaded') {
                howl._state = 'loaded';
                howl._emit('load');
                howl._loadQueue();
            }
        }
    }, {
        key: 'cache',
        get: function get() {
            return cache;
        }
    }]);

    return Loader;
}();

var loader = new Loader();
exports.Loader = loader;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EventEmitter = exports.EventEmitter = function () {
    function EventEmitter() {
        _classCallCheck(this, EventEmitter);
    }

    /**
     * Listen to a custom event.
     * @param  {String}   event Event name.
     * @param  {Function} fn    Listener to call.
     * @param  {Number}   id    (optional) Only listen to events for this sound.
     * @param  {Number}   once  (INTERNAL) Marks event to fire only once.
     * @return {Howl}
     */


    _createClass(EventEmitter, [{
        key: 'on',
        value: function on(event, fn, id, once) {
            var events = this['_on' + event];
            if (typeof fn === 'function') {
                events.push(once ? { id: id, fn: fn, once: once } : { id: id, fn: fn });
            }
            return this;
        }

        /**
         * Remove a custom event. Call without parameters to remove all events.
         * @param  {String}   event Event name.
         * @param  {Function} fn    Listener to remove. Leave empty to remove all.
         * @param  {Number}   id    (optional) Only remove events for this sound.
         * @return {Howl}
         */

    }, {
        key: 'off',
        value: function off(event, fn, id) {
            var events = this['_on' + event];
            var i = 0;

            // Allow passing just an event and ID.
            if (typeof fn === 'number') {
                id = fn;
                fn = null;
            }

            if (fn || id) {
                // Loop through event store and remove the passed function.
                for (i = 0; i < events.length; i++) {
                    var isId = id === events[i].id;
                    if (fn === events[i].fn && isId || !fn && isId) {
                        events.splice(i, 1);
                        break;
                    }
                }
            } else if (event) {
                // Clear out all events of this type.
                this['_on' + event] = [];
            } else {
                // Clear out all events of every type.
                var keys = Object.keys(this);
                for (i = 0; i < keys.length; i++) {
                    if (keys[i].indexOf('_on') === 0 && Array.isArray(this[keys[i]])) {
                        this[keys[i]] = [];
                    }
                }
            }

            return this;
        }

        /**
         * Listen to a custom event and remove it once fired.
         * @param  {String}   event Event name.
         * @param  {Function} fn    Listener to call.
         * @param  {Number}   id    (optional) Only listen to events for this sound.
         * @return {Howl}
         */

    }, {
        key: 'once',
        value: function once(event, fn, id) {
            // Setup the event listener.
            this.on(event, fn, id, 1);

            return this;
        }

        /**
         * Emit all events of a specific type and pass the sound id.
         * @param  {String} event Event name.
         * @param  {Number} id    Sound ID.
         * @param  {Number} msg   Message to go with event.
         * @return {Howl}
         */

    }, {
        key: '_emit',
        value: function _emit(event, id, msg) {
            var events = this['_on' + event];

            // Loop through event store and fire all functions.
            for (var i = events.length - 1; i >= 0; i--) {
                if (!events[i].id || events[i].id === id || event === 'load') {
                    setTimeout(function (fn) {
                        fn.call(this, id, msg);
                    }.bind(this, events[i].fn), 0);

                    // If this event was setup with `once`, remove it.
                    if (events[i].once) {
                        this.off(event, events[i].fn, events[i].id);
                    }
                }
            }
            return this;
        }
    }]);

    return EventEmitter;
}();

/***/ })
/******/ ]);
});