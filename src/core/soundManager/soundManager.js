//var Resource = require('resource-loader').Resource;
import {Howler} from "./howler";
import {getBaseDir} from '../utils/common';
import {default as ASSETS} from '../assets';

const soundType = {
    soundEffect: "sfx",
    music: "music",
    menuSound: "menu"
}

export class SoundManager {
    constructor(){
        this.sounds = {};
        this.sprites = {};
        this.playingSounds = [];
        this.playingMusic = [];
        this.soundsMuted = false;
        this.musicMuted = false;
        this.defaultVolume = 0.3;
        this.idCase = {};
        //create sound in howler to exec _enableMobileAudio set correctly
        this.initializeSound();
    }

    initializeSound() {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        if (window.AudioContext) {
            window.audioContext = new window.AudioContext();

            var buffer = window.audioContext.createBuffer(1, 1, 22050);
            var source = window.audioContext.createBufferSource();
            source.buffer = buffer;

            var silentHowl = Howler.createHowl({
                src: source
            });
        }
    }

    addSprite(name, sprite){
        this.sprites[name] = sprite;
        for(var sound in sprite._sprite){
            if(!this.sounds[sound]){
                var duration = sprite._sprite[sound][1]
                this.sounds[sound] = {
                    data :name,
                    fileType: "sprite",
                    duration: duration
                };
            }
        }
    }

    addSound(name, sound){
        if(!this.sounds[sound]){
            this.sounds[name] = {
                data: sound,
                fileType: "sound",
                duration: sound._duration * 1000};
        }
    }

    set muteSounds(value){
        this.soundsMuted = value;
        this.muteSoundsFunc(value);

        // if(value === true && this.musicMuted === true){
        //     Howler.mute(value);
        // } else if(value === false) {
        //     Howler.mute(value);
        // }
        // Howler.mute(value)
    }

    get muteSounds(){
        return this.soundsMuted;
    }

    // set muteMusic(value){
    //
    //
    //
    //     this.musicMuted = value;
    //     this.muteMusicFunc(value);
    //     if(value === true && this.soundsMuted === true){
    //         Howler.mute(value);
    //     } else if(value === false) {
    //         Howler.mute(value);
    //     }
    // }

    get muteMusic(){
        return this.musicMuted;
    }

    getSoundByName(soundName) {
        var sound = {}
        if(this.sounds[soundName].fileType === "sprite"){
            sound = this.sprites[this.sounds[soundName].data];
        }
        else {
            sound = this.sounds[soundName].data;
        }
        return sound
    }

    playMenuSound(soundName){
        this.startPlaying(soundName, false, {});
        var sound = this.getSoundByName(soundName);
        sound.mute(this.soundsMuted);
        this.sounds[soundName].type = soundType.menuSound;
    }
    playSound(soundName, loop=false, options={}){
        this.startPlaying(soundName, loop, options);
        var sound = this.getSoundByName(soundName);
        sound.mute(this.soundsMuted);
        this.sounds[soundName].type = soundType.soundEffect;
    }

    playMusic(soundName, loop=false, options={}){
        var sound = this.getSoundByName(soundName);
        this.sounds[soundName].type = soundType.music;
        sound.mute(this.soundsMuted);
        this.startPlaying(soundName, loop, options);
    }

    pause(){
        this.pauseFunction({
            pause: true,
            sound: true,
            music: true
        })
    }

    resume(){
        this.pauseFunction({
            pause: false,
            sound: true,
            music: true
        })
    }

    pauseSounds() {
        this.pauseFunction({
            pause: true,
            sound: true,
            music: false
        })
    }

    resumeSounds() {
        this.pauseFunction({
            pause: false,
            sound: true,
            music: false
        })
    }

    pauseMusic() {
        this.pauseFunction({
            pause: true,
            sound: false,
            music: true
        })
    }

    resumeMusic() {
        this.pauseFunction({
            pause: false,
            sound: false,
            music: true
        })
    }

    pauseFunction(pauseOptions){
        for(let key in this.sounds){
            if(pauseOptions.pause){
                //If current sound type should be paused
                if((this.sounds[key].type === soundType.soundEffect && pauseOptions.sound) ||
                    (this.sounds[key].type === soundType.music && pauseOptions.music)){
                    let sound = this.getSoundByName(key)
                    for(let i = 0; i < this.idCase[key].length; i++){
                        sound.pause(this.idCase[key][i]);
                    }
                }
            }
            else{
                //If current sound type should be unpaused
                if((this.sounds[key].type === soundType.soundEffect && pauseOptions.sound) ||
                    (this.sounds[key].type === soundType.music && pauseOptions.music)){
                    let sound = this.getSoundByName(key)
                    for(let i = 0; i < this.idCase[key].length; i++){
                        sound.unpause(this.idCase[key][i]);
                    }
                }
            }
        }
    }

    muteSoundsFunc(mute){
        for(let key in this.sounds){
            this.getSoundByName(key).mute(mute)

            // if(this.sounds[key].type === soundType.soundEffect) {
            //     let sound = this.getSoundByName(key)
            //     sound.mute(mute);
            // }
        }
    }

    // muteMusicFunc(mute){
    //     for(let key in this.sounds){
    //         if(this.sounds[key].type === soundType.music) {
    //             let sound = this.getSoundByName(key)
    //             sound.mute(mute);
    //         }
    //     }
    // }

    startPlaying(soundName, loop=false, options={}){
        if(!this.sounds[soundName])
            throw soundName + ' not found';

        let volume = options.volume || this.defaultVolume
        volume = Math.min(1, Math.max(0, volume))
        let sound = null
        let id = null

        if(this.sounds[soundName].fileType === "sprite"){
            sound = this.sprites[this.sounds[soundName].data]
            sound._volume = volume
            id = sound.play(soundName)
            sound.volume(volume, id)
            sound.loop(loop, id)

        }else{
            sound = this.sounds[soundName].data

            //don't play several instances of looped sound
            if(this.idCase[soundName] && this.idCase[soundName][0] && loop){
                id = sound.play(this.idCase[soundName][0]);
            }
            else{
                id = sound.play();
            }
            sound.loop(loop, id);

            if(options.fadeIn){
                sound.fade(0.0, options.volume || this.defaultVolume, 1000*options.fadeIn.time, id);
            }else{
                sound.volume(options.volume || this.defaultVolume, id);
            }
        }

        if(!this.idCase[soundName]){
            this.idCase[soundName] = [id]
        } else {
            this.idCase[soundName].push(id)
        }

        if(!loop) {
            sound.once('end', () => {
                delete this.idCase[soundName][0]
                this.idCase[soundName].shift()
            }, id)
        }
    }

    stopSound(soundName, options={}){

        if(!this.sounds[soundName])
            throw soundName + ' not found';

        let sound = null
        let id = null

        if(this.idCase[soundName]){
            id = this.idCase[soundName][0]
            if(!id) { return }
        } else { return }

        sound = this.getSoundByName(soundName);
        sound.loop(false, id);

        if(options.fadeOut){
            sound.fade(sound.volume(id), 0.0, 1000*options.fadeOut.time, id);
            window.setTimeout(function(){sound.stop(id);},1000*options.fadeOut.time);
        }else{
            sound.stop(id)
            if (id === this.idCase['CountUp_Music_Loop'])
                this.playSound('CountUp_Music_Tail', false)
        }
        this.clearSoundId(soundName);
    }

    clearSoundId(soundName) {
        if(this.idCase[soundName]){
            delete this.idCase[soundName][0]
            this.idCase[soundName].shift()
        }
    }
}

export function parseSounds(resource, next){
    // if(resource.data && resource.isJson && resource.data.sprite) {
    //
    //     next()
    //     return;
    //
    //     console.log(resource.data)
    //     /*var loadOptions = {
    //      crossOrigin: resource.crossOrigin,
    //      loadType: Resource.LOAD_TYPE.AUDIO,//XHR for files
    //      metadata: resource.metadata.audioMetadata
    //      };*/
    //     var baseDir = getBaseDir(resource.url);
    //     var dataUrls = [];
    //     for (var i = 0; i < resource.data.urls.length; i++)
    //         dataUrls.push(baseDir + resource.data.urls[i]);
    //     console.log(dataUrls)
    //     resource.soundData = resource.data;
    //     resource.howlerSprite = new Howl({src: dataUrls, sprite: resource.data.sprite});
    //     resource.howlerSprite.once('load', function () {
    //         ASSETS.sounds.addSprite(resource.url, resource.howlerSprite);
    //         next();
    //     });

    //--------------------------------------------------------------------
    /*}else*/ if(resource.data && resource.isJson && resource.data.sounds){
        for(let i = 0; i < resource.data.sounds.length; i++){
            resource.howlerSound = Howler.createHowl({
                src:[resource.data.sounds[i].data]
            })
            if(resource.data.sounds[i].name === 'coin_loop_long'){
                resource.howlerSound.on('fade', function() {
                    // console.log(`${resource.data.sounds[i].name} fade ended`)
                })
            }
            ASSETS.sounds.addSound(resource.data.sounds[i].name, resource.howlerSound);
        }
        next()
    //--------------------------------------------------------------------

    // }else if((/\.(mp3|ogg|wav)$/i).test(resource.url) && resource.data){
    //     resource.howlerSound = new Howl({src:resource.url});
    //     resource.howlerSound.once('load', function(){
    //         console.log(`resource.url = ${resource.url}`)
    //         ASSETS.sounds.addSound(resource.url, resource.howlerSound);
    //         next();
    //     })
    } else {
        return next();
    }
}
