export function applyBlendMode(sprite, blendMode){
    sprite.blendMode = blendMode;
    if(sprite.children)
        for(var i = 0; i < sprite.children.length; i++)
            applyBlendMode(sprite.children[i], blendMode);
}

export function parseJSON(rawJSON){
    try{
        var object = JSON.parse(rawJSON);
        return object;
    }catch(err){
        console.log('error parsing JSON: ' + err.toString());
        return null;
    }
}

export function getBaseDir(fullDir){
    var index = fullDir.lastIndexOf('/');
    if(index === -1)return fullDir;
    return fullDir.substr(0, index + 1);
}

export function getFileBaseName(fullPath) {
    var str = fullPath.split('/');
    var fileName = str[str.length - 1];
    return fileName.substr(0, fileName.lastIndexOf('.'));
}

export function changeSuffix(path, suffix) {
    return getBaseDir(path) + getFileBaseName(path) + '.' + suffix;
}

export function getAtlasPath(path) {
    return changeSuffix(path, 'atlas');
}

export function getTexturePath(path) {
    return changeSuffix(path, 'png');
}


// Debounce Method from underscore.js lib(http://underscorejs.org/)
//
// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
export function debounce(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this, args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

export function scaleDown(el, maxWidth) { ( el.scale.set(1), el.width > maxWidth && el.scale.set(maxWidth / el.width) ) };
