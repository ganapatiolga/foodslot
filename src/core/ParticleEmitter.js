var PIXI = require('pixi.js');
import {default as CLOCK} from './clock';

const Vec2 = (x, y) => ({x, y})

const randFloat = (min, max) => Math.random() * (max - min) + min
const randInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

class PixiParticle extends PIXI.Sprite{
    constructor(emitter){
        super()
        this.emitter = emitter 
        this.anchor.set(0.5, 0.5)
        this.velocity = Vec2(0, 0)
        this.acceleration = Vec2(0, 0)
        this.age = 0
        this.maxLife = 0
        this.prev = null
        this.next = null
        this.visible = false
        this.alpha = 0
        this.rotateBy = 0
        this.customFn = null
    }
    init(lifetime, vel, acc, r, a, cfn){
        this.visible = true
        this.alpha = a
        this.maxLife = lifetime
        this.age = 0
        this.velocity = vel
        this.acceleration = acc
        this.rotateBy = r
        var scale = randFloat(0.5, 1.0)
        this.scale.set(scale,scale)
        this.customFn = cfn
    }
    update(delta){
        this.age+=delta
        if(this.age >= this.maxLife)
            return this.kill()
        if(this.customFn)
            this.customFn(this)
        //let lerp = this.age/this.maxLife
        this.velocity.x += this.acceleration.x*delta
        this.velocity.y += this.acceleration.y*delta
        this.position.x += this.velocity.x*delta
        this.position.y += this.velocity.y*delta
        this.rotation += this.rotateBy
    }
    kill(){
        this.visible = false
        this.alpha = 0
        this.emitter.recycle(this)
    }
}

const Particle = emitter => {
    let p = new PixiParticle(emitter)
    return p
}

const spawnRect = rect => particle => particle.position.set(randFloat(rect.left, rect.right), randFloat(rect.top, rect.bottom))
const spawnDefault = () => particle => {}


const ParticleEmitter = (textures, options = {}) => {
    var stopEmitting = true;
    const containerOptions = {
        scale: false,
        position: true,
        rotation: true,
        uvs: true,
        alpha: true
    }
    const maxParticles = options.maxParticles > 0 ? options.maxParticles : 15000
    let container = new PIXI.particles.ParticleContainer(maxParticles, containerOptions)
    container.interactive = false
    container.displayFlag = PIXI.DISPLAY_FLAG.MANUAL_CONTAINER
    
    const minVel = options.minVel || Vec2(0, 0)
    const maxVel = options.maxVel || Vec2(0, 0)
    const acceleration = options.acc || Vec2(0, 0)
    
    let frequency = options.frequency || 0.2
    let spawnAmount = options.spawnAmount || 1
    const addAtBack = !!options.addAtBack
    const lifetime = options.lifetime || 1.2
    const alpha = options.alpha != null ? options.alpha : 1
    const fn = options.customFn || null

    let timer = 0
    
    let spawnFunc = spawnDefault
    if(options.spawnRect)spawnFunc = spawnRect(options.spawnRect)
    
    let activeParticlesFirst = null
    let activeParticlesLast = null
    let poolFirst = null
    let count = 0
    
    const recycle = particle => {
        if(particle.next)
            particle.next.prev = particle.prev
        if(particle.prev)
            particle.prev.next = particle.next
        if(activeParticlesFirst === particle)
            activeParticlesFirst = particle.next
        if(activeParticlesLast === particle)
            activeParticlesLast = particle.last
        particle.prev = null
        particle.next = poolFirst
        poolFirst = particle
        count--
    }
    
    const pullParticle = () => {
        if(poolFirst){
            let p = poolFirst
            poolFirst = p.next
            p.next = null
            return p
        }else{
            return Particle(container)
        }
    }
    
    const emit = delta => {
        timer-=delta
        while(timer <= 0){
            if(count >= maxParticles){
                timer+=frequency
                return false
            }
            
            if(-timer < lifetime){
                for(let i = Math.min(spawnAmount, maxParticles - count); i > 0; i--){
                    let p = pullParticle()
                    p.texture = textures.length > 1 ? textures[randInt(0, textures.length-1)] : textures[0]
                    spawnFunc(p)
                    let vel = Vec2(randFloat(minVel.x,maxVel.x),randFloat(minVel.y, maxVel.y))
                    p.init(lifetime, vel, acceleration, 0.5*vel.x/maxVel.x, alpha, fn)
                    p.update(-timer)
                    addAtBack ? container.addChildAt(p, 0) : container.addChild(p)
                    if(activeParticlesLast){
                        activeParticlesLast.next = p
                        p.prev = activeParticlesLast
                        activeParticlesLast = p
                    }else{
                        activeParticlesLast = activeParticlesFirst = p
                    }
                    count++
                }
            }
            timer+=frequency  
        }
    }
    
    const update = event => {
        const delta = 0.001*event.dt
        var particle, next;
        for(particle = activeParticlesFirst; particle; particle = next){
            next = particle.next
            particle.update(delta)
        }
        if(stopEmitting === false)
        emit(delta)
    }
    
    Object.assign(container, {
        recycle,
        start: () => {
            if(stopEmitting){
                timer = 0
                container.visible = true
                stopEmitting = false
            }
        },
        stop: () => {
            stopEmitting = true;
            ///CLOCK.removeListener('tick', update)
        }
    })
    Object.defineProperty(container, "frequency", {
        get: function() { return frequency },
        set: function(value){ frequency = value }
    })
    Object.defineProperty(container, "spawnAmount", {
        get: function() { return spawnAmount },
        set: function(value){ spawnAmount = value }
    })
    CLOCK.on('tick', update)
    return container
}

export {ParticleEmitter}