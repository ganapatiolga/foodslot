var PIXI = require('pixi.js');
var EventEmitter = require('eventemitter3');
var GreenSock = require('gsap');

class PixiTickerOverrider{
    constructor(){
        PIXI.ticker.shared.autoStart = false; 
        PIXI.ticker.shared.stop();
        this.tickWord = 'tick';
    }
    tick(dt){
        PIXI.ticker.shared._emitter.emit(this.tickWord, dt);
    }
}

class GreensockTickerOverrider{
    constructor(){
        this.tickWord = 'tick';
        TweenMax.ticker.sleep()
        TweenLite.ticker.sleep()
        TweenMax.ticker.wake = () => {}
        TweenLite.ticker.wake = () => {}
    }
    tick(){
        TweenMax.ticker.tick()
    }
}

class FrameEventData{
    constructor(time, dt){
        this.time = time;
        this.dt = dt;
    }
}

class Clock extends EventEmitter{
    constructor(){
        super();
        
        this.pixiTicker = new PixiTickerOverrider();
        this.greensockTicker = new GreensockTickerOverrider();
        
        this.paused = false;
        this.onFrameEvent = 'tick';
        this.updateBound = this.update.bind(this);
        
        this.prevTimestamp = this.time;
        this.update(this.time);
    }
    update(timestamp){
        var deltaTime = timestamp - this.prevTimestamp;
        this.prevTimestamp = timestamp;
        //hack for mozilla firefox
        //firefox (55.0.2) calls requestAnimationFrame while tab is inactive. It happens ~once in a minute
        if(deltaTime < 500 || gAPI.casinoUI.visibility) {
            let frameEventData = new FrameEventData(timestamp, deltaTime);
            super.emit(this.onFrameEvent, frameEventData);

            this.pixiTicker.tick(deltaTime);
            this.greensockTicker.tick();
        }
        if(this.paused === false)
        window.requestAnimationFrame(this.updateBound);
    }
    get time(){
        return performance.now();
    }
    pause(){
        this.paused = true;
        TweenMax.pauseAll();
        window.cancelAnimationFrame(this.updateBound);
    }
    resume(){
        this.paused = false;
        TweenMax.resumeAll();
        this.prevTimestamp = this.time;
        this.update(this.time);
    }
}

var CLOCK = new Clock();
export default CLOCK;