var manifestJSON = require('../../../manifest.json');

class Manifest{
    constructor(){
        this.data = manifestJSON;
    }
}

var MANIFEST = new Manifest();
export default MANIFEST;