import {SoundManager} from './soundManager/soundManager';
import {AnimationManager} from './animations';

class Assets {
    constructor(){
        this.textures = {};
        this.baseTextures = {};
        this.sounds = new SoundManager();
        this.animations = new AnimationManager();
    }
    getTexture(name){
        if(!this.textures[name])
            throw(name + ' not found');

        return this.textures[name];
    }
}

var ASSETS = new Assets();
export default ASSETS;
