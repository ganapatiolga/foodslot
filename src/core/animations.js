var PIXI = require('pixi.js');
var PIXI_SPINE = require('pixi-spine');

var Resource = require('resource-loader').Resource;

PIXI.loaders.Resource.TYPE = {
    UNKNOWN: 0,
    JSON: 1,
    XML: 2,
    IMAGE: 3,
    AUDIO: 4,
    VIDEO: 5,
    TEXT: 6
};


var dragonbones = require('../../external/DragonBonesJS_24/dragonBonesPixi.js');
import {getBaseDir, getAtlasPath, getTexturePath} from './utils/common';
import {default as ASSETS} from './assets';

class dragonBonesContainer extends PIXI.Container{
    constructor(armatureDisplay){
        super();
        super.addChild(armatureDisplay);
        this.armatureDisplay = armatureDisplay;
        this.armatureDisplay.on('FrameEvent', this.dispatchFrameEvent.bind(this));
    }
    get animation(){
        return this.armatureDisplay.animation;
    }
    set(armatureDisplay){
        if(armatureDisplay.armatureDisplay)
            armatureDisplay = armatureDisplay.armatureDisplay
        this.clear();
        super.addChild(armatureDisplay);
        this.armatureDisplay = armatureDisplay;
        this.armatureDisplay.on('FrameEvent', this.dispatchFrameEvent.bind(this));
    }
    clear(){
        this.armatureDisplay.clear();
        this.armatureDisplay.removeListener('FrameEvent', this.dispatchFrameEvent.bind(this));
        super.removeChild(this.armatureDisplay);
        delete this.armatureDisplay;
    }
    dispatchFrameEvent(eventName){
        super.emit(eventName);
    }
}

export class AnimationManager{
    constructor(){
        this.dbones = {};
        this.dbonesFactories = {};
        this.spineData = {};
        this.spineSkeleton = {};
    }
    addDragonBonesFactory(name, factory){
        this.dbonesFactories[name] = factory;
    }
    getDragonBones(name, armatureName=null, scale = 1.0){
        if(!this.dbonesFactories[name])
            throw(name + ' not found');
        var dbonesData = this.dbonesFactories[name].currentDragonBonesData;
        armatureName = armatureName || dbonesData.armatureNames[0];
        this.dbonesFactories[name].defaultScale = scale;
        var armatureDisplay = this.dbonesFactories[name].buildArmatureDisplay(armatureName);
        if(!armatureDisplay)
            throw(name + ' ' + armatureName + ' not found');
        armatureDisplay.setInnerScale(scale)
        return new dragonBonesContainer(armatureDisplay);
    }
    addSpineData(name, spineData) {
        this.spineData[name] = spineData;
    }
    addSpineSkeleton(name, spineData) {
        this.spineSkeleton[name] = spineData;

    }
    getSpineData(name) {
        if (!this.spineData[name])
            throw(name + ' not found');

        return this.spineData[name];
    }
    getSpineSkeleton(name) {
        if (!this.spineSkeleton[name])
            throw(name + ' not found');

        var skeleton = this.spineSkeleton[name];
        var spine = new PIXI.spine.Spine(skeleton);
        spine.myName = name;
        return spine;
    }

}

export function parseDragonbones(resource, next){
    if(!resource.data || !resource.isJson || !resource.data.armature) {
        return next();
    }

    var loader = this;
    var textureAtlasPath = getBaseDir(resource.url) + 'texture.json';

    loader.add(
        textureAtlasPath,
        {crossOrigin: resource.crossOrigin, loadType: Resource.LOAD_TYPE.XHR},
        function(res){
            resource.atlas = res.data;
            var texturePath;
            if(res.data.imagePath){
                texturePath = getBaseDir(resource.url) + res.data.imagePath;
            } else {
                texturePath = getBaseDir(resource.url) + 'texture.png';
            }
            resource.AnimationName = res.name;

            loader.add(
                texturePath,
                {crossOrigin: resource.crossOrigin, loadType: Resource.LOAD_TYPE.IMAGE},
                function(res){
                    res.isSpritesheet = true;
                    resource.texture = res.texture;

                    var factory = new dragonbones.PixiFactory();
                    var dbonesData = factory.parseDragonBonesData(resource.data);
                    factory.parseTextureAtlasData(resource.atlas, resource.texture);
                    factory.currentDragonBonesData = dbonesData;
                    ASSETS.animations.addDragonBonesFactory(resource.url, factory);

                    resource.dbones = dbonesData;
                    //ASSETS.animations.addDragonBones(resource.url, resource.dbones);
                    next();
                }
            );

        }
    );
}

export function parseSpine(resource, next) {
    if (!resource.data || !resource.data.skeleton || !resource.data.skeleton.spine || resource.isImage)
        return next();

    var rawSkeletonData = resource.data;
    var atlasPath = getAtlasPath(resource.url);
    var texturePath = getTexturePath(resource.url);

    /*
    if (resource.name.match(new RegExp(
        'assets\/' + Core.scale + '\/spine\/charSelect/CS_charadeta0[2345]\/skeleton.json')) !== null) {
        atlasPath = atlasPath.replace(/CS_charadeta0[2345]/, 'CS_charadeta01');
        texturePath = texturePath.replace(/CS_charadeta0[2345]/, 'CS_charadeta01');

       return next();
    }
    else  if (resource.name.match(new RegExp(
        'assets\/' + Core.scale + '\/spine\/charSelect/CS_selectframe0[23]\/skeleton.json')) !== null) {
        atlasPath = atlasPath.replace(/CS_selectframe0[23]/, 'CS_selectframe01');
        texturePath = texturePath.replace(/CS_selectframe0[23]/, 'CS_selectframe01');
        return next();
    }
    */

    var atlasLoadOptions = {
        crossOrigin: resource.crossOrigin,
        loadType: Resource.LOAD_TYPE.XHR
    }

    var loader = this;

    /*
    loader.add(atlasPath, atlasLoadOptions, function(atlasData) {
        var rawAtlasData = atlasData.data;
        var spineAtlas = new PIXI.spine.core.TextureAtlas(rawAtlasData, function (line, callback) {
            var textureLoadOptions = {
                crossOrigin: resource.crossOrigin,
                loadType: Resource.LOAD_TYPE.IMAGE,
                parentResource: atlasData
            };

            loader.add(texturePath, textureLoadOptions, function(textureData) {
                callback(textureData.texture.baseTexture);
            });
        }, function (_) {
            var spineAtlasLoader = new PIXI.spine.core.AtlasAttachmentLoader(spineAtlas);
            var spineJsonParser = new PIXI.spine.core.SkeletonJson(spineAtlasLoader);
            var spineData = spineJsonParser.readSkeletonData(rawSkeletonData);

            ASSETS.animations.addSpineData(resource.url, spineData);
            ASSETS.animations.addSpineSkeleton(resource.url, spineData);
            resource.spine = spineData;
            resource.data = null;
            next();
        });
    });
*/

    loader.add(
        atlasPath,
        atlasLoadOptions,
        function(atlasData) {
            var rawAtlasData = atlasData.data;
            var spineAtlas = new PIXI.spine.core.TextureAtlas(
                rawAtlasData,
                function (line, callback) {
                    var textureLoadOptions = {
                        crossOrigin: resource.crossOrigin,
                        loadType: Resource.LOAD_TYPE.IMAGE,
                        parentResource: atlasData
                    };

                    loader.add(
                        texturePath,
                        textureLoadOptions,
                        function(textureData) {
                            callback(textureData.texture.baseTexture);
                        }
                    );
                },
                function (_) {
                    var spineAtlasLoader = new PIXI.spine.core.AtlasAttachmentLoader(spineAtlas);
                    var spineJsonParser = new PIXI.spine.core.SkeletonJson(spineAtlasLoader);
                    var spineData = spineJsonParser.readSkeletonData(rawSkeletonData);

                    ASSETS.animations.addSpineData(resource.url, spineData);
                    ASSETS.animations.addSpineSkeleton(resource.url, spineData);
                    resource.spine = spineData;
                    resource.data = null;
                    next();
                }
            );
        }
    );
}
