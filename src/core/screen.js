var MobileDetect = require('mobile-detect');
var EventEmitter = require('eventemitter3');
import {debounce} from './utils/common';

class Screen extends EventEmitter {
    constructor(){
        super();
        var md = new MobileDetect(navigator.userAgent);
        this.screenWidth = window.screen.width;
        this.screenHeight = window.screen.height;
        this.userAgent = navigator.userAgent;
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.devicePixelRatio = Math.round(window.devicePixelRatio);
        this.minWidth = 128;
        this.minHeight = 128;

        let debouncedResize = debounce(function(e) {
            this.resize()
        }, 500).bind(this);

        window.addEventListener('resize', debouncedResize.bind(this), true);
        // window.addEventListener('resize', this.resize.bind(this), true);
    }
    resize(){
        this.width = Math.floor(window.innerWidth);
        this.height = Math.floor(window.innerHeight);
        this.emit('Resize');
    }
}

var SCREEN = new Screen();
export default SCREEN;