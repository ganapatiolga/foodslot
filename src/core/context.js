var PIXI = require('pixi.js');
var MobileDetect = require('mobile-detect');
import { default as SCREEN } from './screen';

let currentRenderer = null

const usePreserveDrawingBuffer = canvas => {
    const blacklistedGpus = ["Mali-400"];

    let gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
    let dbgRenderInfo = gl.getExtension("WEBGL_debug_renderer_info");

    if ( ! dbgRenderInfo) return false;

    return new RegExp(blacklistedGpus.join('|'), 'i').test(gl.getParameter(dbgRenderInfo.UNMASKED_RENDERER_WEBGL));
}

export class Context {
    constructor() {

        this.canvasElement = window.document.createElement('canvas');
        this.canvasElement.id = 'canvas'
        window.document.body.appendChild(this.canvasElement)
        let options = {
            view: this.canvasElement,
            resolution: SCREEN.devicePixelRatio,
            antialias: false,
            transparent: false,
            autoResize: false,
            backgroundColor: 0x000000,
            roundPixels: false,
            preserveDrawingBuffer: usePreserveDrawingBuffer(this.canvasElement)
        };
        let forceCanvas = false;
        this.renderer = PIXI.autoDetectRenderer(SCREEN.width, SCREEN.height, options, forceCanvas);
        currentRenderer = this.renderer
        this.resize();
    }
    render(stage) {
        if (this.renderer)
            this.renderer.render(stage);
    }
    resize() {
        this.renderer.resize(SCREEN.width, SCREEN.height);
        this.canvasElement.width = SCREEN.width * SCREEN.devicePixelRatio;
        this.canvasElement.height = SCREEN.height * SCREEN.devicePixelRatio;
        this.canvasElement.style.width = SCREEN.width + 'px';
        this.canvasElement.style.height = SCREEN.height + 'px';
    }
}

export { currentRenderer }

//renderer
//canvas//canvas css