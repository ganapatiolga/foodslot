var PIXI = require('pixi.js');
var EventEmitter = require('eventemitter3');
var GreenSock = require('gsap');
var Stats = require('stats.js');
import {Context} from './context';
import {Loader} from './loader';
import {MainStage} from './elements/mainStage';
import {default as SCREEN} from './screen';
import {default as CLOCK} from './clock';
import {default as assets} from './assets';
import {globalContext} from './global'

export default class Engine extends EventEmitter{
    constructor(){
        super();
        this.ctx = new Context();
        this.clockPaused = false;
        this.stage = new MainStage();
        window.mainStage = this.stage;
        this.loader = new Loader(this.ctx);
        
        globalContext.emit('contextChange', this)
        
        this.loader.on('LoadingCompleted', this.stage.initViews.bind(this.stage));
        SCREEN.on('Resize', this.onScreenResize.bind(this));
        SCREEN.emit('Resize');
        CLOCK.on('tick', this.update.bind(this));
    }

    onScreenResize(){
        this.stage.resize.call(this.stage);
        this.ctx.resize.call(this.ctx);
    }

    update(eventData){
        this.ctx.render(this.stage);
    } 
    destroy(){
        PIXI.ticker.shared.remove(this.update, this);
    }
    pause(){
        CLOCK.pause();
        this.clockPaused = true;
    }
    resume(){
        CLOCK.resume();
        this.clockPaused = false;
    }
    get cache(){
        return assets;
    }
}