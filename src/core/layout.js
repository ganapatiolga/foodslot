var AutoLayout = require('autolayout');
import {default as SCREEN} from './screen';

class Layout {
    constructor(){
        this.views = {};
    }
    addLayout(name, evfl){
        this.views[name] = evfl;
    }
    getLayout(name){
        if(!this.views[name])
            throw(name + ' not found');
        return this.views[name];
    }
    getViewElementList(view){
        if(!view)return null; 
        var subViews = [];
        for(var viewName in view.subViews)
            if (view.subViews.hasOwnProperty(viewName))
                if(viewName.indexOf('_') === -1)
                    subViews.push(viewName);
        return subViews;
    }
    
    applyAutoLayout(view, stage){
        if(!view || !stage)return;
        var subViews = this.getViewElementList(view)
        for(var i = 0;i < subViews.length; i++){
            var element = stage.elements[subViews[i]];
            if(element){
                var subView = view.subViews[subViews[i]];
                element.setPosition(subView.left,subView.top);
                element.setSize(subView.width,subView.height);
                if(subView.zIndex)
                    element.zOrder =+subView.zIndex;
            }
        }
    }

    
}

export var LAYOUT = new Layout();

var parseRawEVFL = function(evfl){
    try {           
        var constraints = AutoLayout.VisualFormat.parse(evfl, {extended: true});
        var view = new AutoLayout.View({
            constraints: constraints,
            width: SCREEN.width,
            height: SCREEN.height,
            spacing: 0
        });
        return view;
    }catch(err){
        console.log('Stage.parseLayout: evfl parsing error: ' + err.toString());
        return null;
    }
}

var rawEVFL = [
    "HV:|[screen(==100%)]|",
    "H:|~[frame(<=screen.width,>=screen.height@1)]~|",
    "V:|~[frame(<=screen.height,>=screen.width@1)]~|",
    "H:|~[background(>=screen.width,<=screen.height@1,>=0)]~|",
    "V:|~[background(>=screen.height,<=screen.width@1,>=0)]~|",
    "Z:|[overlayFrame][overlay][ui][topFrame][divider][frame][background][screen]",
    "C:frame.width(==frame.height*1.775)","C:background.width(==background.height*1.775)",
    "C:overlay.left(==background.left).top(==background.top).right(==background.right).bottom(==background.bottom)",
    "C:overlayFrame.left(==frame.left).top(==frame.top).right(==frame.right).bottom(==frame.bottom)",
    "C:ui.left(==frame.left).top(==frame.top).right(==frame.right).bottom(==frame.bottom)",
    "C:topFrame.left(==frame.left).top(==frame.top).right(==frame.right).bottom(==frame.bottom)",
    "C:divider.left(==background.left).top(==background.top).right(==background.right).bottom(==background.bottom)"
]
var evfl = parseRawEVFL(rawEVFL);
LAYOUT.addLayout('config/layouts/main.evfl', evfl);
