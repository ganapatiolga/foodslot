import {parseJSON} from './utils/common';

class Config {
    constructor(){
        this.settings = {};
    }
    addSettingsFile(name, config){
            if(this.settings [name])return;
            this.settings [name] = config;
    }
    getSettings(name){
        if(!this.settings [name])
            throw(name + ' not found');
        return this.settings [name];
    }
}

var CONFIG = new Config();
export default CONFIG;


export function parseConfigFile(resource, next){
    if(!resource.data || !resource.isJson || !((/config/i).test(resource.url) || (/manifest/i).test(resource.url)))
        return next();
    var configName = resource.url;
    CONFIG.addSettingsFile(configName, resource.data);
    next();

}