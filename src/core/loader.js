var EventEmitter = require('eventemitter3');
var PIXI = require('pixi.js');
import {default as CONFIG, parseConfigFile} from './config';
import {default as ASSETS} from './assets';
import {SOUNDS, parseSounds} from './soundManager/soundManager';
import {ANIMATIONS, parseDragonbones, parseSpine} from './animations';

const inloading = []

export {inloading}

export class Loader extends EventEmitter {
    constructor(ctx){
        super();
        PIXI.loader.after(parseConfigFile);
        PIXI.loader.after(parseSounds);
        PIXI.loader.after(parseDragonbones);
        PIXI.loader.after(parseSpine);
        this.ctx = ctx;
    }
    load(resourceList, progressHandler){
        PIXI.loader.add(resourceList);
        if(progressHandler)
            PIXI.loader.on('progress', progressHandler);
        PIXI.loader.load(this.onLoadingCompleted.bind(this));
    }
    onLoadingCompleted(loader, resources){
        for(var res in resources){
            if((/\.json$/i).test(res)){
                //if spritesheet
                if(resources[res].textures)
                for(var subTexture in resources[res].textures){
                    ASSETS.textures[subTexture] = resources[res].textures[subTexture];
                    /*if(this.ctx.renderer.type === PIXI.RENDERER_TYPE.WEBGL)
                        this.ctx.renderer.textureManager.updateTexture(resources[res].textures[subTexture].baseTexture);
                    */}
            }else if((/\.(jpg|png|jpeg)$/i).test(res)){
                /*if(this.ctx.renderer.type === PIXI.WEBGL_RENDERER)
                    this.ctx.renderer.textureManager.updateTexture(resources[res].texture.baseTexture);*/
                if(resources[res].isSpritesheet === true){
                    ASSETS.baseTextures[res] = resources[res].texture;
                }else{
                    ASSETS.textures[res] = resources[res].texture;
                }
            }
        }

        return Promise.all(inloading.map(func => Promise.resolve(func()))).then(() => {
            PIXI.loader.reset();
            this.emit('LoadingCompleted', resources);
        });
    }

}
