export default game => ({
    initialize: initData => new Promise(resolve => {
        game.once('LoadingFinished', resolve)
        game.controller.init(initData)
        game.load(progress => gAPI.loader.default.progress = 0.99 * progress)
    }),
    resume: resumeData => new Promise(resolve => {
        game.controller.once('AnimationsFinished', resolve.bind(null, resumeData && !resumeData.nextAction[0].startsWith('spin')))
        game.controller.afterSoundDialogue(resumeData)

        //TODO
        // if(resumeData)
        //     game.controller.resume(resumeData)
        // else
        //     game.controller.showCharacterSelection()
    }),
    select: actions => new Promise(resolve => {
        let actionString = actions[0].startsWith('freespin') ? 'freespin' : 'spin'

        if(actions[0].startsWith('freespin') && game.controller.slotController.firstFreeSpin) {
            gAPI.casinoUI.once('spinStart', () => {
                game.controller.slotController.firstFreeSpin = false;
                game.controller.slotController.startSpin()
                resolve({
                    action: actionString + game.charSet,
                    payload: null
                })
            })
            gAPI.casinoUI.enableSpinButton()
        }else if (actions[0] === 'battle'){
            resolve({
                action: actions[0],
                payload: null
            })
        }else{
            game.controller.slotController.startSpin()

            resolve({
                action: actionString + game.charSet,
                payload: null
            })
        }
    }),
    handle: response => new Promise(resolve => {
        game.controller.once('AnimationsFinished', resolve.bind(null, !response.nextAction[0].startsWith('spin')))
        game.controller.slotController.onResultReceived(response)
    }),
    reset: error => new Promise(resolve => {
        console.log(`DEBUG.handler: reset happened`)
        console.error(error)

        if(game.controller.slotController.state != 1)
            throw new Error('Unable to reset!')

        const stopSpin = () => {
            game.controller.once('AnimationsFinished', resolve)
            game.controller.slotController.onResultReceived({
                spinResult: {
                    reelDisplay: [[4,2,5],[6,7,3],[4,3,5],[4,0,7],[6,3,6]],
                    wins: []
                },
                totalWin: 0,
                nextAction: [null]
            })
        }
        if(gAPI.casinoUI.errorScreenDisplayed) gAPI.casinoUI.once('alertClosed', stopSpin)
        else stopSpin()
    })
})
