import { Game } from './game/game';
import GameHandler from './handler';

// window.addEventListener('load', init);
// function init(){
//     var game = new Game();
// }

if (DEBUG) {
    var Stats = require('stats.js')
    window.addEventListener('load', () => {
        const s = new Stats()
        s.showPanel(0)
        s.dom.style.removeProperty('left')
        s.dom.style.setProperty('right', 0)
        window.document.body.appendChild(s.dom)
        const a = () => (s.begin(), s.end(), requestAnimationFrame(a))
        requestAnimationFrame(a)
    })
}

gAPI.wrapper.init({
    casinoUI: {
        startingBetMultiplier: 40,
        // paytable: "static/paytable/paytable.html",
        // infoPanel: "static/info/info.html",
        // pdfURL: "static/info/info.pdf"
    },
    handler: GameHandler(new Game())
})
