module.exports = function(grunt) {
    
    var http = require('http');
    var path = require('path');
    var fs = require('fs');
    
    grunt.registerMultiTask('runeditor','start editor server', function() {
        var done = this.async();
        var options = this.options({
            port: 8000,
            base: '.'
        });
        
        var filepath = options.base + '/hierarchy.json';
        
        function appendHierarchy(obj1, obj2){
            for(var prop in obj2)
                if(obj1[prop] === undefined){
                    obj1[prop] = obj2[prop];
                }else{
                    if(obj2[prop] === 'object' && obj2[prop] !== null){
                        appendHierarchy(obj1[prop], obj2[prop]);
                    }else{
                        obj1[prop] = obj2[prop];
                    }
                }
        }
        
        function saveFile(filepath, fileData){
            try{
                var stats = fs.statSync(filepath);
                var data = fs.readFileSync(filepath, 'utf8')
                var resultJSON = JSON.parse(data);
                var appendJSON = JSON.parse(fileData);
                appendHierarchy(resultJSON, appendJSON);
                fileData = JSON.stringify(resultJSON,null,2);
            }catch(e){
                
            }
            fs.writeFileSync(filepath, fileData, {flag: 'w'});
        }
        
        function handleRequest(request, response){
            // Set CORS headers
	response.setHeader('Access-Control-Allow-Origin', '*');
	response.setHeader('Access-Control-Request-Method', '*');
	//response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	//response.setHeader('Access-Control-Allow-Headers', '*');
            
    //response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Methods", "POST, GET");
    //response.setHeader("Access-Control-Max-Age", "3600");
    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
            
            
            request.on('error', function(err) {
                grunt.log.writeln(err.stack);
            });
            var method = request.method;
            var url = request.url;
            var headers = request.headers;
            
            var body = [];
            request.on('data', function(chunk) {
                body.push(chunk);
            }).on('end', function() {
                body = Buffer.concat(body).toString();
                if(body !== ''){
                grunt.log.writeln('Saving to ' + filepath);
                saveFile(filepath, body);
                response.end('Data saved.');
                }else{
                response.end();
                }
                    
            });
        }
        
        var server = http.createServer(handleRequest);
        
        //server.on('request', function(request, response) {
        //});
        server.listen(options.port, function(){
            grunt.log.writeln('Server listening on: http://localhost: ' + options.port);
        });
    });
}