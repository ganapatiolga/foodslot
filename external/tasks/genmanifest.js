module.exports = function(grunt) {
    var path = require('path');
    var fs = require('fs');
    var isWindows = process.platform === 'win32';

    var scopeNames = {
        'assets': 'assets',
        'static': 'static',
        'dBones': 'dBones',
        'textures': 'textures',
        'loading': 'loading',
        'global': 'global',
        'config': 'config',
		'low' : 'low',
		'med' : 'med',
		'high' : 'high',
        'spine' : 'spine'
    };

    var dirFilters = {
        'font': /\.(jpg|png|jpeg)$/i,
        'static': /(css|images|html)/i,
				'old': /\/old\//i
    }

    grunt.registerMultiTask('genmanifest','generate manifest json', function() {
        grunt.log.writeln('Generating manifest file.');

        var detectDestType = function(dest) {
            if (grunt.util._.endsWith(dest, '/')) {
                return 'directory';
            } else {
                return 'file';
            }
        };

        var unixifyPath = function(filepath) {
            if (isWindows) {
                return filepath.replace(/\\/g, '/');
            } else {
                return filepath;
            }
        };

        var isScope = function(name){
            if(scopeNames[name])return true;
            return false;
        };

        var getBaseDir = function(fullDir){
            var index = fullDir.lastIndexOf('/');
            if(index === -1)return fullDir;
            return fullDir.substr(0, index + 1);
        };

        var cleanup = function(scope){
            for(var s in scope)
                if(scope.hasOwnProperty(s))
                    if(s === 'files'){
                        var files = scope[s];
                        var deleteFiles = [];
                        for(var f = 0; f < files.length; f++){
                            var file = files[f];
                            var basePath = getBaseDir(file);
                            if((/\.json$/i).test(file)){
                                try{
                                    //grunt.file.readJSON('mapping.json');
                                    var data = fs.readFileSync(file, 'utf8')
                                    var object = JSON.parse(data);
                                    if(object.meta && object.meta.image){
                                        deleteFiles.push(basePath + object.meta.image);
                                    }else if(object.imagePath){
                                        deleteFiles.push(file);
                                        deleteFiles.push(basePath + object.imagePath);
                                    }else if(object.urls){
                                        for(var i = 0; i < object.urls.length; i++)
                                            deleteFiles.push(basePath + object.urls[i]);
                                    }
                                }catch(err){
                                    grunt.log.writeln('error parsing ' + file + ' ' + err.toString());
                                }
                            }
                            if((/\.atlas$/i).test(file)) {
                                var data = fs.readFileSync(file, 'utf8');
                                var imageFile = data.replace(/^\r?\n/, '').replace(/\r?\n.*/g, '');
                                var skeletonFile = file.replace(/\.atlas$/i, '.json');
                                deleteFiles.push(basePath + imageFile);
                                deleteFiles.push(file);
                            }
                        }
                        for(var df = 0; df < deleteFiles.length; df++){
                            var delIndex = files.indexOf(deleteFiles[df]);
                            files.splice(delIndex,1);

                        }
                        for(var f = 0; f < files.length; f++)
                            files[f] = files[f].substring(files[f].indexOf('/')+1);
                    }else{
                        cleanup(scope[s]);
                    }
            return scope;
        };

        var appendFile = function(outputJson, rawFilepath){
            var filepath = rawFilepath.substring(rawFilepath.indexOf('/')+1);
            var filedata = filepath.split('/');
            var filename = filedata[filedata.length-1];

            var currentScope = outputJson;
            for(var i = 0; i < filedata.length-1; i++){
                var dirName = filedata[i];
                if(dirFilters[dirName])
                        if(dirFilters[dirName].test(filepath))
                            return;
                if(isScope(dirName)){
                    if(currentScope[dirName] === undefined)
                        currentScope[dirName] = {};
                    currentScope = currentScope[dirName];
                }
            }
            if(currentScope.files === undefined)
                currentScope.files = [];
            if(currentScope !== outputJson)
            currentScope.files.push(rawFilepath);
        };

        this.files.forEach(function(filePair){
            var dest = unixifyPath(filePair.dest);
            var outputJson = {};

            filePair.src.forEach(function(src){
                src = unixifyPath(src);
                if(grunt.file.isDir(src)){
                }else{
                    appendFile(outputJson, src);
                }
            });
            //grunt.file.write(dest, JSON.stringify(outputJson, null, 2));
            grunt.log.writeln('Cleanup.');
            outputJson = cleanup(outputJson);
            fs.writeFileSync(dest, JSON.stringify(outputJson,null,2), {flag: 'w'});
            grunt.log.writeln('Manifest file succesfully generated.');
        });

    });
};
