var PIXI = require('pixi.js');
var dragonBones = require('./dragonBones.js');

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};

var getDragonBonesPixi = function(dragonBones){
PIXI.DisplayBridgeContainer = function(){
    PIXI.Container.call(this);
}
PIXI.DisplayBridgeContainer.prototype = Object.create(PIXI.Container.prototype);

PIXI.Container.prototype.setInnerScale = function(value){
    //if(this.texture)
    //    this._innerScale = value;
    //for(let i = 0; i < this.children.length; i++)
    //    if(this.children[i].setInnerScale)
    //        this.children[i].setInnerScale(value)
}

PIXI.DisplayBridgeContainer.prototype.addChildAtZ = function(child, z){
    var children = this.children;
    var z = z || 0;
    for(var i = 0; i < children.length; i++){
        var cz = this.getChildAt(i).z || 0;
        if(z >= cz){
            var newChild = child;
            newChild.z = z;
            this.addChildAt(newChild, i);
        }
    }
};

var isEquivalent = function(a, b, epsilon=0.0001){
return (a - epsilon < b) && (a + epsilon > b);
};
    
var normalizeAngle = function(a){
var angle = a % (2.0*Math.PI);
if (angle < -Math.PI) angle += (2.0*Math.PI);
if (angle >  Math.PI) angle -= (2.0*Math.PI);
return angle;
}

var recursiveBlendApply = function(parent, blendMode){
    parent.blendMode = blendMode;
    if(parent.children)
        for(let i = 0; i < parent.children.length; i++)
            recursiveBlendApply(parent.children[i], blendMode)
}

var PI_Q = Math.PI / 2.0;
PIXI.Container.prototype.setDBTransform = function(matrix, transform){
/*
                //this.pivotX = 0;
                //this.pivotY = 0;
                var x = matrix.tx;
                var y = matrix.ty;
                var skewX = Math.atan(-matrix.c / matrix.d);
                var skewY = Math.atan( matrix.b / matrix.a);
    
                var skewX2 = Math.atan2(-matrix.c , matrix.d);
                var skewY2 = Math.atan2( matrix.b , matrix.a);

            if (skewX != skewX) skewX = 0.0;
            if (skewY != skewY) skewY = 0.0;

            var scaleY = (skewX > -PI_Q && skewX < PI_Q) ?  matrix.d / Math.cos(skewX) : -matrix.c / Math.sin(skewX);
            var scaleX = (skewY > -PI_Q && skewY < PI_Q) ?  matrix.a / Math.cos(skewY) :  matrix.b / Math.sin(skewY);

    
    
            
            var rotation = 0;
            if (isEquivalent(skewX, skewY)){
                rotation = skewX;
                //skewY = skewY - skewX;
                skewX = 0;
                skewY = 0;
    
            }else{
                rotation = 0;
            }*/
    //let scale = this._innerScale || 1.0
    //this.setTransform(x, y, scaleX, scaleY, rotation, skewX, skewY);
    this.setTransform(transform.x, transform.y, transform.scaleX, transform.scaleY, transform.skewX,0,(transform.skewY-transform.skewX));
    //this.setTransform(transform.x, transform.y, transform.scaleX, transform.scaleY, 0,transform.skewX,transform.skewY);
    //this.parent = parent;
    //this.transform.setFromMatrix(matrix);
    
};

//**********************************************
var dragonBones;
(function (dragonBones) {
    (function (display) {
        var PIXIDisplayBridge = (function () {
            function PIXIDisplayBridge() {
            }
            PIXIDisplayBridge.prototype.getVisible = function () {
                return this._display ? this._display.visible : false;
            };
            PIXIDisplayBridge.prototype.setVisible = function (value) {
                if (this._display) {
                    this._display.visible = value;
                }
            };

            PIXIDisplayBridge.prototype.getDisplay = function () {
                return this._display;
            };
            PIXIDisplayBridge.prototype.setDisplay = function (value) {
                if (this._display == value) {
                    return;
                }
                if (this._display) {
                    var parent = this._display.parent;
                    if (parent) {
                        var children = parent.children;
                        var index = children.indexOf(this._display);
                    }
                    this.removeDisplay();
                }
                this._display = value;
                this.addDisplay(parent, index);
            };

            PIXIDisplayBridge.prototype.dispose = function () {
                this.removeDisplay();
                this._display = null;
            };
            

            PIXIDisplayBridge.prototype.updateTransform = function (matrix, transform) {
                var pixiMatrix = new PIXI.Matrix();
                pixiMatrix.a = matrix.a;
                pixiMatrix.b = matrix.b;
                pixiMatrix.c = matrix.c;
                pixiMatrix.d = matrix.d;
                pixiMatrix.tx = matrix.tx;
                pixiMatrix.ty = matrix.ty;
                this._display.setDBTransform(pixiMatrix, transform);
            };

            PIXIDisplayBridge.prototype.updateColor = function (aOffset, rOffset, gOffset, bOffset, aMultiplier, rMultiplier, gMultiplier, bMultiplier) {
                if (this._display) {
                    this._display.alpha = aMultiplier;
                }
            };
            
             PIXIDisplayBridge.prototype.updateBlendMode = function (blendMode) {
                if (this._display) {
                    var pixiBlendMode = PIXI.BLEND_MODES.NORMAL
                    switch(blendMode){
                        case "auto":
                        case "normal": 
                            pixiBlendMode = PIXI.BLEND_MODES.NORMAL;
                            break;
                        case "add":
                            pixiBlendMode = PIXI.BLEND_MODES.ADD;
                            break;
                        case "darken":
                            pixiBlendMode = PIXI.BLEND_MODES.DARKEN;
                            break;
                        case "difference":
                            pixiBlendMode = PIXI.BLEND_MODES.DIFFERENCE;
                            break;
                        case "hardlight":
                            pixiBlendMode = PIXI.BLEND_MODES.HARD_LIGHT;
                            break;
                        case "lighten":
                            pixiBlendMode = PIXI.BLEND_MODES.LIGHTEN;
                            break;
                        case "multiply":
                            pixiBlendMode = PIXI.BLEND_MODES.MULTIPLY;
                            break;
                        case "overlay":
                            pixiBlendMode = PIXI.BLEND_MODES.OVERLAY;
                            break;
                        case "screen":
                            pixiBlendMode = PIXI.BLEND_MODES.SCREEN;
                            break;
                        default:
                            break;
                    }
                    recursiveBlendApply(this._display, pixiBlendMode)
                }
            };

            PIXIDisplayBridge.prototype.addDisplay = function (container, index) {
                var parent = container;
                if (parent && this._display) {
                    this.removeDisplay();
                    if (index < 0) {
                        if(this._display._zOrder > 0) {
							parent.addChildAtZ(this._display, this._display._zOrder);
						} else {
							parent.addChild(this._display);
						}
                    } else {
                        parent.addChildAt(this._display, Math.min(index, parent.children.length));
                    }
                }
            };

            PIXIDisplayBridge.prototype.removeDisplay = function () {
                if (this._display && this._display.parent) {
                    this._display.parent.removeChild(this._display);
                }
            };
            PIXIDisplayBridge.RADIAN_TO_ANGLE = 180 / Math.PI;
            return PIXIDisplayBridge;
        })();
        display.PIXIDisplayBridge = PIXIDisplayBridge;
    })(dragonBones.display || (dragonBones.display = {}));
    var display = dragonBones.display;
    //**********************************************
    (function (textures) {
        var PIXITextureAtlas = (function () {
            function PIXITextureAtlas(image, textureAtlasRawData, scale) {
                if (typeof scale === "undefined") { scale = 1; }
                this._regions = {};

                this.image = image;
                this.scale = scale;

                this.parseData(textureAtlasRawData);
            }
            PIXITextureAtlas.prototype.dispose = function () {
                this.image = null;
                this._regions = null;
            };

            PIXITextureAtlas.prototype.getRegion = function (subTextureName) {
                return this._regions[subTextureName];
            };

            PIXITextureAtlas.prototype.parseData = function (textureAtlasRawData) {
                var textureAtlasData = dragonBones.objects.DataParser.parseTextureAtlasData(textureAtlasRawData, this.scale);
                this.name = textureAtlasData.__name;
                delete textureAtlasData.__name;
                var originSize = new PIXI.Rectangle(0, 0, this.image.width, this.image.height);
                var subTextureData;
                var rect;
                var spriteFrame;
                 for (var subTextureName in textureAtlasData) {
                    subTextureData = textureAtlasData[subTextureName];
                    rect = new PIXI.Rectangle(subTextureData.x, subTextureData.y, subTextureData.width, subTextureData.height);
                    spriteFrame = new PIXI.Texture(this.image, rect, originSize, null, 0);
                    this._regions[subTextureName] = spriteFrame;
                }
            };
            return PIXITextureAtlas;
        })();
        textures.PIXITextureAtlas = PIXITextureAtlas;
    })(dragonBones.textures || (dragonBones.textures = {}));
    var textures = dragonBones.textures;
    
    var advanceTime = function(dt){
        dragonBones.animation.WorldClock.clock.advanceTime(0.001*dt);
        //dragonBones.animation.WorldClock.clock.advanceTime(-1);
    };
    PIXI.ticker.shared.add(advanceTime, this);
    
    Object.defineProperty(dragonBones.animation.Animation.prototype, 'lastAnimationName', {
        get: function(){
            if(!this._lastAnimationState)return null;
            return this._lastAnimationState.name;
        }
    });
    
    Object.defineProperty(dragonBones.objects.SkeletonData.prototype, 'armatureNames', {
        get: function(){
            return this.getArmatureNames();
       }
    });
    
    //**********************************************
    (function (factorys) {
        var PIXIFactory = (function (_super) {
            __extends(PIXIFactory, _super);
            function PIXIFactory() {
                _super.call(this);
            }
            PIXIFactory.prototype.parseDragonBonesData = function(skeletonData){
                var skeleton = dragonBones.objects.DataParser.parseSkeletonData(skeletonData)
                this.addSkeletonData(skeleton);
                return skeleton;
            };
            PIXIFactory.prototype.parseTextureAtlasData = function(textureData, texture){
                this.addTextureAtlas(new dragonBones.textures.PIXITextureAtlas(texture, textureData));
            }
            PIXIFactory.prototype._generateArmature = function () {
                var armature = new dragonBones.Armature(new PIXI.DisplayBridgeContainer());
                return armature;
            };
            PIXIFactory.prototype.buildArmatureDisplay = function(armatureName){
                var armature = this.buildArmature(armatureName);
                dragonBones.animation.WorldClock.clock.add(armature);
                
                
                var display = armature.getDisplay();
                display.animation = armature.animation;
                armature.addEventListener(dragonBones.events.FrameEvent.ANIMATION_FRAME_EVENT, function(data){
                    display.emit('FrameEvent', data.frameLabel);
                });
                armature.addEventListener(dragonBones.events.AnimationEvent.COMPLETE, function(data){
                    display.emit('FrameEvent', 'Complete');
                });
                display.clear = function(){
                    dragonBones.animation.WorldClock.clock.remove(armature)
                }

                return display;
            }

            PIXIFactory.prototype._generateSlot = function () {
                var slot = new dragonBones.Slot(new display.PIXIDisplayBridge());
                return slot;
            };

            PIXIFactory.prototype._generateDisplay = function (textureAtlas, fullName, pivotX, pivotY) {
                var spriteFrame = textureAtlas.getRegion(fullName);
                let scale = this.defaultScale || 1.0
                //matrix scale!
                //var scale = 1 / textureAtlas.scale;
                var anchorX = (pivotX / (spriteFrame.width*scale));
                var anchorY = (pivotY / (spriteFrame.height*scale));
                var sprite = new PIXI.Sprite(spriteFrame);
                sprite.anchor.set(anchorX, anchorY);
                //sprite.transform.pivot.set(pivotX, pivotY);
                sprite.scale.set(scale, scale);
                let display = new PIXI.Container()
                display.addChild(sprite)
                return display;
            };
            
            return PIXIFactory;
        })(factorys.BaseFactory);
        factorys.PIXIFactory = PIXIFactory;
    })(dragonBones.factorys || (dragonBones.factorys = {}));
    var factorys = dragonBones.factorys;
    dragonBones.PixiFactory = dragonBones.factorys.PIXIFactory;
})(dragonBones || (dragonBones = {}));

     return dragonBones;
}

module.exports = getDragonBonesPixi(dragonBones);